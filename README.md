# SOSL

The Set-based Ontology Seed Language (SOSL) allows defining (the seed of) an ontology definition (with some instances), that then get transformed into different graphical (Class diagrams, Object diagrams, ER diagrams, ...) and textual (DOL, eCore, ...) representations.

[![GitLab pipeline](https://img.shields.io/gitlab/pipeline/mko575/sosl?logo=GitLab)](https://gitlab.com/mko575/sosl/builds)
[![pipeline status](https://gitlab.com/mko575/sosl/badges/master/pipeline.svg)](https://gitlab.com/mko575/sosl/-/commits/master)
[![coverage report](https://gitlab.com/mko575/sosl/badges/master/coverage.svg)](https://gitlab.com/mko575/sosl/-/commits/master)
[![licence](https://img.shields.io/badge/licence-GPLv3-informational?logo=Open-Source-Initiative)](https://gitlab.com/mko575/sosl/-/blob/master/LICENSE)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=sosl_sosl&metric=ncloc)](https://sonarcloud.io/dashboard?id=sosl_sosl)
<!-- ([![Twitter](https://img.shields.io/twitter/url?style=social)]
(https://twitter.com/intent/tweet?text=Wow:&url=https%3A%2F%2Fgitlab.com%2Fmko575%2Fsosl) -->

## Using the SOSL tools ##

### Configuring the environment ###

SOSL allows the use of less common UTF-8 characters. In order to be able to
"easily" type them on your keyboard you may want to have a look at the page documenting 
[keyboard layout modification](resources/editors/modifyingKeyboardLayout.md).

### Starting the Forestry application ###

SOSL is a language. The tool to interact with ontology definitions or instances
written in SOSL is called _Forestry_.

Forestry can be started from the local git repository using gradlew:
```bash
# To get some help
./gradlew run --args="--help"
# To run a simple exemple
./gradlew run --args="cultivate src/test/resources/SOSL_files/integrationTestFiles/pizza.sosl"
# To run a simple exemple
./gradlew clean build run --args="harvest src/test/resources/SOSL_files/integrationTestFiles/pizza.sosl -o build/test/resources/SOSL_files/integrationTestFiles/pizza.CD.pdf --class-diagram-with-graphviz Pizza"
```

Forestry can also be run using only a JRE 11 or above. To do so, unpack one of the
distribution files in the [distributions directory](build/distributions) and run
one of the files in the bin directory.

For installation of a _Java Runtime Environment_ (JRE) or _Java Development Kit_
(JDK), see:
* [Oracle's Java website](https://www.java.com/en/download/);
* [Oracle's OpenJDK website](https://jdk.java.net/)
or [OpenJDK Community's website](https://openjdk.java.net/);
* [AdoptOpenJDK's website](https://adoptopenjdk.net/);
* or your favorite distribution packaging tool.

Further information will gradually be added to the [user manual](documentation/user-manual.md).

## Building the SOSL project ##

To build the SOSL project, simply run:
```bash
./gradlew build
```

Further information will gradually be added to the
[developer manual](documentation/developer-manual.md).

### Versioning ###

Versioning of SOSL will follow the conventions of [Semantic Versioning](https://semver.org/).

SOSL is currently in an early development phase, its major version number is
therefore 0, and anything can change at anytime.

### Code Quality ###

#### According to SonarCloud ####

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sosl_sosl&metric=alert_status)](https://sonarcloud.io/dashboard?id=sosl_sosl)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=sosl_sosl&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=sosl_sosl)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=sosl_sosl&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=sosl_sosl)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=sosl_sosl&metric=security_rating)](https://sonarcloud.io/dashboard?id=sosl_sosl)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=sosl_sosl&metric=sqale_index)](https://sonarcloud.io/dashboard?id=sosl_sosl)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=sosl_sosl&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=sosl_sosl)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=sosl_sosl&metric=bugs)](https://sonarcloud.io/dashboard?id=sosl_sosl)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=sosl_sosl&metric=code_smells)](https://sonarcloud.io/dashboard?id=sosl_sosl)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=sosl_sosl&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=sosl_sosl)

## Licence

In the current state of the project, I do not really expect anyone to contribute to or use SOSL
(except maybe "just to have a look"). Hence, the GPLv3 licence has been quickly chosen
as a defensive default.

If this licence is a problem for what you want to do with SOSL, please raise an issue on GitLab.
I will contact you to see what can be done.

As a consequence for contributors, a "complete" licence or the copyrights of any contribution (which I am not expecting at
this stage) have to be transferred to me (see below), in order to allow me to easily modify the
licence of the project.

Other than that, as long as you respect the current licence, feel free to fork and do what ever you
want in the freedom space provided by the licence.

### Contributor License Agreement (CLA)

In case a "substantial" contribution is made to the project, a [Contributor License  Agreement
(CLA)][CLA@Wikipedia] would have to be signed.

Information on CLA can be found here:
- [CLA on Wikipedia][CLA@Wikipedia]
- [OSS Watch page on CLA](http://oss-watch.ac.uk/resources/cla)
- [ASF](https://apache.org "Apache Software Foundation")'s [CLA page](https://apache.org/licenses/contributor-agreements.html)
- [FSFE](https://fsfe.org "Free Software Foundation Europe")'s [Fiduciary Licence Agreement (FLA) page](https://fsfe.org/activities/fla/fla.en.html)
- [Harmony](http://harmonyagreements.org)'s CLA generator
- [ContributorAgreements.org](http://contributoragreements.org)'s CLA chooser

[CLA@Wikipedia]: https://en.wikipedia.org/wiki/Contributor_License_Agreement
                 "Wikipedia's page on Contributor License Agreement (CLA)"
