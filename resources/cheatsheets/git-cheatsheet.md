This file contains various commands and tricks for day-to-day git use.

Many of the commands come from the [Pro Git book, written by Scott Chacon and Ben Straub, and published by Apress](https://git-scm.com/book/en/v2).

# Committing

## Handling messes

The [flowchart on this page](http://justinhileman.info/article/git-pretty/) helps sort out (coarse-grained) what to do.

### Adding files to previous commit

```bash
git add ...
git commit --amend [--no-edit]
```

# Branching

[[Git book] 3.2 Git Branching - Basic Branching and Merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

## Creating a branch

To create a branch:
```bash
git checkout -b [branch name]
```
This is a short cut for:
```bash
git branch ... # Create the branch
git checkout ... # "Move" to the branch in your working repository
```

## Listing existing branches

```bash
git branch
```

## Switching between branches

To revert the working directory to the last committed state of a branch:
```bash
git checkout [branch name]
```

## Merging

To merge a branch ([branch name]) into the working directory:
```bash
git merge [branch name]
```

To merge into master:
```bash
git checkout master
git merge [branch name]
```

Or to merge the modifications made by others on master into (the branch of) your working directory:
```bash
git merge master
```

### Handling conflicts

1. Find the files having conflicts:
```bash
git status
```
2. Fix conflicts
   - either by opening every file in the usual editor
   - or by using
    ```bash
    git mergetool
    ```
3. Add the edited files and commit.

## Deleting a branch

To delete a branch not needed anymore (work done, committed and merged):
```bash
git branch -d hotfix
```

# Stashing

This allows to stash uncommitted modifications of the working directory (or staging area).
