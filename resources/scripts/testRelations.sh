#!/bin/bash

./gradlew run --args="src/test/resources/SOSL_files/unitTestsFiles/successTests/kindRelation.sosl harvest -v entity-relationship-diagram-with-graphviz -o build/tmp/RelationsTest.erd -f dot pdf -- .:KindRelationOntology"
