#!/bin/bash

./gradlew run --args="src/test/resources/SOSL_files/unitTestsFiles/successTests/values.sosl harvest -v entity-relationship-diagram-with-graphviz -o build/tmp/ValuesTest.erd -f dot pdf -- .:ValuesTest"
