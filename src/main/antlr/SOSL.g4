/**
 * This is SOSL's grammar
 */

grammar SOSL;

import LexBasic, SOSLKeywords_asTokens;

/** LEXING RULES **/

fragment UC_Letter : [A-Z] ;
fragment LC_Letter : [a-z] ;
fragment Letter : ( UC_Letter | LC_Letter ) ;
fragment Digit : DecDigit ;
fragment EoL : ('\r'? '\n' | '\r') ;

fragment TQuote : '"""' ;

// Getting rid of unmeaningfull whitespaces
DISPOSABLE_SPACE : Ws+ -> channel(HIDDEN) ;

// Getting rid of semanticless comments
COMMENT : ( LineComment | BlockComment )+ -> channel(HIDDEN) ;

// Recognizing strings
STRING
    : SQuote ( ~['\r\n] | Esc SQuote )* SQuote
    | DQuote ( ~["\r\n] | Esc DQuote )* DQuote
    | TQuote ( ~["] | Esc DQuote )* TQuote
    ;

/** PARSING RULES **/

sosFile : ( nsInfixDef | loadCmd | ontologyDefinition | instanceDefinition )* EOF ;

/** NAMESPACE DEFINITION AND POPULATION **/

namespaceRooting : nsROOT_KW? nsSeparator_KW ;

namespaceAtom
  : id=WORD           #NSA_Id
  | nsCurrent_KW      #NSA_Current
  | nsUpTraveler_KW   #NSA_Up
  ;

namespace :
  (absolute=namespaceRooting)? components+=namespaceAtom
  ( nsSeparator_KW components+=namespaceAtom )* ;

globalOntologyId : ( ns=namespace ':' )? lid=WORD ;

/*** NS definition ***/

nsInfixDef : nsInfix_KW ':' namespace ';'? ;

/*** NS population ***/

loadCmd :
  load_KW
  ( file=STRING ( withoutFileNSI=without_NSInfix_KW )?
  | oid=globalOntologyId ( as_KW newId=globalOntologyId )?
  )
  ( insideSpecificNS=inside_KW namespace ( withoutLocalNSI=without_NSInfix_KW )? )?
  ';'? ;

/** ONTOLOGY DEFINITION PARSING **/

ontologyDefinition
  : ontologyId=WORD (name=STRING)? ontology_KW '{' body=ontologyDefBody '}'
  | begin_KW ontology_KW ontologyId=WORD body=ontologyDefBody end_KW ontology_KW?
  ;

ontologyDefBody : ( ( importStmt | atomicFormula ) ';'? )* ;

/*** NS picking ***/

importCmd : implicitly_KW? ( copy_KW | link_KW ) ;

importStmt
  : importCmd
    importedSets+=setIdImportDef ( ',' importedSets+=setIdImportDef )*
    from_KW ontologyId=globalOntologyId
  | from_KW ontologyId=globalOntologyId ( ',' | ':' )?
    importCmd
    importedSets+=setIdImportDef ( ',' importedSets+=setIdImportDef )*
  ;

setIdImportDef
  // : ( all_KW implicitly_KW? | implicitly_KW )          # all_SIID
  : all_KW? implicitly=implicitly_KW?              # all_SIID
  | oldHdle=WORD
      (as_KW newHdle=WORD? newName=STRING? )?
      implicitly=implicitly_KW?                    # unique_SIID
  | others_KW implicitly=implicitly_KW?            # others_SIID
  ;

/** FORMULA **/

atomicFormula
  : pred=unaryPred termsSequence                                        #unaryPred_AF
  | lhs=termsSequence embeddedFormulae pred=binPred rhs=termsSequence   #binaryPred_AF
  // | lhs=term embeddedFormulae pred=binPred rhs=term   #binaryPred_AF
  | pred=naryPred termsSequence                                         #naryPred_AF
  // | term ( pred=naryPred term )+   #naryPred_AF
  ;

/*
binaryFormulaOnSets
  : lhs=term embeddedFormulae pred=binPred rhs=binaryPredAF_RHS
  ;
binaryPredAF_RHS
  : binaryPredAF
  | term
  ;
*/

embeddedFormulae: ( ( '+' | ':' ) pred=embedablePred )* ;

unaryPred
  : finite_KW | atomic_KW
  | functional_KW | total_KW | injective_KW | surjective_KW
  ;
binPred
  : subset_KW | subseteq_KW | superset_KW | superseteq_KW
  | in_KW | equal_KW
  ;
naryPred : distinct_KW ;

embedablePred : unaryPred | naryPred ;

/** TERM **/

userDefinedId: id=WORD ;

setId
  : emptysetId_KW                 #emptysetId
  | entitySetId_KW                #entitiesId
  | valueSetId_KW                 #valuesId
  | userDefinedId (name=STRING)?  #userDefinedSetId
  ;

tupleField: ( id=WORD ':' )? term ;

// closedTupleField: ... // ???

/*
Introduce "closed term" notion and reduce constraints on grammar ?

tupleField: ( id=WORD ':' )? closedTerm ;

term
  : op=unaryPrefixOp term                             #unaryFunOnSet_ST
  | lhs=term op=binOp rhs=term                        #binaryFunOnSet_ST
  // | term ( naryOp term )+                        #naryFunOnSet_ST
  // | naryOp termSequences                         #naryFunOnSet_ST
  | tupleField ( cartesianProd_KW tupleField )+        #cartesianProduct_ST
  | closedTerm                                        #closedTerm_ST
  ;

closedTerm
  : setId                                               #setId_ST
  | '{' ( term ( ',' term )* )? '}'                     #set_ST
  | '[' tupleField ( ',' tupleField )+ ']'              #tuple_ST
  | '(' term ')'                                        #closingTerm_ST
  ;
*/

term
  : setId                                               #setId_ST
  | '{' ( term ( ',' term )* )? '}'                     #set_ST
  // TODO: Fix the grammar for tuples ...
  // What is "( setId )" or more generally "( term )" ?
  | '(' tupleField ( ',' tupleField )+ ')'              #tuple_ST
  | op=unaryPrefixOp term                               #unaryOpOnSet_ST
  | lhs=term op=binaryOp rhs=term                       #binaryOpOnSet_ST
  | term ( op+=associativeOp term )+                    #associativeOpOnSet_ST
  | tupleSet=term restriction_KW
     '(' dimension=WORD ',' on=term ')'                 #restriction_ST
  | lhs=term op=joinOp
     ( '(' dimensions+=WORD ',' dimensions+=WORD
      ( ';' dimensions+=WORD ',' dimensions+=WORD)* ')' )?
     rhs=term                                           #join_ST
  // | on=term projectionOnFirst_KW tupleSet=term          #projectionOnFirst_ST
  // | tupleSet=term projectionOnLast_KW on=term           #projectionOnLast_ST
  // | term ( naryFun term )+                              #naryFunOnSet_ST
  // | naryFun termSequences                               #naryFunOnSet_ST
  | '(' tupleField ( cartesianProd_KW tupleField )+ ')' #cartesianProduct_ST
  | '(' term ')'                                        #closingTerm_ST
  ;

termsSequence : term ( ',' term )* ;

unaryPrefixOp
  : complement_prefix_KW
  | powerset_prefix_KW
  ;

associativeOp : union_KW | intersection_KW | unionProd_KW ;

binaryOp
  : functionalBinOp
  | restrictionBinOp
  | joinOp
  ;
functionalBinOp
  : partialFunction_KW   | totalFunction_KW       // X-to-1
  | partialInjection_KW  | totalInjection_KW      // 1-to-1
  | partialSurjection_KW | totalSurjection_KW     // onto
  | partialBijection_KW  | totalBijection_KW      // 1-to-1 and onto | 1-to-1 correspondence
  ;
restrictionBinOp : restrictionOnFirst_KW | restrictionOnLast_KW ; // Restriction operations
joinOp : join_KW | collapsingJoin_KW ;

/** INSTANCE DEFINITION PARSING **/

instanceDefinition
  : instanceId=WORD (name=STRING)? instance_KW
      ontologyId=globalOntologyId '{' body=ontologyDefBody '}'
  | begin_KW instance_KW ontologyId=globalOntologyId
      instanceId=WORD body=ontologyDefBody end_KW instance_KW?
  ;

// instanceDefBody : ( atomicFormula ';'? )* ;
