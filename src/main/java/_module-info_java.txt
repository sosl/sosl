module com.gitlab.mko575.sosl {
    requires java.logging;
    requires info.picocli;
    requires org.antlr.antlr4.runtime;
    requires javafx.controls;

    exports com.gitlab.mko575.sosl;
}