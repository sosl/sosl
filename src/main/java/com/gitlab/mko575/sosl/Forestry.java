/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl;

import com.gitlab.mko575.sosl.cli.CLIOutput;
import com.gitlab.mko575.sosl.cli.ForestryCLI;
import java.util.Arrays;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import picocli.CommandLine;

/**
 * This is the main class of the Forestry application.
 * Its job is to initialize an instance of {@link ForestryCLI} and one of {@link CommandLine}
 * to handle the CLI (Command Line Interface) using picocli.
 *
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
public class Forestry {

  public static final CLIOutput mainAppOutput = CLIOutput.getCLIOutput("MainForestryOutput");
  public static final Logger mainAppLogger = Logger.getLogger("MainForestryLogger");
  private static final Level INITIAL_LOGGING_LEVEL = Level.INFO;

  /**
   * https://docs.oracle.com/cd/E17277_02/html/GettingStartedGuide/managelogging.html
   * https://www.logicbig.com/tutorials/core-java-tutorial/logging/levels.html
   * https://stackoverflow.com/questions/6307648/change-global-setting-for-logger-instances
   */
  {
    final ConsoleHandler mainLoggerCH = new ConsoleHandler();
    mainAppLogger.addHandler(mainLoggerCH);
  }

  /**
   * Sets the logging level globally.
   * @param logLevel the desired logging level.
   */
  private static void setGlobalLoggingLevel(Level logLevel) {
    Logger rootLogger = LogManager.getLogManager().getLogger("");
    rootLogger.setLevel(logLevel);
    for (Handler h : rootLogger.getHandlers()) {
      h.setLevel(logLevel);
    }
    mainAppLogger.setLevel(logLevel);
    Arrays.stream(mainAppLogger.getHandlers()).forEach(h -> h.setLevel(logLevel));
  }

  /*-----------------*/
  /* MAIN STARTUP    */
  /*-----------------*/

  /**
   * Main function starting the program.
   * @param args arguments passed on the CLI.
   */
  public static void main(String[] args) {
    setGlobalLoggingLevel(INITIAL_LOGGING_LEVEL);
    CommandLine cmd = new CommandLine(new ForestryCLI());
    if (args.length == 0) {
      cmd.usage(System.out);
    } else {
      cmd.execute(args);
    }
  }

}
