/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl;

import static com.gitlab.mko575.sosl.Forestry.mainAppLogger;
import static com.gitlab.mko575.sosl.Forestry.mainAppOutput;
import static java.nio.charset.StandardCharsets.UTF_8;

import com.gitlab.mko575.sosl.ids.gos.OldOntologyGraph;
import com.gitlab.mko575.sosl.ids.gos.OntologyEntityRelationshipDiagram;
import com.gitlab.mko575.sosl.ids.gos.OntologyRawGraph;
import com.gitlab.mko575.sosl.ids.RootNamespace$;
import com.gitlab.mko575.sosl.ids.OntologyId;
import com.gitlab.mko575.sosl.ids.sos.OntologySeed;
import com.gitlab.mko575.sosl.gui.ForestryGUI;
import com.gitlab.mko575.sosl.parser.CST2ASTVisitor;
import com.gitlab.mko575.sosl.parser.ParsingUtils;
import com.gitlab.mko575.sosl.parser.SOSLParser;
import com.gitlab.mko575.sosl.parser.SOSLParsers;
import com.gitlab.mko575.sosl.parser.ast.ASOntologies;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.stream.Collectors;
import org.antlr.v4.gui.TestRig;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Pair;
import scala.jdk.javaapi.CollectionConverters;

/**
 * Static class containing the code of commands runnable from {@link Forestry}.
 *
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
public final class ForestryUtils {

  /**
   * Enumerate representing graphviz engines.
   */
  public enum GraphvizEngine { dot, neato, twopi, circo, fdp, sfdp, patchwork, osage }

  /** Error string for file paths corresponding to files that can not be read. */
  private static final String
      FILE_S_CAN_NOT_BE_FOUND_OR_READ = "File '%s' can not be found or read.";

  /**
   * Private constructor throwing an exception to discourage instantiating or
   * inheriting from this class.
   *
   * @throws AssertionError always
   */
  private ForestryUtils() throws AssertionError {
    throw new AssertionError(
        "ForestryUtils is a utility class."
            + " No instance of it should ever be created!");
  }

  /**
   * Returns the ontology seeds parsed from the provided file.
   *
   * @param verbose increase verbosity if {@code true}.
   * @param soslFile SOSL {@link File} containing SOS to retrieve.
   * @return the parsed ontology seeds.
   */
  private static Map<OntologyId, OntologySeed> retrieveSeeds(
      final boolean verbose, final File soslFile
  ) {
    return SOSLParsers
        .parseSOSLFile(verbose, RootNamespace$.MODULE$, true, soslFile)
        .getOntologySeeds();
  }

  /**
   * Returns the ontology seeds parsed from the provided files.
   *
   * @param verbose increase verbosity if {@code true}.
   * @param soslFiles SOSL {@link File} containing SOS to retrieve.
   * @return the parsed ontology seeds.
   */
  private static Map<OntologyId, OntologySeed> retrieveSeeds(
      final boolean verbose, final Collection<File> soslFiles
  ) {
    final Map<OntologyId, OntologySeed> ontologySeeds = new HashMap<>();
    for (File soslFile : soslFiles) {
      ontologySeeds.putAll(retrieveSeeds(verbose, soslFile));
    }
    return ontologySeeds;
  }

  /**
   * Convert a collection of seed identifier strings into proper seed identifiers.
   * @param verbose increase verbosity if {@code true}
   * @param soslFiles collection of files containing the SOS to harvest
   * @param seedIdStrings a set of strings representing the global identifiers
   *                      of the seeds to convert.
   * @return a collection of seed identifier objects.
   */
  private static Pair<Map<OntologyId, OntologySeed>, Collection<OntologyId>>
  processOntologyParameters(
      final boolean verbose,
      final Collection<File> soslFiles,
      final Collection<String> seedIdStrings
  ) {
    // Retrieving the ontology seeds
    final Map<OntologyId, OntologySeed> ontologySeeds =
        retrieveSeeds(verbose, soslFiles);
    // Retrieving the identifiers of the ontologies to process
    final Collection<OntologyId> seedIds;
    if (seedIdStrings.contains("*")) {
      seedIds = ontologySeeds.keySet();
    } else {
      seedIds = new HashSet<>();
      for (String sIdStr : seedIdStrings) {
        OntologyId newOId = SOSLParsers.parseGlobalOntologyIdStringFromRoot(sIdStr);
        seedIds.add(newOId);
      }
    }
    return new Pair<>(ontologySeeds, seedIds);
  }

  /**
   * Process a dot description to produce the associated graphs using Graphiz's
   * dot processor.
   * @param dotDescr the string containing the graph description in the dot
   *                 language.
   * @param outputFilePrefix prefix of the files to produce.
   * @param outputFormats formats to produce (dot, pdf, svg, ...)
   */
  private static void processDotString(
      final String dotDescr,
      final GraphvizEngine engine,
      final File outputFilePrefix,
      final Collection<String> outputFormats
  ) {
    final Path outputPathPrefix = outputFilePrefix.toPath().toAbsolutePath().normalize();
    final String outputFileNamePrefix =
        Normalizer.normalize(
            outputPathPrefix.getFileName().toString()
            , Form.NFD
            //).replaceAll("[^\\p{L}\\p{N}]", "");
            //).replaceAll("\\W", "");
        ).replaceAll("[\\P{Print}\\p{Space}]", "");
    Path dotFilePath;
    if (outputFormats.contains("dot")) {
      dotFilePath = outputPathPrefix.resolveSibling(outputFileNamePrefix + ".dot");
    } else {
      String tmpDir = System.getProperty("java.io.tmpdir");
      dotFilePath = Paths.get(tmpDir, outputFileNamePrefix + ".dot");
    }
    try {
      Files.write(dotFilePath, dotDescr.getBytes(UTF_8));
      for (String format :
          outputFormats.stream().filter(s -> ! s.equals("dot")).collect(
              Collectors.toSet())) {
        Path outFilePath =
            outputPathPrefix.resolveSibling(outputFileNamePrefix + "." + format);
        mainAppLogger.info(
            String.format(
                "Producing '%s' file from '%s' using GraphViz's dot processor.",
                outFilePath, dotFilePath
            )
        );
        String dotCmd =
            engine + " -T " + format + " -o " + outFilePath + " " + dotFilePath;
        Process gvProc = Runtime.getRuntime().exec(dotCmd);
        BufferedInputStream errorOut = new BufferedInputStream(gvProc.getErrorStream());
        try { gvProc.waitFor(10, TimeUnit.SECONDS); }
        catch (InterruptedException e) {
          mainAppLogger.log(
              Level.SEVERE,
              "While writing '{0}' and converting it to '{1}', the following interruption occurred:\n{2}",
              new String[]{dotFilePath.toString(), outFilePath.toString(), e.getMessage()}
          );
        }
        if ( gvProc.exitValue() != 0) {
          String errorMsg = new BufferedReader(new InputStreamReader(errorOut))
              .lines().collect(Collectors.joining("\n"));
          mainAppLogger.log(Level.SEVERE, errorMsg);
        }
      }
    } catch (IOException e) {
      mainAppLogger.log(
          Level.SEVERE,
          "While writing dot file '{0}', the following error occurred:\n{1}",
          new String[]{dotFilePath.toString(), e.getMessage()}
      );
    }
  }

  /**
   * Produces a class diagram (using Graphviz) of the designated ontologies.
   *
   * @param verbose increase verbosity if {@code true}
   * @param soslFiles collection of files containing the SOS to harvest
   * @param seedsAsClassDiagram the global identifiers (a namespace composed of
   *   the seed namespace and local identifier) of the seeds to be displayed in
   *   the class diagram
   * @param outputFilePrefix path prefix of all output files
   * @param outputFormats file formats to produce
   */
  public static void produceClassDiagramWithGraphviz(
      final boolean verbose,
      final Collection<File> soslFiles,
      final Collection<String> seedsAsClassDiagram,
      final File outputFilePrefix,
      final Collection<String> outputFormats
  ) {
    final Pair<Map<OntologyId, OntologySeed>, Collection<OntologyId>> ontologyParameters =
        processOntologyParameters(verbose, soslFiles, seedsAsClassDiagram);
    final Map<OntologyId, OntologySeed> ontologySeeds = ontologyParameters.a;
    final Collection<OntologyId> seedIds = ontologyParameters.b;
    // Processing ontologies
    if (! seedIds.isEmpty()) {
      OldOntologyGraph ontologyGraph =
          new OldOntologyGraph(
              CollectionConverters.asScala(Collections.emptyMap()),
              CollectionConverters.asScala(Collections.emptySet())
          );
      for (OntologyId seedId : seedIds) { // NOPMD
        if (!ontologySeeds.containsKey(seedId)) {
          mainAppLogger.info(
              String.format("Ontology %s was not found!", seedId.toDetailedString())
          );
          ontologySeeds.keySet().forEach(k -> System.out.println(k.toDetailedString()));
        } else {
          final OntologySeed seed = ontologySeeds.get(seedId);
          ontologyGraph = ontologyGraph.mergeWith(seed.toOntologyGraph());
          final String dotDescr = ontologyGraph.toClassDiagramWithDot();
          processDotString(
              dotDescr, GraphvizEngine.dot, outputFilePrefix, outputFormats
          );
        }
      }
    }
  }

  /**
   * Produces a "raw ontology graph" (using Graphviz) of the designated ontology.
   *
   * @param verbose increase verbosity if {@code true}
   * @param soslFiles collection of files containing the SOS to harvest
   * @param seedIdStr the global identifier of the seed to graph.
   * @param outputFilePrefix path prefix of all output files
   * @param outputFormats file formats to produce
   */
  public static void produceRawOntologyGraphWithGraphviz(
      final boolean verbose,
      final Collection<File> soslFiles,
      final String seedIdStr,
      final File outputFilePrefix,
      final Collection<String> outputFormats
  ) {
    final Map<OntologyId, OntologySeed> ontologySeeds =
        retrieveSeeds(verbose, soslFiles);
    OntologyId seedId = SOSLParsers.parseGlobalOntologyIdStringFromRoot(seedIdStr);
    if (!ontologySeeds.containsKey(seedId)) {
      mainAppLogger.info(
          String.format("Ontology %s was not found!", seedId.toDetailedString())
      );
    } else {
      final OntologySeed seed = ontologySeeds.get(seedId);
      final OntologyRawGraph graph = new OntologyRawGraph(seed);
      final String dotDescr = graph.getGraphAsDotString();
      processDotString(
          dotDescr, GraphvizEngine.dot, outputFilePrefix, outputFormats
      );
    }
  }

  /**
   * Produces an "entity-relationship diagram" (using Graphviz) of the designated
   * ontology.
   *
   * @param verbose increase verbosity if {@code true}.
   * @param soslFiles collection of files containing the SOS to harvest.
   * @param seedIdStr the global identifier of the seed to represent.
   * @param engine the Graphviz engine to use.
   * @param outputFilePrefix path prefix of all output files.
   * @param outputFormats file formats to produce.
   */
  public static void produceEntityRelationshipDiagramWithGraphviz(
      final boolean verbose,
      final Collection<File> soslFiles,
      final String seedIdStr,
      final GraphvizEngine engine,
      final File outputFilePrefix,
      final Collection<String> outputFormats
  ) {
    final Map<OntologyId, OntologySeed> ontologySeeds =
        retrieveSeeds(verbose, soslFiles);
    OntologyId seedId = SOSLParsers.parseGlobalOntologyIdStringFromRoot(seedIdStr);
    if (!ontologySeeds.containsKey(seedId)) {
      mainAppLogger.info(
          String.format("Ontology %s was not found!", seedId.toDetailedString())
      );
    } else {
      final OntologySeed seed = ontologySeeds.get(seedId);
      final OntologyEntityRelationshipDiagram ERDiagram =
          new OntologyEntityRelationshipDiagram(seed);
      final String dotDescr = ERDiagram.getGraphAsDotString();
      processDotString(dotDescr, engine, outputFilePrefix, outputFormats);
    }
  }

  /**
   * Method used to display ontology seeds.
   *
   * @param verbose increase verbosity if {@code true}
   * @param soslFiles collection of files containing the SOS to display
   * @param viewedSeeds the seeds to be displayed.
   */
  public static void viewUsingJGraphx(
      final boolean verbose,
      final Collection<File> soslFiles,
      final Collection<String> viewedSeeds
  ) {
    final Pair<Map<OntologyId, OntologySeed>, Collection<OntologyId>> ontologyParameters =
        processOntologyParameters(verbose, soslFiles, viewedSeeds);
    final Map<OntologyId, OntologySeed> ontologySeeds = ontologyParameters.a;
    final Collection<OntologyId> seedIds = ontologyParameters.b;
    if (! seedIds.isEmpty()) {
      for (OntologyId seedId : seedIds) { // NOPMD
        if (!ontologySeeds.containsKey(seedId)) {
          mainAppLogger.log(Level.INFO, "Ontology {0} was not found!", seedId);
        } else {
          final OntologySeed seed = ontologySeeds.get(seedId);
          ForestryGUI.viewOntologyGraph(seed.toMXGraph());
        }
      }
    }
  }

  /**
   * Runs the provided SOS file through the 'grun' tool (see {@link TestRig}).
   *
   * @param withTree whether or not to display the parse tree.
   * @param withGui whether or not to use a GUI to display the parse tree.
   * @param withTokens whether or not to display tokens as interpreted by the lexer.
   * @param withTrace whether or not to display a parsing trace.
   * @param withSLL whether or not to use SLL parsing algorithm.
   * @param withDiagnostics whether or not to
   * @param encoding the specific encoding to use.
   * @param psFile the postscript file to produce.
   * @param soslFile SOSL {@link File} to be checked.
   */
  public static void grun(// NOPMD
      final boolean withTree,
      final boolean withGui,
      final boolean withTokens,
      final boolean withTrace,
      final boolean withSLL,
      final boolean withDiagnostics,
      final String encoding,
      final String psFile,
      final File soslFile
  ) {
    if (!soslFile.canRead()) {
      final String errorMsg = String.format(FILE_S_CAN_NOT_BE_FOUND_OR_READ, soslFile.getPath());
      mainAppOutput.out(errorMsg);
      mainAppLogger.log(Level.SEVERE, errorMsg);
    } else {
      try {
        final SOSLTestRig grun = new SOSLTestRig();
        if (withTree) {
          grun.withTree();
        }
        if (withGui) {
          grun.withGui();
        }
        if (withTokens) {
          grun.withTokens();
        }
        if (withTrace) {
          grun.withTrace();
        }
        if (withSLL) {
          grun.withSLL();
        }
        if (withDiagnostics) {
          grun.withDiagnostics();
        }
        if (encoding != null) {
          grun.withEncoding(encoding);
        }
        if (psFile != null) {
          grun.withPS(psFile);
        }
        grun.process(soslFile);
      } catch (Exception e) { // NOPMD
        mainAppOutput.out(
            "GRUN returned the following error message: "
                + "["
                + e.getClass()
                + "] "
                + e.getMessage());
      }
    }
  }

  /**
   * Runs the provided file through the SOSL parser.
   *
   * @param soslFile SOSL {@link File} to be checked.
   */
  public static void tryToParse(final File soslFile) {
    if (!soslFile.canRead()) {
      final String errorMsg = String.format(FILE_S_CAN_NOT_BE_FOUND_OR_READ, soslFile);
      mainAppOutput.out(errorMsg);
      mainAppLogger.log(Level.SEVERE, errorMsg);
    } else {
      try {
        final SOSLParser parser = ParsingUtils.getParserOfFile(soslFile.toPath());
        ParserRuleContext cst = null; // NOPMD
        try {
          cst = parser.sosFile();
        } catch (Exception e) { // NOPMD
          if (mainAppLogger.isLoggable(Level.SEVERE)) {
            mainAppLogger.severe("Parsing did not went well: [" + e + "] " + e.getMessage());
            mainAppOutput.out("Parsing did not went well. Have a look to the logs.");
          } else {
            mainAppOutput.out("Parsing did not went well: " + e.getMessage());
          }
        }
        final int nbErrors = parser.getNumberOfSyntaxErrors();
        if (nbErrors == 0) {
          mainAppOutput.out("Parsing seems to have gone through.");
        } else {
          mainAppOutput.out();
          mainAppOutput.out("Found " + nbErrors + " syntax error(s).");
          mainAppOutput.out("Parser has reached: " + parser.getCurrentToken());
          if (parser.getState() >= 0) {
            mainAppOutput.out("Parser was expecting: " + parser.getExpectedTokens());
          }
          mainAppOutput.out("Parser's call stack is: " + parser.getRuleInvocationStack());
          final ParserRuleContext ctx = parser.getContext();
          if (ctx == null) {
            mainAppOutput.out("There is no context available.");
          } else {
            mainAppOutput.out("Parser's context is: " + ctx.toInfoString(parser));
            mainAppOutput.out("Parsing context: " + ctx.getText());
          }
        }
        if (cst != null) {
          final ASOntologies assos = (ASOntologies) (new CST2ASTVisitor()).visit(cst); // NOPMD
          final Map<OntologyId, OntologySeed> sosMap = assos.getOntologySeeds(); // NOPMD
          mainAppOutput.out("Found the following ontologies!");
          sosMap.forEach(
              (id, sos) -> {
                mainAppOutput.out();
                mainAppOutput.out(String.format("In namespace '%s' as '%s': ", id.ns(), id.lid()));
                mainAppOutput.out(sos.toMultilineVerboseString());
              });
        }
      } catch (IOException e) {
        mainAppLogger.log(Level.SEVERE, "Parsing failed with an IOException!", e);
      }
    }
  }

}
