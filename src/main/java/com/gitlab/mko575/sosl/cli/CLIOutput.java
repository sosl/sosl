package com.gitlab.mko575.sosl.cli;

import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/** Used to output CLI feedbacks. */
public class CLIOutput extends Logger {
  public static final long serialVersionUID = 283712505610L;

  /** Internal class to extend logging levels with more appropriate names. */
  static final class CLIOLevel extends Level {
    public static final long serialVersionUID = 350657986107L;

    /** Level for important outputs. */
    public static final CLIOLevel IMPORTANT = new CLIOLevel("IMPORTANT", 1000);
    /** Default level for outputs. */
    public static final CLIOLevel DEFAULT = new CLIOLevel("DEFAULT", 500);

    private CLIOLevel(final String name, final int value) {
      super(name, value);
    }
  }

  /** Internal class to format output without additional strings. */
  private static final class DfltCLIFormatter extends Formatter {
    @Override
    public String format(final LogRecord record) {
      return record.getMessage();
    }
  }

  /** Internal class to get handler that output to System.out. */
  private static final class CliConsoleHandler extends StreamHandler {
    public static final long serialVersionUID = 835193369067L;

    /**
     * Constructor that returns a <code>StreamHandler</code> that outputs to
     * <code>System.out</code> using a {@link DfltCLIFormatter}.
     */
    private CliConsoleHandler() {
      super(System.out, new DfltCLIFormatter());
    }

    @Override
    public synchronized void publish(final LogRecord record) { // NOPMD
      super.publish(record);
      flush();
    }

    @Override
    public synchronized void close() { // NOPMD
      flush();
    }
  }

  private CLIOutput(final String name) {
    super(name, null);
  }

  /**
   * Returns a CLIOuptut, as {@link Logger#getLogger(String)} does. It also initializes an handler
   * that outputs to {@link System#out}.
   *
   * @param name The name of the output.
   * @return the CLIOutptu created.
   */
  public static CLIOutput getCLIOutput(final String name) {
    final CLIOutput cliOut = new CLIOutput(name);
    final StreamHandler mainOutputHandler = new CliConsoleHandler();
    mainOutputHandler.setLevel(Level.ALL);
    cliOut.addHandler(mainOutputHandler);
    cliOut.setLevel(Level.ALL);
    return cliOut;
  }

  /**
   * Outputs the message, with the {@link CLIOLevel#DEFAULT} importance level.
   *
   * @param msg message to be outputted.
   */
  public void out(final String msg) {
    this.log(CLIOLevel.DEFAULT, msg + "\n");
  }

  /**
   * Simply calls {@link #nlf()} to output a linefeed.
   */
  public void out() {
    nlf();
  }

  /**
   * Outputs a linefeed, with the {@link CLIOLevel#DEFAULT} importance level.
   */
  public void nlf() {
    log(CLIOLevel.DEFAULT, "\n");
  }
}
