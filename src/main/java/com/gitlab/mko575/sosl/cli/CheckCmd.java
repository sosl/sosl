/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl.cli;

import com.gitlab.mko575.sosl.ForestryUtils;
import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import org.antlr.v4.gui.TestRig;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParentCommand;

/**
 * This class handles the CLI subcommand "check" and delegates all logic to {@link ForestryUtils}.
 * This subcommand is used to verify the validity of ontology seed files, mainly
 * checking they conform to SOSL.
 *
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
@Command(
    name = "check",
    description = "Checks a Set-based Ontology Seed (SOS) for potential defects.",
    subcommands = {CommandLine.HelpCommand.class},
    subcommandsRepeatable = true
)
public class CheckCmd extends CLICmd {

  /**
   * Definition of empty arguments feed to {@link TestRig} constructor.
   */
  private static final String[] EMPTY_ARGS = {};

  private static final String TRUE = "true";
  private static final String FALSE = "false";

  /**
   * Method called to handle the 'grun' CLI subcommand (see {@link TestRig}).
   *
   * @param withTree whether or not to display the parse tree.
   * @param withGui whether or not to use a GUI to display the parse tree.
   * @param withTokens whether or not to display tokens as interpreted by the lexer.
   * @param withTrace whether or not to display a parsing trace.
   * @param withSLL whether or not to use SLL parsing algorithm.
   * @param withDiagnostics whether or not to
   * @param encoding the specific encoding to use.
   * @param psFile the postscript file to produce.
   * @param ignoreParentSOSLFiles whether or not to ignore SOSL files provided
   *                              as parameters of parent commands
   * @param soslFiles SOSL files to pass through grun
   */
  @Command(
      name = "grun",
      description = "Run a Set-based Ontology Seed (SOS) file through the grun tool."
  )
  void grun(
      @Option(
              names = {"--tree"},
              defaultValue = FALSE,
              negatable = true,
              description = "Display the parse tree.")
          boolean withTree,
      @Option(
              names = {"--no-gui"},
              defaultValue = TRUE,
              negatable = true,
              description = "Use a GUI to display the parse tree.")
          boolean withGui,
      @Option(
              names = {"--no-tokens"},
              defaultValue = TRUE,
              negatable = true,
              description = "Display tokens list.")
          boolean withTokens,
      @Option(
              names = {"--trace"},
              defaultValue = FALSE,
              negatable = true,
              description = "Display a parsing trace.")
          boolean withTrace,
      @Option(
              names = {"--SLL"},
              defaultValue = FALSE,
              negatable = true,
              description = "Use SLL parsing algorithm.")
          boolean withSLL,
      @Option(
              names = {"--diagnostic"},
              defaultValue = FALSE,
              negatable = true,
              description = "Display diagnostic information?")
          boolean withDiagnostics,
      @Option(
              names = {"--encoding"},
              paramLabel = "ENC",
              description = "Use a specific encoding.")
          String encoding,
      @Option(
              names = {"--postscript"},
              paramLabel = "FILE",
              description = "Output a postscript file.")
          String psFile,
      @Option(
          names = {"--ignore-parent-SOSL-files"},
          description = "Ignore SOSL files provided as parameters of parent commands"
      )
          boolean ignoreParentSOSLFiles,
      @Parameters(
              arity = "0..*",
              paramLabel = "<SOSL file>",
              description = "SOSL files to pass through grun")
          Collection<File> soslFiles
  ) {
    soslFiles = collectSOSLFiles(ignoreParentSOSLFiles, soslFiles);
    for (File soslFile : soslFiles) {
      ForestryUtils.grun(
          withTree,
          withGui,
          withTokens,
          withTrace,
          withSLL,
          withDiagnostics,
          encoding,
          psFile,
          soslFile
      );
    }
  }

  /**
   * Method called to handle the 'parsing' CLI subcommand.
   *
   * @param ignoreParentSOSLFiles whether or not to ignore SOSL files provided
   *                              as parameters of parent commands
   * @param soslFiles SOSL files to check
   */
  @Command(
      name = "parsing",
      description = "Try to parse the provided Set-based Ontology Seed (SOS) file.")
  void parsing(
      @Option(
              names = {"--ignore-parent-SOSL-files"},
              description = "Ignore SOSL files provided as parameters of parent commands")
          boolean ignoreParentSOSLFiles,
      @Parameters(
              arity = "0..*",
              paramLabel = "<SOSL file>",
              description = "SOSL files to check")
          Collection<File> soslFiles
  ) {
    soslFiles = collectSOSLFiles(ignoreParentSOSLFiles, soslFiles);
    for (File soslFile : soslFiles) {
      ForestryUtils.tryToParse(soslFile);
    }
  }
}
