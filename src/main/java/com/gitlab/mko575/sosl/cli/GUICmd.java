/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl.cli;

import static com.gitlab.mko575.sosl.Forestry.mainAppLogger;

import com.gitlab.mko575.sosl.gui.ForestryGUI;
import picocli.CommandLine;
import picocli.CommandLine.Command;

/**
 * This class handles the CLI subcommand "GUI" that parses command line arguments
 * and start the GUI.
 *
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
@Command(
    name = "GUI",
    description = "Starts a Graphical User Interface.",
    subcommands = {CommandLine.HelpCommand.class}
)
public class GUICmd extends CLICmd implements Runnable {

  @Override
  public void run() {
    boolean verbose = collectVerbosity(verbosity);
    soslFiles = collectSOSLFiles(ignoreParentSOSLFiles, soslFiles);
    mainAppLogger.info("Starting GUI!");
    ForestryGUI gui = ForestryGUI.get();
  }
}
