/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl.cli;

import com.gitlab.mko575.sosl.ForestryUtils;
import com.gitlab.mko575.sosl.ForestryUtils.GraphvizEngine;
import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 * This class handles the CLI subcommand "harvest" and delegates all logic to {@link ForestryUtils}.
 *
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
@Command(
    name = "harvest",
    description = "Harvest the desired products from Set-based Ontology Seeds (SOS).",
    subcommands = {CommandLine.HelpCommand.class},
    subcommandsRepeatable = true
)
class HarvestCmd extends CLICmd {

  /**
   * Method called to produce a class diagram (using Graphviz) of the designated ontologies.
   *
   * @param verbosity verbosity options entered on command line
   * @param ignoreParentSOSLFiles whether or not to ignore SOSL files provided
   *                              as parameters of parent commands
   * @param soslFiles SOSL files to load
   * @param outputPath path prefix of all output files
   * @param outputFormats file formats to produce
   * @param seedsRepresentedAsClassDiagram SOS to include in the diagram
   */
  @Command(
      name = "class-diagram-with-graphviz",
      description = "Produces a class diagram (using Graphviz) of the designated ontologies."
  )
  void classDiagramWithGraphviz(
      @Option(
          names = {"-v", "--verbose"},
          description = "Increase verbosity"
      )
      boolean[] verbosity,
      @Option(
          names = {"-i", "--ignore-parent-SOSL-files"},
          description = "Ignore SOSL files provided as parameters of parent commands"
      )
      boolean ignoreParentSOSLFiles,
      @Option(
          names = {"-l", "--SOSL-files"},
              arity = "1..*",
              paramLabel = "<SOSL file>",
              description = "SOSL files to load")
      Collection<File> soslFiles,
      @Option(
          names = {"-o", "--output-path"},
          arity = "1",
          paramLabel = "<path>",
          description = "Path of the output files to produce (without file suffix)"
      )
      File outputPath,
      @Option(
          names = {"-f", "--output-formats"},
          arity = "1..*",
          paramLabel = "dot|pdf|png|svg",
          defaultValue = "pdf",
          description = "Formats (default: ${DEFAULT-VALUE})"
      )
      Collection<String> outputFormats,
      @Parameters(
          arity = "1..*",
          paramLabel = "<ontology>",
          description = "The ontology seeds whose content is to be represented in the diagram."
      )
      Collection<String> seedsRepresentedAsClassDiagram
  ) {
    boolean verbose = collectVerbosity(verbosity);
    soslFiles = collectSOSLFiles(ignoreParentSOSLFiles, soslFiles);
    ForestryUtils.produceClassDiagramWithGraphviz(
        verbose, soslFiles,
        seedsRepresentedAsClassDiagram,
        outputPath, outputFormats
    );
  }

  /**
   * Method called to produce a "raw ontology graph" (using Graphviz) of the
   * designated ontology.
   *
   * @param verbosity verbosity options entered on command line
   * @param ignoreParentSOSLFiles whether or not to ignore SOSL files provided
   *                              as parameters of parent commands
   * @param soslFiles SOSL files to load
   * @param outputPath path prefix of all output files
   * @param outputFormats file formats to produce
   * @param seedId global identifier of the SOS to graph.
   */
  @Command(
      name = "raw-ontology-graph-with-graphviz",
      description = "Produces a \"raw ontology graph\" (using Graphviz) of the"
          + " designated ontology."
  )
  void rawOntologyGraphWithGraphviz(
      @Option(
          names = {"-v", "--verbose"},
          description = "Increase verbosity"
      )
      boolean[] verbosity,
      @Option(
          names = {"-i", "--ignore-parent-SOSL-files"},
          description = "Ignore SOSL files provided as parameters of parent commands"
      )
      boolean ignoreParentSOSLFiles,
      @Option(
          names = {"-l", "--SOSL-files"},
              arity = "1..*",
              paramLabel = "<SOSL file>",
              description = "SOSL files to load")
      Collection<File> soslFiles,
      @Option(
          names = {"-o", "--output-path"},
          arity = "1",
          paramLabel = "<path>",
          description = "Path of the output files to produce (without file suffix)"
      )
      File outputPath,
      @Option(
          names = {"-f", "--output-formats"},
          arity = "1..*",
          paramLabel = "dot|pdf|png|svg",
          defaultValue = "pdf",
          description = "Formats (default: ${DEFAULT-VALUE})"
      )
      Collection<String> outputFormats,
      @Parameters(
          arity = "1",
          paramLabel = "<ontology>",
          description = "The global identifier of the ontology to graph."
      )
      String seedId
  ) {
    boolean verbose = collectVerbosity(verbosity);
    soslFiles = collectSOSLFiles(ignoreParentSOSLFiles, soslFiles);
    ForestryUtils.produceRawOntologyGraphWithGraphviz(
        verbose, soslFiles, seedId, outputPath, outputFormats
    );
  }

  /**
   * Method called to produce (using Graphviz) an "entity-relationship diagram"
   * of the designated ontology.
   *
   * @param verbosity verbosity options entered on command line.
   * @param ignoreParentSOSLFiles if "set", ignore SOSL files provided as parameters
   *                             of parent commands.
   * @param soslFiles SOSL files to load for this specific command.
   * @param outputPath path prefix of all output files.
   * @param outputFormats file formats to produce.
   * @param seedId global identifier of the SOS to represent.
   */
  @Command(
      name = "entity-relationship-diagram-with-graphviz",
      description = "Produces an \"entity-relationship diagram\" (using Graphviz)"
          + " of the designated ontology."
  )
  void entityRelationshipDiagramWithGraphviz(
      @Option(
          names = {"-v", "--verbose"},
          description = "Increase verbosity"
      )
      boolean[] verbosity,
      @Option(
          names = {"-i", "--ignore-parent-SOSL-files"},
          description = "Ignore SOSL files provided as parameters of parent commands"
      )
      boolean ignoreParentSOSLFiles,
      @Option(
          names = {"-l", "--SOSL-files"},
          arity = "1..*",
          paramLabel = "<SOSL file>",
          description = "SOSL files to load")
      Collection<File> soslFiles,
      @Option(
          names = {"-e", "--engine"},
          arity = "1",
          paramLabel = "dot|neato|...",
          defaultValue = "dot",
          description = "Graphviz engine to use. Valid values: ${COMPLETION-CANDIDATES} (default: ${DEFAULT-VALUE})"
      )
      GraphvizEngine engine,
      @Option(
          names = {"-o", "--output-path"},
          arity = "1",
          paramLabel = "<path>",
          description = "Path of the output files to produce (without file suffix)"
      )
      File outputPath,
      @Option(
          names = {"-f", "--output-formats"},
          arity = "1..*",
          paramLabel = "dot|pdf|png|svg",
          defaultValue = "pdf",
          description = "Formats (default: ${DEFAULT-VALUE})"
      )
      Collection<String> outputFormats,
      @Parameters(
          arity = "1",
          paramLabel = "<ontology>",
          description = "The global identifier of the ontology to graph."
      )
      String seedId
  ) {
    boolean verbose = collectVerbosity(verbosity);
    soslFiles = collectSOSLFiles(ignoreParentSOSLFiles, soslFiles);
    ForestryUtils.produceEntityRelationshipDiagramWithGraphviz(
        verbose, soslFiles, seedId, engine, outputPath, outputFormats
    );
  }

}
