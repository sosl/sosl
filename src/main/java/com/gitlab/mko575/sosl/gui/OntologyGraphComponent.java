package com.gitlab.mko575.sosl.gui;

import javax.swing.JComponent;

/**
 * Interface for components that can be integrated to a GUI and display an
 * ontology or an instance as a graph.
 */
public interface OntologyGraphComponent {

  /**
   * Returns the component to integrate to the GUI and that displays the
   * ontology or instance as a graph.
   *
   * @return The {@link JComponent} to integrate
   */
  JComponent getJComponent();

}
