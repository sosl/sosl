/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.mko575.sosl.gui.jgraphx;

import com.gitlab.mko575.sosl.gui.OntologyGraphComponent;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import javax.swing.JComponent;

/**
 * Components of that class display an ontology or instance as a graph using the JGraphX technology.
 */
public class JGXOntologyGraphComponent extends mxGraphComponent implements OntologyGraphComponent {

  public static final long serialVersionUID = 283712205610L;

  /** Create a GUI component that display the graph passed as parameter.
   * @param graph The graph containing the ontology or instance to display
   */
  public JGXOntologyGraphComponent(final mxGraph graph) {
    super(graph);
  }

  @Override
  public JComponent getJComponent() {
    return this;
  }
}
