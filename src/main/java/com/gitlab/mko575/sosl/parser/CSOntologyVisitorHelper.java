package com.gitlab.mko575.sosl.parser;

import com.gitlab.mko575.sosl.ErrorMessages;
import com.gitlab.mko575.sosl.ErrorMessages.ErrorMessagesId;
import com.gitlab.mko575.sosl.ids.GlobalSetId;
import com.gitlab.mko575.sosl.ids.SetId;
import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.sos.OntologySeed;
import com.gitlab.mko575.sosl.parser.ast.ASImport.ImportType;
import com.gitlab.mko575.sosl.parser.ast.ASOntology;
import com.gitlab.mko575.sosl.ids.OntologyId;
import com.gitlab.mko575.sosl.parser.ast.ASOntology.OntologyType;
import com.gitlab.mko575.sosl.parser.ast.ASOntologies;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import scala.Tuple3;
import scala.jdk.javaapi.OptionConverters;

public class CSOntologyVisitorHelper {

  /**
   * Global identifier of the visitor helped.
   */
  private final CST2ASTVisitor visitorHelped;

  /**
   * Global identifier of the ontology visited.
   */
  private final OntologyId oid;

  /**
   * The abstract syntax of the ontology visited.
   */
  private final ASOntology asOntology;

  /**
   * Set of (ontological set) local identifiers already used in the visited ontology.
   */
  private final Set<Object> usedLocalIdentifiers = new HashSet<>();

  /**
   * Current mapping from handles to (ontological set) global identifiers.
   */
  // TODO: move all handling of the mapping handle to sid here
  // At the end of ASOntology creation copy this information to the ASontology
  private final Map<String, SetId> hdl2sid = new HashMap<>();

  /**
   * Current mapping from (ontological set) global identifiers to ontological set
   */
  // TODO: delete if not used; if used see if "usedLocalIdentifiers" can be
  //  implemented using keys()
  // TODO: try to remove OSets from ASElements, excepts ASontology that gets
  //  those here at the end of its parsing
  private final Map<SetId, OSet> sid2oset = new HashMap<>();

  /**
   * Global identifiers of the ontological sets whose import is delayed to their
   * first use. Those correspond to the implicit imports. If one of those ontological
   * sets is never used, then it is not imported in the ontology.<br/>
   * Mapping:<br/>
   *  - from (new) handle in the ontology importing to;<br/>
   *  - to a pair composed of:<br/>
   *     - the global identifier of the ontological set to import (the one in the ontology
   *        importing from)<br/>
   *     - the updated ontological set to import.<br/>
   *  handle -> global id of the OSet to import
   */
  private final Map<String, Tuple3<ImportType, SetId, OSet>> delayedImports = new HashMap<>();

  /**
   * Implicit imports stack. Stack of pairs of, for every import statement:
   *  - the identifier {@link OntologyId} of the ontology whose ontological sets
   *     ({@link OSet}s) are imported,
   *  - the type of import (see {@link ImportType}),
   *  - and sets of global identifiers of ontological sets which are excluded by
   *     this implicit import.
   */
  private final Deque<Tuple3<OntologyId, ImportType, Set<SetId>>> implicitImports = new ArrayDeque<>();

  /**
   * Empty constructor. Should never be used, except for unit test of ontology body fragments.
   */
  public CSOntologyVisitorHelper(CST2ASTVisitor visitor) {
    this.visitorHelped = visitor;
    this.oid = new OntologyId();
    this.asOntology = new ASOntology(oid, oid.lid().toString(), OntologyType.CONCEPT, visitor);
  }

  /**
   * Default constructor. Initializes internal data structure with empty non-null values.
   */
  public CSOntologyVisitorHelper(CST2ASTVisitor visitor, OntologyId oid, ASOntology aso) {
    this.visitorHelped = visitor;
    this.oid = oid;
    this.asOntology = aso;
  }

  /**
   * Returns the global identifier of the ontology visited.
   *
   * @return the global identifier of the ontology visited.
   */
  public OntologyId getVisitedOId() {
    return oid;
  }

  /**
   * Returns the set of ontology ASTs visited so far.
   * @return the set of ontology ASTs visited so far.
   */
  public ASOntologies getDefinedOntologies() {
    return visitorHelped.getDefinedOntologies();
  }

  /**
   * Returns the ontology ({@link OntologySeed}) corresponding to the provided global
   * identifier.
   * @param oid the global identifier of the ontology to return.
   * @return the designated ontology, or null if not present.
   * @see ASOntologies#getOntologySeed(OntologyId)
   */
  public OntologySeed getOntologySeed(OntologyId oid) {
    return getDefinedOntologies().getOntologySeed(oid);
  }

  /**
   * Returns the ontological set global identifier currently associated to the
   * provided handle.
   *
   * @param handle the handle to look for.
   * @return the associated global identifier.
   */
  /* TODO: delete, unused and wrong (only local)
  public SetId peekSIdForHandle(String handle) {
    return asOntology.getSIdForHandle(handle);
  }
  */

  /**
   * Returns the next local identifier in the infinite poset (partially ordered
   * set) of local identifiers.
   *
   * @param fromLId local identifier for which to compute the next one.
   * @return the next local identifier.
   */
  private Object nextDisambiguatedLId(Object fromLId) {
    final Pattern p = Pattern.compile("^(.*)_([0-9]+)$");
    final Matcher m = p.matcher(fromLId.toString());
    final String lidHeader;
    final int lidSuffix;
    if(m.find()) {
      lidHeader = m.group(1);
      lidSuffix = Integer.parseInt(m.group(2));
    } else {
      lidHeader = fromLId.toString();
      lidSuffix = 0;
    }
    return lidHeader + "_" + (lidSuffix + 1);
  }

  /**
   * Returns the fresh local identifier "closest" to the provided local identifier.
   * It does not had this local identifier to the set of used local identifiers.
   * Hence, calling this method on the result of this method returns the same local
   * identifier.
   *
   * @param candidateLId the local identifier for which to find the closest fresh
   *                    local identifier.
   * @return the "closest" fresh local identifier.
   */
  public Object closestFreshLId(Object candidateLId) {
    while ( ! isFreshLId(candidateLId) ) {
      candidateLId = nextDisambiguatedLId(candidateLId);
    }
    return candidateLId;
  }

  /**
   * Test if an ontology local identifier is fresh.
   *
   * @param candidateLId the local identifier to test for freshness.
   * @return {@code true} iff the identifier is fresh.
   */
  public boolean isFreshLId(Object candidateLId) {
    return ! usedLocalIdentifiers.contains(candidateLId);
  }

  /**
   * Add the provided local identifier to the set of used identifiers.
   *
   * @param usedLId the identifier to be added.
   * @return {@code true} iff the identifier was not already registered as used.
   */
  public boolean registerNewLId(Object usedLId) {
    return usedLocalIdentifiers.add(usedLId);
  }

  /**
   * Register the association between an handle and the global identifier it
   * references.
   *
   * @param handle the handle whose association to register.
   * @param sid the associated globale identifier.
   * @return the previous global identifier associated to this handle if there
   * was one.
   */
  public SetId registerNewHandleAssociation(String handle, SetId sid) {
    if (handle != null) {
      if (sid != null ) {
        return hdl2sid.put(handle, sid);
      } else {
        return hdl2sid.remove(handle);
      }
    } else {
      ErrorMessages.logImplementationError(
          "An handle/alias is not supposed to be null"
      );
    }
    return null;
  }

  /**
   * Register an ontological set used in the ontology being parsed.
   *
   * @param sid the global identifier of the ontological set.
   * @param oset the ontological set "description".
   * @return the previous (version) associated to the global identifier.
   */
  public OSet registerOSet(SetId sid, OSet oset) {
    if (sid != null) {
      if (sid2oset.containsKey(sid))
        // This happens regularly because the information associated to the oset can change.
        ErrorMessages.signal(Level.FINEST, false,
            String.format("Registering an oset for the sid '%s' multiple times", sid)
        );
      if (oset != null ) {
        return sid2oset.put(sid, oset);
      } else {
        ErrorMessages.logImplementationError(
            "A global identifier mapping is not supposed to be removed"
        );
        ErrorMessages.signal(Level.SEVERE, true, "while working on "+sid);
        StackWalker stackWalker =
            StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE);
        stackWalker.forEach(f -> System.out.println(f.toStackTraceElement()));
      }
    } else {
      ErrorMessages.logImplementationError(
          "A global identifier is not supposed to be null"
      );
    }
    return null;
  }

  /**
   * Updates the information related to delayed or implicit imports.
   *
   * @param fromOId the identifier of the ontology whose ontological sets are imported.
   * @param importType the type of import (see {@link ImportType}).
   * @param delayedImports the delayed imports to add.
   * @param notImplicitlyImported if not {@code null} then all other ontological
   *                             sets (those whose identifier is not included in
   *                             the provided set) are implicitly imported.
   */
  public void updateImports(
      OntologyId fromOId,
      ImportType importType,
      Map<String, Tuple3<ImportType, SetId, OSet>> delayedImports,
      Set<SetId> notImplicitlyImported
  ) {
    this.delayedImports.putAll(delayedImports);
    if (notImplicitlyImported != null)
      implicitImports.push(new Tuple3<>(fromOId, importType, notImplicitlyImported));
  }

  /**
   * Retrieves the ontological set global identifier associated to the provided
   * handle. If no identifier is currently associated to the handle, then it looks
   * in delayed imports, and then in implicit imports. If an associated delayed
   * or implicit import is found, the associated ontological set is imported and
   * and its new global identifier is returned.
   *
   * @param hdl the handle to look for.
   * @param assos the known ontologies, if an implicit import must be searched
   *              for.
   * @return the associated global identifier, or {@code null} if there is none.
   */
  public SetId getSId4handle(String hdl, ASOntologies assos) {
    if (hdl2sid.containsKey(hdl)) return hdl2sid.get(hdl);
    // TODO: update code below if alias handling is moved here!
    SetId sidOfHdl = asOntology.getSIdForHandle(hdl);
    if ( sidOfHdl == null ) {
      if (delayedImports.containsKey(hdl)) {
        Tuple3<ImportType, SetId, OSet> importedOSet = delayedImports.get(hdl);
        delayedImports.remove(hdl);
        sidOfHdl = asOntology.importOSet(
            null, importedOSet._1(), importedOSet._2(), importedOSet._3()
        );
        registerOSet(sidOfHdl, importedOSet._3());
      } else {
        Iterator<Tuple3<OntologyId, ImportType, Set<SetId>>> ite = implicitImports.iterator();
        SetId importedSId = null;
        ImportType importType = null;
        while ( importedSId == null && ite.hasNext() ) {
          Tuple3<OntologyId, ImportType, Set<SetId>> e = ite.next();
          final OntologyId fromOId = e._1();
          importedSId = assos.getOntologyProxy(fromOId).getSIdForHandle(hdl);
          if ( importedSId != null) {
            final Set<SetId> excludedOSets = e._3();
            if (
                OptionConverters.toJava(importedSId.getOId())
                    .map(oid -> oid.equals(fromOId))
                    .orElse(true)// It's a local identifier
                && ! excludedOSets.contains(importedSId)
            ) {
              importType = e._2();
            } else {
              importedSId = null;
            }
          }
        }
        if ( importedSId != null ) {
          OSet importedOSet = getOSet4SID(importedSId, assos);
          if (importedOSet == null) {
            ErrorMessages.signal(Level.INFO, false,
              String.format(
                  "While looking for the global identifier associated to handle '%s',"
                      + " it was found to be associated to '%s' but no ontological"
                      + " set could be retrieved for this global identifier.",
                  hdl, importedSId
              )
            );
          }
          sidOfHdl = asOntology.importOSet(
              null, importType, importedSId, importedOSet
          );
          if (sidOfHdl == null) {
            ErrorMessages.signal(Level.INFO, false,
              String.format(
                  "While looking for the global identifier associated to handle '%s', "
                  + "ASOntology.importOSet returned a null identifier.",
                  hdl
              )
            );
          }
          // Below statement removed because sidOfHdl can be null, and ASOntology.importOSet
          // already registers the OSet.
          // TODO: cleanup the code.
          registerOSet(sidOfHdl, importedOSet);
        }
      }
      if ( sidOfHdl != null ) {
        asOntology.updateAlias(hdl, sidOfHdl);
        registerNewHandleAssociation(hdl, sidOfHdl);
      }
    }
    return sidOfHdl;
  }

  /**
   * Returns the ontological set associated to the provided global identifier,
   * according to this helper knowledge.
   * @param sid the global identifier to look for.
   * @return the associated ontological set, if it exists.
   */
  public OSet getOSet4SID(SetId sid) {
    if (sid == null) {
      ErrorMessages.logImplementationError(
          "Calling this method with a null identifier is likely an error"
      );
    }
    return sid2oset.get(sid);
  }

  public OSet getOSet4SID(SetId sid, ASOntologies assos) {
    if (sid == null) {
      ErrorMessages.logImplementationError(
          "Calling this method with a null identifier is likely an error"
      );
    }
    OSet res = getOSet4SID(sid);
    if (res == null) {
      if (
          Optional.ofNullable(sid)
              .flatMap(id -> OptionConverters.toJava(id.getOId()))
              .map(oid -> oid.equals(getVisitedOId()))
              .orElse(false)
      ) {
        res = getOSet(sid.lid());
      } else {
        res = assos.getOSet(sid);
      }
    }
    return res;
  }

  /**
   * Returns the ontological set associated, in the visited ontology, with the provided
   * local identifier.
   *
   * @param lid the local identifier.
   * @return the ontological set associated with the provided local identifier.
   */
  public OSet getOSet(Object lid) {
    if (lid == null) {
      ErrorMessages.logImplementationError(
          "Calling this method with a null identifier is likely an error"
      );
    }
    OSet res = getOSet4SID(new GlobalSetId(getVisitedOId(), lid));
    if (res == null)
      res = asOntology.getOSet4lId(lid);
    return res;
  }
}
