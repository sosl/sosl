package com.gitlab.mko575.sosl.parser;

import static com.gitlab.mko575.sosl.Forestry.mainAppLogger;
import static com.gitlab.mko575.sosl.Forestry.mainAppOutput;

import com.gitlab.mko575.sosl.ErrorMessages;
import com.gitlab.mko575.sosl.ErrorMessages.ErrorMessagesId;
import com.gitlab.mko575.sosl.ids.GlobalSetId;
import com.gitlab.mko575.sosl.ids.LocalSetId;
import com.gitlab.mko575.sosl.ids.OntologyId;
import com.gitlab.mko575.sosl.ids.SetId;
import com.gitlab.mko575.sosl.ids.Atomic;
import com.gitlab.mko575.sosl.ids.Complement;
import com.gitlab.mko575.sosl.ids.Distinct;
import com.gitlab.mko575.sosl.ids.Emptyset$;
import com.gitlab.mko575.sosl.ids.Entities$;
import com.gitlab.mko575.sosl.ids.Equal;
import com.gitlab.mko575.sosl.ids.Finite;
import com.gitlab.mko575.sosl.ids.Functional;
import com.gitlab.mko575.sosl.ids.In;
import com.gitlab.mko575.sosl.ids.Injective;
import com.gitlab.mko575.sosl.ids.Intersection;
import com.gitlab.mko575.sosl.ids.Junction;
import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.Powerset;
import com.gitlab.mko575.sosl.ids.Property;
import com.gitlab.mko575.sosl.ids.Relations;
import com.gitlab.mko575.sosl.ids.Restriction;
import com.gitlab.mko575.sosl.ids.SetCons;
import com.gitlab.mko575.sosl.ids.SetTerm;
import com.gitlab.mko575.sosl.ids.Subset;
import com.gitlab.mko575.sosl.ids.Surjective;
import com.gitlab.mko575.sosl.ids.Total;
import com.gitlab.mko575.sosl.ids.Union;
import com.gitlab.mko575.sosl.ids.UnionProduct;
import com.gitlab.mko575.sosl.ids.Values$;
import com.gitlab.mko575.sosl.ids.CurrentNamespace$;
import com.gitlab.mko575.sosl.ids.ExtendedNamespace;
import com.gitlab.mko575.sosl.ids.Namespace;
import com.gitlab.mko575.sosl.ids.ParentNamespace$;
import com.gitlab.mko575.sosl.ids.RootNamespace$;
import com.gitlab.mko575.sosl.ids.sos.OntologySeed;
import com.gitlab.mko575.sosl.parser.SOSLParser.All_SIIDContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.AssociativeOpContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.AssociativeOpOnSet_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.BinPredContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.BinaryOpContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.BinaryOpOnSet_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.BinaryPred_AFContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.CartesianProduct_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.ClosingTerm_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.EmbedablePredContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.EmptysetIdContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.EntitiesIdContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.FunctionalBinOpContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.GlobalOntologyIdContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.ImportStmtContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.InstanceDefinitionContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.JoinOpContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.Join_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.LoadCmdContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.NSA_CurrentContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.NSA_IdContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.NSA_UpContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.NamespaceContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.NamespaceRootingContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.NaryPred_AFContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.NsInfixDefContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.NsSeparator_KWContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.OntologyDefinitionContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.Others_SIIDContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.RestrictionBinOpContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.Restriction_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.SetIdImportDefContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.SetId_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.Set_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.SosFileContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.TermsSequenceContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.TupleFieldContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.Tuple_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.UnaryOpOnSet_STContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.UnaryPred_AFContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.Unique_SIIDContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.UserDefinedSetIdContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.ValuesIdContext;
import com.gitlab.mko575.sosl.parser.ast.ASElement;
import com.gitlab.mko575.sosl.parser.ast.ASFormulae;
import com.gitlab.mko575.sosl.parser.ast.ASImport;
import com.gitlab.mko575.sosl.parser.ast.ASImport.ImportType;
import com.gitlab.mko575.sosl.parser.ast.ASNamespace;
import com.gitlab.mko575.sosl.parser.ast.ASOntologies;
import com.gitlab.mko575.sosl.parser.ast.ASOntology;
import com.gitlab.mko575.sosl.parser.ast.ASOntology.OntologyType;
import com.gitlab.mko575.sosl.parser.ast.ASOntologyId;
import com.gitlab.mko575.sosl.parser.ast.ASTermSeq;
import com.gitlab.mko575.sosl.parser.ast.ASTupleFieldSeq;
import com.gitlab.mko575.sosl.parser.ast.ASVoid;
import com.gitlab.mko575.sosl.parser.libraries.AbstractBuiltinOP;
import com.gitlab.mko575.sosl.parser.libraries.BuiltinOPLibrary;
import com.google.common.collect.ImmutableSet;
import java.io.File;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import scala.Option;
import scala.Tuple2;
import scala.jdk.javaapi.CollectionConverters;
import scala.jdk.javaapi.OptionConverters;

/**
 * Top level visitor class transforming SOSL file Concrete Syntax Trees (CST),
 * as parsed by {@link SOSLParser}, into Abstract Syntax Trees, as defined by
 * {@link OntologySeed}.
 */
public class CST2ASTVisitor
    extends SOSLOverridingAssertedVisitor<ASElement> {

  {
    BuiltinOPLibrary.init();
  }

  /**
   * Last seen parsing context.
   */
  private final Deque<ParserRuleContext> ctxStack = new ArrayDeque<>();

  @Override
  public ASElement visit(ParseTree tree) {
    mainAppLogger.finest(
        "Entering 'CST2ASTVisitor::visit' on '" + tree.getText() + "'."
    );
    if (tree instanceof ParserRuleContext) {
      ctxStack.push((ParserRuleContext) tree);
    }
    final ASElement res =  super.visit(tree);
    if (tree instanceof ParserRuleContext) {
      ctxStack.pop();
    }
    return res;
  }

  /***************************************************************************/

  /**
   * Default logging level for (non-error) log messages from this class.
   */
  private static final Level DFLT_DEBUG_LOG_LEVEL = Level.FINEST;

  /**
   * The file being parsed.
   */
  private final File parsedFile;

  /**
   * The file being parsed.
   */
  private final Object parsedFileMarker;

  /**
   * Namespace at time the ontology file has been loaded (i.e. the namespace in
   * which the file "exists").
   */
  private final Namespace namespacePrefix;

  /**
   * Whether or not to use the namespaceInfix provided in the file.
   */
  private final boolean useNamespaceInfix;

  /**
   * Namespace addition made by the ontology file.
   */
  private Namespace namespaceInfix = CurrentNamespace$.MODULE$;

  /**
   * Current set of {@link ASOntology}s retrieved so far.
   */
  private final ASOntologies definedOntologies = new ASOntologies();

  /**
   * Collected data while visiting an `ontologyDefinition`. This data is collected
   * on the way down. The goal is to have the necessary data available when it's
   * consumed and not reconstructed on the way up while building the resulting
   * {@link ASOntology}.
   */
  private CSOntologyVisitorHelper ontologyVisitorHelper = new CSOntologyVisitorHelper(this);

  /******************/
  /** CONSTRUCTORS **/
  /******************/

  /**
   * Default constructor for CST2ASTVisitor.
   */
  public CST2ASTVisitor() {
    this(RootNamespace$.MODULE$, null, true);
  }

  /**
   * Constructor for CST2ASTVisitor that allows parametrization of parameters.
   *
   * @param fileNamespace Namespace to start from when parsing the file.
   * @param soslFile the file being parsed.
   * @param useNamespaceInfix Whether or not to use the NSInfix provided in the file.
   */
  public CST2ASTVisitor(Namespace fileNamespace, File soslFile, boolean useNamespaceInfix) {
    super();
    this.parsedFile = soslFile;
    this.parsedFileMarker =
        soslFile != null ? ParsingUtils.getMarkerForSoslFile(soslFile) : null;
    this.namespacePrefix = fileNamespace;
    this.useNamespaceInfix = useNamespaceInfix;
  }

  /*************************/
  /** SETTERS AND GETTERS **/
  /*************************/

  /**
   * Set the namespace infix added by the ontology file.
   * @param nsInfix the namespace infix to use from now on.
   */
  public void setNamespaceInfix(Namespace nsInfix) {
    this.namespaceInfix = nsInfix;
  }

  /**
   * Get the current namespace, which is the concatenation of the
   * namespacePrefix and namespaceInfix.
   * @return the current namespace.
   */
  private Namespace getCurrentNamespace() {
    if (useNamespaceInfix) return namespacePrefix.andThen(namespaceInfix);
    else return namespacePrefix;
  }

  /**
   * Returns the set of ontology ASTs visited so far.
   * @return the set of ontology ASTs visited so far.
   */
  public ASOntologies getDefinedOntologies() {
    return definedOntologies;
  }

  /********************************/
  /** Helpers of 'visitChildren' **/
  /********************************/

  /**
   * Initial aggregator to be used for the next visitChildren call.
   */
  private ASElement visitChildrenInitialAggregator = null;

  /**
   * Sets the initial aggregator to be used for the next visitChildren call.
   * @param aggregator the value to use.
   */
  protected void setVisitChildrenInitialAggregator(ASElement aggregator) {
    visitChildrenInitialAggregator = aggregator;
  }

  /**
   * Cleanup specializations of {@link AbstractParseTreeVisitor#visitChildren(RuleNode)}.
   */
  protected void finalizeVisitChildren() {
    setVisitChildrenInitialAggregator(null);
  }

  @Override
  protected ASElement defaultResult() {
    return
        ( visitChildrenInitialAggregator != null )
            ? visitChildrenInitialAggregator
            : super.defaultResult();
  }

  @Override
  protected ASElement aggregateResult(ASElement aggregate, ASElement nextResult) {
    if ( nextResult == null ) {
      final ParserRuleContext ctx = ctxStack.peek();
      final Token pos = ctx != null ? ctx.getStart() : null;
      final String errorMsg =
          String.format(
              "Aggregating %s with a null nextResult at %d:%d. This is a strange behavior.",
              aggregate,
              pos != null ? pos.getLine() : -1,
              pos != null ? pos.getCharPositionInLine() : -1
          );
      // assert false : errorMsg; // Will uncomment when issue fixed!
      ErrorMessages.signal(Level.FINEST, false, errorMsg);
      return aggregate;
    }
    if ( nextResult instanceof ASVoid ) {
      return aggregate;
    }
    if ( nextResult == aggregate ) {
      return aggregate;
    }
    if (aggregate.aggregatable(nextResult)) {
      return aggregate.aggregate(nextResult);
    }
    return super.aggregateResult(aggregate, nextResult);
  }

  /**********************/
  /** Generic helpers  **/
  /**********************/

  /**
   * Private method logging a generic error message about syntactic error if at
   * least one of its parameters is null.
   * @param ctx the parsing context where the test is done.
   * @param elements the elements that must be non null.
   * @return
   */
  private boolean allRequiredSyntacticElementsPresent(ParserRuleContext ctx, Object... elements) {
    final boolean missingElement =
        Arrays.stream(elements).anyMatch(Objects::isNull);
    if ( missingElement ) {
      final String errorMsg =
          String.format(
              ErrorMessages.get(ErrorMessagesId.SYNTAX_ERROR_AT_P),
              ctx.getStart().getLine(),
              ctx.getStart().getCharPositionInLine()
          );
      mainAppOutput.out(errorMsg);
      if (mainAppLogger.isLoggable(Level.WARNING)) {
        mainAppLogger.warning(errorMsg);
      }
    }
    return ! missingElement;
  }

  /**************/
  /** SOS-FILE **/
  /**************/

  @Override
  public ASOntologies visitSosFile(final SosFileContext ctx) {
    setVisitChildrenInitialAggregator(new ASOntologies());
    ASOntologies res = (ASOntologies) visitChildren(ctx);
    // res.finalizeParsing();
    finalizeVisitChildren();
    return res;
  }

  /****************/
  /** NAMESPACES **/
  /****************/

  @Override
  public ASVoid visitNsSeparator_KW(NsSeparator_KWContext ctx) {
    return ASVoid.instance();
  }

  @Override
  public ASNamespace visitNamespaceRooting(NamespaceRootingContext ctx) {
    return new ASNamespace(RootNamespace$.MODULE$);
  }

  @Override
  public ASNamespace visitNSA_Up(NSA_UpContext ctx) {
    return new ASNamespace(ParentNamespace$.MODULE$);
  }

  @Override
  public ASNamespace visitNSA_Current(NSA_CurrentContext ctx) {
    return new ASNamespace(CurrentNamespace$.MODULE$);
  }

  @Override
  public ASNamespace visitNSA_Id(NSA_IdContext ctx) {
    return new ASNamespace(new ExtendedNamespace(CurrentNamespace$.MODULE$, ctx.id.getText()));
  }

  @Override
  public ASNamespace visitNamespace(NamespaceContext ctx) {
    return visitNamespace(CurrentNamespace$.MODULE$, ctx);
  }

  /**
   * Parse a namespace starting in the namespace provided as parameter.
   *
   * @param startNS namespace into which to start parsing the namespace.
   * @param ctx context containing the namespace to parse.
   * @return the namespace parsed.
   */
  public ASNamespace visitNamespace(Namespace startNS, NamespaceContext ctx) {
    setVisitChildrenInitialAggregator(new ASNamespace(startNS));
    ASNamespace res = (ASNamespace) visitChildren(ctx);
    finalizeVisitChildren();
    return res;
  }

  @Override
  public ASVoid visitNsInfixDef(final NsInfixDefContext ctx) {
    final ASNamespace nsi = visitNamespace(ctx.namespace());
    setNamespaceInfix(nsi.getNamespace());
    // System.out.println("ASVoid is: " + ASVoid.instance());
    return ASVoid.instance();
  }

  /******************/
  /** ONTOLOGY IDS **/
  /******************/

  @Override
  public ASOntologyId visitGlobalOntologyId(GlobalOntologyIdContext ctx) {
    if ( ! allRequiredSyntacticElementsPresent(ctx, ctx.lid) ) {
      return null;
    }
    final ASNamespace asns;
    if ( ctx.ns == null ) {
      asns = new ASNamespace(CurrentNamespace$.MODULE$);
    } else {
      asns = visitNamespace(ctx.ns);
    }
    final Namespace oidNS = getCurrentNamespace().andThen(asns.getNamespace());
    final OntologyId oid = OntologyId.apply(oidNS, ctx.lid.getText());
    return new ASOntologyId(oid);
  }

  /************************/
  /** LOADING ONTOLOGIES **/
  /************************/

  /**
   * Load ontologies from a SOSL file.
   *
   * @param ctx the parse tree containing the load command.
   * @return the loaded ontologies.
   */
  private ASOntologies loadOntologyFromFile(LoadCmdContext ctx) {
    if ( ! allRequiredSyntacticElementsPresent(ctx, ctx.file) ) {
      ErrorMessages.logImplementationError("Method preconditions not respected");
      return null;
    }
    final String soslFileStr = ctx.file.getText();
    final String soslFile = soslFileStr.substring(1, soslFileStr.length() - 1);
    Namespace fileNS = RootNamespace$.MODULE$;
    if ( ctx.insideSpecificNS != null ) {
      fileNS = (ctx.withoutLocalNSI != null) ? this.namespacePrefix : getCurrentNamespace();
      fileNS = visitNamespace(fileNS, ctx.namespace()).getNamespace();
    }
    ASOntologies assos =
        SOSLParsers.parseSOSLFile(
            false,
            fileNS,
            ctx.withoutFileNSI == null,
            new File(parsedFile.getParent(), soslFile)
        );
    definedOntologies.putAll(assos.getOntologyProxies());
    return assos;
  }

  /**
   * Load an ontology from the library.
   *
   * @param ctx the parse tree containing the load command.
   * @return the loaded ontologies.
   */
  private ASOntologies loadOntologyFromLibrary(LoadCmdContext ctx) {
    if ( ! allRequiredSyntacticElementsPresent(ctx, ctx.oid) ) {
      ErrorMessages.logImplementationError("Method preconditions not respected");
      return null;
    }
    final OntologyId oid = visitGlobalOntologyId(ctx.oid).getOntologyId();
    final AbstractBuiltinOP lib = BuiltinOPLibrary.getOPLibrary(oid);
    Namespace insideNS = RootNamespace$.MODULE$;
    if ( ctx.insideSpecificNS != null ) {
      insideNS = (ctx.withoutLocalNSI != null) ? this.namespacePrefix : getCurrentNamespace();
      insideNS = visitNamespace(insideNS, ctx.namespace()).getNamespace();
    }
    final OntologyId loadedAs;
    if ( ctx.newId != null ) {
      OntologyId newId = visitGlobalOntologyId(ctx.newId).getOntologyId();
      loadedAs = new OntologyId(insideNS.andThen(newId.ns()), newId.lid());
    } else {
      if ( ctx.insideSpecificNS != null ) {
        loadedAs = new OntologyId(insideNS, oid.lid());
      } else {
        loadedAs = oid;
      }
    }
    lib.setOId(loadedAs);
    definedOntologies.put(loadedAs,lib);
    mainAppLogger.log(DFLT_DEBUG_LOG_LEVEL, "Adding ontology proxy for " + oid + " as " + loadedAs);
    return new ASOntologies(loadedAs,lib);
  }

  @Override
  public ASElement visitLoadCmd(LoadCmdContext ctx) {
    if (ctx.file != null) {
      return loadOntologyFromFile(ctx);
    } else if ( ctx.oid != null ) {
      return loadOntologyFromLibrary(ctx);
    } else {
      ErrorMessages.logImplementationError("Case missing");
      return ASVoid.instance();
    }
  }

  /*******************/
  /** ONTOLOGY BODY **/
  /*******************/

  @Override
  public ASOntology visitOntologyDefinition(final OntologyDefinitionContext ctx) {
    // Retrieving parsed data
    final String seedLocalId = ctx.ontologyId.getText();
    final String seedName;
    if (ctx.name != null) {
      final String seedNameStr = ctx.name.getText();
      seedName = seedNameStr.substring(1, seedNameStr.length() - 1);
    } else {
      seedName = seedLocalId;
    }
    final OntologyId seedId =
        OntologyId.apply(
            parsedFileMarker, RootNamespace$.MODULE$.andThen(namespaceInfix),
            getCurrentNamespace(), seedLocalId);
    // Resetting ontology-related global data structures
    ASOntology aso = new ASOntology(seedId, seedName, OntologyType.CONCEPT, this);
    ontologyVisitorHelper = aso.getVisitHelper();
    // Visiting children
    setVisitChildrenInitialAggregator(aso);
    if ( ctx.body != null )
      aso = (ASOntology) visitChildren(ctx.body);
    finalizeVisitChildren();
    // Finalizing ontology seed visit
    aso.finalizeCreation(definedOntologies);
    definedOntologies.put(seedId, aso);
    return aso;
  }

  /**************************/
  /** IMPORTING CATEGORIES **/
  /**************************/

  @Override
  public ASImport visitImportStmt(ImportStmtContext ctx) {
    // Parsing import source
    OntologyId fromOId = visitGlobalOntologyId(ctx.ontologyId).getOntologyId();
    mainAppLogger.log(DFLT_DEBUG_LOG_LEVEL, "Looking for ontology " + fromOId);
    IOntologyProxy fromASOntology = definedOntologies.getOntologyProxy(fromOId);
    if ( fromASOntology == null ) {
      String errorMsg =
          String.format(
              ErrorMessages.get(ErrorMessagesId.ONTOLOGY_S_NOT_LOADED_AT_P)
                  + "\nKnown ontologies are: %s."
                  + "\nSkipping this import statement!",
              fromOId.lid().toString(),
              ctx.getStart().getLine(),
              ctx.getStart().getCharPositionInLine(),
              definedOntologies.getOntologyProxies().keySet()
          );
      ErrorMessages.signal(Level.INFO, true, errorMsg);
      /*
      System.out.println("****************** FromOId = " + fromOId);
      System.out.println(definedOntologies.getOntologyProxies().keySet());
      System.out.println(definedOntologies.getOntologyProxies().entrySet());
      */
      return null;
    }
    assert fromOId.equals(fromASOntology.getSeed().getGlobalIdentifier()) :
        "Identifiers of retrieved and looked for ontologies are different!";

    // Parsing import command
    final ImportType importType;
    if (ctx.importCmd().copy_KW() != null) {
      importType = ImportType.COPY;
    } else if (ctx.importCmd().link_KW() != null) {
      importType = ImportType.LINK;
    } else {
      importType = null;
    }
    final boolean implicitGlobaly = (ctx.importCmd().implicitly_KW() != null);

    ASImport asImport = new ASImport(
        importType, implicitGlobaly, fromOId, fromASOntology,
        ontologyVisitorHelper.getVisitedOId()
    );
    for(SetIdImportDefContext ictx : ctx.importedSets) {
      if (ictx instanceof All_SIIDContext) {
        asImport.importAll(
            ontologyVisitorHelper,
            implicitGlobaly || ((All_SIIDContext) ictx).implicitly != null,
            true);
      }
      if (ictx instanceof Others_SIIDContext) {
        asImport.importAll(
            ontologyVisitorHelper,
            implicitGlobaly || ((Others_SIIDContext) ictx).implicitly != null,
            false);
      }
      if (ictx instanceof Unique_SIIDContext) {
        // retrieve parsed data
        final String importedHdle = ((Unique_SIIDContext) ictx).oldHdle.getText();
        final SetId importedSId = fromASOntology.getSIdForHandle(importedHdle);
        if ( importedSId == null ) {
          ErrorMessages.signal(
              Level.INFO, true,
              ErrorMessagesId.ONTOLOGY_S_DOES_NOT_DEFINE_S_ERROR_AT_L,
              fromASOntology.getName(), importedHdle,
              parsedFile.getName(),
              ctx.getStart().getLine(),
              ctx.getStart().getCharPositionInLine()
          );
          // if unknown oldId, skip it:
          continue;
        }
        final String newHdle =
            Optional.ofNullable(((Unique_SIIDContext) ictx).newHdle)
                .map(Token::getText)
                .orElse(importedHdle);
        final Optional<String> newNameStr =
            Optional.ofNullable(((Unique_SIIDContext) ictx).newName)
                .map(Token::getText);
        final String newName =
            newNameStr.map(s -> s.substring(1, s.length() - 1))
                .orElse(null);
        asImport.importOne(
            ontologyVisitorHelper,
            importedSId, newHdle, newName,
            implicitGlobaly || ((Unique_SIIDContext) ictx).implicitly != null, true
        );
      }
    }
    mainAppLogger.log(DFLT_DEBUG_LOG_LEVEL,
        String.format("Parsing the following import statement%n%s", asImport.toDetailedString())
    );
    return asImport;
  }

  /*********************/
  /** ATOMIC FORMULAE **/
  /*********************/

  @Override
  public ASFormulae visitUnaryPred_AF(final UnaryPred_AFContext ctx) {
    Function<SetTerm, Property> constructor = null;
    if (ctx.pred.finite_KW() != null) {
      constructor = Finite::new;
    } else if (ctx.pred.atomic_KW() != null) {
      constructor = Atomic::new;
    } else if (ctx.pred.total_KW() != null) {
      constructor = Total::new;
    } else if (ctx.pred.functional_KW() != null) {
      constructor = Functional::new;
    } else if (ctx.pred.surjective_KW() != null) {
      constructor = Surjective::new;
    } else if (ctx.pred.injective_KW() != null) {
      constructor = Injective::new;
    } else {
      String errorMsg =
          String.format(
              ErrorMessages.get(ErrorMessagesId.CASE_IMPLEMENTATION_MISSING_IN_S_OF_S),
              "visitUnaryPred_AF", this.getClass()
          );
      assert false : errorMsg;
      if (mainAppLogger.isLoggable(Level.SEVERE)) { mainAppLogger.severe(errorMsg); }
    }

    final ASTermSeq asTermSeq = (ASTermSeq) visit(ctx.termsSequence());

    ASFormulae res = new ASFormulae(asTermSeq.getAliases(), asTermSeq.getReferencedCategories());
    for (SetTerm s : asTermSeq.getTerms()) { // NOPMD: s is not 'final'!
      res.addProperty(constructor.apply(s));
    }

    return res;
  }

  /**
   * Transforms a {@link Collection} of {@link EmbedablePredContext} applied on a
   * {@link Collection} of {@link SetTerm} into a {@link Collection} of {@link Property}.
   *
   * @param setColl the {@link SetTerm}s to which the embedable predicates are applied.
   * @param embeddablePreds  the embeddable predicates to be applied.
   * @return the result of the transformation
   */
  private Collection<Property> embeddedFormulaVisitor(
      final Collection<SetTerm> setColl,
      final List<EmbedablePredContext> embeddablePreds
  ) {
    final Collection<Property> propRes = new HashSet<>();
    for (EmbedablePredContext epCtx : embeddablePreds) { // NOPMD: epCtx is not 'final'!
      if (epCtx.unaryPred() != null) {
        final Function<SetTerm, Property> newProperty;
        if (epCtx.unaryPred().atomic_KW() != null) {
          newProperty = Atomic::new;
        } else if (epCtx.unaryPred().finite_KW() != null) {
          newProperty = Finite::new;
        } else {
          String errorMsg =
              String.format(
                  ErrorMessages.get(ErrorMessagesId.CASE_IMPLEMENTATION_MISSING_IN_S_OF_S),
                  "embeddedFormulaVisitor", this.getClass()
              );
          assert false : errorMsg;
          if (mainAppLogger.isLoggable(Level.SEVERE)) mainAppLogger.severe(errorMsg);
          newProperty = null;
        }
        setColl.forEach(s -> propRes.add(newProperty.apply(s)));
      } else if (epCtx.naryPred() != null) {
        if (epCtx.naryPred().distinct_KW() != null) {
          propRes.add(
              new Distinct(// NOPMD: I do want to create a new Object
                  CollectionConverters.asScala(new HashSet<SetTerm>(setColl)).toSet() // NOPMD
                  // CollectionConverters.asScala(setColl
                  // .stream().collect(Collectors.toSet()))
                  ));
        } else {
          String errorMsg =
              String.format(
                  ErrorMessages.get(ErrorMessagesId.CASE_IMPLEMENTATION_MISSING_IN_S_OF_S),
                  "embeddedFormulaVisitor", this.getClass()
              );
          assert false : errorMsg;
          if (mainAppLogger.isLoggable(Level.SEVERE)) mainAppLogger.severe(errorMsg);
        }
      }
    }
    return propRes;
  }

  /**
   * Returns a {@link Property} constructor for the predicate provided as parameter.
   *
   * @param predCtx the CST of the predicate to get a constructor for.
   * @return the desired constructor.
   */
  private BiFunction<SetTerm, SetTerm, Property> getBinPred(BinPredContext predCtx) {
    final BiFunction<SetTerm, SetTerm, Property> constructor;
    if (predCtx.subset_KW() != null) {
      constructor = (lhs, rhs) -> new Subset(lhs, rhs, true);
    } else if (predCtx.subseteq_KW() != null) {
      constructor = (lhs, rhs) -> new Subset(lhs, rhs, false);
    } else if (predCtx.superset_KW() != null) {
      constructor = (lhs, rhs) -> new Subset(rhs, lhs, true);
    } else if (predCtx.superseteq_KW() != null) {
      constructor = (lhs, rhs) -> new Subset(rhs, lhs, false);
    } else if (predCtx.in_KW() != null) {
      constructor = (lhs, rhs) -> new In(lhs, rhs);
    } else if (predCtx.equal_KW() != null) {
      constructor =
          (lhs, rhs) -> new Equal(CollectionConverters.asScala(Set.of(lhs, rhs)).toSet());
    } else {
      String errorMsg =
          String.format(
              ErrorMessages.get(ErrorMessagesId.CASE_IMPLEMENTATION_MISSING_IN_S_OF_S),
              "getBinPred", this.getClass()
          );
      assert false : errorMsg;
      if (mainAppLogger.isLoggable(Level.SEVERE)) mainAppLogger.severe(errorMsg);
      constructor = (lhs, rhs) -> null;
    }
    return constructor;
  }

  @Override
  public ASFormulae visitBinaryPred_AF(final BinaryPred_AFContext ctx) {
    final ASTermSeq lhsASTermSeq = (ASTermSeq) visit(ctx.lhs);
    final ASTermSeq rhsASTermSeq = (ASTermSeq) visit(ctx.rhs);

    final ASFormulae res = new ASFormulae();
    res.addReferences(lhsASTermSeq.getAliases(), lhsASTermSeq.getReferencedCategories());
    res.addReferences(rhsASTermSeq.getAliases(), rhsASTermSeq.getReferencedCategories());

    if (ctx.embeddedFormulae() != null) {
      res.addProperties(
          embeddedFormulaVisitor(
              lhsASTermSeq.getTerms(),
              ctx.embeddedFormulae().embedablePred()
          )
      );
    }

    final BiFunction<SetTerm, SetTerm, Property> constructor = getBinPred(ctx.pred);

    final int nbLHSTerms = lhsASTermSeq.getTerms().size();
    final int nbRHSTerms = rhsASTermSeq.getTerms().size();
    if ( nbLHSTerms == nbRHSTerms ) {
      IntStream.range(0,nbLHSTerms).forEach(
          i -> res.addProperty(constructor.apply(
              lhsASTermSeq.getTerms().get(i),
              rhsASTermSeq.getTerms().get(i)
              ))
      );
    } else if ( nbLHSTerms == 1 || nbRHSTerms == 1 ) {
      for (SetTerm lhs : lhsASTermSeq.getTerms()) { // NOPMD: lhs is not 'final'!
        for (SetTerm rhs : rhsASTermSeq.getTerms()) { // NOPMD: rhs is not 'final'!
          res.addProperty(constructor.apply(lhs, rhs));
        }
      }
    } else {
      String errorMsg =
          String.format(
              "Unbalanced binary formula at (%s,%s).",
              ctx.start.getLine(),
              ctx.start.getCharPositionInLine()
          );
      mainAppOutput.out(errorMsg);
      if (mainAppLogger.isLoggable(Level.SEVERE)) mainAppLogger.severe(errorMsg);
    }

    return res;
  }

  @Override
  public ASFormulae visitNaryPred_AF(final NaryPred_AFContext ctx) {
    final ASTermSeq termsSeq = (ASTermSeq) visit(ctx.termsSequence());
    final ASFormulae res = new ASFormulae();
    res.addReferences(termsSeq.getAliases(), termsSeq.getReferencedCategories());

    if (ctx.pred.distinct_KW() != null) {
      res.addProperty(
          new Distinct(
              CollectionConverters.asScala(
                  new HashSet<SetTerm>(termsSeq.getTerms())
              ).toSet()
          )
      );
    } else {
      String errorMsg =
          String.format(
              ErrorMessages.get(ErrorMessagesId.CASE_IMPLEMENTATION_MISSING_IN_S_OF_S),
              "visitNaryPred_AF", this.getClass()
          );
      assert false : errorMsg;
      if (mainAppLogger.isLoggable(Level.SEVERE)) mainAppLogger.severe(errorMsg);
    }

    return res;
  }

  /***********/
  /** TERMS **/
  /***********/

  /* VISITING  SET CONSTANTS */

  @Override
  public ASTermSeq visitSetId_ST(final SetId_STContext ctx) {
    return (ASTermSeq) visit(ctx.setId());
  }

  @Override
  public ASTermSeq visitEmptysetId(final EmptysetIdContext ctx) {
    return new ASTermSeq(Emptyset$.MODULE$);
  }

  @Override
  public ASTermSeq visitEntitiesId(final EntitiesIdContext ctx) {
    return new ASTermSeq(Entities$.MODULE$);
  }

  @Override
  public ASTermSeq visitValuesId(final ValuesIdContext ctx) {
    return new ASTermSeq(Values$.MODULE$);
  }

  @Override
  public ASTermSeq visitUserDefinedSetId(final UserDefinedSetIdContext ctx) {
    final String hdl = ctx.userDefinedId().id.getText();
    SetId sid = ontologyVisitorHelper.getSId4handle(hdl, definedOntologies);
    OSet oset;
    if ( sid != null ) {
      if (
          sid instanceof LocalSetId
          || OptionConverters.toJava(sid.getOId())
              .map(oid -> oid.equals( ontologyVisitorHelper.getVisitedOId() ))
              .get() // If an exception is generated then we have a non local identifier
                     // without ontology identifier ???
      ) {
        oset = ontologyVisitorHelper.getOSet(sid.lid());
      } else {
        oset = definedOntologies.getOSet(sid);
      }
    } else {
      final Object lid = ontologyVisitorHelper.closestFreshLId(hdl);
      ontologyVisitorHelper.registerNewLId(lid);
      sid = new GlobalSetId(ontologyVisitorHelper.getVisitedOId(), lid);
      ontologyVisitorHelper.registerNewHandleAssociation(hdl, sid);
      oset = new OSet(lid, hdl);
      ontologyVisitorHelper.registerOSet(sid, oset);
    }
    // Renaming if needed
    Token name = ctx.name;
    if ( name != null ) {
      String nStr = name.getText();
      oset = oset.updateName(nStr.substring(1, nStr.length() - 1));
      ontologyVisitorHelper.registerOSet(sid, oset);
      // TODO: Verify that changing the name of a category (identifying the same
      //  category twice with different names) does not break everything.
    }
    // Returning the result
    // return new ASTermSeq(sid); ???
    return new ASTermSeq(hdl, sid, oset);
  }

  @Override
  public ASTupleFieldSeq visitTupleField(TupleFieldContext ctx) {
    final ASTermSeq termsSeq = (ASTermSeq) visit(ctx.term());
    if ( termsSeq.getTerms().size() != 1 ) {
      final String errorMsg =
        String.format(
            "Multiple terms defined in a single tuple field in %s at (%d:%d).",
            parsedFile.getName(),
            ctx.start.getLine(),
            ctx.start.getCharPositionInLine()
        );
      mainAppOutput.out(errorMsg);
      if (mainAppLogger.isLoggable(Level.SEVERE)) mainAppLogger.severe(errorMsg);
    }
    ASTupleFieldSeq res = new ASTupleFieldSeq();
    res.addField(
        Optional.ofNullable(ctx.id).map(Token::getText).orElse(null),
        termsSeq.getTerms().get(0)
    );
    return res;
  }

  /* VISITING BASIC TERM CONSTRUCTIONS */

  @Override
  public ASTermSeq visitClosingTerm_ST(final ClosingTerm_STContext ctx) {
    return (ASTermSeq) visit(ctx.term());
  }

  @Override
  public ASTermSeq visitSet_ST(final Set_STContext ctx) {
    setVisitChildrenInitialAggregator(new ASTermSeq());
    final ASTermSeq subterms = (ASTermSeq) visitChildren(ctx);
    finalizeVisitChildren();
    return subterms.toSet();
  }

  @Override
  public ASTermSeq visitTuple_ST(Tuple_STContext ctx) {
    setVisitChildrenInitialAggregator(new ASTupleFieldSeq());
    final ASTupleFieldSeq tupleFields = (ASTupleFieldSeq) visitChildren(ctx);
    finalizeVisitChildren();
    return tupleFields.toTuple();
  }

  /* VISITING FUNCTION ON SET */

  @Override
  public ASTermSeq visitUnaryOpOnSet_ST(final UnaryOpOnSet_STContext ctx) {
    final ASTermSeq termASE = (ASTermSeq) visit(ctx.term());

    final UnaryOperator<SetTerm> operator;
    if (ctx.op.complement_prefix_KW() != null) {
      operator = t -> new Complement(t, Option.<SetTerm>empty());
    } else if (ctx.op.powerset_prefix_KW() != null) {
      operator = t -> new Powerset(t);
    } else {
      final String errorMsg =
          String.format(
              ErrorMessages.get(
                  ErrorMessagesId.CASE_IMPLEMENTATION_MISSING_IN_S_OF_S_FOR_S_AT_P),
              "visitUnaryFunOnSet_ST", this.getClass(),
              parsedFile.getName(),
              ctx.start.getLine(),
              ctx.start.getCharPositionInLine()
          );
      assert false : errorMsg;
      if (mainAppLogger.isLoggable(Level.SEVERE)) mainAppLogger.severe(errorMsg);
      operator = UnaryOperator.identity(); // Trying to continue ...
    }

    final ASTermSeq res = new ASTermSeq();
    for (SetTerm t : termASE.getTerms()) { // NOPMD
      res.addTerm(operator.apply(t)); // NO PMD: I do create new Object
    }
    res.addReferences(termASE.getAliases(), termASE.getReferencedCategories());
    return res;
  }

  /**
   * Returns the correct binary function AST node constructor for the provided CST node.
   *
   * @param op the CST of the binary expression.
   * @return the binary function AST node.
   */
  private BinaryOperator<SetTerm> getConstructorFor(final  BinaryOpContext op) { // NOPMD
    final BinaryOperator<SetTerm> res;

    if (op.functionalBinOp() != null) {
      FunctionalBinOpContext fOp = op.functionalBinOp();
      if (fOp.partialFunction_KW() != null) {
        res = (lhs, rhs) -> new Relations(lhs, rhs, false, false, true, false);
      } else if (fOp.totalFunction_KW() != null) {
        res = (lhs, rhs) -> new Relations(lhs, rhs, true, false, true, false);
      } else if (fOp.partialInjection_KW() != null) {
        res = (lhs, rhs) -> new Relations(lhs, rhs, false, false, true, true);
      } else if (fOp.totalInjection_KW() != null) {
        res = (lhs, rhs) -> new Relations(lhs, rhs, true, false, true, true);
      } else if (fOp.partialSurjection_KW() != null) {
        res = (lhs, rhs) -> new Relations(lhs, rhs, false, true, true, false);
      } else if (fOp.totalSurjection_KW() != null) {
        res = (lhs, rhs) -> new Relations(lhs, rhs, true, true, true, false);
      } else if (fOp.partialBijection_KW() != null) {
        res = (lhs, rhs) -> new Relations(lhs, rhs, false, true, true, true);
      } else if (fOp.totalBijection_KW() != null) {
        res = (lhs, rhs) -> new Relations(lhs, rhs, true, true, true, true);
      } else {
        ErrorMessages.logImplementationError("Missing case for function operator");
        // Trying to continue ...
        res = (lhs, rhs) -> new SetCons(CollectionConverters.asScala(Set.of(lhs, rhs)).toSet());
      }
    } else if (op.restrictionBinOp() != null) {
      RestrictionBinOpContext rOp = op.restrictionBinOp();
      if (rOp.restrictionOnFirst_KW() != null) {
        res = (lhs, rhs) -> new Restriction<>(rhs, 1, lhs);
      } else if (rOp.restrictionOnLast_KW() != null) {
        res = (lhs, rhs) -> new Restriction<>(lhs, -1, rhs);
      } else {
        ErrorMessages.logImplementationError("Missing case for restriction operator");
        // Trying to continue ...
        res = (lhs, rhs) -> new SetCons(CollectionConverters.asScala(Set.of(lhs, rhs)).toSet());
      }
    } else if (op.joinOp() != null) {
      JoinOpContext jOp = op.joinOp();
      if (jOp.join_KW() != null) {
        res = (lhs, rhs) ->
            new Junction(lhs, rhs, OptionConverters.toScala(Optional.empty()), false);
      } else if (jOp.collapsingJoin_KW() != null) {
        res = (lhs, rhs) ->
            new Junction(lhs, rhs, OptionConverters.toScala(Optional.empty()), true);
      } else {
        ErrorMessages.logImplementationError("Missing case for join operator");
        // Trying to continue ...
        res = (lhs, rhs) -> new SetCons(CollectionConverters.asScala(Set.of(lhs, rhs)).toSet());
      }
    } else {
      ErrorMessages.logImplementationError("Missing case for binary operator");
      // Trying to continue ...
      res = (lhs, rhs) -> new SetCons(CollectionConverters.asScala(Set.of(lhs, rhs)).toSet());
    }
    return res;
  }

  @Override
  public ASElement visitBinaryOpOnSet_ST(final BinaryOpOnSet_STContext ctx) {
    final ASTermSeq lhsASTermSeq = (ASTermSeq) visit(ctx.lhs);
    final ASTermSeq rhsASTermSeq = (ASTermSeq) visit(ctx.rhs);

    final ASTermSeq res = new ASTermSeq();
    res.addReferences(lhsASTermSeq.getAliases(), lhsASTermSeq.getReferencedCategories());
    res.addReferences(rhsASTermSeq.getAliases(), rhsASTermSeq.getReferencedCategories());

    final BinaryOperator<SetTerm> constructor = getConstructorFor(ctx.op);
    for (SetTerm lhs : lhsASTermSeq.getTerms()) { // NOPMD
      for (SetTerm rhs : rhsASTermSeq.getTerms()) { // NOPMD
        res.addTerm(constructor.apply(lhs, rhs));
      }
    }
    return res;
  }

  /**
   * Returns the correct associative operation AST node constructor for the provided CST node.
   *
   * @param op the CST of the associative operation.
   * @return a constructor for the associative operation expression AST node.
   */
  private Function<Set<SetTerm>, SetTerm> getConstructorFor(final AssociativeOpContext op) { // NOPMD
    final Function<Set<SetTerm>, SetTerm> res;
    if ( op.union_KW() != null ) {
      res = ts -> new Union(CollectionConverters.asScala(ts).toSet());
    } else if ( op.intersection_KW() != null ) {
      res = ts -> new Intersection(CollectionConverters.asScala(ts).toSet());
    } else if ( op.unionProd_KW() != null ) {
      res = ts -> new UnionProduct(CollectionConverters.asScala(ts).toSet());
    } else {
      ErrorMessages.logImplementationError("unknown associative operation");
      // Trying to continue ...
      res = ts -> new SetCons(CollectionConverters.asScala(ts).toSet());
    }
    return res;
  }

  @Override
  public ASElement visitAssociativeOpOnSet_ST(final AssociativeOpOnSet_STContext ctx) {
    Set<Function<Set<SetTerm>,SetTerm>> astCons = ImmutableSet.copyOf(
        ctx.op.stream().map(this::getConstructorFor).collect(Collectors.toSet())
    );
    if ( astCons.size() == 0 ) {
      ErrorMessages.logImplementationError(
          "Associative operation expression without operator (this should be unparsable)"
      );
    }
    if ( astCons.size() > 1 ) {
      ErrorMessages.logSyntaxError(parsedFile.getName(), ctx,
          "Associative operation using different operators."
      );
    }

    final ASTermSeq res = new ASTermSeq();
    final ASTermSeq aggregate = new ASTermSeq();
    ctx.term().forEach( stCtx -> {
      final ASTermSeq subASTermSeq = (ASTermSeq) visit(stCtx);
      aggregate.aggregate(subASTermSeq);
      res.addReferences(subASTermSeq.getAliases(), subASTermSeq.getReferencedCategories());
    } );

    astCons.stream().findAny()
        .map(c -> c.apply(ImmutableSet.copyOf(aggregate.getTerms())))
        .map(res::addTerm);

    return res;
  }

  @Override
  public ASTermSeq visitRestriction_ST(Restriction_STContext ctx) {
    final ASTermSeq tupleSetASTermSeq = (ASTermSeq) visit(ctx.tupleSet);
    final String dimStr = ctx.dimension.getText();
    final ASTermSeq onASTermSeq = (ASTermSeq) visit(ctx.on);

    if (tupleSetASTermSeq.getTerms().size() != 1 || onASTermSeq.getTerms().size() != 1) {
      final String errorMsg =
          String.format(
              ErrorMessages.get(ErrorMessagesId.SYNTAX_ERROR_AT_L_S),
              parsedFile.getName(),
              ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
              "restriction statements does not apply to sequences of terms!"
          );
      assert false : errorMsg;
      ErrorMessages.signal(Level.SEVERE, true, errorMsg);
    }

    final ASTermSeq res = new ASTermSeq();
    res.addReferences(tupleSetASTermSeq.getAliases(), tupleSetASTermSeq.getReferencedCategories());
    res.addReferences(onASTermSeq.getAliases(), onASTermSeq.getReferencedCategories());

    final SetTerm tupleSet = tupleSetASTermSeq.getTerms().get(0);
    final SetTerm onSet = onASTermSeq.getTerms().get(0);
    try {
      final int dimInt = Integer.parseInt(dimStr);
      res.addTerm(new Restriction<>(tupleSet, dimInt, onSet));
    } catch (NumberFormatException e) {
      res.addTerm(new Restriction<>(tupleSet, dimStr, onSet));
    }

    return res;
  }

  @Override
  public ASTermSeq visitJoin_ST(Join_STContext ctx) {
    final ASTermSeq lhsASTermSeq = (ASTermSeq) visit(ctx.lhs);
    final ASTermSeq rhsASTermSeq = (ASTermSeq) visit(ctx.rhs);

    if (lhsASTermSeq.getTerms().size() != 1 || rhsASTermSeq.getTerms().size() != 1) {
      final String errorMsg =
          String.format(
              ErrorMessages.get(ErrorMessagesId.SYNTAX_ERROR_AT_L_S),
              parsedFile.getName(),
              ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
              "junction statements does not apply to sequences of terms!"
          );
      assert false : errorMsg;
      ErrorMessages.signal(Level.SEVERE, true, errorMsg);
    }
    final ASTermSeq res = new ASTermSeq();
    res.addReferences(lhsASTermSeq.getAliases(), rhsASTermSeq.getReferencedCategories());
    res.addReferences(lhsASTermSeq.getAliases(), rhsASTermSeq.getReferencedCategories());

    final SetTerm lhs = lhsASTermSeq.getTerms().get(0);
    final SetTerm rhs = rhsASTermSeq.getTerms().get(0);
    Optional<scala.collection.immutable.Set<scala.Tuple2<String,String>>> dimStr;
    if (ctx.dimensions.size() > 0) {
      ListIterator<Token> fstDimLI = ctx.dimensions.listIterator();
      ListIterator<Token> sndDimLI = ctx.dimensions.listIterator(1);
      Set<scala.Tuple2<String,String>> tmpDimStr = new HashSet<>();
      while (fstDimLI.hasNext() && sndDimLI.hasNext()) {
        tmpDimStr.add(new Tuple2<>(fstDimLI.next().getText(), sndDimLI.next().getText()));
        if (fstDimLI.hasNext()) fstDimLI.next();
        if (sndDimLI.hasNext()) sndDimLI.next();
      }
      dimStr = Optional.of(CollectionConverters.asScala(tmpDimStr).toSet());
    } else {
      dimStr = Optional.empty();
    }
    final boolean collapsing;
    if (ctx.op.join_KW() != null) {
      collapsing = false;
    } else {
      collapsing = true;
    }
    res.addTerm(new Junction(lhs, rhs, OptionConverters.toScala(dimStr), collapsing));

    return res;
  }

  /*
  @Override
  public ASElement visitProjectionOnFirst_ST(ProjectionOnFirst_STContext ctx) {
    final ASTermSeq tupleSetASTermSeq = (ASTermSeq) visit(ctx.tupleSet);
    final ASTermSeq onASTermSeq = (ASTermSeq) visit(ctx.on);
    return buildProjection(ctx, tupleSetASTermSeq, 1, onASTermSeq);
  }

  @Override
  public ASElement visitProjectionOnLast_ST(ProjectionOnLast_STContext ctx) {
    final ASTermSeq tupleSetASTermSeq = (ASTermSeq) visit(ctx.tupleSet);
    final ASTermSeq onASTermSeq = (ASTermSeq) visit(ctx.on);
    return buildProjection(ctx, tupleSetASTermSeq, -1, onASTermSeq);
  }
   */

  @Override
  public ASTermSeq visitCartesianProduct_ST(CartesianProduct_STContext ctx) {
    ASTupleFieldSeq aggregate = new ASTupleFieldSeq();
    ctx.tupleField().forEach(
        tfCtx -> aggregate.aggregate(visitTupleField(tfCtx))
    );
    return aggregate.toCartesianProduct();
  }

  @Override
  public ASTermSeq visitTermsSequence(TermsSequenceContext ctx) {
    setVisitChildrenInitialAggregator(new ASTermSeq());
    final ASTermSeq res = (ASTermSeq) visitChildren(ctx);
    finalizeVisitChildren();
    return res;
  }

  /*******************/
  /** INSTANCE BODY **/
  /*******************/

  @Override
  public ASOntology visitInstanceDefinition(final InstanceDefinitionContext ctx) {
    if ( ! allRequiredSyntacticElementsPresent(
        ctx, ctx.instanceId, ctx.ontologyId, ctx.body
    ) ) {
      return null;
    }
    // Retrieving parsed data
    final String seedLocalId = ctx.instanceId.getText();
    final String seedName =
        Optional.ofNullable(ctx.name)
            .map(Token::getText)
            .map(s -> s.substring(1, s.length() - 1))
            .orElse(seedLocalId);
    final OntologyId seedId =
        OntologyId.apply(
            parsedFileMarker, RootNamespace$.MODULE$.andThen(namespaceInfix),
            getCurrentNamespace(), seedLocalId);
    final OntologyId parentOntologyId =
        visitGlobalOntologyId(ctx.ontologyId).getOntologyId();
    // Resetting ontology-related global data structures
    ASOntology aso = new ASOntology(seedId, seedName, OntologyType.INSTANCE, this);
    ontologyVisitorHelper = aso.getVisitHelper();
    final ASImport initialImport =
        new ASImport(
            ImportType.LINK, true, parentOntologyId,
            definedOntologies.getOntologyProxy(parentOntologyId), seedId);
    initialImport.importAll(ontologyVisitorHelper, true, true);
    aso.aggregate(initialImport);
    // Visiting children
    setVisitChildrenInitialAggregator(aso);
    aso = (ASOntology) visitChildren(ctx.body);
    finalizeVisitChildren();
    // Finalizing ontology seed visit
    aso.finalizeCreation(definedOntologies);
    definedOntologies.put(seedId, aso);
    return aso;
  }
}
