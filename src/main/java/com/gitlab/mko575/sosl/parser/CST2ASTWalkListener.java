package com.gitlab.mko575.sosl.parser;

import java.util.HashSet;
import java.util.Set;

/**
 * Top level {@link SOSLListener} for SOSL files Concrete Syntax Trees (CST),
 * as parsed by {@link SOSLParser}, that only collects the set of user defined categories.
 */
public class CST2ASTWalkListener extends SOSLBaseListener {

  /** Stores set ids encountered in the SOS. */
  private final Set<String> sets; // NOPMD

  /**
   * Default constructor.
   */
  public CST2ASTWalkListener() {
    super();
    sets = new HashSet<>();
  }

  /**
   * Returns the set of categories/sets defined.
   * @return the set of categories/sets defined.
   */
  public Set<String> getClassMap() {
    return sets;
  }

  @Override
  public void enterUserDefinedId(final SOSLParser.UserDefinedIdContext ctx) {
    final String setId = ctx.id.getText();
    if (!sets.contains(setId)) {
      sets.add(setId);
    }
  }
}
