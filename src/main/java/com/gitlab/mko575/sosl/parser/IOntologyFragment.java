package com.gitlab.mko575.sosl.parser;

import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.SetId;
import java.util.Map;

public interface IOntologyFragment {

  /**
   * Returns the global identifier of the ontological set associated to the provided
   * handle in this {@link IOntologyFragment}.
   *
   * @param handle the handle to look for.
   * @return the associated global identifier, or {@code null} if there is none.
   */
  SetId getSIdForHandle(String handle);

  /**
   * Returns the mapping, at the end of this {@link IOntologyFragment},
   * from ontological set ({@link OSet}) handles ('userDefinedId' in SOSL's
   * grammar) to ontological set global identifiers (global ontology identifier
   * and ontology-local set identifier).
   * By default, handles are used as ontology-local set identifiers, unless there is some
   * handle reuse.
   *
   * @return the final mapping from handle to global identifier
   */
  Map<String, SetId> getAliases();

}
