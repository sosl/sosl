package com.gitlab.mko575.sosl.parser;

import com.gitlab.mko575.sosl.ids.SetId;
import com.gitlab.mko575.sosl.ids.sos.OntologySeed;
import com.gitlab.mko575.sosl.ids.OSet;

public interface IOntologyProxy extends IOntologyFragment {

  /**
   * Returns the name of the ontology proxying for.
   *
   * @return the name of the ontology.
   */
  String getName();

  /**
   * Returns the ontological set associated with the provided local identifier
   * in the proxied ontology.
   *
   * @param lid the local identifier to look for.
   * @return the associated ontological set, or {@code null} if there is none.
   */
  OSet getOSet4lId(Object lid);

  /**
   * Returns the {@link OntologySeed} for which this object is a proxy.
   * @return the ontology seed.
   */
  OntologySeed getSeed();

}
