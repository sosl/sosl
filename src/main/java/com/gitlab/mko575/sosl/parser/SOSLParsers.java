package com.gitlab.mko575.sosl.parser;

import static com.gitlab.mko575.sosl.Forestry.mainAppLogger;
import static com.gitlab.mko575.sosl.Forestry.mainAppOutput;

import com.gitlab.mko575.sosl.parser.ast.ASOntology;
import com.gitlab.mko575.sosl.ids.Namespace;
import com.gitlab.mko575.sosl.ids.RootNamespace$;
import com.gitlab.mko575.sosl.ids.OntologyId;
import com.gitlab.mko575.sosl.ids.sos.OntologySeed;
import com.gitlab.mko575.sosl.ErrorMessages;
import com.gitlab.mko575.sosl.ErrorMessages.ErrorMessagesId;
import com.gitlab.mko575.sosl.parser.SOSLParser.GlobalOntologyIdContext;
import com.gitlab.mko575.sosl.parser.SOSLParser.NamespaceContext;
import com.gitlab.mko575.sosl.parser.ast.ASOntologies;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import org.antlr.v4.runtime.ParserRuleContext;

public class SOSLParsers {

  /**
   * Private constructor throwing an exception to discourage instantiating or
   * inheriting from this class.
   *
   * @throws AssertionError always
   */
  private SOSLParsers() throws AssertionError {
    throw new AssertionError(
        "SOSLParsers is a utility class."
            + " No instance of it should ever be created!");
  }
  
  /**
   * Parses the string to retrieve a namespace.
   * @param str the string to parse.
   * @return the namespace corresponding to the string parsed.
   */
  public static Namespace parseNamespaceStringFromRoot(String str) {
    NamespaceContext nsCtx = ParsingUtils.getParserOfString(str).namespace();
    return (new CST2ASTVisitor()).visitNamespace(RootNamespace$.MODULE$, nsCtx).getNamespace();
  }

  /**
   * Parses the string to retrieve a global ontology identifier.
   * @param str the string to parse.
   * @return the OntologyId corresponding to the string parsed.
   */
  public static OntologyId parseGlobalOntologyIdStringFromRoot(String str) {
    GlobalOntologyIdContext goiCtx = ParsingUtils.getParserOfString(str).globalOntologyId();
    return (new CST2ASTVisitor()).visitGlobalOntologyId(goiCtx).getOntologyId();
  }

  /**
   * Returns the information parsed from the provided file (mainly a set of {@link ASOntology}).
   *
   * @param verbose increase verbosity if {@code true}.
   * @param fileNamespace the {@link Namespace} to start parsing the file in.
   * @param useNSInfix whether or not to use the NSInfix declarations in the SOSL file.
   * @param soslFile SOSL {@link File} containing SOS to retrieve.
   * @return the parsed information.
   */
  public static ASOntologies parseSOSLFile(
      final boolean verbose,
      final Namespace fileNamespace,
      final boolean useNSInfix,
      final File soslFile
  ) {
    if (!soslFile.canRead()) {
      final String errorMsg =
          String.format(
            ErrorMessages.get(ErrorMessagesId.FILE_S_CAN_NOT_BE_FOUND_OR_READ),
            soslFile.getPath()
          );
      mainAppOutput.out(errorMsg);
      if (mainAppLogger.isLoggable(Level.SEVERE)) {
        mainAppLogger.severe(errorMsg);
      }
    }

    ASOntologies assos = null;
    try {
      final SOSLParser parser = ParsingUtils.getParserOfFile(soslFile.toPath());
      final ParserRuleContext cst = parser.sosFile();
      if (cst != null) {
        assos = (ASOntologies) new CST2ASTVisitor(fileNamespace, soslFile, useNSInfix).visit(cst);
        Map<OntologyId, OntologySeed> ontologySeeds = assos.getOntologySeeds();
        if (verbose) { // NOPMD
          if (ontologySeeds.isEmpty()) {
            mainAppOutput.out(String.format("Found no ontology in '%s':", soslFile.getPath()));
          } else {
            mainAppOutput.out("Found the following ontologies!");
            ontologySeeds.forEach(
                (id, sos) -> {
                  mainAppOutput.out();
                  mainAppOutput.out(sos.toMultilineVerboseString());
                });
          }
        }
      }
    } catch (IOException e) {
      mainAppLogger.log(
          Level.SEVERE,
          "Error while opening ''{0}'': {1}",
          new String[]{soslFile.getPath(), e.getMessage()}
      );
    }
    return assos;
  }

}
