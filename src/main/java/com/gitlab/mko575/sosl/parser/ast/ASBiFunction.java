package com.gitlab.mko575.sosl.parser.ast;

import java.util.function.BiFunction;

public class ASBiFunction<T extends BiFunction> implements ASElement {

  /**
   * The operator represented by this {@link ASElement}.
   */
  private T function;

  /**
   * Constructor of an {@link ASElement} representing the provided function.
   *
   * @param function the operator represented by this {@link ASBiFunction}.
   */
  public ASBiFunction(T function) {
    this.function = function;
  }

}

