package com.gitlab.mko575.sosl.parser.ast;

import static com.gitlab.mko575.sosl.Forestry.mainAppLogger;
import static com.gitlab.mko575.sosl.Forestry.mainAppOutput;

import com.gitlab.mko575.sosl.ErrorMessages;
import com.gitlab.mko575.sosl.ErrorMessages.ErrorMessagesId;
import java.util.logging.Level;

public interface ASElement {

  /**
   * Check if the provided {@link ASElement} can be aggregated into this {@link ASElement}.
   *
   * @param e the {@link ASElement} to aggregate into this {@link ASElement}.
   * @return true iff the provided {@link ASElement} can be aggregated into this {@link ASElement}.
   */
  default boolean aggregatable(ASElement e) {
    return false;
  }

  /**
   * Aggregates the provided {@link ASElement} into this {@link ASElement}.
   *
   * @param e the {@link ASElement} to aggregate into this {@link ASElement}.
   * @return the {@link ASElement} resulting from aggregating the provided
   * {@link ASElement} into this {@link ASElement}.
   */
  default ASElement aggregate(ASElement e) {
    final String errorMsg =
        String.format(
            ErrorMessages.get(ErrorMessagesId.TRYING_TO_AGGREGATE_INCOMPATIBLE_DATA_S_AND_S),
            "aggregate in "+ASElement.class, this.getClass().getCanonicalName(),
            this.toString(), e.toString()
        );
    mainAppOutput.out(errorMsg);
    if (mainAppLogger.isLoggable(Level.SEVERE)) {
      mainAppLogger.severe(errorMsg);
    }
    return this;
  }

}
