package com.gitlab.mko575.sosl.parser.ast;

import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.Property;
import com.gitlab.mko575.sosl.ids.SetId;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

public abstract class ASElementContainingProperties extends ASElementReferencingCategories {

  /**
   * The set of {@link Property}s represented by this {@link ASElementContainingProperties}.
   */
  protected Collection<Property> properties = new HashSet<>();

  /**
   * Default constructor.
   * Initializes its private parameters with default values and empty containers.
   */
  protected ASElementContainingProperties() {
    super();
  }

  /**
   * Constructor for {@link ASElementContainingProperties} initialized with the
   * provided parameters.
   *
   * @param hdl2sid a mapping from handle to global identifiers.
   * @param sid2cats a mappings from global identifiers to ontological sets.
   */
  protected ASElementContainingProperties(
      final Map<String, SetId> hdl2sid,
      final Map<SetId, OSet> sid2cats
  ) {
    this();
    addReferences(hdl2sid, sid2cats);
  }

  /**
   * Constructor for {@link ASElementContainingProperties} initialized with the
   * provided parameters.
   *
   * @param hdl2sid a mapping from handle to global identifiers.
   * @param sid2cats a mappings from global identifiers to ontological sets.
   * @param properties the set of {@link Property} represented by the constructed
   *                   {@link ASElementContainingProperties}.
   */
  protected ASElementContainingProperties(
      final Map<String, SetId> hdl2sid,
      final Map<SetId, OSet> sid2cats,
      final Collection<Property> properties
  ) {
    this(hdl2sid, sid2cats);
    addProperties(properties);
  }

  /**
   * Returns the set of {@link Property}s represented by this
   * {@link ASElementContainingProperties}.
   * @return the set of {@link Property}s represented by this
   *         {@link ASElementContainingProperties}.
   */
  public Collection<Property> getProperties() {
    return properties;
  }

  /**
   * Add the provided {@link Property} to the set of {@link Property}s represented
   * by this {@link ASElement}. This method does not update the set of
   * Set/Categories identifiers referenced in this {@link ASElementContainingProperties}.
   * It has to be done manually using
   * {@link ASElementReferencingCategories#addReferencedCategory(String, SetId, OSet)}.
   *
   * @param p the {@link Property} to be added to the set of {@link Property}s
   *          represented by this {@link ASElement}.
   * @return this {@link ASElementContainingProperties} (after addition).
   */
  public ASElementContainingProperties addProperty(Property p) {
    properties.add(p);
    return this;
  }

  /**
   * Add the provided {@link Property}s to the set of {@link Property}s represented
   * by this {@link ASElement}. This method does not update the set of
   * Set/Categories identifiers referenced in this {@link ASElementContainingProperties}.
   * It has to be done manually using
   * {@link ASElementReferencingCategories#addReferencedCategory(String, SetId, OSet)}.
   *
   * @param p the {@link Property}s to be added to the set of {@link Property}s
   *          represented by this {@link ASElement}.
   * @return this {@link ASElementContainingProperties} (after addition).
   */
  public ASElementContainingProperties addProperties(Collection<Property> p) {
    p.forEach(this::addProperty);
    return this;
  }

  @Override
  public boolean aggregatable(ASElement e) {
    return e instanceof ASElementContainingProperties;
  }

  @Override
  public ASElement aggregate(ASElement e) {
    if (e instanceof ASElementContainingProperties) {
      addProperties(((ASElementContainingProperties) e).getProperties());
    }
    return super.aggregate(e);
  }
}
