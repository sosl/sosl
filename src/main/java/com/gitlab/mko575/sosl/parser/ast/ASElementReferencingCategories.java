package com.gitlab.mko575.sosl.parser.ast;

import com.gitlab.mko575.sosl.parser.IOntologyFragment;
import com.gitlab.mko575.sosl.parser.SOSLParser.OntologyDefinitionContext;
import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.SetId;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

public abstract class ASElementReferencingCategories implements IOntologyFragment, ASElement {

  /**
   * Mapping, at the end of this {@link ASElementReferencingCategories}, from ontological
   * set ({@link OSet}) handles ('userDefinedId' in SOSL's grammar) to ontological set
   * global identifiers (global ontology identifier and ontology-local set identifier).
   * By default, handles are used as ontology-local set identifiers, unless there is some
   * handle reuse.<br/>
   * <br/>
   * This data structure is:
   *  - updated while visiting (down) the concrete syntax ({@link OntologyDefinitionContext})
   *      of the associated ontology (mainly imports and occurrence of new categories);
   *  - used whenever an ontological set is "referenced" by an handler;
   *  - unmodified and unused while creating (up) the abstract syntax ({@link ASOntology})
   *      corresponding to associated ontology;
   *  - copied into the associated abstract syntax ({@link ASOntology}) when finalizing it.
   */
  private final Map<String, SetId> aliases;

  /**
   * The mapping of global identifiers to ontological sets ({@link OSet} referenced
   * in this {@link ASElementReferencingCategories}.
   */
  private final Map<SetId, OSet> referencedCategories;

  /**
   * Default constructor.
   * Initializes its private fields with default values and empty containers.
   */
  protected ASElementReferencingCategories() {
    aliases = new HashMap<>();
    referencedCategories = new HashMap<>();
  }

  /**
   * Constructor for {@link ASElementReferencingCategories} initialized with the
   * provided parameters identifying the ontological sets referenced in this
   * {@link ASElementReferencingCategories} as well as the handles used to access
   * some of them.
   *
   * @param hdl2sid a mapping from handle to global identifiers.
   * @param sid2cats a mappings from global identifiers to ontological sets.
   */
  protected ASElementReferencingCategories(
      final Map<String, SetId> hdl2sid,
      final Map<SetId, OSet> sid2cats
  ) {
    this();
    assert sid2cats.keySet().containsAll(hdl2sid.values());
    this.addReferences(hdl2sid, sid2cats);
  }

  @Override
  public Map<String, SetId> getAliases() {
    return aliases;
  }

  @Override
  public SetId getSIdForHandle(String handle) {
    return aliases.get(handle);
  }

  /**
   * Returns the ontological set associated with the provided global identifier.
   *
   * @param sid the local identifier.
   * @return the ontological set associated with the provided global identifier.
   */
  public OSet getOSet4sId(SetId sid) {
    return referencedCategories.get(sid);
  }

  /**
   * Returns the ontological set currently associated with the provided handle.
   *
   * @param hdl the handle to look for.
   * @return the ontological set currently associated with the provided handle.
   */
  public OSet getOSet4handle(String hdl) {
    return
        Optional.ofNullable(getSIdForHandle(hdl))
            .map(this::getOSet4sId).orElse(null);
  }

  /**
   * Updates the mapping from handle to associated ontological set ({@link OSet})
   * global identifier.
   *
   * @param hdl the handle whose association to modify.
   * @param sId the global identifier to associate to the provided handle.
   * @return the previous identifier associated to the handle, or {@code null}
   * if there was none.
   */
  public SetId updateAlias(String hdl, SetId sId) {
    return aliases.put(hdl, sId);
  }

  /**
   * Frees the provided handle, i.e. remove any association from this handle to
   * an ontological set global identifier.
   *
   * @param hdl the handle to free.
   * @return the identifier previously associated to the handle, or {@code null}
   * if there was none.
   */
  public SetId freeHandle(String hdl) {
    return aliases.remove(hdl);
  }

  /**
   * Returns the mapping of global identifiers to ontological sets ({@link OSet}
   * referenced in this {@link ASElementReferencingCategories}.
   *
   * @return the mapping of global identifiers to sets referenced.
   */
  public Map<SetId, OSet> getReferencedCategories() {
    return referencedCategories;
  }

  /**
   * Update the mapping of handles to global identifiers and global identifiers
   * to ontological sets ({@link OSet} referenced in this
   * {@link ASElementReferencingCategories}.
   *
   * @param handle the handle used to reference the ontological set.
   * @param sid the global identifier used to reference the ontological set.
   * @param cat the ontological set to be added.
   * @return this {@link ASElementReferencingCategories} (after addition).
   */
  public ASElementReferencingCategories addReferencedCategory(
      final String handle, final SetId sid, final OSet cat
  ) {
    if ( handle != null ) {
      aliases.put(handle, sid);
    }
    if ( referencedCategories.containsKey(sid) ) {
      referencedCategories.put(sid, referencedCategories.get(sid).updateWith(cat));
    } else {
      referencedCategories.put(sid, cat);
    }
    return this;
  }

  /**
   * Add the provided mappings of global identifiers to ontological sets ({@link OSet}
   * to the mapping in this {@link ASElementReferencingCategories}.
   *
   * @param hdl2sid the mapping from handle to global identifiers to be added.
   * @param sid2cats the mappings from global identifiers to ontological sets to be added.
   * @return this {@link ASElementReferencingCategories} (after addition).
   */
  public ASElementReferencingCategories addReferences(
      final Map<String, SetId> hdl2sid,
      final Map<SetId, OSet> sid2cats
  ) {
    aliases.putAll(hdl2sid);
    referencedCategories.putAll(sid2cats);
    return this;
  }

  @Override
  public boolean aggregatable(ASElement e) {
    return (e instanceof ASElementReferencingCategories);
  }

  @Override
  public ASElement aggregate(ASElement e) {
    if (e instanceof ASElementReferencingCategories) {
      assert
          getReferencedCategories().keySet().stream().allMatch(
              Predicate.not(
                  ((ASElementReferencingCategories) e).getReferencedCategories()::containsKey
              )
          ) :
          String.format(
              "Aggregating elements sharing handles:\n - %s;\n - %s",
              getReferencedCategories().keySet(),
              ((ASElementReferencingCategories) e).getReferencedCategories().keySet()
          )
      ;
      this.addReferences(
          ((ASElementReferencingCategories) e).getAliases(),
          ((ASElementReferencingCategories) e).getReferencedCategories()
      );
      return this;
    }
    return ((ASElement) this).aggregate(e);
  }
}
