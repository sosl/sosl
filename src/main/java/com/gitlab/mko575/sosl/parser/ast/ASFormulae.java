package com.gitlab.mko575.sosl.parser.ast;

import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.Property;
import com.gitlab.mko575.sosl.ids.SetId;
import java.util.Collection;
import java.util.Map;

public class ASFormulae extends ASElementContainingProperties {

  /**
   * Default constructor.
   * Initializes its private parameters with default values and empty containers.
   */
  public ASFormulae() {
    super();
  }

  /**
   * Constructor for {@link ASFormulae} initialized with the provided parameters.
   *
   * @param hdl2sid the mapping from handle to global identifiers referenced in the
   *            constructed {@link ASFormulae}.
   * @param sid2cats the mappings from global identifiers to ontological sets referenced
   *                in the constructed {@link ASFormulae}.
   */
  public ASFormulae(
      final Map<String, SetId> hdl2sid,
      final Map<SetId, OSet> sid2cats
  ) {
    super(hdl2sid, sid2cats);
  }

  /**
   * Constructor for {@link ASFormulae} initialized with the provided parameters.
   *
   * @param hdl2sid the mapping from handle to global identifiers referenced in the
   *            constructed {@link ASFormulae}.
   * @param sid2cats the mappings from global identifiers to ontological sets referenced
   *                in the constructed {@link ASFormulae}.
   * @param properties the set of {@link Property} represented by the constructed
   *                   {@link ASFormulae}.
   */
  public ASFormulae(
      final Map<String, SetId> hdl2sid,
      final Map<SetId, OSet> sid2cats,
      final Collection<Property> properties
  ) {
    super(hdl2sid, sid2cats, properties);
  }

}
