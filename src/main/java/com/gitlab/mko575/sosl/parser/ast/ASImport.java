package com.gitlab.mko575.sosl.parser.ast;

import com.gitlab.mko575.sosl.ErrorMessages;
import com.gitlab.mko575.sosl.ErrorMessages.ErrorMessagesId;
import com.gitlab.mko575.sosl.ids.GlobalSetId;
import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.OntologyId;
import com.gitlab.mko575.sosl.ids.sos.OntologySeed;
import com.gitlab.mko575.sosl.ids.SetId;
import com.gitlab.mko575.sosl.parser.CSOntologyVisitorHelper;
import com.gitlab.mko575.sosl.parser.IOntologyProxy;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.antlr.v4.runtime.misc.Pair;
import scala.jdk.javaapi.CollectionConverters;
import scala.jdk.javaapi.OptionConverters;

public class ASImport extends ASElementReferencingCategories {

  /**
   * Different types of import:
   *  - COPY: creates a new copy of the element imported in the importing ontology;
   *  - LINK: does not create a new element, it only allows referencing it.
   */
  public enum ImportType { COPY, LINK }

  /**
   * The type of import (see {@link ImportType}).
   */
  private final ImportType type;

  /**
   * The type of import (see {@link ImportType}).
   */
  private final boolean isImplicitGlobally;

  /**
   * The global identifier of the ontology importing FROM.
   */
  private final OntologyId fromOId;

  /**
   * The ontology seed importing from.
   */
  private final IOntologyProxy ontologyImportingFrom;

  /**
   * The global identifier of the ontology importing TO.
   */
  private final OntologyId toOId;

  /**
   * Global identifiers of the ontological sets effectively imported. Those correspond
   * to the explicit imports.
   * Mapping: from (new) global identifier in the ontology importing to; to the
   * global identifier of the ontological set to import (the one in the ontology
   * importing from)<br/>
   *  new global id -> global id of the OSet to import
   */
  private final Map<SetId, SetId> realizedImports = new HashMap<>();

  /**
   * Global identifiers of the ontological sets whose import is delayed to their
   * first use. Those correspond to the implicit imports. If one of those ontological
   * sets is never used, then it is not imported in the ontology.<br/>
   * Mapping:<br/>
   *  - from (new) handle in the ontology importing to;<br/>
   *  - to a pair composed of:<br/>
   *     - the global identifier of the ontological set to import (the one in the ontology
   *        importing from)<br/>
   *     - the updated ontological set to import.<br/>
   *  handle -> (global id of the OSet to import, new imported OSet)
   */
  private final Map<String, Pair<SetId, OSet>> delayedImports = new HashMap<>();

  /**
   * If not {@link Optional#empty()}, then all ontological sets ({@link OSet}s) of the imported
   * ontology are implicitly imported, except those whose (imported) global ids
   * are contained in the optional set.
   */
  private Optional<Set<SetId>> notImplicitlyImported = Optional.empty();

  /**
   * Default constructor.
   *
   * @param type type of import: linking or copying.
   * @param isImplicit whether all imports are implicit.
   * @param fromOId the global identifier of the ontology importing from.
   * @param fromOntology the ontology importing from.
   * @param toOId the global identifier of the ontology importing to.
   */
  public ASImport(ImportType type, boolean isImplicit, OntologyId fromOId,
      IOntologyProxy fromOntology, OntologyId toOId) {
    this.type = type;
    this.isImplicitGlobally = isImplicit;
    this.fromOId = fromOId;
    this.ontologyImportingFrom = fromOntology;
    this.toOId = toOId;
  }

  /**
   * Returns the type of import (see {@link ImportType}).
   *
   * @return the type of import.
   */
  public ImportType getType() {
    return type;
  }

  /**
   * Queries if the import statement is globally implicit (i.e. all imports
   * defined in this statement are implicit).
   *
   * @return {@code true} if this import statement is globally implicit.
   */
  public boolean isGloballyImplicit() {
    return isImplicitGlobally;
  }

  /**
   * Returns the global identifier of the ontology importing from.
   *
   * @return the global identifier.
   */
  public OntologyId getFromOId() {
    return fromOId;
  }

  /**
   * Returns the ontology importing from.
   *
   * @return the ontology importing from.
   */
  public IOntologyProxy getOntologyProxyImportingFrom() {
    return ontologyImportingFrom;
  }

  /**
   * Returns the ontology importing from.
   *
   * @return the ontology importing from.
   */
  public OntologySeed getOntologySeedImportingFrom() {
    return ontologyImportingFrom.getSeed();
  }

  /**
   * Returns the global identifier of the ontology importing to.
   *
   * @return the global identifier.
   */
  public OntologyId getToOId() {
    return toOId;
  }

  /**
   * Import, either explicitly or implicitly, all ontological sets ({@link OSet}s)
   * of the imported ontology, including or excluding those already imported.
   *
   * @param helper the helper taking care of current use of (ontological set)
   *               local identifiers and their current mapping from handlers.
   * @param implicitly if {@code true} then import implicitly, otherwise explicitly.
   * @param includingAlreadyImportedOnes if {@code true} then includes already imported
   *          ontological sets ({@link OSet}s); otherwise exclude them.
   */
  public void importAll(
      final CSOntologyVisitorHelper helper,
      boolean implicitly, boolean includingAlreadyImportedOnes
  ) {
    if ( implicitly ) {
      if ( includingAlreadyImportedOnes ) {
        notImplicitlyImported = Optional.of(Collections.emptySet());
      } else {
        Collection<SetId> exclusions = realizedImports.values();
        notImplicitlyImported =
            notImplicitlyImported.map(
                s -> s.stream().filter(exclusions::contains)
                    .collect(Collectors.toSet())
            ).or(() -> Optional.of(Set.copyOf(exclusions)));
      }
    } else {
      Set<SetId> importedLIds =
          CollectionConverters.asJava(
              getOntologySeedImportingFrom().getEnumerableOSets()
          ).keySet().stream().map(
              lid -> new GlobalSetId(getFromOId(),lid)
          ).collect(Collectors.toSet());
      if ( ! includingAlreadyImportedOnes ) {
        importedLIds.removeAll(realizedImports.values());
      }
      Map<SetId, Set<String>> sid2hdls =
          getOntologyProxyImportingFrom().getAliases().entrySet().stream().collect(
              Collectors.groupingBy(
                  Map.Entry::getValue,
                  Collectors.mapping(Map.Entry::getKey, Collectors.toSet())
              )
          );
      importedLIds.forEach(
          impLId -> {
            SetId newSId =
                importOne(
                    helper, new GlobalSetId(getFromOId(),impLId),
                    null, null,
                    false, includingAlreadyImportedOnes
                );
            if (newSId != null)
              sid2hdls.getOrDefault(newSId, Collections.emptySet()).forEach(
                  hdl -> updateAlias(hdl,newSId)
              );
          }
      );
    }
  }

  /**
   * Adds import specification.
   *
   * @param helper the helper taking care of current use of (ontological set)
   *               local identifiers and their current mapping from handlers.
   * @param importedSId global identifier of the imported ontological set ({@link OSet})
   *                    in the {@link OntologySeed} importing from.
   * @param newHandle (new) handle under which the imported ontological set ({@link OSet})
   *                  will be accessible.
   * @param newName name to be used in the {@link OntologySeed} importing to.
   * @param implicit {@code true} iff the import is implicit.
   * @param redundant if {@code false} and imported ontological set already imported
   *                  then ignore.
   * @return the global identifier of the imported ontological set ({@link OSet})
   * in the importing ontology. {@code null} if the import has not been explicitly
   * realized (due to an implicit import or errors).
   */
  public SetId importOne(
      final CSOntologyVisitorHelper helper,
      final SetId importedSId,
      String newHandle, String newName,
      final boolean implicit, final boolean redundant
  ) {
    if (importedSId == null) {
      ErrorMessages.logImplementationError(
          "Imported ontological set must be specified"
      );
      return null;
    }
    if (
        ! OptionConverters.toJava(importedSId.getOId())
            .map(oid -> oid.equals(getFromOId()))
            .orElse(true)
    ) {
      ErrorMessages.logImplementationError(
          String.format(
              "Importing from the wrong ontology (importing %s from %s)",
              importedSId, getFromOId()
          )
      );
      return null;
    }
    if ( getOntologySeedImportingFrom().defines(importedSId.lid()) ) {
      if (redundant || ! realizedImports.containsValue(importedSId)) {
        // Retrieving the OSet to import
        OSet importedOSet =
            getOntologySeedImportingFrom().get(importedSId.lid()).getOrElse(null);
        if (importedOSet == null) {
          ErrorMessages.logImplementationError(
                "Calling 'get' after guarding with 'defines' should not return None."
          );
          return null;
        }
        // Updating the imported OSet
        if (newName != null)
          importedOSet = importedOSet.updateName(newName);
        // Imported the OSet
        if (implicit) {
          if (newHandle != null) {
            // If no new handle provided then no implicit import done
            // (even if we have an imported handle).
            freeHandle(newHandle); // TODO: Hold on till the imported is realized?
            delayedImports.put(newHandle, new Pair<>(importedSId, importedOSet));
          }
          return null;
        } else {
          final SetId newSId;
          switch(type) {
            case COPY:
              final Object newLId = helper.closestFreshLId(newHandle);
              helper.registerNewLId(newLId);
              // newSId = new LocalSetId(newLId);
              newSId = new GlobalSetId(getToOId(), newLId);
              break;
            case LINK:
              newSId = importedSId;
              break;
            default:
              ErrorMessages.logImplementationError("Missing case");
              newSId = null;
          }
          if (newHandle != null)
            delayedImports.remove(newHandle);
          addReferencedCategory(newHandle, newSId, importedOSet);
          // TODO: check validity of following if statement
          if (newSId != null) {
            if (newHandle != null) helper.registerNewHandleAssociation(newHandle, newSId);
            if (importedOSet != null) helper.registerOSet(newSId, importedOSet);
          }
          realizedImports.put(newSId, importedSId);
          return newSId;
        }
      } else {
        return null;
      }
    } else {
      ErrorMessages.signal(
          Level.WARNING, true,
          ErrorMessagesId.ONTOLOGY_S_DOES_NOT_DEFINE_S,
          fromOId, importedSId.lid()
      );
      return null;
    }
  }

  /**
   * Retrieves the ontological sets ({@link OSet}) effectively imported. Every
   * (new) global identifier of the imported {@link OSet}s is associated to a pair
   * composed of: the global identifier of the ontological set to import (the one
   * in the ontology importing from), and the updated ontological set to import.
   *
   * @return the ontological sets ({@link OSet}) explicitly imported.
   */
  public Map<SetId, Pair<SetId, OSet>> getRealizedImports() {
    return getReferencedCategories().entrySet().stream().collect(Collectors.toMap(
        ((Function<Entry<SetId, OSet>,SetId>) Entry::getKey),
        e -> new Pair<SetId, OSet>(realizedImports.get(e.getKey()),e.getValue())
    ));
    // return getFilteredImports(Predicate.not(implicitlyImported::contains));
  }

  /**
   * Retrieves the global identifiers of the ontological sets imported (the identifier
   * in the ontology importing from).
   *
   * @return a mapping from (new) global identifiers in the importing ontology
   * to the (original) global identifier in the ontology imported from.
   */
  public Map<SetId, SetId> getSetIdsOfRealizedImports() {
    return realizedImports;
  }

  /**
   * Retrieves the mapping from the global identifier of imported ontological sets
   * to the global identifiers under which they are imported.
   *
   * @return the mapping from imported identifier to importing identifier.
   */
  public Map<SetId, Set<SetId>> getRealizedImportsFromSource() {
    HashMap<SetId, Set<SetId>> res = new HashMap<>();
    realizedImports.forEach(
        (newSid, oldSid) ->
            res.merge(
                oldSid, Set.of(newSid),
                (v1,v2) -> Stream.concat(v1.stream(),v2.stream()).collect(Collectors.toSet())
            )
    );
    return res;
  }

  /**
   * Retrieves the ontological sets ({@link OSet}) whose imports are delayed
   * to their first use. Every handle to be used is associated to a pair composed
   * of: the global identifier of the ontological set to import (the one in the
   * ontology importing from), and the updated ontological set to import.
   *
   * @return the ontological sets ({@link OSet}) whose imports are delayed.
   */
  public Map<String, Pair<SetId, OSet>> getDelayedImports() {
    return delayedImports;
  }

  /**
   * Retrieves the global identifiers of the ontological sets whose imports are
   * delayed to their first use.
   *
   * @return the (original) global identifiers of the ontological sets whose imports are delayed.
   */
  public Map<String, SetId> getSetIdsOfDelayedImports() {
    return delayedImports.entrySet().stream().collect(Collectors.toMap(
        Entry::getKey,
        e -> e.getValue().a
    ));
  }

  /**
   * Retrieves the set of global identifiers corresponding to ontological sets
   * that must not be implicitly imported. Their import can however be delayed.
   *
   * @return the set of identifiers not to import implicitly.
   */
  public Optional<Set<SetId>> getNotImplicitlyImported() {
    return notImplicitlyImported;
  }

  /**
   * Returns a detailed description of this object.
   *
   * @return a detailed description.
   */
  public String toDetailedString() {
    return String.format(
        "I%smport as %s from %s into %s using %s\n - realized: %s\n - delayed: %s\n - not implicitly imported: %s",
        isImplicitGlobally?"mplicit ":"",
        type, fromOId, toOId, ontologyImportingFrom,
        realizedImports, delayedImports, notImplicitlyImported
    );
  }
}
