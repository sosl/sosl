package com.gitlab.mko575.sosl.parser.ast;

import com.gitlab.mko575.sosl.ids.Namespace;

public class ASNamespace implements ASElement {

  private final Namespace ns;

  /**
   * Default constructor.
   *
   * @param ns the namespace defined by this {@link ASElement}.
   */
  public ASNamespace(Namespace ns) {
    this.ns = ns;
  }

  /**
   * Returns the {@link Namespace} defined by this {@link ASElement}.
   *
   * @return the namespace.
   */
  public Namespace getNamespace() {
    return ns;
  }

  @Override
  public boolean aggregatable(ASElement e) {
    return e instanceof ASNamespace;
  }

  @Override
  public ASElement aggregate(ASElement e) {
    if (e instanceof ASNamespace) {
      Namespace thisNS = getNamespace();
      Namespace nextResultNS = ((ASNamespace) e).getNamespace();
      return new ASNamespace(thisNS.andThen(nextResultNS));
    }
    return ASElement.super.aggregate(e);
  }

}
