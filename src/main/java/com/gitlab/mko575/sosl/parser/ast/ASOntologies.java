package com.gitlab.mko575.sosl.parser.ast;

import com.gitlab.mko575.sosl.ErrorMessages;
import com.gitlab.mko575.sosl.ids.GlobalSetId;
import com.gitlab.mko575.sosl.ids.SetId;
import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.OntologyId;
import com.gitlab.mko575.sosl.ids.sos.OntologySeed;
import com.gitlab.mko575.sosl.parser.IOntologyProxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class ASOntologies implements ASElement {

  /**
   * The set of ontologies ({@link ASOntology}) defined by this {@link ASElement}.
   */
  private final Map<OntologyId, IOntologyProxy> ontologies;

  /**
   * Default constructor.
   * Initializes {@link ASOntologies#ontologies} with an empty map.
   */
  public ASOntologies() {
    ontologies = new HashMap<>();
  }

  /**
   * Construct an {@link ASOntologies} containing a single ontology.
   * @param oid the global identifier of the ontology.
   * @param ontology the single ontology contained in this {@link ASOntologies}.
   */
  public ASOntologies(OntologyId oid, IOntologyProxy ontology) {
    this();
    put(oid, ontology);
  }

  /**
   * Construct an {@link ASOntologies} initialized with a set of ontologies.
   * @param initialOntologies the ontologies contained in the created {@link ASOntologies}.
   */
  public ASOntologies(Map<OntologyId,IOntologyProxy> initialOntologies) {
    this();
    putAll(initialOntologies);
  }

  /**
   * Returns the ontology ({@link ASOntology}) corresponding to the provided global
   * identifier.
   *
   * @param oid the global identifier of the ontology to return.
   * @return the designated ontology, or null if not present.
   */
  public IOntologyProxy getOntologyProxy(OntologyId oid) {
    return ontologies.get(oid);
  }

  /**
   * Returns the ontology ({@link OntologySeed}) corresponding to the provided global
   * identifier.
   *
   * @param oid the global identifier of the ontology to return.
   * @return the designated ontology, or null if not present.
   */
  public OntologySeed getOntologySeed(OntologyId oid) {
    return Optional.ofNullable(ontologies.get(oid))
        .map(IOntologyProxy::getSeed).orElse(null);
  }

  /**
   * Returns the set of ontologies ({@link ASOntology}) defined by this {@link ASElement}
   * (see {@link ASOntologies#ontologies}).
   *
   * @return the set of ontologies defined by this {@link ASElement}.
   */
  public Map<OntologyId, IOntologyProxy> getOntologyProxies() {
    return ontologies;
  }

  /**
   * Returns the set of ontologies ({@link OntologySeed}) defined by this {@link ASElement}
   * (see {@link ASOntologies#ontologies}).
   *
   * @return the set of ontologies defined by this {@link ASElement}.
   */
  public Map<OntologyId, OntologySeed> getOntologySeeds() {
    ErrorMessages.debugMessage(
        String.format(
            "Calling getOntologySeeds() on: %s",
            ontologies.keySet().toString()
        )
    );
    return ontologies.entrySet().stream().collect(Collectors.toMap(
        Entry::getKey,
        ((Function<Entry<OntologyId,IOntologyProxy>,IOntologyProxy>) Entry::getValue)
            .andThen(IOntologyProxy::getSeed)
    ));
  }

  /**
   * Retrieves the ontological set associated to the provided global identifier,
   * it it exists.
   *
   * @param sid the global identifier to look for.
   * @return the ontological set if ti exists, {@code null} otherwise.
   */
  public OSet getOSet(SetId sid) {
    if (sid == null)
      return null;
    else {
      return
          Optional.ofNullable(ontologies.get(sid.getOId().getOrElse(() -> null)))
              .map(o -> o.getOSet4lId(sid.lid()))
              .orElse(null);
    }
  }

  /**
   * Add an ontology ({@link ASOntology}) to the set of ontologies defined by this
   * {@link ASElement} (see {@link ASOntologies#ontologies}).
   * It has the same effect as {@link Map#put(Object, Object)} on
   * {@link ASOntologies#ontologies}.
   *
   * @param oid the global identifier of the ontology, used as key in he map.
   * @param ontology the ontology to add.
   * @return the previous value associated to the identifier if it exists, otherwise
   * {@code null}.
   */
  public IOntologyProxy put(OntologyId oid, IOntologyProxy ontology) {
    return ontologies.put(oid, ontology);
  }

  /**
   * Add ontologies ({@link ASOntology}) to the set of ontologies defined by this
   * {@link ASElement} (see {@link ASOntologies#ontologies}).
   * It has the same effect as {@link Map#putAll(Map)} on {@link ASOntologies#ontologies}.
   *
   * @param m the set of ontologies to be added.
   */
  public void putAll(Map<OntologyId, IOntologyProxy> m) {
    ontologies.putAll(m);
  }

  @Override
  public boolean aggregatable(ASElement e) {
    return (e instanceof ASOntologies) || (e instanceof ASOntology);
  }

  @Override
  public ASElement aggregate(ASElement e) {
    if (e instanceof ASOntologies) {
      putAll(((ASOntologies) e).getOntologyProxies());
      return this;
    }
    if (e instanceof ASOntology) {
      put(((ASOntology) e).getOId(), (ASOntology) e);
      return this;
    }
    return ASElement.super.aggregate(e);
  }
}
