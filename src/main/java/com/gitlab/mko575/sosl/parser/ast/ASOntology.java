package com.gitlab.mko575.sosl.parser.ast;

import com.gitlab.mko575.sosl.ErrorMessages;
import com.gitlab.mko575.sosl.ErrorMessages.ErrorMessagesId;
import com.gitlab.mko575.sosl.ids.GlobalSetId;
import com.gitlab.mko575.sosl.ids.LocalSetId;
import com.gitlab.mko575.sosl.ids.sos.InstanceOntologySeed;
import com.gitlab.mko575.sosl.ids.SetId;
import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.Property;
import com.gitlab.mko575.sosl.ids.OntologyId;
import com.gitlab.mko575.sosl.ids.sos.OntologySeed;
import com.gitlab.mko575.sosl.ids.sos.ConceptOntologySeed;
import com.gitlab.mko575.sosl.parser.CSOntologyVisitorHelper;
import com.gitlab.mko575.sosl.ids.EquippedProperty;
import com.gitlab.mko575.sosl.parser.CST2ASTVisitor;
import com.gitlab.mko575.sosl.parser.IOntologyProxy;
import com.gitlab.mko575.sosl.parser.ast.ASImport.ImportType;
import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import scala.Tuple3;
import scala.jdk.javaapi.CollectionConverters;
import scala.jdk.javaapi.OptionConverters;

/**
 * Abstract syntax representation of an ontology seed.
 */
public class ASOntology extends ASElementContainingProperties implements IOntologyProxy {

  /**
   * Ontology types
   */
  public enum OntologyType { CONCEPT, INSTANCE };

  /**
   * Global identifier of the ontology.
   */
  private final OntologyId oid;

  /**
   * Name of the ontology.
   */
  private final String name;

  /**
   * Type of the ontology.
   */
  private final OntologyType type;

  /**
   * Collected data while visiting an `ontologyDefinition`. This data is collected
   * on the way down. The goal is to have the necessary data available when it's
   * consumed and not have to reconstruct on the way up while building the resulting
   * {@link ASOntology}.
   */
  private final CSOntologyVisitorHelper visitHelper;

  /**
   * Realized imports.
   *   from OId -> ( imported SId -> { as SId } ) }
   */
  // private final Map<OntologyId, Map<SetId, Set<SetId>>> realizedImports = new HashMap<>();

  /**
   * Realized imports of type LINK.
   *   from OId -> ( imported SId -> { as SId } ) }
   */
  private final Map<OntologyId, Map<SetId, Set<SetId>>> realizedLinkImports = new HashMap<>();

  /**
   * Realized imports of type COPY.
   *   from OId -> ( imported SId -> { as SId } ) }
   */
  private final Map<OntologyId, Map<SetId, Set<SetId>>> realizedCopyImports = new HashMap<>();

  /**
   * Default constructor of {@link ASOntology}.
   * @param id the global identifier of the ontology.
   * @param name the name of the ontology.
   * @param type the type of ontology.
   */
  public ASOntology(OntologyId id, String name, OntologyType type, CST2ASTVisitor visitor) {
    super();
    this.oid = id;
    this.name = name;
    this.type = type;
    this.visitHelper = new CSOntologyVisitorHelper(visitor, id, this);
  }

  /**
   * Getter of the global ontology identifier.
   * @return the global identifier.
   */
  public OntologyId getOId() {
    return oid;
  }

  /**
   * Retrieves the helper use to visit the concrete syntax corresponding to this
   * {@link ASOntology}.
   *
   * @return the help used to visit the concrete syntax of this ontology.
   */
  public CSOntologyVisitorHelper getVisitHelper() {
    return visitHelper;
  }

  /**
   * Returns the realized imports
   *   from OId -> ( imported SId -> { as SId } ) }
   */
  private Map<OntologyId, Map<SetId, Set<SetId>>> getRealizedImports() {
    return
        Stream.concat(
            realizedCopyImports.entrySet().stream(),
            realizedLinkImports.entrySet().stream()
        ).collect(
            Collectors.toMap(
              e -> e.getKey(), e -> e.getValue(),
              (vMap1, vMap2) ->
                  Stream.concat(
                      vMap1.entrySet().stream(), vMap2.entrySet().stream()
                  ).collect(Collectors.toMap(
                      e -> e.getKey(), e -> e.getValue(),
                      (v1,v2) -> Sets.union(v1,v2)
                  ))
            )
        );
  }

  /**
   * Imports the provided ontological set into this ontology.
   *
   * @param handle the handle to associate to the imported ontological set, can
   *              be {@code null} if the ontological set wil not be accessible.
   * @param importType type of import (see {@link ImportType}).
   * @param importedSId the global identifier of the ontological set to import.
   * @param importedOSet the ontological set to import.
   * @return the new global identifier of the imported ontological set.
   */
  public SetId importOSet(
      String handle, ImportType importType, SetId importedSId, OSet importedOSet
  ) {
    final Map<OntologyId, Map<SetId, Set<SetId>>> realizedImportsOfType;
    final SetId newSId;
    switch (importType) {
      case COPY:
        realizedImportsOfType = realizedCopyImports;
        Object newLId = visitHelper.closestFreshLId(importedSId.lid());
        newSId = new GlobalSetId(getOId(), newLId);
        visitHelper.registerNewLId(newLId);
        break;
      case LINK:
        realizedImportsOfType = realizedLinkImports;
        newSId = importedSId;
        break;
      default:
        ErrorMessages.logImplementationError("Missing case");
        realizedImportsOfType = null;
        newSId = null;
    }
    addReferencedCategory(handle, newSId, importedOSet);
    // TODO: check validity of following if statement
    if (newSId != null) {
      if (handle != null)
        visitHelper.registerNewHandleAssociation(handle, newSId);
      if (importedOSet != null)
        visitHelper.registerOSet(newSId, importedOSet);
    }
    if (realizedImportsOfType != null && importedSId instanceof GlobalSetId) {
      realizedImportsOfType.merge(
          ((GlobalSetId) importedSId).getOId().get(),
          Map.of(importedSId, Set.of(newSId)),
          (m1, m2) ->
              Stream.concat(m1.entrySet().stream(), m2.entrySet().stream())
                  .collect(Collectors.toMap(
                      Entry::getKey, Entry::getValue,
                      (s1, s2) ->
                          Stream.concat(s1.stream(), s2.stream()).collect(Collectors.toSet())
                  ))
      );
    }
    return newSId;
  }

  /**
   * Returns the set of global identifiers mappings from imported ontological sets
   * to importing ontological sets.
   *
   * @param importOId the global identifier of the ontology to produce to mapping
   *                 for.
   * @return the mappings from imported OSet global identifier to imported OSet
   * identifier
   */
  private Set<Map<SetId,SetId>> getImportMappingsFor(OntologyId importOId) {
    Map<OntologyId, Map<SetId, Set<SetId>>> realizedImports = getRealizedImports();
    Set<Map<SetId,SetId>> envs = new HashSet<>();
    envs.add(new HashMap<>());
    if ( realizedImports.containsKey(importOId) ) {
      realizedImports.get(importOId).forEach(
          (oldSId, newSids) -> {
            Set<Map<SetId,SetId>> envsIterator = new HashSet<>(envs);
            envsIterator.forEach(
                env -> {
                  envs.remove(env);
                  newSids.forEach(
                      newSId -> {
                        Map<SetId,SetId> newEnv = new HashMap<>(env);
                        newEnv.put(oldSId, newSId);
                        envs.add(newEnv);
                      }
                  );
                }
            );
          }
      );
    }
    return envs;
  }

  /**
   * Finalizes the creation of this {@link ASOntology} by:<br/>
   *  - importing properties of copied (import) ontological sets
   */
  public void finalizeCreation(ASOntologies assos) {
    realizedCopyImports.forEach(
        (fromOid, imports) -> {
          Set<Map<SetId,SetId>> envs = getImportMappingsFor(fromOid);
          CollectionConverters.asJava(
              assos.getOntologySeed(fromOid).getPropertiesInvolvingOnly(
                  CollectionConverters.asScala(imports.keySet()).toSet()
              )
          ).forEach(
              (Property p) ->
                  envs.forEach(
                      env ->
                          addProperty(
                              EquippedProperty.equipProperty(p)
                                  .updateReferences(CollectionConverters.asScala(env))
                          )
                  )
          );
        }
    );
  }

  // Implementing IOntologyProxy methods

  @Override
  public String getName() {
    return name;
  }

  @Override
  public OSet getOSet4lId(Object lid) {
    return getOSet4sId(new GlobalSetId(getOId(), lid));
  }

  @Override
  public OntologySeed getSeed() {
    ErrorMessages.debugMessage(
        String.format("Calling getSeed() on %s", this.getName())
    );
    final Map<Object, OSet> sets =
        getReferencedCategories().entrySet().stream()
            .filter(
                e ->
                    (e.getKey() instanceof LocalSetId)
                    ||
                    OptionConverters.toJava(e.getKey().getOId())
                        .map(oid -> oid.equals(this.getOId()))
                        .orElse(false)
            )
            .collect(Collectors.toMap(
                ((Function<Entry<SetId, OSet>,SetId>) Entry::getKey)
                    .andThen(SetId::lid),
                Entry::getValue
            ));
    final Map<OntologyId,OntologySeed> referencesOntologies =
        getReferencedCategories().keySet().stream()
            .map(sId -> sId.getOId().getOrElse(this::getOId))
            .collect(Collectors.toSet())
            .stream()
            .filter(oId -> ! oId.equals(this.getOId()))
            .collect(Collectors.toMap(
                Function.identity(),
                oId -> getVisitHelper().getOntologySeed(oId)
            ));

    final OntologySeed seed;
    switch (type) {
      case CONCEPT:
        seed = new ConceptOntologySeed(
            getOId(), getName(), referencesOntologies, sets, getProperties());
        break;
      case INSTANCE:
        seed = new InstanceOntologySeed(
            getOId(), getName(), referencesOntologies, sets, getProperties());
        break;
      default:
        seed = null;
        ErrorMessages.logImplementationError(
            ErrorMessages.get(ErrorMessagesId.CASE_IMPLEMENTATION_MISSING)
        );
    }
    if (seed != null) seed.finalizeSeedCreation();
    return seed;
  }

  // Overriding ASElement methods

  @Override
  public boolean aggregatable(ASElement e) {
    return (e instanceof ASFormulae) || (e instanceof ASImport);
  }

  @Override
  public ASElement aggregate(ASElement e) {
    if (e instanceof ASImport) {
      final Map<OntologyId, Map<SetId, Set<SetId>>> realizedImportsOfType;
      switch (((ASImport) e).getType()) {
        case COPY:
          realizedImportsOfType = realizedCopyImports;
          break;
        case LINK:
          realizedImportsOfType = realizedLinkImports;
          break;
        default:
          ErrorMessages.logImplementationError("Missing case");
          realizedImportsOfType = null;
      }
      if (realizedImportsOfType != null) {
        realizedImportsOfType.merge(
            ((ASImport) e).getFromOId(), ((ASImport) e).getRealizedImportsFromSource(),
            (oSId2nSId_1, oSId2nSId_2) ->
                Stream.concat(oSId2nSId_1.entrySet().stream(), oSId2nSId_2.entrySet().stream())
                    .collect(Collectors.toMap(
                        Entry::getKey, Entry::getValue,
                        (nSId1, nSId2) ->
                            Stream.concat(nSId1.stream(), nSId2.stream())
                                .collect(Collectors.toSet())
                    ))
        );
      }
      visitHelper.updateImports(
          ((ASImport) e).getFromOId(),
          ((ASImport) e).getType(),
          ((ASImport) e).getDelayedImports()
              .entrySet().stream().collect(Collectors.toMap(
                  Entry::getKey,
                  me -> new Tuple3<ImportType, SetId, OSet>(
                      ((ASImport) e).getType(),
                      me.getValue().a,
                      me.getValue().b
                  )
          )),
          ((ASImport) e).getNotImplicitlyImported().orElse(null)
      );
    }
    return super.aggregate(e);
  }

}
