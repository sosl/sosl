package com.gitlab.mko575.sosl.parser.ast;

import com.gitlab.mko575.sosl.ids.OntologyId;

/**
 * {@link ASElement} representing a global ontology identifier ({@link OntologyId}).
 */
public class ASOntologyId implements ASElement {

  /**
   * The global ontology identifier represented by this {@link ASElement}.
   */
  private OntologyId oid;

  /**
   * Default constructor.
   *
   * @param oid the global ontology identifier ({@link OntologyId}) represented
   *            by the constructed {@link ASElement}.
   */
  public ASOntologyId(OntologyId oid) {
    this.oid = oid;
  }

  /**
   * Returns the global ontology identifier ({@link OntologyId}) represented by
   * this {@link ASElement}.
   *
   * @return the global ontology identifier ({@link OntologyId}) represented by
   * this {@link ASElement}.
   */
  public OntologyId getOntologyId() {
    return oid;
  }
}
