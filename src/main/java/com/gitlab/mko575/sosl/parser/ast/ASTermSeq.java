package com.gitlab.mko575.sosl.parser.ast;

import com.gitlab.mko575.sosl.ids.SetCons;
import com.gitlab.mko575.sosl.ids.SetId;
import com.gitlab.mko575.sosl.ids.Reference;
import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.SetTerm;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import scala.jdk.javaapi.CollectionConverters;

public class ASTermSeq extends ASElementReferencingCategories {

  /**
   * The sequence of {@link SetTerm}s represented by this {@link ASTermSeq}.
   */
  protected List<SetTerm> terms = new ArrayList<>();

  /**
   * Default constructor.
   * Initializes its private parameters with default values and empty containers.
   */
  public ASTermSeq() {
    super();
  }

  /**
   * Constructor for {@link ASTermSeq} containing a single set term.
   * This method does not update the mapping of local identifiers to Set/Category
   * referenced in this {@link ASTermSeq}. It has to be done manually afterwards using
   * {@link ASTermSeq#addReferences(Map, Map)}.
   *
   * @param term the only {@link SetTerm} contained in the constructed {@link ASTermSeq}.
   */
  public ASTermSeq(final SetTerm term) {
    addTerm(term);
  }

  /**
   * Constructor for {@link ASTermSeq} containing a single reference to a user
   * defined ontological set ({@link OSet}).
   *
   * @param hdl the handle used to address the ontological set.
   * @param sid the global identifier of the ontological set.
   * @param oset the ontological set.
   */
  public ASTermSeq(String hdl, SetId sid, OSet oset) {
    addReferencedCategory(hdl, sid, oset);
    addTerm(new Reference(sid));
  }

  /**
   * Transform an {@link ASTermSeq} containing (multiple) {@link SetTerm}s into
   * an {@link ASTermSeq} containing a single {@link SetTerm} of type "set" ({@link SetCons})
   * containing the {@link SetTerm}s contained in this {@link ASTermSeq}.
   *
   * @return this {@link ASTermSeq} as a set.
   */
  public ASTermSeq toSet() {
    SetTerm resTerm = new SetCons(CollectionConverters.asScala(
        Set.copyOf(this.getTerms())
    ).toSet());
    ASTermSeq res = new ASTermSeq(resTerm);
    res.addReferences(this.getAliases(), this.getReferencedCategories());
    return res;
  }

  /**
   * Returns the sequence of {@link SetTerm}s represented by this {@link ASTermSeq}.
   *
   * @return the sequence of {@link SetTerm}s represented by this {@link ASTermSeq}.
   */
  public List<SetTerm> getTerms() {
    return terms;
  }

  /**
   * Add the provided {@link SetTerm} to the sequence of {@link SetTerm}s contained
   * in this {@link ASTermSeq}. This method does not update the mapping of local
   * identifiers to Set/Category referenced in this {@link ASTermSeq}. It has to be
   * done manually using {@link ASTermSeq#addReferencedCategory(String, SetId, OSet)}.
   *
   * @param t the {@link SetTerm} to be added to the sequence of {@link SetTerm}s
   *          contained in this {@link ASElement}.
   * @return this {@link ASTermSeq} (after addition).
   */
  public ASTermSeq addTerm(SetTerm t) {
    terms.add(t);
    return this;
  }

  /**
   * Add the provided {@link SetTerm}s to the end of the sequence of {@link SetTerm}s
   * contained in this {@link ASTermSeq}. This method does not update the mapping of local
   * identifiers to Set/Category referenced in this {@link ASTermSeq}. It has to be
   * done manually using {@link ASTermSeq#addReferences(Map, Map)}.
   *
   * @param ct the {@link SetTerm}s to be added to the sequence of {@link SetTerm}s
   *           contained in this {@link ASElement}.
   * @return this {@link ASTermSeq} (after addition).
   */
  public ASTermSeq addTerms(List<SetTerm> ct) {
    ct.forEach(this::addTerm);
    return this;
  }

  @Override
  public boolean aggregatable(ASElement e) {
    return e instanceof ASTermSeq;
  }

  @Override
  public ASElement aggregate(ASElement e) {
//    assert this != e :
//        String.format(
//            "Aggregating an ASTerm with itself. It seems wrong!\n - %s\n"
//                + " - %s\n - terms: %s",
//            this, e, this.terms
//        )
//    ;
    if (e instanceof ASTermSeq) {
      if ( this != e ) // TODO: check why the reverse can be the case and remove it
        addTerms(((ASTermSeq) e).getTerms());
    }
    return super.aggregate(e);
  }
}
