package com.gitlab.mko575.sosl.parser.ast;

import com.gitlab.mko575.sosl.ids.CartesianProduct;
import com.gitlab.mko575.sosl.ids.TupleCons;
import com.gitlab.mko575.sosl.ids.SetTerm;
import com.gitlab.mko575.sosl.ids.SetId;
import com.gitlab.mko575.sosl.ids.OSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import scala.jdk.javaapi.CollectionConverters;

public class ASTupleFieldSeq extends ASTermSeq {

  private List<String> fieldNames;

  public ASTupleFieldSeq() {
    super();
    fieldNames = new ArrayList<>();
  }

  public ASTupleFieldSeq(final String name, final SetTerm term) {
    super(term);
    fieldNames = new ArrayList<>(Collections.singleton(name));
  }

  public ASTupleFieldSeq(
      final String name, final String hdl, final SetId sid, final OSet oset
  ) {
    super(hdl, sid, oset);
    fieldNames = new ArrayList<>(Collections.singleton(name));
  }

  /**
   * ransform an {@link ASTupleFieldSeq} containing (multiple) {@link SetTerm}s
   * into an {@link ASTermSeq} containing a single {@link SetTerm} returned by
   * the provided constructor applied on the fields (name and {@link SetTerm})
   * contained in this {@link ASTupleFieldSeq}.
   *
   * @param constructor the constructor to use to construct the set term.
   * @return this {@link ASTupleFieldSeq} as a single {@link SetTerm} contained
   * in a {@link ASTermSeq}
   */
  private ASTermSeq toASTerm(
      BiFunction<
          scala.collection.immutable.List<SetTerm>,
          scala.collection.mutable.Map<String,Integer>,
          SetTerm>
          constructor
  ) {
    SetTerm resTerm = constructor.apply(
        CollectionConverters.asScala(this.getTerms()).toList(),
        CollectionConverters.asScala((this.getKeys()))
    );
    ASTermSeq res = new ASTermSeq(resTerm);
    res.addReferences(this.getAliases(), this.getReferencedCategories());
    return res;
  }

  /**
   * Transform an {@link ASTupleFieldSeq} containing (multiple) {@link SetTerm}s
   * into an {@link ASTermSeq} containing a single {@link SetTerm} of type "tuple"
   * ({@link TupleCons}) containing the fields (name and {@link SetTerm}) contained
   * in this {@link ASTupleFieldSeq}.
   *
   * @return this {@link ASTupleFieldSeq} as a tuple.
   */
  public ASTermSeq toTuple() {
    return toASTerm(TupleCons::new);
  }

  /**
   * Transform an {@link ASTupleFieldSeq} containing (multiple) {@link SetTerm}s
   * into an {@link ASTermSeq} containing a single {@link SetTerm} of type
   * "cartesian product" ({@link CartesianProduct}) containing the fields (name
   * and {@link SetTerm}) contained in this {@link ASTupleFieldSeq}.
   *
   * @return this {@link ASTupleFieldSeq} as a cartesian product.
   */
  public ASTermSeq toCartesianProduct() {
    return toASTerm(CartesianProduct::new);
//    SetTerm resTerm = new CartesianProduct(
//        CollectionConverters.asScala(this.getTerms()).toList(),
//        CollectionConverters.asScala(this.getKeys())
//    );
//    ASTermSeq res = new ASTermSeq(resTerm);
//    res.addReferences(this.getAliases(), this.getReferencedCategories());
//    return res;
  }

  public ASTupleFieldSeq addField(final String name, final SetTerm term) {
    super.addTerm(term);
    fieldNames.add(name);
    return this;
  }

  public int getNbFields() {
    return fieldNames.size();
  }

  /**
   * Returns the field names as a mapping from names to the (latest) field number
   * associated to this name/key. No check for duplicated keys is done.
   *
   * @return this tuple fields keying.
   */
  public Map<String,Integer> getKeys() {
    Map<String,Integer> res = new HashMap<>();
    IntStream.range(0, getNbFields()).forEach(
        i -> {
          if (fieldNames.get(i) != null)
            res.put(fieldNames.get(i), i);
        }
    );
    return res;
  }

  public List<Entry<String, SetTerm>> getFields() {
    return IntStream.range(0, getNbFields()).mapToObj(
        i -> getField(i)
    ).collect(Collectors.toList());
  }

  public Entry<String, SetTerm> getField(int i) {
    return Map.entry(fieldNames.get(i), getTerms().get(i));
  }

  public List<String> getFieldNames() {
    return fieldNames;
  }

  public List<SetTerm> getFieldValues() {
    return getTerms();
  }

  @Override
  public boolean aggregatable(ASElement e) {
    return e instanceof ASTupleFieldSeq;
  }

  @Override
  public ASElement aggregate(ASElement e) {
    assert this != e :
        String.format(
            "Aggregating an ASTupleFieldSeq with itself. It seems wrong!\n - %s\n"
                + " - %s\n - names: %s\n - terms: %s",
            this, e, getFieldNames(), getFieldValues()
        )
    ;
    if (e instanceof ASTupleFieldSeq) {
      if ( this != e ) // TODO: check why the reverse can be the case and remove it
        fieldNames.addAll(((ASTupleFieldSeq) e).fieldNames);
    }
    return super.aggregate(e);
  }
}
