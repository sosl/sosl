package com.gitlab.mko575.sosl.parser.ast;

/**
 * {@link ASElement} representing "nothing".
 */
public class ASVoid implements ASElement {

  /**
   * Number of instances of this class.
   */
  private int nbInstances = 0;

  /**
   * A singleton instance used as much as possible.
   */
  private static final ASVoid singletonInstance = new ASVoid();

  /**
   * Private constructor used to create the sole instance of this class
   * ({@link ASVoid#singletonInstance}).
   *
   * @throws AssertionError iff this constructor has already been called or
   *         {@link ASVoid#singletonInstance} is already initialized.
   */
  private ASVoid() {
    if ( nbInstances != 0 || singletonInstance != null ) {
      throw new AssertionError(
          "A sole instance of ASVoid should ever be created!"
      );
    }
    nbInstances += 1;
  }

  /**
   * Returns the singleton instance of {@link ASVoid}.
   *
   * @return an instance of {@link ASVoid}.
   */
  public static ASVoid instance() {
    return singletonInstance;
  }
}
