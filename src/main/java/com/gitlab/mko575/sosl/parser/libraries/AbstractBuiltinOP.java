package com.gitlab.mko575.sosl.parser.libraries;

import com.gitlab.mko575.sosl.ids.GlobalSetId;
import com.gitlab.mko575.sosl.ids.OntologyId;
import com.gitlab.mko575.sosl.ids.SetId;
import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.sos.OntologySeed;
import com.gitlab.mko575.sosl.parser.IOntologyProxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import scala.Option;
import scala.jdk.javaapi.OptionConverters;

public abstract class AbstractBuiltinOP implements IOntologyProxy {

  /**
   * Global identifier of the ontology proxy.
   */
  protected OntologyId oid;

  /**
   * Returns the global identifier of the ontology proxying for.
   *
   * @return the global identifier of the ontology proxying for.
   */
  public OntologyId getOId() {
    return oid;
  }

  /**
   * Set the global identifier of the ontology proxying for to the one provided
   * as parameter.
   *
   * @param oid the global identifier to use.
   */
  public void setOId(final OntologyId oid) {
    this.oid = oid;
  }

  /**
   * Set the global identifier of the ontology proxying for to the default one of
   * the ontology proxying for.
   */
  protected void setOIdToDefault() {
    setOId(getOntologySeedProxyingFor().getGlobalIdentifier());
  }

  /**
   * Returns the seed of the ontology proxying for.
   *
   * @return the seed of the ontology proxying for.
   */
  protected abstract OntologySeed getOntologySeedProxyingFor();

  /**
   * Returns the exported mapping from handles to local ids.
   *
   * @return the exported mapping from handles to local ids.
   */
  protected abstract Map<String,Object> getExportedHandles();

  @Override
  public String getName() {
    return getSeed().getName();
  }

  @Override
  public OntologySeed getSeed() {
    return getOntologySeedProxyingFor();
  }

  @Override
  public OSet getOSet4lId(Object lid) {
    return
        Optional.ofNullable(getSeed())
            // .map(s -> s.get(lid).getOrElse(() -> null))
            .flatMap(s -> OptionConverters.toJava(s.get(lid)))
            .orElse(null);
  }

  @Override
  public SetId getSIdForHandle(String handle) {
    return getAliases().get(handle);
  }

  @Override
  public Map<String, SetId> getAliases() {
    final Map<String, SetId> res = new HashMap<>();
    getExportedHandles().forEach(
        (h, lid) -> res.put(h, new GlobalSetId(oid, lid))
    );
    return res;
  }

}
