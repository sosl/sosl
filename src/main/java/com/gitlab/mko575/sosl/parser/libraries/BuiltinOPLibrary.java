package com.gitlab.mko575.sosl.parser.libraries;

import com.gitlab.mko575.sosl.ErrorMessages;
import com.gitlab.mko575.sosl.ids.OntologyId;
import com.gitlab.mko575.sosl.ids.Entities$;
import com.gitlab.mko575.sosl.ids.Values$;
import com.gitlab.mko575.sosl.ids.Namespace;
import com.gitlab.mko575.sosl.ids.RootNamespace$;
import com.gitlab.mko575.sosl.ids.ExtendedNamespace;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Static class registering all the builtin ontology proxies.
 */
public class BuiltinOPLibrary {

  /**
   * Namespace for SOSL built-in ontology proxies.
   */
  private static final Namespace SOSL_BUILTIN_PROXIES_NS =
      new ExtendedNamespace(RootNamespace$.MODULE$, "SOSL");

  public static void init() {
    // registerLibrary(OP4Integers.class);
    registerLibrary(
        new OntologyId(SOSL_BUILTIN_PROXIES_NS, "IntegersAsEntities"),
        new OP4Integers(Entities$.MODULE$)
    );
    registerLibrary(
        new OntologyId(SOSL_BUILTIN_PROXIES_NS, "IntegersAsValues"),
        new OP4Integers(Values$.MODULE$)
    );
  }

  private static final Map<OntologyId, AbstractBuiltinOP> knownLibraries = new HashMap<>();

  protected static void registerLibrary(OntologyId oid, AbstractBuiltinOP lib) {
    knownLibraries.put(oid, lib);
  }

  protected static void registerLibrary(Class<? extends AbstractBuiltinOP> c) {
    try {
      AbstractBuiltinOP op = c.getDeclaredConstructor().newInstance();
      registerLibrary(op.getOId(), op);
    } catch ( InvocationTargetException
            | InstantiationException
            | NoSuchMethodException
            | IllegalAccessException e
    ) {
      if (e != null) throw new Error(e);
      ErrorMessages.logImplementationError(
          String.format(
              "Missing default constructor for '%s': %s",
              c.getName(),
              e.toString()
          )
      );
    }
  }

  /**
   * Returns the builtin ontology proxy which is identified by the provided global
   * identifier.
   *
   * @param oid the global identifier to look for.
   * @return the associated builtin ontology proxy, or {@code null} if there is none.
   */
  public static AbstractBuiltinOP getOPLibrary(OntologyId oid) {
    return knownLibraries.get(oid);
  }

  /**
   * Returns the known builtin ontology proxies as a mapping from their global identifiers
   * to themselves.
   *
   * @return the known builtin ontology proxies.
   */
  public static Map<OntologyId, AbstractBuiltinOP> getKnownProxies() {
    return knownLibraries;
  }
}
