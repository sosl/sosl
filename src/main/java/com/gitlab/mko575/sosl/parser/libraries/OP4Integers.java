package com.gitlab.mko575.sosl.parser.libraries;

import com.gitlab.mko575.sosl.ids.GlobalSetId;
import com.gitlab.mko575.sosl.ids.LocalSetId;
import com.gitlab.mko575.sosl.ids.OntologyId;
import com.gitlab.mko575.sosl.ids.SetId;
import com.gitlab.mko575.sosl.ids.AxiomaticSet;
import com.gitlab.mko575.sosl.ids.Entities$;
import com.gitlab.mko575.sosl.ids.IdentifiableEntity;
import com.gitlab.mko575.sosl.ids.OSet;
import com.gitlab.mko575.sosl.ids.Subset;
import com.gitlab.mko575.sosl.ids.World$;
import com.gitlab.mko575.sosl.ids.Property;
import com.gitlab.mko575.sosl.ids.Reference;
import com.gitlab.mko575.sosl.ids.SetTerm;
import com.gitlab.mko575.sosl.ids.common.LeafType;
import com.gitlab.mko575.sosl.ids.common.SetOf;
import com.gitlab.mko575.sosl.ids.common.SetTypeConstraints;
import com.gitlab.mko575.sosl.ids.sos.ConceptOntology$;
import com.gitlab.mko575.sosl.ids.sos.OntologySeed;
import com.gitlab.mko575.sosl.ids.sos.OntologySeedFactory;
import com.gitlab.mko575.sosl.ids.EquippedProperty;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import scala.Option;
import scala.Tuple2;
import scala.jdk.OptionConverters;
import scala.jdk.javaapi.CollectionConverters;

public class OP4Integers extends AbstractBuiltinOP {

  /**
   * "Macro" to access more easily the string "Integers".
   */
  private static final String INTEGERS_STR = "Integers";

  /**
   * "Macro" to access more easily the "Integers" ontological set.
   */
  private static final OSet INTEGERS_OSET = new OSet(INTEGERS_STR, INTEGERS_STR);

  /**
   * Initial mapping from ontological sets exported by this ontology proxy to:
   * supersets of each ontological set; and handles under which each ontological
   * set is exported.
   */
  private static final Map<OSet, Tuple2<Set<SetTerm>, Set<String>>>
      initiallyExportedOntologicalSets =
      new HashMap<>(Map.of(
          INTEGERS_OSET,
          new Tuple2<>(
              new HashSet<>(Collections.singleton(World$.MODULE$)),
              Set.of(INTEGERS_STR)
          )
      ));

  /**
   * Default ontological seed proxying for.
   */
  private static final OntologySeed dfltSeedProxyingFor =
      (new OntologySeedFactory("SOSL")).build(
          ConceptOntology$.MODULE$,
          INTEGERS_STR,
          CollectionConverters.asScala(
              initiallyExportedOntologicalSets.entrySet().stream()
                  .collect(Collectors.toMap(
                      Map.Entry::getKey ,
                      (Map.Entry<OSet, Tuple2<Set<SetTerm>, Set<String>>> e)
                          -> CollectionConverters.asScala(e.getValue()._1))
                  )
          )
      );

  /**
   * The ontological seed this object is proxying for.
   */
  private OntologySeed seedProxyingFor;

  /**
   * The ontological sets exported by this ontology proxy.
   */
  private final Map<OSet, Tuple2<Set<SetTerm>, Set<String>>> exportedOntologicalSets;

  /**
   * The axiomatic superset of integers in the ontological seed this object is proxying for.
   */
  private final AxiomaticSet integersSuperset;

  /**
   * Default constructor for a builtin ontology proxy for integers parameterized
   * with a specific global identifier and axiomatic superset for integers.
   *
   * @param oid the global identifier to use for this ontology proxy.
   * @param ss axiomatic superset to use for integers.
   */
  protected OP4Integers(OntologyId oid, AxiomaticSet ss) {
    if (oid == null)
      oid = dfltSeedProxyingFor.getGlobalIdentifier();
    if (ss == null)
      ss = Entities$.MODULE$;
    integersSuperset = ss;
    seedProxyingFor = dfltSeedProxyingFor;
    CollectionConverters.asJava(seedProxyingFor.sets().values()).forEach(
        s -> s.setAxiomaticSuperset(integersSuperset)
            .setAxiomaticType(
                SetOf.apply(new LeafType<>(integersSuperset))
            )
    );
    exportedOntologicalSets = initiallyExportedOntologicalSets;
    addProperty(new Subset(new Reference(new LocalSetId(INTEGERS_OSET.lid())), ss, true));
    setOId(oid);
  }

  /**
   * Constructor for a builtin ontology proxy for integers with default parametrization.
   */
  protected OP4Integers() {
    this(null, null);
  }

  /**
   * Constructor for a builtin ontology proxy for integers with the provided axiomatic
   * superset for integers and default values for other parameters.
   *
   * @param ss axiomatic superset to use for integers.
   */
  protected OP4Integers(AxiomaticSet ss) {
    this(null, ss);
  }

  /**
   * Constructor for a builtin ontology proxy for integers with the provided global
   * identifier and default values for other parameters.
   *
   * @param oid the global identifier to use for this ontology proxy.
   */
  protected OP4Integers(OntologyId oid) {
    this(oid, null);
  }

  /**
   * Adds the provided ontological set to the ontology proxying for.
   *
   * @param s the ontological set to add.
   */
  protected void addOSet(OSet s) {
    seedProxyingFor = seedProxyingFor.add(
        s.setAxiomaticSuperset(integersSuperset)
            .setAxiomaticType(new LeafType<>(integersSuperset))
    );
  }

  /***
   * Add the provided property to the ontology proxying for. No verification is done.
   *
   * @param prop the property to add
   */
  protected void addProperty(Property prop) {
    seedProxyingFor = seedProxyingFor.add(prop);
  }

  @Override
  public void setOId(OntologyId oid) {
    super.setOId(oid);
    seedProxyingFor = seedProxyingFor.setGlobalIdentifier(oid);
  }

  @Override
  protected OntologySeed getOntologySeedProxyingFor() {
    return seedProxyingFor;
  }

  @Override
  protected Map<String,Object> getExportedHandles() {
    return
        exportedOntologicalSets.entrySet().stream().flatMap(
            (Map.Entry<OSet, Tuple2<Set<SetTerm>, Set<String>>> e) ->
                e.getValue()._2.stream().map(
                    (String hdl) -> Map.entry(hdl, e.getKey().lid())
                )
        ).collect(
            Collectors.toMap(Entry::getKey, Entry::getValue, (a,b) -> b)
        );
  }

  @Override
  public SetId getSIdForHandle(String handle) {
    SetId res = super.getSIdForHandle(handle);
    if (res == null) {
      try {
        int hdlAsInt = Integer.parseInt(handle);
        res = new GlobalSetId(oid, hdlAsInt);
        addOSet(new OSet(hdlAsInt, handle));
        addProperty(
            new Subset(
                new Reference(new LocalSetId(hdlAsInt)),
                new Reference(new LocalSetId(INTEGERS_OSET.lid())),
                true
            )
        );
      } catch (NumberFormatException ignored) {
      }
    }
    return res;
  }

}
