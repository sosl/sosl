/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids

import scala.language.implicitConversions
import com.gitlab.mko575.sosl.ids.EquippedSetTerm.equipSetTerm
import com.gitlab.mko575.sosl.ids.EquippedProperty.equipProperty
import com.gitlab.mko575.sosl.ids.identifiers.OSetLocalId
import com.gitlab.mko575.sosl.ids.OntologyId
import com.gitlab.mko575.sosl.ids.common.TypeExpr
import com.gitlab.mko575.sosl.ids.sos.OntologySeed

// import scala.collection.Set
import scala.collection.mutable.{Map => MMap}

object Commons // Here only for IntelliJ to be happy

package object commons {
  type FileId = Object
  type GlobalId = Object
  type LocalId = Object
}

// package commons {

  // GENERAL TRAITS

  sealed trait IdentifiableEntity {
    val lid: OSetLocalId
  }

  sealed trait NamedEntity {
    var name: String
  }

  // ONTOLOGICAL SETS

  case class OSet(lid: OSetLocalId) extends IdentifiableEntity with NamedEntity {
    override var name: String = lid.toString
    private var owningSeed: Option[OntologySeed] = None
    private var axiomaticSuperset: Option[SetTerm] = None
    private var axiomaticType: Option[TypeExpr[AxiomaticSet]] = None

    def this(lid:Any, name:String) = {
      this(lid)
      this.name = name
    }

    def updateWith(c: OSet): OSet =
      OSet(c.lid, if (c.name != null) { c.name } else { name } )

    def updateName(n: String): OSet =  {
      this.name = n
      this
    }

    def getOwningSeed: Option[OntologySeed] = owningSeed
    def setOwningSeed(s: OntologySeed): OSet = {
      this.owningSeed = Some(s)
      this
    }

    def getAxiomaticSuperset: Option[SetTerm] = axiomaticSuperset
    def setAxiomaticSuperset(t:SetTerm): OSet = {
      this.axiomaticSuperset = Some(t)
      this
    }

    def getAxiomaticType: Option[TypeExpr[AxiomaticSet]] = axiomaticType
    def setAxiomaticType(value: TypeExpr[AxiomaticSet]): OSet = {
      this.axiomaticType = Some(value)
      this
    }

    def toTypedString: String =
      s"${lid}: (${axiomaticSuperset.getOrElse("???")} <: ${axiomaticType.getOrElse("???")})"
  }

  // SET TERMS

  sealed trait SetTerm {
    /**
     * Returns a (human readable compact) string representation of this set-term
     * in the context of the ontology whose global identifier is provided as parameter.
     *
     * @param oid the global identifier of the ontology into which the string
     *            is to be interpreted.
     */
    def toString(oid: OntologyId): String
  }

  // SET TERM LEAVES

  sealed trait SetTermLeaf extends SetTerm

  case class Reference(sId: SetId) extends SetTermLeaf {
    override def toString(oid: OntologyId): String = sId.toString(oid)
  }

  sealed trait AxiomaticSet extends SetTermLeaf with IdentifiableEntity with NamedEntity {
    override def toString(oid: OntologyId): String = name
  }

  /**
   * The set of everything.
   */
  case object World extends AxiomaticSet {
    override val lid: OSetLocalId = World
    override var name: String = "World"
  }

  /**
   * The empty set.
   */
  case object Emptyset extends AxiomaticSet {
    override val lid: OSetLocalId = Emptyset
    override var name: String = "Empty"
  }

  /**
   * The set of ??? (all singletons ?).
   */
  case object Singletons extends AxiomaticSet {
    override val lid: OSetLocalId = Singletons
    override var name: String = "Singletons"
  }

  /**
   * The set of all entities.
   */
  case object Entities extends AxiomaticSet {
    override val lid: OSetLocalId = Entities
    override var name: String = "Entities"
  }

  /**
   * The set of all values.
   */
  case object Values extends AxiomaticSet {
    override val lid: OSetLocalId = Values
    override var name: String = "Values"
  }

  object AxiomaticSets {
    val content : Set[AxiomaticSet] =
      Set[AxiomaticSet](World, Emptyset, Singletons, Entities, Values)

    def lub(a:AxiomaticSet, b:AxiomaticSet) : AxiomaticSet =
      (a,b) match {
        case (am, bm) if am == bm => am
        case (World, _) | (_, World) => World
        case (Singletons, _) | (_, Singletons) => Singletons
        case (Entities, Values) | (Values, Entities) => Singletons
        case (Emptyset, bm) => bm
        case (am, Emptyset) => am
      }

    def glb(a:AxiomaticSet, b:AxiomaticSet) : AxiomaticSet =
      (a,b) match {
        case (Emptyset, _) | (_, Emptyset) => Emptyset
        case (World, bm) => bm
        case (am, World) => am
        case (am, bm) if am == bm => am
        case (Singletons, Entities) | (Entities, Singletons) => Entities
        case (Singletons, Values) | (Values, Singletons) => Values
        case (Entities, Values) | (Values, Entities) => Emptyset
      }

    def lub(a:Option[AxiomaticSet], b:Option[AxiomaticSet]) : Option[AxiomaticSet] = {
      (a,b) match {
        case (None,_) | (_, None) => None
        case (Some(ac), Some(bc)) => Some(lub(ac,bc))
      }
    }
  }

  // SET TERM NODES

  sealed trait SetTermNode extends SetTerm {
    val symbol: String
    val subterms: Iterable[SetTerm]
  }

  case class SetCons(subterms: Set[SetTerm]) extends SetTermNode {
    override val symbol: String = "{}"
    override def toString(oid: OntologyId): String =
      s"{${subterms.map(t => t.toString(oid)).addString(new StringBuilder, ", ")}}"

  }

  case class TupleCons(subterms: List[SetTerm], keys: Map[String, Int]) extends SetTermNode {
    override val symbol: String = "[]"
    def this(fields: List[SetTerm], keys: MMap[String, Integer]) =
      this(fields, keys.toMap.map({case (k, v) => (k, v.toInt)}))

    override def toString(oid: OntologyId): String =
      s"(${subterms.map(t => t.toString(oid)).addString(new StringBuilder, ", ")})"
  }

  case class Complement(set: SetTerm, relativeTo: Option[SetTerm]) extends SetTermNode {
    override val symbol: String = "C"
    override val subterms: Set[SetTerm] =
      Set(set) union relativeTo.toSet

    override def toString(oid: OntologyId): String =
      s"$symbol(${set.toString(oid)}${
        relativeTo
          .map(t => s" in ${t.toString(oid)}")
          .getOrElse("")
      })"
  }

  case class Powerset(set: SetTerm) extends SetTermNode {
    override val symbol: String = "P"
    override val subterms: Set[SetTerm] = Set(set)
    override def toString(oid: OntologyId): String = s"$symbol(${set.toString(oid)})"
  }

  abstract sealed class NAryInfixOpSetTerm(
                                            subterms: Iterable[SetTerm],
                                            isAssociative: Boolean
                                          ) extends SetTermNode {
    override def toString(oid: OntologyId): String =
      s"(${subterms.map(t => t.toString(oid)).addString(new StringBuilder, s" $symbol ")})"
  }

  abstract sealed class AssociativeOpSetTerm(subterms: Set[SetTerm])
    extends NAryInfixOpSetTerm(subterms, true)

  case class Union(subterms: Set[SetTerm]) extends AssociativeOpSetTerm(subterms) {
    override val symbol: String = "∪"
  }

  case class Intersection(subterms: Set[SetTerm]) extends AssociativeOpSetTerm(subterms) {
    override val symbol: String = "∩"
  }

  case class UnionProduct(subterms: Set[SetTerm]) extends AssociativeOpSetTerm(subterms) {
    override val symbol: String = "⊗"
  }

  case class CartesianProduct(
                               subterms: Seq[SetTerm], keys: Map[String, Int]
                             )
    extends NAryInfixOpSetTerm(subterms, false) {
    override val symbol: String = "×"
    // def this(sets: List[SetTerm], keys: MMap[String, Int]) = this(sets, keys.toMap)
    def this(sets: List[SetTerm], keys: MMap[String, Integer]) =
      this(sets, keys.toMap.map({case (k, v) => (k, v.toInt)}))
  }

  // For more complex dimension typing see:
  //  - https://www.baeldung.com/scala/type-disjunction
  //  - https://stackoverflow.com/questions/3508077/how-to-define-type-disjunction-union-types
  //  - https://stackoverflow.com/questions/23378029/scala-union-types-with-closures
  // Or move to Scala 3
  case class Restriction[T](tupleSet: SetTerm, dimension:T, on: SetTerm) extends SetTermNode {
    override val symbol: String = "▽"
    override val subterms: Set[SetTerm] = Set(tupleSet,on)
    override def toString(oid: OntologyId): String =
      s"(${tupleSet.toString(oid)}$symbol($dimension, ${on.toString(oid)}))"
  }

  case class Junction(
    lhs: SetTerm, rhs: SetTerm, dimensions:Option[Set[(String, String)]], collapsing: Boolean
  ) extends SetTermNode {
    override val symbol: String = if ( collapsing ) { "⧓" } else { "⋈" }
    override val subterms: Set[SetTerm] = Set(lhs, rhs)
    override def toString(oid: OntologyId): String = {
      val dimStr =
        dimensions.map(
          set =>
            set.map{case (s1,s2) => s"$s1, $s2"}
              .map(s => s.addString(new StringBuilder, "(", "; ", ")"))
        ).getOrElse("")
      s"(${lhs.toString(oid)} $symbol$dimStr ${rhs.toString(oid)})"
    }
  }

  case class Relations(
                        domain: SetTerm, codomain: SetTerm,
                        areAllTotal: Boolean, areAllSurjective: Boolean,
                        areAllFunctional: Boolean, areAllInjective: Boolean
                      ) extends SetTermNode {
    override val symbol: String =
      if ( ! areAllFunctional ) {
        "℘(×)"
      } else {
        (areAllTotal, areAllInjective, areAllSurjective) match {
          case (false, false, false) => "⇸"
          case (true, false, false) => "→"
          case (false, true, false) => "⤔"
          case (true, true, false) => "↣"
          case (false, false, true) => "⤀"
          case (true, false, true) => "↠"
          case (false, true, true) => "⤗"
          case (true, true, true) => "⤖"
        }
      }
    override val subterms: Set[SetTerm] = Set(domain, codomain)

    override def toString(oid: OntologyId): String = {
      if ( ! areAllFunctional ) {
        s"℘(${domain.toString(oid)} × ${codomain.toString(oid)})"
      } else {
        s"(${domain.toString(oid)} $symbol ${codomain.toString(oid)})"
      }
    }
  }

  // PROPERTIES

  sealed trait Property {
    // override def equals(obj: Any): Boolean = this == obj

    val symbol: String
    val terms: Iterable[SetTerm]
    /**
     * Returns a (human readable compact) string representation of this property
     * in the context of the ontology whose global identifier is provided as parameter.
     *
     * @param oid the global identifier of the ontology into which the string
     *            is to be interpreted.
     */
    def toString(oid: OntologyId):String
  }

  sealed abstract class BinaryProperty(val lhs: SetTerm, val rhs: SetTerm) extends Property {
    override val terms: Iterable[SetTerm] = Seq(lhs, rhs)
    override def toString(oid: OntologyId): String =
      s"${lhs.toString(oid)} $symbol ${rhs.toString(oid)}"
  }

  case class Subset(subset: SetTerm, superset: SetTerm, isStrict: Boolean)
    extends BinaryProperty(subset, superset) {
    override val symbol: String = if (isStrict) "⊂" else "⊆"
  }

  case class In(element: SetTerm, set: SetTerm)
    extends BinaryProperty(element, set) {
    override val symbol: String = "∊"
  }

  sealed trait NAryProperty extends Property

  case class Equal(terms: Set[SetTerm]) extends NAryProperty {
    override val symbol: String = "="
    override def toString(oid: OntologyId): String =
      s"${terms.map(t => t.toString(oid)).addString(new StringBuilder, s" $symbol ")}"
  }

  case class Distinct(terms: Set[SetTerm]) extends NAryProperty {
    override val symbol: String = "distinct"
    override def toString(oid: OntologyId): String =
      s"$symbol ${terms.map(t => t.toString(oid)).addString(new StringBuilder, ", ")}"
  }

  sealed trait Predicate extends Property {
    val set: SetTerm
    override val terms: Iterable[SetTerm] = Iterable(set)
    override def toString(oid: OntologyId): String = s"$symbol(${set.toString(oid)})"
  }
  case class Finite(set: SetTerm) extends Predicate {
    override val symbol: String = "finite"
  }
  case class Atomic(set: SetTerm) extends Predicate {
    override val symbol: String = "atomic"
  }
  case class Total(set: SetTerm) extends Predicate {
    override val symbol: String = "total"
  }
  case class Functional(set: SetTerm) extends Predicate {
    override val symbol: String = "functional"
  }
  case class Surjective(set: SetTerm) extends Predicate {
    override val symbol: String = "surjective"
  }
  case class Injective(set: SetTerm) extends Predicate {
    override val symbol: String = "injective"
  }

  /** ************************************************************************** */

  /**
   * Object containing the implicit functions to "equip" (or "unequip) a SetTerm
   * with useful functions.
   */
  object EquippedSetTerm {
    implicit def equipSetTerm(st: SetTerm): EquippedSetTerm = EquippedSetTerm(st)
    implicit def unequipSetTerm(est: EquippedSetTerm): SetTerm = est.term
  }

  case class EquippedSetTerm(term: SetTerm) {

    /**
     * Returns the identifiers of all the ontological sets referenced in this
     * (Equipped)SetTerm.
     *
     * @return the identifiers of the OSets referenced.
     */
    def getOSets: Iterable[SetId] = {
      term match {
        case Reference(id : SetId) => Set(id)
        case _ : AxiomaticSet => Set()
        case t : SetTermNode => t.subterms.flatMap(st => st.getOSets)
//        case t : NAryOpSetTerm => t.subterms.flatMap(st => st.getOSets)
//        case CartesianProduct(sets, _) => sets.toSet[SetTerm].flatMap(st => st.getOSets)
//        case Complement(set, relativeTo) =>
//          set.getOSets ++ relativeTo.toSet[SetTerm].flatMap(st => st.getOSets)
//        case Relations(dom, codom, _, _, _, _) => dom.getOSets ++ codom.getOSets
      }
    }

    /**
     * Returns a new {@link SetTerm} whose references to {@link OSet} have been
     * replaced by the ones in the update map provided.
     *
     * @param update the update map
     * @return the updated SetTerm
     */
    def updateReferences(update: Map[SetId,SetId]) : SetTerm =
      term match {
        case Reference(setId) => Reference(update.getOrElse(setId, setId))
        case _: AxiomaticSet => term
        case SetCons(terms) =>
          SetCons(terms.map(t => t.updateReferences(update)))
        case TupleCons(terms, keys) =>
          TupleCons(terms.map(t => t.updateReferences(update)), keys)
        case Complement(set, relativeTo) =>
          Complement(
            set.updateReferences(update),
            relativeTo.map(t => t.updateReferences(update))
          )
        case Powerset(t) => Powerset(t.updateReferences(update))
        case term: AssociativeOpSetTerm =>
          term match {
            case Union(subterms) =>
              Union(subterms.map(t => t.updateReferences(update)))
            case Intersection(subterms) =>
              Intersection(subterms.map(t => t.updateReferences(update)))
            case UnionProduct(subterms) =>
              UnionProduct(subterms.map(t => t.updateReferences(update)))
          }
        case Restriction(t, d, o) =>
          Restriction(t.updateReferences(update), d, o.updateReferences(update))
        case Junction(lhs, rhs, dims, c) =>
          Junction(lhs.updateReferences(update), rhs.updateReferences(update), dims, c)
        case CartesianProduct(sets, keys) =>
          CartesianProduct(sets.map(t => t.updateReferences(update)), keys)
        case Relations(domain, codomain, areAllT, areAllS, areAllF, areAllI) =>
          Relations(
            domain.updateReferences(update),
            codomain.updateReferences(update),
            areAllT, areAllS, areAllF, areAllI
          )
      }

    /**
     * If all the optional elements of the provided iterable are Some identitical
     * value, then returns it. Otherwise returns None.
     *
     * @param coll the iterable to iterate over
     * @tparam A ...
     * @return the unique value if it exists.
     */
    private def getUniqueValue[A](coll: Iterable[Option[A]]) : Option[A] =
      coll.reduce(
            (o1,o2) =>
              (o1,o2) match {
                case (Some(a1), Some(a2)) if a1 == a2 => Some(a1)
                case _ => None
              }
      )

    /**
     * Returns the width (or arity) of this (equipped) SetTerm, if it can be computed
     * with the information contained in {@link env}. Otherwise, it returns None.
     *
     * @param env known width (arity) of the OSets identified by their global identifier.
     * @return this term width (or arity) if it can be computed.
     */
      // TODO: check that implementation (and how its used)
      //  - Relations are setOf(setOf(pairs/2-tuples))
      //  - CartesianProducts are setOf(tuples)
    @SuppressWarnings(Array("checkstyle:cyclomatic.complexity"))
    // scalastyle:off cyclomatic.complexity
    def getWidth(env:Map[SetId,Int]) : Option[Int] = { // or arity
      term match {
        case Reference(sid) => env.get(sid)
        case a: AxiomaticSet =>
          a match {
            case World | Emptyset => None
            case Singletons | Entities | Values => Some(1)
          }
        case _: Relations => Some(2)
        case CartesianProduct(subterms, _) => Some(subterms.size)
        case SetCons(subterms) =>
          getUniqueValue(subterms.map(t => t.getWidth(env)))
        case TupleCons(subterms, _) =>
          Some(subterms.size)
        case Complement(_, superset) => superset.getOrElse(World).getWidth(env)
        case Powerset(subterm) => subterm.getWidth(env)
        case node: AssociativeOpSetTerm =>
          getUniqueValue(node.subterms.map(t => t.getWidth(env)))
        case Restriction(t,_,_) => t.getWidth(env)
        case Junction(lhs, rhs, dims, collapsing) =>
          (lhs.getWidth(env), rhs.getWidth(env)) match {
            case (Some(lhsW), Some(rhsW)) =>
              Some(lhsW + rhsW - (if (collapsing) { dims.map(s => s.size).getOrElse(1) } else 0))
            case _ => None
          }
      }
    }
    // scalastyle:on cyclomatic.complexity

    /**
     * Returns a function which computes the axiomatic set term (a set term whose
     * leaves are axiomatic sets) which is the "bound" (lub or glb) of this term
     * and the other term provided as parameter, assuming they are both axiomatic
     * set terms.
     *
     * @param boundingOp a binary operation computing the "bound" of two elements
     * @param zero the absorbing element (or annihilating element) for the bounding
     *             operation
     * @param neutral the neutral element for the bounding operation
     * @return the above mentioned function
     */
    private def axiomaticBound(
      boundingOp : (AxiomaticSet, AxiomaticSet) => AxiomaticSet,
      zero : AxiomaticSet,
      neutral : AxiomaticSet
    ) : SetTerm => SetTerm = {
      def thisAxiomaticBoundFct(t:EquippedSetTerm): SetTerm => SetTerm =
        t.axiomaticBound(boundingOp, zero, neutral)
      other => {
        (this: SetTerm, other) match {
          case (a: AxiomaticSet, other) if a == neutral => other
          case (other, a: AxiomaticSet) if a == neutral => other
          case (a: AxiomaticSet, b: AxiomaticSet) => boundingOp(a, b)
            // TODO: add other cases, including restrictions and junctions
          case (CartesianProduct(aSts, aKeys), CartesianProduct(bSts, bKeys))
            if aSts.length == bSts.length =>
            CartesianProduct(
              (aSts.zip(bSts): Seq[(SetTerm, SetTerm)]).map[SetTerm](
                { case (aSt, bSt) => thisAxiomaticBoundFct(aSt: EquippedSetTerm)(bSt) }
              ),
              aKeys.filter{ case (k,v) => ! bKeys.contains(k) || bKeys.get(k).contains(v) }
                ++ bKeys.filter{ case (k,v) => ! aKeys.contains(k) || aKeys.get(k).contains(v) }
            )
          case
            (
              Relations(domA, codA, _, _, _, _),
              Relations(domB, codB, _, _, _, _)
              )
          =>
            Relations(
              thisAxiomaticBoundFct(domA: EquippedSetTerm)(domB),
              thisAxiomaticBoundFct(codA: EquippedSetTerm)(codB),
              areAllTotal = false, areAllSurjective = false,
              areAllFunctional = false, areAllInjective = false
            )
          case _ => zero
        }
      }
    }

    /**
     * The function which computes the axiomatic set term (a set term whose
     * leaves are axiomatic sets) which is the smallest superset (or Least Upper
     * Bound, a.k.a lub) of this term and the other term provided as parameter,
     * assuming they are both axiomatic set terms.
     */
    val axiomaticLUB : SetTerm => SetTerm =
      axiomaticBound(AxiomaticSets.lub, World, Emptyset)

    /**
     * The function which computes the axiomatic set term (a set term whose
     * leaves are axiomatic sets) which is the biggest subset (or Greatest Lower
     * Bound, a.k.a glb) of this term and the other term provided as parameter,
     * assuming they are both axiomatic set terms.
     */
    val axiomaticGLB : SetTerm => SetTerm =
      axiomaticBound(AxiomaticSets.glb, Emptyset, World)

    /**
     * The operator which computes the axiomatic set term (a set term whose leaves
     * are axiomatic sets) which is the smallest superset (or Least Upper Bound,
     * a.k.a glb) of the two terms provided as parameter, assuming they are both
     * axiomatic set terms.
     *
     * @see axiomaticLUB
     */
    private val optLUB: (Option[SetTerm], Option[SetTerm]) => Option[SetTerm] =
      (aOpt,bOpt) => (aOpt, bOpt) match {
        case (None, _) | (_, None) => None
        case (Some(a), Some(b)) => Some(a.axiomaticLUB(b))
      }

    /**
     * The operator which computes the axiomatic set term (a set term whose leaves
     * are axiomatic sets) which is the biggest subset (or Greatest Lower Bound,
     * a.k.a glb) of the two terms provided as parameter, assuming they are both
     * axiomatic set terms.
     *
     * @see axiomaticGLB
     */
    private val optGLB: (Option[SetTerm], Option[SetTerm]) => Option[SetTerm] =
      (aOpt,bOpt) => (aOpt, bOpt) match {
        case (None, _) | (_, None) => None
        case (Some(a), Some(b)) => Some(a.axiomaticGLB(b))
      }

    /**
     * Computes the axiomatic set term (a set term whose leaves are axiomatic sets)
     * which is the smallest superset (or Least Upper Bound, a.k.a lub) of this
     * set term, if it is possible to compute it using the provided environment.
     *
     * @param env a "typing" environment containing the known axiomatic supersets
     *            of some ontological sets identified by their global identifier.
     * @return the smallest axiomatic superset of this set term
     */
    @SuppressWarnings(Array("checkstyle:cyclomatic.complexity"))
    // scalastyle:off cyclomatic.complexity
    def getAxiomaticSuperset(seed: OntologySeed, env:Map[SetId,SetTerm]) : Option[SetTerm] = {
      term match {
        case a: AxiomaticSet => Some(a)
        case Reference(sid @ LocalSetId(_)) => env.get(sid)
        case Reference(sid @ GlobalSetId(oid, _)) if oid.equals(seed.getGlobalIdentifier()) =>
          env.get(sid)
        case Reference(sid @ GlobalSetId(oid, _)) if ! oid.equals(seed.getGlobalIdentifier()) =>
          seed.getAxiomaticSupersetOf(sid)
        case Union(subterms) =>
            subterms.foldLeft(Some(Emptyset):Option[SetTerm])(
              (acc, t) => optLUB(acc, t.getAxiomaticSuperset(seed, env))
            )
        case Intersection(subterms) =>
            subterms.foldLeft(Some(World):Option[SetTerm])(
              (acc, t) => optGLB(acc, t.getAxiomaticSuperset(seed, env)) // !!!!!
            )
        case Complement(_, None) =>
          Some(World)
        case Complement(subset, Some(superset)) if subset == superset =>
          Some(Emptyset)
        case Complement(_, Some(superset)) => superset.getAxiomaticSuperset(seed, env)
        case Powerset(t) =>
          t.getAxiomaticSuperset(seed, env).map(t => Powerset(t))
        case Restriction(t,_,_) => t.getAxiomaticSuperset(seed, env)
        case CartesianProduct(Nil, keys) => Some(CartesianProduct(Nil, keys))
        case CartesianProduct(hd :: tl, keys) =>
          (hd.getAxiomaticSuperset(seed, env),
            CartesianProduct(tl, keys.transform((_,v) => v - 1)).getAxiomaticSuperset(seed, env)
          )
          match {
            case (Some(hdAS), Some(CartesianProduct(tlAS, _))) =>
              Some(CartesianProduct(hdAS +: tlAS, keys))
            case _ => None
          }
        case Relations(dom, codom, tP, sP, fP, iP) =>
          (dom.getAxiomaticSuperset(seed, env), codom.getAxiomaticSuperset(seed, env)) match {
            case (None, _) | (_, None) => None
            case (Some(domAS), Some(codomAS)) => Some(Relations(domAS, codomAS, tP, sP, fP, iP))
          }
      }
    }
    // scalastyle:on cyclomatic.complexity

  }

  /** ************************************************************************** */

  object EquippedProperty {
    implicit def equipProperty(st: Property): EquippedProperty = EquippedProperty(st)
    implicit def unequipProperty(est: EquippedProperty): Property = est.prop
  }

  case class EquippedProperty(prop: Property) {

    /**
     * Returns all the identifiers of the ontological sets referenced in this
     * (Equipped)Property.
     *
     * @return the identifiers of the OSets referenced.
     */
    def getOSets: Iterable[SetId] = {
      prop match {
        case p: Predicate => p.set.getOSets
        case p: NAryProperty => p.terms.flatMap(t => t.getOSets)
        case Subset(subset, superset, _) => subset.getOSets ++ superset.getOSets
        case In(_, set) => set.getOSets
      }
    }

    /**
     * Returns a new {@link Property} whose references to OSets have been replaced
     * by the one in the update map provided.
 *
     * @param update the update map
     * @return the updated SetTerm
     */
    def updateReferences(update: Map[SetId, SetId]) : Property =
      prop match {
        case Subset(subset, superset, isStrict) =>
          Subset(
            subset.updateReferences(update),
            superset.updateReferences(update),
            isStrict
          )
        case In(element, set) =>
          In(
            element.updateReferences(update),
            set.updateReferences(update)
          )
        case property: NAryProperty =>
          property match {
            case Distinct(terms) =>
              Distinct(terms.map(t => t.updateReferences(update)))
            case Equal(terms) =>
              Equal(terms.map(t => t.updateReferences(update)))
          }
        case predicate: Predicate =>
          predicate match {
            case Finite(set) => Finite(set.updateReferences(update))
            case Atomic(set) => Atomic(set.updateReferences(update))
            case Total(set) => Total(set.updateReferences(update))
            case Functional(set) => Functional(set.updateReferences(update))
            case Surjective(set) => Surjective(set.updateReferences(update))
            case Injective(set) => Injective(set.updateReferences(update))
          }
      }

    /**
     * Proxy for {@link EquippedProperty#updateReferences}.
     *
     * @param update see homonym
     * @return see homonym
     */
    def updateReferences(update: MMap[SetId, SetId]) : Property =
      updateReferences(update.toMap)

  }

//}
