/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids

import com.gitlab.mko575.sosl.ids.commons.FileId
import com.gitlab.mko575.sosl.ids.identifiers.{OSetLocalId, OntologyLocalId}
import com.gitlab.mko575.sosl.ids.{Namespace, RootNamespace}

object Identifiers // Here only for IntelliJ idea to be happy

package object identifiers {
  type OntologyLocalId = Object
  type OSetLocalId = Any
}

/**
 * Trait of ontological set identifiers.
 */
sealed trait SetId {
  val lid: OSetLocalId
  /**
   * Returns the identifier of the ontology this set belongs to if this set identifier
   * is a global one, otherwise it returns nothing.
   * @return the global identifier of the ontology this set belongs to for a global
   *         identifier, {@link None} otherwise.
   */
  def getOId: Option[OntologyId]
  /**
   * Returns a (human readable compact) string representation of this set identifier
   * relative to the ontology whose global identifier is provided as parameter.
   *
   * @param ctxOid the global identifier of the ontology into which the string
   *               is to be interpreted.
   * @return the string describing this identifier in the context of the ontology
   *         whose global identifier is provided
   */
  def toString(ctxOid: OntologyId): String
}

/**
 * Class representing local set identifiers. It is composed only of the "local"
 * set identifier inside the ontology.
 *
 * @param lid Unique identifier (in its ontology, i.e. local) of the set.
 */
sealed case class LocalSetId(lid: OSetLocalId) extends SetId {
  override def getOId: Option[OntologyId] = None
  override def toString: String = lid.toString
  override def toString(ctxOid: OntologyId): String = lid.toString
}

/**
 * Class representing global set identifiers. It is composed of the global identifier
 * of the ontology containing the set and the "local" set identifier inside the ontology.
 *
 * @param oid Global identifier of the ontology containing the set.
 * @param lid Unique identifier (in its ontology, i.e. local) of the set.
 */
sealed case class GlobalSetId(oid: OntologyId, lid: OSetLocalId) extends SetId {
  override def getOId: Option[OntologyId] = Some(oid)
  override def toString: String = oid.toString + "::" + lid.toString
  override def toString(ctxOid: OntologyId): String =
    (if (oid != ctxOid) s"${oid.toString(ctxOid)}::" else "") + lid.toString
}

/**
 * Class representing global ontology identifiers, composed of a namespace
 * and the "local" ontology identifier.
 *
 * @param srcFile      the file in which the ontology is defined.
 * @param srcNamespace the original namespace of the ontology.
 * @param ns           Namespace into which this ontology is defined.
 * @param lid          Unique identifier (in its namespace, i.e. local) of the ontology seed.
 */
sealed case class OntologyId(
    srcFile: Option[FileId], srcNamespace: Option[Namespace],
    ns: Namespace, lid: OntologyLocalId
  ) {

  // Proxy constructors
  // TODO: Find out why they are not accessible,
  //  and the apply functions in an associated singleton object is required ?
  def this() = this(None, None, RootNamespace, "Null")
  def this(ns: Namespace, lid: OntologyLocalId) = this(None, None, ns, lid)
  def this(srcFile: FileId, srcNS: Namespace, ns: Namespace, lid: OntologyLocalId) =
    this(Some(srcFile), Some(srcNS), ns, lid)

  override def hashCode(): Int =
    (ns.hashCode() * lid.hashCode()) % Int.MaxValue

  override def equals(obj: Any): Boolean = {
    obj match {
      case OntologyId(_, _, ns, lid) => this.ns.equals(ns) && this.lid.equals(lid)
      case _ => false
    }
  }

  override def toString: String = s"${ns.toString}:$lid"

  /**
   * Returns a (human readable compact) string representation of this global
   * ontology identifier relative to the ontology whose global identifier is
   * provided as parameter.
   *
   * @param ctxOid the global identifier of the ontology into which the string
   *               is to be interpreted.
   */
  def toString(ctxOid: OntologyId): String =
    (if (ns != ctxOid.ns) s"${ns.toString}:" else "") + lid.toString

  def toDetailedString: String = s"${toString} ($srcFile::$srcNamespace)"
}

// TODO: Why do I need this object and can't use directly the constructors defined in the class ?
object OntologyId {
  def apply(ns: Namespace, lid: OntologyLocalId): OntologyId =
    OntologyId(None, None, ns, lid)

  def apply(srcFile: FileId, srcNS: Namespace, ns: Namespace, lid: OntologyLocalId): OntologyId =
    OntologyId(Some(srcFile), Some(srcNS), ns, lid)
}