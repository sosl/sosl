/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids

// object Namespace // Here only for IntelliJ idea to be happy

sealed trait Namespace {
  def parent : Namespace
  def andThen(ns:Namespace) : Namespace = {
    ns match {
      case RootNamespace => RootNamespace
      case CurrentNamespace => this
      case ParentNamespace => this.parent
      case AncestorNamespace(0) => this
      case AncestorNamespace(n) if n > 0 => this.parent.andThen(AncestorNamespace(n-1))
      case ExtendedNamespace(p, a) => ExtendedNamespace(this.andThen(p), a)
    }
  }
}

sealed trait AbsoluteNamespace extends Namespace
sealed trait RelativeNamespace extends Namespace

sealed trait NamespaceRoot extends Namespace
case object RootNamespace extends NamespaceRoot with AbsoluteNamespace {
  override def parent: Namespace = RootNamespace
  override def toString: String = ""
}
case object CurrentNamespace extends NamespaceRoot with RelativeNamespace {
  override def parent: Namespace = ParentNamespace
  override def toString: String = "."
}
case object ParentNamespace extends NamespaceRoot with RelativeNamespace {
  override def parent: Namespace = AncestorNamespace(2)
  override def toString: String = ".."
}
case class AncestorNamespace(degree:Int) extends NamespaceRoot with RelativeNamespace {
  override def parent: Namespace = AncestorNamespace(degree + 1)
  override def toString: String = (1 to degree).map(_ => ParentNamespace.toString).mkString("/")
}

case class ExtendedNamespace(parent:Namespace, id:String) extends Namespace {
  def apply(parent:Namespace, id:String) : Namespace = ExtendedNamespace(parent,id) //.asInstanceOf[T]
  def apply(prefix:Namespace, suffix:List[String]) : Namespace = {
    suffix match {
      case Nil => prefix
      case s :: Nil => this(prefix, s) ;
      case hd :: tl => this(this(prefix,hd), tl)
    }
  }
  override def toString: String = parent.toString + "/" + id
}
