/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.common

case class Arity(lowerBound:Int, upperBound:Option[Int]) {

  override def toString: String =
    s"$lowerBound..${upperBound.getOrElse("∞")}"

  def toUMLString : String =
    (lowerBound, upperBound) match {
      case (0, None) => "*"
      case (lb, None) if 0 < lb => lb.toString + "..*"
      case (1, Some(1)) => "1"
      case (lb, Some(ub)) if lb == ub => lb.toString
      case (lb, Some(ub)) if lb < ub => lb.toString + ".." + ub.toString
      case _ => "???"
    }

  def lub(that: Arity): Arity =
    Arity(
      this.lowerBound min that.lowerBound,
      (this.upperBound, that.upperBound) match {
        case (Some(i), Some(j)) => Some(i max j)
        case _ => None
      }
    )

  def glb(that: Arity): Arity =
    Arity(
      this.lowerBound max that.lowerBound,
      (this.upperBound, that.upperBound) match {
        case (Some(i), Some(j)) => Some(i min j)
        case (Some(i), None) => Some(i)
        case (None, Some(i)) => Some(i)
        case _ => None
      }
    )
}

object Arity {
  val any : Arity = Arity(0, None)
  val optional : Arity = Arity(0, Some(1))
  val lone : Arity = optional
  val one : Arity = Arity(1, Some(1))
  val some : Arity = Arity(1, None)

  /**
   * Returns the arity associated to a mapping having the provided characteristics.
   * @param isFunctional every element is associated to at most one "(pre)image".
   * @param isTotal every element is associated to at least one "(pre)image".
   * @return
   */
  def get(isFunctional: Boolean, isTotal: Boolean): Arity =
    (isFunctional, isTotal) match {
      case (true, true) => Arity.one
      case (true, false) => Arity.lone
      case (false, true) => Arity.some
      case (false, false) => Arity.any
    }

  def toSymbol(domArity: Option[Arity], codArity: Option[Arity]): String =
    toSymbol(domArity.getOrElse(any), codArity.getOrElse(any))

  def toSymbol(domArity: Arity, codArity: Arity): String =
    (domArity, codArity) match {
      case (Arity.lone, Arity.lone) => "⤔"
      case (Arity.lone, Arity.one) => "⤗"
      case (Arity.lone, Arity.some) => "⤀"
      case (Arity.lone, _) => "⇸"
      case (Arity.one, Arity.lone) => "↣"
      case (Arity.one, Arity.one) => "⤖"
      case (Arity.one, Arity.some) => "↠"
      case (Arity.one, _) => "→"
      case _ => "×"
    }

  def toAttributeLabel(domArOpt: Option[Arity], codArOpt: Option[Arity]): String = {
    val domAr: Arity = domArOpt.getOrElse(Arity.any)
    val codAr: Arity = codArOpt.getOrElse(Arity.any)
    s"""${codAr.toUMLString} ${toSymbol(domArOpt, codArOpt)} ${domAr.toUMLString}"""
  }
}
