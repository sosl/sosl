/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.common

import com.gitlab.mko575.sosl.ids.{Property, SetTerm, SetTermLeaf}

import java.util.concurrent.Flow
import scala.collection.mutable
import scala.util.Failure

abstract class TypeError(msg: String, cause: Option[Seq[Throwable]] = None)
  extends Exception(msg, cause.flatMap(_.lastOption).orNull)

case class ExpansionError[T](t: Any, cause: Option[Seq[Throwable]] = None)
  extends TypeError(s"Error trying to expand $t", cause)

case class ExpansionLoopError[T <: TypeExpr.LeavesTypeLUB](t: Any, v: TypeVariable, env: TypingEnvironment[T])
  extends TypeError(s"Error trying to expand $t: there is a loop on $v in $env", None)

case class UnificationError[T](t1: Any, t2: Any, cause: Option[Seq[Throwable]] = None)
  extends TypeError(s"Error trying to unify $t1 and $t2", cause)

case class TypeCheckError[T <: TypeExpr.LeavesTypeLUB](env: TypingEnvironment[T], term: SetTerm, aimedType: TypeExpr[T], cause: Option[Seq[Throwable]] = None)
  extends TypeError(
    s"Error trying to type check $term with type $aimedType under $env", cause)

case class TypeInferenceError[T <: TypeExpr.LeavesTypeLUB](env: TypingEnvironment[T], thing: Any, cause: Option[Seq[Throwable]] = None)
  extends TypeError(s"Error trying to infer type of $thing under $env", cause)

// TODO: move somewhere else
case class ImplementationError(msg: String, cause: Option[Seq[Throwable]] = None)
  extends Exception(msg, cause.flatMap(_.lastOption).orNull)

// TODO: See
//  - SubmissionPublisher,
//  - scala.collection.mutable.Publisher,
//  - https://alvinalexander.com/java/jwarehouse/scala-2.11/library/scala/collection/mutable/Publisher.scala.shtml,
//  - https://github.com/scala/scala/blob/v2.12.2/src/library/scala/collection/mutable/Publisher.scala#L1
object TypeError {

  private val subscribers: mutable.Set[Subscriber] =
    new mutable.HashSet[Subscriber]()
  subscribe(DefaultTypeErrorSubscriber)
  def subscribe(subscriber: Subscriber): Unit = subscribers.add(subscriber)
  def notifyTypingError(err: TypeError): Unit = subscribers.foreach(_.onNext(err))
  def notifyError(err: Exception): Unit = subscribers.foreach(_.onNext(err))

  def notifyAndReturnError[T](err: TypeError): Failure[T] = {
    notifyTypingError(err)
    Failure(err)
  }
}

trait Subscriber {
  def onNext(t: Any): Unit
}

object DefaultTypeErrorSubscriber extends Subscriber {
  def onSubscribe(subscription: Flow.Subscription): Unit = {}
  def onNext(t: Any): Unit = System.out.println(t.toString)
  def onError(throwable: Throwable): Unit = {}
  def onComplete(): Unit = {}
}
