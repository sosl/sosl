/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.common

// Java classes are compiled after Scala classes ...
// import com.gitlab.mko575.sosl.ErrorMessages

import com.gitlab.mko575.sosl.ids.{AxiomaticSet, AxiomaticSets, Entities, SetTerm, SetTermLeaf, Values, World}

import java.util.logging.Level

/**
 * Trait of "type expressions".
 * @tparam T upperbound of those expressions leaves types
 */
sealed trait TypeExpr[+T <: TypeExpr.LeavesTypeLUB] {
  // def getLeaves[U <: T]: Set[U]
  def getLeaves[U >: T <: SetTermLeaf]: Set[U]
  def getVars: Set[TypeVariable]
}

object TypeExpr {
  type LeavesTypeLUB = SetTermLeaf
}

/*****************************************************************************/

case class TypeVariable(id: Int) extends TypeExpr[Nothing] {
  override def toString: String = TypeVariable.getStringFor(this)
  override def getLeaves[U <: SetTerm]: Set[U] = Set.empty
  override def getVars: Set[TypeVariable] = Set(this)
}
object TypeVariable {
  private var lastTypeVariableId: Int = -1
  private var toStringMapping: Map[Int, Int] = Map()
  def getFresh: TypeVariable = {
    lastTypeVariableId += 1
    TypeVariable(lastTypeVariableId)
  }
  def getStringFor(tv: TypeVariable): String = {
    val strId = toStringMapping.getOrElse(tv.id, toStringMapping.size)
    toStringMapping = toStringMapping + (tv.id -> strId)
    ('α'.toInt + strId).toChar.toString
  }
  def reset: Unit = {
    lastTypeVariableId = -1
    toStringMapping = Map()
  }
}

case class LUBType[+T <: SetTermLeaf, U <: T](subtypes: Set[TypeExpr[U]]) extends TypeExpr[T] {
  override def toString: String = subtypes.mkString("(", "⊔", ")")
  override def getLeaves[V >: T <: SetTermLeaf]: Set[V] = subtypes.flatMap(_.getLeaves)
  override def getVars: Set[TypeVariable] = subtypes.flatMap(_.getVars)
}
object LUBType {
  /*
  def apply(lhs:TypeExpr[AxiomaticSet], rhs: TypeExpr[AxiomaticSet]): TypeExpr[AxiomaticSet] =
    (lhs, rhs) match {
      case (LeafType(lhsLeaf: AxiomaticSet), LeafType(rhsLeaf: AxiomaticSet)) =>
        LeafType(AxiomaticSets.lub(lhsLeaf, rhsLeaf))
      case _ => apply[AxiomaticSet](lhs, rhs)
    }
  */

  /**
   * Compute le LUB (least upper bound) type of its 2 parameters.
   * @param lhs first type.
   * @param rhs second type.
   * @tparam T type of TypeExpr leaves
   * @return the LUB TypeExpr of both parameters.
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  def apply[T >: AxiomaticSet <: SetTermLeaf](lhs:TypeExpr[T], rhs: TypeExpr[T])
  : TypeExpr[T] =
    (lhs, rhs) match {
      case (LeafType(a1: AxiomaticSet), LeafType(a2: AxiomaticSet)) =>
        LeafType(AxiomaticSets.lub(a1,a2))
      case (_, LeafType(_: AxiomaticSet)) =>
        LUBType.apply[T](rhs, lhs)
      case (LeafType(World), _)
         | (LeafType(Entities), _)
         | (LeafType(Values), _) =>
        lhs
      case (TupleOf(l1, ct1, ck1), TupleOf(l2, ct2, ck2))
        if (l1.toSet ++ l2.toSet).size < 2 => {
        val m: Map[String, Seq[TuplePos]] =
          (ck1.toSeq ++ ck2.toSeq)
            .groupMapReduce(_._1)(p => Seq(p._2)){
              case (Seq(CanonicTuplePos(i)), Seq(CanonicTuplePos(j))) if i != j =>
                TypeError.notifyError(
                  ImplementationError(
                    "Computing LUB of tuple types having different mappings for a key!"
                    , None
                  )
                )
                Seq()
              case (tps1, tps2) => (tps1.toSet ++ tps2.toSet).toSeq
            }
        val (keys: Map[String, TuplePos], tpSubs: Map[TuplePos, TuplePos]) =
          m.foldLeft(
            (Map(): Map[String, TuplePos], Map(): Map[TuplePos, TuplePos])
          ){
            case ((accK, accM), (k, tps)) if tps.size == 1 =>
              (accK + (k -> tps.head), accM)
            case ((accK, accM), (k, tps)) if tps.size == 2 =>
              tps match {
                case Seq(CanonicTuplePos(_), UnknownTuplePos(_))
                     | Seq(UnknownTuplePos(_), CanonicTuplePos(_)) =>
                  val ctp = tps.collectFirst{ case c:CanonicTuplePos => c }.get
                  val utp = tps.collectFirst{ case c:UnknownTuplePos => c }.get
                  (accK + (k -> ctp), accM + (utp -> ctp))
                case Seq(UnknownTuplePos(a), UnknownTuplePos(b)) =>
                  val utpPrimary = UnknownTuplePos(a min b)
                  val utpSecondary = UnknownTuplePos(a max b)
                  (accK + (k -> utpPrimary), accM + (utpSecondary -> utpPrimary))
              }
            case (acc, _) => acc
          }
        TupleOf(
          (l1.toSet ++ l2.toSet).headOption,
          (ct1.toSeq ++ ct2.toSeq)
            .groupMapReduce{
              case (tp, _) => tpSubs.getOrElse(tp, tp)
            }(_._2){
              case (t1, t2) => LUBType[T](t1, t2)
            },
          keys
        )
      }
      case (SetOf(t1, tcs1), SetOf(t2, tcs2)) =>
        SetOf(LUBType[T](t1, t2), tcs1 lub tcs2)
        /*
      case (TupleSet(ct1, ck1, ca1), TupleSet(ct2, ck2, ca2)) =>
        TupleSet(
          (ct1 zip ct2).map{ case (t1, t2) => LUBType[T](t1, t2) },
          ck1 ++ ck2,
          (ca1 zip ca2).map{ case (a1, a2) => a1 lub a2 }
        )
         */
      case _ => LUBType(Set(lhs, rhs))
    }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length
}

/*
case class TupleSetRestrictionType[T](tupleSetType: TypeExpr[T], dimension: Any, on: TypeExpr[T]) extends TypeExpr[T] {
  private val symbol: String = "▽"
  override def toString: String = s"(${tupleSetType}$symbol($dimension, $on)"
  override def getLeaves: Set[Any] = tupleSetType.getLeaves union on.getLeaves
}

case class TupleSetJunctionType[T](
                                    lhsTupleSetType: TypeExpr[T], rhsTupleSetType: TypeExpr[T], dimensions: Option[scala.collection.Set[(String, String)]], collapsing: Boolean
                                  ) extends TypeExpr[T] {
  private val symbol: String = if ( collapsing ) { "⧓" } else { "⋈" }
  override def toString: String = {
    val dimStr =
      dimensions.map(
        set =>
          set.map { case (s1, s2) => s"$s1, $s2" }
            .map(s => s.addString(new StringBuilder, "(", "; ", ")"))
      ).getOrElse("")
    s"(${lhsTupleSetType} $symbol$dimStr ${rhsTupleSetType})"
  }
  override def getLeaves: Set[Any] =
    Set(lhsTupleSetType, rhsTupleSetType).flatMap(_.getLeaves)
}
 */

/*****************************************************************************/

// sealed trait Type[+T, +U <: TypeExpr[T]] extends TypeExpr[T]

case class LeafType[+T <: SetTermLeaf](leaf: T) extends TypeExpr[T] {
  override def toString: String = leaf.toString
  override def getLeaves[U >: T <: SetTermLeaf]: Set[U] = Set(leaf)
  // override def getLeaves[V <: T]: Set[V] = Set(leaf)
  override def getVars: Set[TypeVariable] = Set.empty
}

/*****/

sealed trait TuplePos
case class CanonicTuplePos(i:Int) extends TuplePos
case class UnknownTuplePos(i:Int) extends TuplePos
object TuplePos {
  private var lastUTP: Int = -1
  private var toStringMapping: Map[UnknownTuplePos, String] = Map()
  def getFreshUnknown: UnknownTuplePos = {
    lastUTP += 1
    UnknownTuplePos(lastUTP)
  }
  def getStringFor(k: TuplePos): String = {
    k match {
      case CanonicTuplePos(i) => i.toString
      case utk : UnknownTuplePos =>
        val strId: String =
          toStringMapping.getOrElse(
            utk,
            ('a'.toInt + toStringMapping.size).toChar.toString
          )
        toStringMapping = toStringMapping + (utk -> strId)
        strId
    }
  }
  def getKnownMax(keys: Iterable[TuplePos]): Option[Int] =
    keys.collect{case CanonicTuplePos(i) => i}.maxOption
  def reset: Unit = {
    lastUTP = -1
    toStringMapping = Map()
  }
}

case class TupleOf[+T <: SetTermLeaf]
(
  length: Option[Int],
  contentTypes: Map[TuplePos, TypeExpr[T]],
  contentKeys: Map[String, TuplePos]
) extends TypeExpr[T]
{
  override def toString: String = {
    (
      (1 to (length.getOrElse(0) max TuplePos.getKnownMax(contentTypes.keys).getOrElse(0)))
        .map(
          i => {
            val ctk = CanonicTuplePos(i)
            val kStr =
              contentKeys
                .collectFirst{
                  case (str, `ctk`) => str
                }
                .getOrElse(TuplePos.getStringFor(ctk))
            s"$kStr: ${contentTypes.getOrElse(ctk, "???")}"
          }
        ) :++ (if (length.isEmpty) Seq("...") else Seq())
    ).mkString("<", ",", ">")
    // .concat(s"(${length.getOrElse("?")})")
    .concat(
      (
        Seq(s"lgth=${length.getOrElse("?")}")
        ++
        contentTypes
          .collect {
            case (utk: UnknownTuplePos, value) => {
              val kStr =
                contentKeys
                  .collectFirst {
                    case (str, `utk`) => str
                  }
                  .getOrElse(TuplePos.getStringFor(utk))
              s"$kStr: $value"
            }
          }
        )
        .mkString("[", ",", "]")
    : String)
  }

  override def getLeaves[U >: T <: SetTermLeaf]: Set[U] = {
    contentTypes.values.flatMap(_.getLeaves).toSet
  }
  override def getVars: Set[TypeVariable] = {
    contentTypes.values.flatMap(_.getVars).toSet
  }
}
object TupleOf {
  def apply[T <: SetTermLeaf]
  (contentTypes: Seq[TypeExpr[T]], contentKeys: Map[String, Int] = Map()
  ): TupleOf[T]
  = TupleOf(
    Some(contentTypes.size),
    contentTypes.zipWithIndex.map{ case (t,i) => (CanonicTuplePos(i + 1), t) }.toMap,
    contentKeys.map{ case (s,i) => (s, CanonicTuplePos(i)) }
  )
}

/*****/

case class SetOf[+T <: SetTermLeaf]
(
  contentType: TypeExpr[T], constraints: SetTypeConstraints
) extends TypeExpr[T] {
  override def toString: String =
    s"{${contentType.toString}${
      if (! constraints.isEmpty) { constraints.mkString(" | ", " ∧ ", "") }
      else { "" }
    }}"
  override def getLeaves[U >: T <: SetTermLeaf]: Set[U] = contentType.getLeaves
  override def getVars: Set[TypeVariable] = contentType.getVars
}
object SetOf {
  def apply[T <: SetTermLeaf](contentType: TypeExpr[T]): SetOf[T] =
    SetOf(contentType, SetTypeConstraints())
  def apply[T <: SetTermLeaf]
  (contentType: TypeExpr[T], optCST: Option[SetTypeConstraints])
  : SetOf[T] =
    SetOf(contentType, optCST.getOrElse(SetTypeConstraints()))
}

case class SetTypeConstraints(internals: Set[SetTypeConstraint]) extends Set[SetTypeConstraint] {
  // private val internals: Set[SetTypeConstraint] = new HashSet[SetTypeConstraint]()

  override def incl(elem: SetTypeConstraint): SetTypeConstraints =
    internals.incl(elem).asInstanceOf[SetTypeConstraints]
  override def excl(elem: SetTypeConstraint): SetTypeConstraints =
    internals.excl(elem).asInstanceOf[SetTypeConstraints]
  override def contains(elem: SetTypeConstraint): Boolean =
    internals.contains(elem)
  override def iterator: Iterator[SetTypeConstraint] =
    internals.iterator

  def this(elems: SetTypeConstraint*) = {
    this(Set(): Set[SetTypeConstraint])
    elems.foldLeft(this){
      case (acc, elem) => acc.incl(elem)
    }
  }

  def apply(elems: SetTypeConstraint*): SetTypeConstraints =
    elems.foldLeft(new SetTypeConstraints()){
      case (acc, elem) => acc.incl(elem)
    }

  def inject(stc: SetTypeConstraint): SetTypeConstraints =
    this.internals.toSeq match {
      case Seq() => SetTypeConstraints(stc)
      case hdSTC :: tlSTCs =>
        (stc glb hdSTC) match {
          case Some(lubSTC) => SetTypeConstraints((lubSTC :: tlSTCs).toSet)
          case None => SetTypeConstraints(tlSTCs.toSet).inject(stc).incl(hdSTC)
        }
    }

  def glb(that: SetTypeConstraints): SetTypeConstraints =
    that.foldLeft(this){
      (acc, thatSTC) => acc.inject(thatSTC)
    }

  def lub(that: SetTypeConstraints): SetTypeConstraints =
    this.foldLeft(new SetTypeConstraints){
      (acc, thisSTC) =>
        that
          .find(_.isInstanceOf[thisSTC.type])
          .flatMap(thisSTC.lub(_))
          .map(acc.incl(_))
          .getOrElse(acc)
    }
}
object SetTypeConstraints {
  def apply(elems: SetTypeConstraint*): SetTypeConstraints =
    SetTypeConstraints(elems.toSet)
}

sealed trait SetTypeConstraint {
  // def lub(that: SetTypeConstraint): Option[SetTypeConstraint]

  /**
   * Computes the least upper bound (LUB) of 2 SetTypeConstraints (this and that)
   * if it exists, otherwise returns None.
   * @param that the other SetTypeConstraints
   * @return the lub of the 2 SetTypeConstraints if it exists, otherwise returns None
   */
  def lub(that: SetTypeConstraint): Option[SetTypeConstraint] =
    (this, that) match {
      case (SetOfTuplesWithArity(lArities), SetOfTuplesWithArity(rArities)) =>
        Some(
          SetOfTuplesWithArity(
            (lArities zip rArities).map{ case (a1, a2) => a1 lub a2 }
          )
        )
      case _ =>
        None
    }

  /**
   * Computes the greatest lower bound (GLB) of 2 SetTypeConstraints (this and that)
   * if it exists, otherwise returns None.
   * @param that the other SetTypeConstraints
   * @return the glb of the 2 SetTypeConstraints if it exists, otherwise returns None
   */
  def glb(that: SetTypeConstraint): Option[SetTypeConstraint] =
    (this, that) match {
      case (SetOfTuplesWithArity(lArities), SetOfTuplesWithArity(rArities)) =>
        Some(
          SetOfTuplesWithArity(
            (lArities zip rArities).map{ case (a1, a2) => a1 glb a2 }
          )
        )
      case _ =>
        None
    }
}
case class SetOfTuplesWithArity(contentArity: Seq[Arity]) extends SetTypeConstraint {
  override def toString: String = contentArity.mkString("<", ",", ">")
  /*
  override def lub(that: SetTypeConstraint): Option[SetOfTuplesWithArity] = {
    that match {
      case SetOfTuplesWithArity(thatContentArity) =>
        Some(SetOfTuplesWithArity(
          (this.contentArity zip thatContentArity)
            .map{ case (a1, a2) => a1 lub a2 }
        ))
      case _ => None
    }
  }
   */
}

/*
case class TupleSet[T](
                        contentTypes: Seq[TypeExpr[T]],
                        contentKeys: Map[String, Int],
                        contentArity: Seq[Arity]
                     )
  extends SetOf[T](TupleOf[T](contentTypes, contentKeys), Some(SetTypeConstraints(SetOfTuplesWithArity(contentArity))))
{
  override def toString: String =
    s"{<${
      (contentTypes lazyZip (1 to contentTypes.length) lazyZip contentArity).map(
        (t, i, a) =>
          s"${
            contentKeys
              .find{ case (_, p) => p == i }
              .map{ case (str, _) => str + ':' }
              .getOrElse("")
          }${
            a.toString + ":"
          }${
            t.toString
          }"
      ).addString(new StringBuilder, ", ")}>}"
}
*/

object TupleSet {
  def apply[T <: SetTermLeaf](contentTypes: Seq[TypeExpr[T]],
            contentKeys: Map[String, Int],
            contentArity: Seq[Arity]
           ): SetOf[T] =
    SetOf(
      TupleOf(contentTypes, contentKeys),
      Some(SetTypeConstraints( SetOfTuplesWithArity(contentArity) ))
    )
}