/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.common

import com.gitlab.mko575.sosl.ids.EquippedProperty.equipProperty
import com.gitlab.mko575.sosl.ids.{AssociativeOpSetTerm, Atomic, AxiomaticSet, AxiomaticSets, BinaryProperty, CartesianProduct, Complement, Distinct, Emptyset, Equal, EquippedProperty, Finite, Functional, GlobalSetId, In, Injective, Intersection, Junction, NAryInfixOpSetTerm, NAryProperty, Powerset, Predicate, Property, Reference, Relations, Restriction, SetCons, SetTerm, SetTermLeaf, SetTermNode, Subset, Surjective, Total, TupleCons, Union, UnionProduct, World}

import scala.reflect.runtime.universe._
import com.gitlab.mko575.sosl.ids.sos.OntologySeed

import java.util
import scala.collection.immutable.HashMap
import scala.reflect.ClassTag
import scala.util.{Failure, Success, Try}

class TypeInferrer[T <: AxiomaticSet](
  private val ontology: OntologySeed
                                     ) {

  def inferAxiomaticTypes(): TypingEnvironment[AxiomaticSet] =
    infer(ontology.properties).expandAll()

  /**
   * Infers as much typing information as possible from the properties provided.
   * If inference on a given property fails, the function skips it and continues
   * with the rest of properties.
   * @param props the properties analyzed to infer typing information.
   * @return the typing information inferred.
   */
  def infer(props: Iterable[Property]): TypingEnvironment[AxiomaticSet] = {
    val finalTypingEnv =
      props.toSeq
        // TODO: uncomment the following line
        .sortBy[Int]( _.getOSets.size ) // Hides some bugs
        .foldLeft(
          new TypingEnvironment[AxiomaticSet]()
        )(
          (env, p) =>
            infer(env, p)
              .flatMap(unifyEnv(env, _))
              .map(_.expandAll())
              .getOrElse(env)
        )
    // println(s"===== INFERED TYPING ENV for $props:\n ${finalTypingEnv.collapse()}")
    finalTypingEnv
  }

  private def buildTypeErrorFromTrySeq
  (trySeq: Seq[Try[Any]],
   errorBuilder: Seq[Throwable] => TypeError
  ): TypeError =
    if ( trySeq.forall(_.isSuccess) ) {
      throw new Error("Implementation error. This function should never be called without a 'Failure'")
    } else {
      errorBuilder(
        trySeq.collect { case Failure(e) => e }
      )
    }

  private def buildTypeErrorFromTryPair
  ( pair: (Try[Any], Try[Any]),
    errorBuilder: Seq[Throwable] => TypeError
  ): TypeError =
    buildTypeErrorFromTrySeq(Seq(pair._1, pair._2), errorBuilder)

  /**
   * Assuming the provided typing environment, infers as much additional typing
   * information as possible from the property provided. If inference fails, it
   * returns a {@see TypeError}.
   * @param env the assumed typing environment.
   * @param p the property analyzed to infer typing information.
   * @return the additional typing information inferred, or failure in case of
   *         typing error.
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  def infer(env: TypingEnvironment[AxiomaticSet], p: Property)
  : Try[TypingEnvironment[AxiomaticSet]] = {
    // System.out.println(s"===== Starting type inference for $p")
    p match {
      case p: BinaryProperty =>
        p match {
          case Subset(subset, supset, _) =>
            val setsType: TypeExpr[T] = SetOf(TypeVariable.getFresh, None)
            // println( s"===== Infering: ${env.collapse()}: $subset subset $supset" )
            (typeCheck(env, subset, setsType), typeCheck(env, supset, setsType)) match {
              case (Success(subTEnv), Success(supTEnv)) =>
                val ne = unifyEnv(subTEnv, supTEnv)
                // println(s"subset: $subTEnv\nsuperset: $supTEnv\nres: $ne")
                ne
              case ep: Any =>
                TypeError.notifyAndReturnError(
                  buildTypeErrorFromTryPair(ep, s => TypeInferenceError(env, p, Some(s)))
                )
            }
          case In(elem, set) =>
            val freshVT: TypeExpr[T] = TypeVariable.getFresh
            // println( s"===== Infering: ${env}: ${elem} in $set" )
            (typeCheck(env, elem, freshVT), typeCheck(env, set, SetOf(freshVT, None))) match {
              case (Success(elemTEnv), Success(setTEnv)) =>
                // println(s"cst elem: $elemTEnv\ncst set: $setTEnv")
                unifyEnv(elemTEnv, setTEnv)
              case ep: Any =>
                TypeError.notifyAndReturnError(
                  buildTypeErrorFromTryPair(ep, s => TypeInferenceError(env, p, Some(s)))
                )
            }
        }
      case p: NAryProperty =>
        p match {
          case Equal(_) | Distinct(_) =>
            val freshVT: TypeExpr[T] = TypeVariable.getFresh
            typeCheckSeq(env, p.terms.toSeq, freshVT)
        }
      case p: Predicate =>
        p match {
          case Finite(_) | Atomic(_) =>
            val setType: TypeExpr[T] = SetOf(TypeVariable.getFresh, None)
            typeCheck(env, p.set, setType)
          case Total(set) =>
            val relType: TypeExpr[T] =
              TupleSet(
                List(TypeVariable.getFresh, TypeVariable.getFresh),
                Map(), List(Arity.one, Arity.any)
              )
            typeCheck(env, set, relType)
          case Functional(set) =>
            val relType: TypeExpr[T] =
              TupleSet(
                List(TypeVariable.getFresh, TypeVariable.getFresh),
                Map(), List(Arity.lone, Arity.any)
              )
            typeCheck(env, set, relType)
          case Surjective(set) =>
            val relType: TypeExpr[T] =
              TupleSet(
                List(TypeVariable.getFresh, TypeVariable.getFresh),
                Map(), List(Arity.lone, Arity.some)
              )
            typeCheck(env, set, relType)
          case Injective(set) =>
            val relType: TypeExpr[T] =
              TupleSet(
                List(TypeVariable.getFresh, TypeVariable.getFresh),
                Map(), List(Arity.lone, Arity.lone)
              )
            typeCheck(env, set, relType)
        }
    }
  }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  /**
   * Assuming the provided typing environment, infers the type of the provided
   * term. If inference fails, it returns a {@see TypeError}.
   * @param env the assumed typing environment.
   * @param term the set term whose type is to infer.
   * @return the inferred type, or failure in case of typing error.
   */
  def infer(env: TypingEnvironment[AxiomaticSet]): SetTerm => Try[TypeExpr[AxiomaticSet]] =
    term => infer(env, term).map(_._1)

  /**
   * Assuming the provided typing environment, infers the type of the provided
   * term as well as additional typing information. If inference fails, it
   * returns a {@see TypeError}.
   * @param env the assumed typing environment.
   * @param term the set term whose type is to infer.
   * @return the inferred type and additional typing information, or failure in
   *         case of typing error.
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  def infer(env: TypingEnvironment[AxiomaticSet], term: SetTerm)
           // (implicit tag: ClassTag[T])
           // (implicit tag: TypeTag[T])
  : Try[(TypeExpr[AxiomaticSet], TypingEnvironment[AxiomaticSet])] = {
    term match {
      // case _: T => Success(LeafType(term), new TypingEnvironment[T]())
      case leaf: SetTermLeaf =>
        leaf match {
          case Reference(GlobalSetId(oid, lid)) if oid != ontology.id =>
            ontology.referencedOntologies.get(oid)
              .flatMap(_.get(lid))
              .flatMap(_.getAxiomaticType)
              .map( t => Success((t, new TypingEnvironment[AxiomaticSet]())) )
              .getOrElse {
                /*
                println(s""">>>>> Failing type inference for $leaf: ${
                  ontology.referencedOntologies.get(oid)
                    .flatMap(_.get(lid))
                    .flatMap(_.getAxiomaticType)
                }""")
                 */
                Failure(TypeInferenceError(env, term, None))
              }
          case Reference(_) =>
            env.getTypeAndUpdatedEnv(TypedSetTerm(leaf)) match {
              case Some((t, _)) =>
                Success(t, new TypingEnvironment[AxiomaticSet]())
              case None =>
                val nt: TypeExpr[AxiomaticSet] = TypeVariable.getFresh
                Success(
                  nt,
                  new TypingEnvironment[AxiomaticSet](TypedSetTerm(leaf): Typable, nt: TypeExpr[AxiomaticSet])
                )
                // Success(nt, new TypingEnvironment[AxiomaticSet]((TypedSetTerm(leaf) -> nt): Map[Typable, TypeExpr[AxiomaticSet]]))
            }
            // Success(env.getTypeOrTVar(TypedSetTerm(leaf)))
          case set: AxiomaticSet =>
            // tag.tpe match {
            //   case _ : AxiomaticSet =>
                Success(LeafType(set), new TypingEnvironment[AxiomaticSet]())
            // }
        }
      case node: SetTermNode =>
        node match {
          case SetCons(subterms) =>
            val freshTV: TypeExpr[AxiomaticSet] = TypeVariable.getFresh
            typeCheckSeq(env, subterms.toSeq, freshTV) match {
              case Failure(e) =>
                Failure(TypeInferenceError(env, term, Some(Seq(e))))
              case Success(topLevelITC) =>
                Success(SetOf(freshTV, None), topLevelITC)
            }
          case TupleCons(subterms, keys) =>
            inferSeq(env, subterms) match {
              case Failure(e) =>
                Failure(TypeInferenceError(env, term, Some(Seq(e))))
              case Success((itSeq, topLevelITC)) =>
                Success(TupleOf(itSeq, keys), topLevelITC)
            }
          case Complement(set, relativeTo) =>
            val freshTV: TypeExpr[AxiomaticSet] = TypeVariable.getFresh
            typeCheckSeq(env, Seq(set) ++ relativeTo.toSeq, SetOf(freshTV, None)) match {
              case Failure(e) =>
                Failure(TypeInferenceError(env, term, Some(Seq(e))))
              case Success(topLevelITC) =>
                Success(SetOf(freshTV, None), topLevelITC)
            }
          case Powerset(set) =>
            val freshTV: TypeExpr[AxiomaticSet] = TypeVariable.getFresh
            typeCheck(env, set, SetOf(freshTV, None)) match {
              case Failure(exception) =>
                Failure(TypeInferenceError(env, term, Some(Seq(exception))))
              case Success(topLevelITC) =>
                Success(SetOf(SetOf(freshTV, None), None), topLevelITC)
            }
          case term: NAryInfixOpSetTerm =>
            term match {
              case term: AssociativeOpSetTerm =>
                term match {
                  case Union(_) | Intersection(_) =>
                    val freshTV: TypeExpr[AxiomaticSet] = TypeVariable.getFresh
                    typeCheckSeq(env, term.subterms.toSeq, SetOf(freshTV, None)) match {
                      case Failure(exception) =>
                        Failure(TypeInferenceError(env, term, Some(Seq(exception))))
                      case Success(topLevelITC) =>
                        Success(SetOf(freshTV, None), topLevelITC)
                    }
                  case UnionProduct(subterms) =>
                    val freshTV: TypeExpr[AxiomaticSet] = TypeVariable.getFresh
                    typeCheckSeq(env, subterms.toSeq, SetOf(freshTV, None))
                      // TODO: revise
                      // SetOf(SetOf(freshTV)) or SetOf(TupleSet(....)))
                    match {
                      case Failure(exception) =>
                        Failure(TypeInferenceError(env, term, Some(Seq(exception))))
                      case Success(topLevelITC) =>
                        Success(SetOf(freshTV, None), topLevelITC)
                    }
                }
              case CartesianProduct(subterms, keys) => {
                // val internalsTypes = {
                //   Seq.fill(subterms.length)( SetOf(TypeVariable.getFresh, None) )
                // }
                val internalsTypes: Seq[TypeExpr[AxiomaticSet]] = {
                  Seq.fill(subterms.length)( TypeVariable.getFresh )
                }
                typeCheckSeq(env, subterms, internalsTypes.map( SetOf(_, None) )) match {
                  case Failure(exception) =>
                    Failure(TypeInferenceError(env, term, Some(Seq(exception))))
                  case Success(topLevelITC) =>
                    Success(
                      TupleSet(
                        internalsTypes, keys,
                        Seq.fill(internalsTypes.length)(Arity.any)
                      ),
                      topLevelITC
                    )
                }
              }
            }
          case Restriction(tupleSet, dimension, on) =>
            // TODO: revise to allow restriction on tuple set without the provided dimension
            val tupleSetTopType: TypeExpr[AxiomaticSet] =
              SetOf(TupleOf(None, Map(), Map()), None)
            val setTopType: TypeExpr[AxiomaticSet] =
              SetOf(TypeVariable.getFresh, None)
            typeCheckSeq(env, Seq(tupleSet, on), Seq(tupleSetTopType, setTopType)) match {
              case Failure(e) =>
                Failure(TypeInferenceError(env, term, Some(Seq(e))))
              case Success(topLevelITC) =>
                unifyEnv(env, topLevelITC) match {
                  case Failure(exception) =>
                    Failure(TypeInferenceError(env, term, Some(Seq(exception))))
                  case Success(updatedEnv) =>
                    inferSeqSep(updatedEnv, Seq(tupleSet, on)) match {
                      case Failure(exception) =>
                        Failure(TypeInferenceError(env, term, Some(Seq(exception))))
                      // case Success((Seq(tst, ont), _)) => // Todo: check correctness
                      case Success((Seq(tst, SetOf(ont, _)), _)) =>
                        val (km:Map[String, TuplePos], tp: TuplePos) =
                          dimension match {
                            case i:Int => (Map(), CanonicTuplePos(i))
                            case s:String =>
                              val tp = TuplePos.getFreshUnknown
                              (Map(s -> tp), tp)
                          }
                        val nt =
                          LUBType(
                            tst,
                            SetOf(TupleOf(None, Map(tp -> ont), km), None)
                          )
                        Success(nt, topLevelITC)
                    }
                }
            }
          case Junction(lhs, rhs, dimensions, collapsing) =>
            // TODO: revise?
            val tupleSetTopType: TypeExpr[AxiomaticSet] =
              SetOf(TupleOf(None, Map(), Map()), None)
            (typeCheck(env, lhs, tupleSetTopType),
              typeCheck(env, rhs, tupleSetTopType)) match {
              case ep @ ( (Failure(_), _) | (_, Failure(_)) ) =>
                TypeError.notifyAndReturnError(
                  buildTypeErrorFromTryPair(ep, s => TypeInferenceError(env, term, Some(s)))
                )
              case (Success(lhsITC), Success(rhsITC)) =>
                unifyEnv(lhsITC, rhsITC) match {
                  case Failure(exception) =>
                    Failure(TypeInferenceError(env, term, Some(Seq(exception))))
                  case Success(topLevelITC) =>
                    unifyEnv(env, topLevelITC) match {
                      case Failure(exception) =>
                        Failure(TypeInferenceError(env, term, Some(Seq(exception))))
                      case Success(updatedEnv) =>
                        inferSeqSep(updatedEnv, Seq(lhs, rhs)) match {
                          case Failure(exception) =>
                            Failure(TypeInferenceError(env, term, Some(Seq(exception))))
                          case Success((Seq(lhsT, rhsT), _)) =>
                            Success(
                              computeTupleSetJunction(lhsT, rhsT, dimensions, collapsing),
                              topLevelITC
                            )
                        }
                    }
                }
            }
          case Relations(domain, codomain, areAllTotal, areAllSurjective, areAllFunctional, areAllInjective) =>
            val domFreshTV: TypeExpr[AxiomaticSet] = TypeVariable.getFresh
            val codFreshTV: TypeExpr[AxiomaticSet] = TypeVariable.getFresh
            (typeCheck(env, domain, SetOf(domFreshTV, None)),
              typeCheck(env, codomain, SetOf(codFreshTV, None))) match {
              case ep @ ( (Failure(_), _) | (_, Failure(_)) ) =>
                TypeError.notifyAndReturnError(
                  buildTypeErrorFromTryPair(ep, s => TypeInferenceError(env, term, Some(s)))
                )
              case (Success(domITC), Success(codITC)) =>
                unifyEnv(domITC, codITC) match {
                  case Failure(exception) =>
                    Failure(TypeInferenceError(env, term, Some(Seq(exception))))
                  case Success(topLevelITC) =>
                    Success(
                      SetOf(
                        TupleSet(Seq(domFreshTV, codFreshTV), Map(),
                          Seq(
                            Arity.get(areAllFunctional, areAllTotal),
                            Arity.get(areAllInjective, areAllSurjective)
                          )
                        ),
                        None
                      ),
                      topLevelITC
                    )
                }
            }
        }
    }
  }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  /**
   * Compute the (lub) type of values resulting from the junction of values of
   * the 2 provided types.
   * @param lhs the type of the value at the left of the junction
   * @param rhs the type of the value at the right of the junction
   * @param dimensions dimensions on which the junction is done
   * @param collapsing whether the junction is collapsing or not
   * @return the "most precise" inferable type of the resulting value
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  private def computeTupleSetJunction
  (
    lhs: TypeExpr[AxiomaticSet], rhs: TypeExpr[AxiomaticSet],
    dimensions: Option[Set[(String,String)]], collapsing: Boolean
  ): TypeExpr[AxiomaticSet] =
    (lhs, rhs) match {
      case (
        SetOf(TupleOf(lhsLgth, lhsCT, lhsK), _),
        SetOf(TupleOf(rhsLgth, rhsCT, rhsK), _)
        ) =>
        dimensions match {
          case None => {
            val resLgth: Option[Int] =
              lhsLgth.flatMap(i => rhsLgth.map(i + _ - (if (collapsing) 2 else 1)))
            (lhsLgth, rhsLgth) match {
              case (Some(lhsL), Some(rhsL)) =>
                val resCT: Map[TuplePos, TypeExpr[AxiomaticSet]] =
                  lhsCT
                    .filter {
                      case (CanonicTuplePos(i), _) if !collapsing || i < lhsL => true
                    } ++
                    rhsCT.collect {
                      case (CanonicTuplePos(i), v) if 1 < i =>
                        (CanonicTuplePos(lhsL - (if (collapsing) 2 else 1) + i), v)
                    }
                val resK: Map[String, TuplePos] =
                  lhsK
                    .filter {
                      case (_, CanonicTuplePos(i)) if !collapsing || i < lhsL => true
                    } ++
                    rhsK.collect{
                      case (s, CanonicTuplePos(i)) if 1 < i =>
                        (s, CanonicTuplePos(lhsL - (if (collapsing) 2 else 1) + i))
                    }
                SetOf(TupleOf(resLgth, resCT, resK), None)
              case _ =>
                // TODO: deal with this case
                SetOf(TupleOf(None, Map(), Map()), None)
            }
          }
          case Some(_) =>
            // TODO: deal with this case
            SetOf(TupleOf(None, Map(), Map()), None)
        }
      case _ => LeafType(Emptyset)
    }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  /**
   * Infers the types of a sequence of terms. Returns the inferred types and individual
   * additional typing constraints if it succeeds, or the sequence of failures if
   * it does not.
   * @param env the assumed typing environment.
   * @param subterms the sequence of terms for which to infer the type.
   * @return the inferred types and individual additional typing constraints if
   *         it succeeds, or the sequence of failures if it does not.
   */
  private def inferSeqSep(env: TypingEnvironment[AxiomaticSet], subterms: Seq[SetTerm])
  : Try[(Seq[TypeExpr[AxiomaticSet]], Seq[TypingEnvironment[AxiomaticSet]])] =
    subterms
      .map(infer(env, _))
      .foldLeft(
        Right((Seq(), Seq())):
          Either[
            Seq[Throwable],
            (Seq[TypeExpr[AxiomaticSet]], Seq[TypingEnvironment[AxiomaticSet]])
          ]
      ) {
        case (Right((itAcc, itcAcc)), Success((inferredType, inferredTypingConstraints))) =>
          Right((itAcc.appended(inferredType), itcAcc.appended(inferredTypingConstraints)))
        case (Right(_), Failure(e)) => Left(Seq(e))
        case (Left(es), Success(_)) => Left(es)
        case (Left(es), Failure(e)) => Left(es.appended(e))
      } match {
      case Left(es) => Failure(TypeInferenceError(env, subterms, Some(es)))
      case Right(inferredRes) => Success(inferredRes)
    }

  /**
   * Infers the types of a sequence of terms. Returns the inferred type and merged
   * additional typing constraints if it succeed.
   * @param env the assumed typing environment.
   * @param subterms the sequence of terms for which to infer the type.
   * @return the inferred type and merged additional typing constraints if it succeed.
   */
  private def inferSeq(env: TypingEnvironment[AxiomaticSet], subterms: Seq[SetTerm])
  : Try[(Seq[TypeExpr[AxiomaticSet]], TypingEnvironment[AxiomaticSet])] =
    inferSeqSep(env, subterms) match {
      case Failure(exception) => Failure(exception)
      case Success((itSeq, itcSeq)) =>
        itcSeq.foldLeft(
          Success(new TypingEnvironment[AxiomaticSet]()): Try[TypingEnvironment[AxiomaticSet]]
        ) {
          case (Failure(exception), _) => Failure(exception)
          case (Success(itcAcc), itc) => unifyEnv(itcAcc, itc)
        } match {
          case Failure(exception) => Failure(exception)
          case Success(accITC) => Success(itSeq, accITC)
        }
    }

  /**
   * Checks that each term of a given sequence can be typed as the provided type.
   * Returns the merged additional typing constraints implied.
   * @param env the assumed typing environment.
   * @param subterms the sequence of terms to type check.
   * @param aimedType the type aimed for.
   * @return the merged additional typing constraints implied, if it succeeds.
   */
  private def typeCheckSeq
  (env: TypingEnvironment[AxiomaticSet],
   subterms: Seq[SetTerm],
   aimedType: TypeExpr[AxiomaticSet])
  : Try[TypingEnvironment[AxiomaticSet]] =
    typeCheckSeq(env, subterms, Seq.fill(subterms.length)(aimedType))

  /**
   * Checks that each term of a given sequence can be typed as the corresponding
   * type in the provided type sequence. Returns the merged additional typing constraints
   * implied.
   * @param env the assumed typing environment.
   * @param subterms the sequence of terms to type check.
   * @param aimedTypes the types aimed for.
   * @return the merged additional typing constraints implied, if it succeeds.
   */
  private def typeCheckSeq
  (env: TypingEnvironment[AxiomaticSet],
   subterms: Seq[SetTerm],
   aimedTypes: Seq[TypeExpr[AxiomaticSet]])
  : Try[TypingEnvironment[AxiomaticSet]] =
    inferSeqSep(env, subterms) match {
      case Failure(e) => Failure(e)
      case Success((itSeq, itcSeq)) => {
        (itSeq zip aimedTypes zip itcSeq zip itSeq.indices)
          .map {
            case (((it, aimedType), itc), index) =>
              unifyTypeGLB(it, aimedType) match {
                case Failure(e) =>
                  Failure(TypeCheckError(env, subterms(index), aimedType, Some(Seq(e))))
                case Success((glb,itcUni)) =>
                  unifyEnv(itc, itcUni)
              }
          }
          .foldLeft(
            Success(new TypingEnvironment[AxiomaticSet]())
              : Try[TypingEnvironment[AxiomaticSet]]
          ) {
            case (Failure(e), _) => Failure(e)
            case (Success(_), Failure(e)) => Failure(e)
            case (Success(itcAcc), Success(itc)) => unifyEnv(itcAcc, itc)
          }
      }
    }

  /**
   * Checks that the provided term can type as the provided type in the provided
   * typing environment. If it succeeds, then it also returns the additional typing
   * constraints allowing the type check to succeed.
   * @param env the typing environment to assume.
   * @param term the term to type check.
   * @param aimedType the expected type of the term
   * @return if the type checks, the additional typing constraints required; otherwise
   *         the reason for failure.AxiomaticSet
   */
  def typeCheck
  (env: TypingEnvironment[AxiomaticSet], term: SetTerm, aimedType: TypeExpr[AxiomaticSet])
  : Try[TypingEnvironment[AxiomaticSet]] =
    infer(env, term) match {
      case Success((inferredType, inferredTEnv)) =>
        unifyTypeGLB(inferredType, aimedType)
          .flatMap(res => unifyEnv(res._2, inferredTEnv))
      case Failure(cause) =>
        // println(s""">>>>> $cause""")
        TypeError.notifyAndReturnError(TypeCheckError(env, term, aimedType, Some(Seq(cause))))
    }

  /**
   * If the 2 provided types can be unified, then it returns: the most generic
   * type subtype of both (glb, Greatest Lower Bound), and some typing constraints
   * allowing to unify them. Otherwise, it returns a unification error ({@see UnificationError}).
   * @param t1 the first type to unify with the second.
   * @param t2 the second type to unify with the first.
   * @return a pair composed of: the most generic type subtype of both (glb, Greatest
   *         Lower Bound), and some typing constraints allowing to unify them;
   *         or a unification failure.
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  def unifyTypeGLB
  (t1: TypeExpr[AxiomaticSet], t2: TypeExpr[AxiomaticSet])
  : Try[(TypeExpr[AxiomaticSet], TypingEnvironment[AxiomaticSet])] = {
    (t1, t2) match {
      case (_, _) if t1 == t2 =>
        Success(t1, new TypingEnvironment[AxiomaticSet])
      // TYPE VARIABLE CASES
      case (TypeVariable(id1), TypeVariable(id2)) =>
        Success(
          TypeVariable(id1 min id2),
          new TypingEnvironment[AxiomaticSet](
            TypedTypeVar(TypeVariable(id1 max id2))
              -> TypeVariable(id1 min id2)
          )
        )
      case (_, TypeVariable(_)) =>
        // swapping parameters to always have the type variable as first element
        unifyTypeGLB(t2, t1)
      case (tv1 @ TypeVariable(_), _) =>
        Success(
          t2,
          new TypingEnvironment[AxiomaticSet](TypedTypeVar(tv1) -> t2)
        )
      // AXIOMATIC SET LEAFTYPE CASES
      case (LeafType(a1: AxiomaticSet), LeafType(a2: AxiomaticSet)) =>
        val glb = AxiomaticSets.glb(a1, a2)
        ( glb == a1 || glb == a2 ) match {
          case true => Success(LeafType(glb), new TypingEnvironment[AxiomaticSet])
          case false => Failure(UnificationError(t1, t2))
        }
      case (_, LeafType(_: AxiomaticSet)) =>
        // swapping parameters to always have the axiomatic set as first element
        unifyTypeGLB(t2, t1)
      case (LeafType(_: AxiomaticSet), SetOf(st, setCst)) =>
        unifyTypeGLB(t1, st) match {
          case Failure(exception) =>
            Failure(UnificationError(t1, t2, Some(Seq(exception))))
          case Success((glb, unifCst)) =>
            Success(SetOf(glb, setCst), unifCst)
        }
      case (LeafType(_: AxiomaticSet), t : TupleOf[_]) =>
        Success(t, new TypingEnvironment[AxiomaticSet])
      // OTHER TYPE EXPRESSION CASES
      case (LUBType(st1), LUBType(st2)) =>
        if (st1 == st2) {
          Success(t1, new TypingEnvironment[AxiomaticSet])
        } else {
          // There are other unifiable cases here
          // TODO: revise
          TypeError.notifyAndReturnError(UnificationError(t1, t2))
        }
        /*
      case (tst1: TupleSetRestrictionType[T], tst2: TupleSetRestrictionType[T]) =>
        unifyTupleSetRestrictionType(tst1, tst2)
      case (tsj1: TupleSetJunctionType[T], tsj2: TupleSetJunctionType[T]) =>
        ???
         */
      case (LeafType(l1), LeafType(l2)) =>
        if (l1 == l2) {
          Success(t1, new TypingEnvironment[AxiomaticSet])
        } else {
          TypeError.notifyAndReturnError(UnificationError(t1, t2))
        }
      case (TupleOf(optLgth1, ts1, ks1), TupleOf(optLgth2, ts2, ks2)) =>
        val resLgth: Try[Option[Int]] = {
          (optLgth1, optLgth2) match {
            case (Some(lgth1), Some(lgth2)) if lgth1 == lgth2 =>
              Success(Some(lgth1))
            case (Some(lgth1), Some(lgth2)) if lgth1 != lgth2 =>
              TypeError.notifyAndReturnError(UnificationError(t1, t2))
            case (Some(lgth), None) => Success(Some(lgth))
            case (None, Some(lgth)) => Success(Some(lgth))
            case (None, None) => Success(None)
          }
        }
        val resTs: Try[(Map[TuplePos, TypeExpr[AxiomaticSet]], TypingEnvironment[AxiomaticSet])] =
          (ts1.toSeq ++ ts2.toSeq)
            .groupBy(_._1)
            .foldLeft(
              Success(Map(), new TypingEnvironment[AxiomaticSet])
                : Try[(Map[TuplePos, TypeExpr[AxiomaticSet]], TypingEnvironment[AxiomaticSet])]
            ) {
              case (acc, mappings) =>
                val mapping
                : Try[((TuplePos, TypeExpr[AxiomaticSet]), TypingEnvironment[AxiomaticSet])]
                = {
                  mappings match {
                    case (tp: TuplePos, Seq((_: TuplePos, te: TypeExpr[AxiomaticSet]))) =>
                      Success(((tp, te), new TypingEnvironment[AxiomaticSet]))
                    case (tp: TuplePos, Seq((_, te1), (_, te2))) =>
                      unifyTypeGLB(te1, te2).flatMap {
                        case (glb, unifCst) => Success((tp, glb), unifCst)
                      }
                  }
                }
                (acc, mapping) match {
                  case (Failure(UnificationError(_, _, causes)), Failure(c)) =>
                    Failure(UnificationError(t1, t2, Some(causes.getOrElse(Seq()) :+ c)))
                  case (acc@Failure(_), Success(_)) =>
                    acc
                  case (Success(_), Failure(c)) =>
                    Failure(UnificationError(t1, t2, Some(Seq(c))))
                  case (Success((accTs, accTC)), Success((m, tc))) =>
                    unifyEnv(accTC, tc)
                      .map((accTs + m, _))
                }
            }
            val resKs: Try[Map[String, TuplePos]] =
              (ks1.toSeq ++ ks2.toSeq)
                .groupMap(_._1)(_._2)
                .foldLeft(
                  Success(Map()): Try[Map[String, TuplePos]]
                ) {
                  case (acc@Failure(_), _) => acc
                  case (Success(accMap), (s, Seq(tp))) =>
                    Success(accMap + (s -> tp))
                  case (Success(accMap), (s, Seq(tp1, tp2))) if tp1 == tp2 =>
                    Success(accMap + (s -> tp1))
                  case _ =>
                    TypeError.notifyAndReturnError(UnificationError(t1, t2))
                }
        (resLgth, resTs, resKs) match {
          case (Success(optLgth), Success((ts, tc)), Success(ks)) =>
            Success((TupleOf(optLgth, ts, ks), tc))
          case _ =>
            Failure(
              buildTypeErrorFromTrySeq(
                Seq(resLgth, resTs, resKs),
                c => UnificationError(t1, t2, Some(c))
              )
            )
        }
        /*
              case (f @ Failure(_), _) => f
              case (Success((accTs, accTC)), (tp: TuplePos, te: TypeExpr[AxiomaticSet])) =>
                accTs.get(tp) match {
                  case Some(value) =>
                  case None =>
                }
                acc.flatMap {
                  case (accCT, accCst) =>
                    unifyTypeGLB(
                      ts1.getOrElse(CanonicTuplePos(i), LeafType(World)),
                      ts2.getOrElse(CanonicTuplePos(i), LeafType(World))
                    ).flatMap {
                      case (glb, unifCst) =>
                        unifyEnv(accCst, unifCst)
                          .map((accCT + (CanonicTuplePos(i) -> glb), _))
                    }
                }
            }
        if (
          lgth1 == lgth2
            /*
          ks1.map{ case (k,v) => ks2.get(k).forall(_ == v) }
            .forall(b => b)
             */
        ) {
          (1 to lgth1).foldLeft(
            Success(Map(), new TypingEnvironment[AxiomaticSet])
              : Try[(Map[TuplePos, TypeExpr[AxiomaticSet]], TypingEnvironment[AxiomaticSet])]
          ){
            case (acc, i) =>
              acc.flatMap {
                case (accCT, accCst) =>
                  unifyTypeGLB(
                    ts1.getOrElse(CanonicTuplePos(i), LeafType(World)),
                    ts2.getOrElse(CanonicTuplePos(i), LeafType(World))
                  ).flatMap {
                      case (glb, unifCst) =>
                        unifyEnv(accCst, unifCst)
                          .map((accCT + (CanonicTuplePos(i) -> glb), _))
                    }
              }
          }.map{
            case (resCT, unifCst) =>
              (TupleOf(Some(lgth1), resCT, ks1 ++ ks2), unifCst)
          }
        } else {
          TypeError.notifyAndReturnError(UnificationError(t1, t2))
        }

         */
      case (SetOf(st1, cs1), SetOf(st2, cs2)) =>
        unifyTypeGLB(st1, st2) match {
          case Failure(e) =>
            TypeError.notifyAndReturnError(UnificationError(t1, t2, Some(Seq(e))))
          case Success((glb, unifCst)) =>
            Success(SetOf(glb, cs1 glb cs2), unifCst)
        }
        /*
      case (TupleSet(ts1, ks1, as1), TupleSet(ts2, ks2, as2)) =>
        if (
          ks1.map{ case (k,v) => ks2.get(k).map(_ == v).getOrElse(true) }
            .forall(b => b)
        ) {
          (ts1 zip ts2).foldLeft(
            Success(new TypingEnvironment[AxiomaticSet]): Try[TypingEnvironment[AxiomaticSet]]
          ){
            case (acc, (st1, st2)) => acc.flatMap(
              oldEnv => unifyType(st1, st2).flatMap(unifyEnv(oldEnv, _))
            )
          }
        } else {
          TypeError.notifyAndReturnError(UnificationError(t1, t2))
        }
         */
      case _ =>
        TypeError.notifyAndReturnError(
          UnificationError(
            t1, t2,
            Some(Seq(ImplementationError("Unhandled case in unifyTypeGLB")))
          )
        )
    }
  }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  /*
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  private def unifyTupleSetRestrictionType
  (tst1: TupleSetRestrictionType[T], tst2: TupleSetRestrictionType[T])
  : Try[(TypeExpr[AxiomaticSet], TypingEnvironment[AxiomaticSet])] = {
  // TODO: revise
    val (t1, d1, o1) = (tst1.tupleSetType, tst1.dimension, tst1.on)
    val (t2, d2, o2) = (tst2.tupleSetType, tst2.dimension, tst2.on)
    unifyTypeGLB(tst1, tst2) match {
      case Failure(exception) =>
        TypeError.notifyAndReturnError(UnificationError(t1, t2, Some(Seq(exception))))
      case Success((glb, unifCst)) =>
        unifCst.unfold(tst1) match {
          case (SetOf(TupleOf(ts, ks), _), _) =>
            val i1: Option[Int] =
              d1 match {
                case i: Int => Some(i)
                case _ => ks.get(d1.toString)
              }
            val i2: Option[Int] =
              d2 match {
                case i: Int => Some(i)
                case _ => ks.get(d2.toString)
              }
            (i1, i2) match {
              case (Some(i1), Some(i2)) if i1 == i2 =>
                unifyTypeGLB(o1, o2) match {
                  case Failure(exception) =>
                    TypeError.notifyAndReturnError(
                      UnificationError(t1, t2, Some(Seq(exception)))
                    )
                  case Success((oGLB, env2)) =>
                    unifyEnv(unifCst, env2).map( (oGLB, _) )
                }
              case (Some(i1), Some(i2)) if i1 != i2 =>
                val e1 = unifyTypeGLB(ts(i1), o2)
                val e2 = unifyTypeGLB(ts(i2), o1)
                (e1, e2) match {
                  case (Success((glb12, ie1)), Success((glb21, ie2))) =>
                    unifyEnv(unifCst, ie1).flatMap(unifyEnv(_,ie2)).map( (glb, _) )
                  case _ =>
                    TypeError.notifyAndReturnError(UnificationError(t1, t2))
                }
              case _ =>
                TypeError.notifyAndReturnError(UnificationError(t1, t2))
            }
          case _ => Success((glb, unifCst))
        }
    }
  }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length
   */

  def unifyEnv
  (e1: TypingEnvironment[AxiomaticSet], e2: TypingEnvironment[AxiomaticSet])
  : Try[TypingEnvironment[AxiomaticSet]] =
    Seq(e1, e2)
      .flatten(_.iterator)
      /*
      .groupMapReduce(_._1)(_._2)(
        unifyType(_, _) match {
          case Failure(e) =>
            TypeError.notifyAndReturnError(UnificationError(e1, e2, Some(Seq(e))))
          case Success(itc) =>???
        }
      )
       */
      .foldLeft(
        Success(new TypingEnvironment[AxiomaticSet]): Try[TypingEnvironment[AxiomaticSet]]
      ){
        case (Failure(e), _) =>
          TypeError.notifyAndReturnError(UnificationError(e1, e2, Some(Seq(e))))
        case (Success(envAcc), (typable, ttype)) =>
          // TODO: not so sure it's the GLB I should compute in the general case
          envAcc.getType(typable).map(unifyTypeGLB(ttype, _)) match {
            case None =>
              Success(envAcc + (typable, ttype))
            case Some(Failure(e)) =>
              TypeError.notifyAndReturnError(UnificationError(e1, e2, Some(Seq(e))))
            case Some(Success((glb, unifCst))) if unifCst.isEmpty =>
              Success(envAcc + (typable, glb))
            case Some(Success((glb, unifCst))) if ! unifCst.isEmpty =>
              // println(s"While unifying $e1 and $e2 on $typable:\n ${envAcc.getType(typable)} was unified with $ttype resulting in $glb under $unifCst")
              unifyEnv(envAcc + (typable, glb), unifCst)
          }
      }
      /*
      .map(
        env => {
          println(s"===== UNIFYING $e1 and $e2: $env")
          env
        }
      )
      */

}