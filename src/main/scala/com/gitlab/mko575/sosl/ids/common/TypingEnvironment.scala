/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.common

import com.gitlab.mko575.sosl.ids.{SetTerm, SetTermLeaf}
import com.gitlab.mko575.sosl.ids.typing.ExpansionContext
import com.gitlab.mko575.sosl.ids.typing.ExpansionContext.TypeVariableExpansions

sealed trait Typable
case class TypedSetTerm(t: SetTerm) extends Typable
case class TypedTypeVar[T](v: TypeVariable) extends Typable

/**
 * Data structure representing typing environments used by the type system.
 * @param internalDataStructure private internal data structure (can be initialized, otherwise empty.
 * @tparam T the type of type leaves
 */
class TypingEnvironment[T <: TypeExpr.LeavesTypeLUB]
(
  private val internalDataStructure: Map[Typable, TypeExpr[T]] = Map(): Map[Typable, TypeExpr[T]]
) // extends Map[Typable, TypeExpr[T]]
{

  def this(kv: (Typable, TypeExpr[T])) = this(Map(kv): Map[Typable, TypeExpr[T]])

  /*
  // private val internalDataStructure: Map[Typable, TypeExpr[T]] = HashMap()
  override def removed(key: Typable): TypingEnvironment[T] =
    new TypingEnvironment[T](internalDataStructure.removed(key))
  // The following error on overriding updated is fine with the compiler
  override def updated[V1 >: TypeExpr[T]](key: Typable, value: V1): Map[Typable, V1] =
    new TypingEnvironment[T](internalDataStructure.updated(key, value): Map[Typable, V1])
  // def myUpdated(key: Typable, value: TypeExpr[T]): TypingEnvironment[T] =
  //  new TypingEnvironment[T](internalDataStructure.updated(key, value): Map[Typable, TypeExpr[T]])
  override def get(key: Typable): Option[TypeExpr[T]] =
    internalDataStructure.get(key)
  override def iterator: Iterator[(Typable, TypeExpr[T])] =
    internalDataStructure.iterator
   */

  def isEmpty: Boolean =
    internalDataStructure.isEmpty

  def get(key: Typable): Option[TypeExpr[T]] =
    internalDataStructure.get(key)

  def map[K2, V2](f: ((Typable, TypeExpr[T])) => (K2,V2)): Map[K2, V2] =
    internalDataStructure.map(f)

  def foldLeft[B](z: B)(f: (B, (Typable, TypeExpr[T])) => B): B =
    internalDataStructure.foldLeft(z)(f)

  def iterator: Iterator[(Typable, TypeExpr[T])] = internalDataStructure.iterator

  def contains(key: Typable): Boolean = internalDataStructure.contains(key)

  def +(kv: (Typable, TypeExpr[T])): TypingEnvironment[T] = {
    kv._1 match {
      case TypedTypeVar(v) if kv._2.getVars contains v =>
        TypeError.notifyError(ImplementationError(
          s"Trying to add a direct loop to the typing environment when mapping $v to ${kv._2}"
        ))
        this
      case _ =>
        new TypingEnvironment[T](internalDataStructure + kv)
    }
  }

  def ++(that: Map[Typable, TypeExpr[T]]): TypingEnvironment[T] =
    new TypingEnvironment[T](internalDataStructure ++ that)


  override def toString: String = s"Γ${internalDataStructure.toString()}"

  /**
   * Recursively replaces type variables by their type according to this typing
   * environment. Returns the expanded type associated to an updated environment
   * taking into account expansions computed during the top level expansion.
   * @param t the type to expand.
   * @return the expanded type and updated typing environment.
   */
  private def expand(ec: ExpansionContext[T], t: TypeExpr[T])
  : (TypeExpr[T], TypeVariableExpansions[T]) = {
    // System.out.println(s"===== Expanding $t in $this")
    // System.out.println(s"===== Expanding $t for ${ec.root}")

    t match {
      case v: TypeVariable =>
        if ( ec.variablesUnderExpansion contains v ) {
          TypeError.notifyTypingError(ExpansionLoopError(ec.root, v, this))
          (t, ec.getTypeVariableExpansions)
        } else {
          ec.getKnownExpansionOf(v).map( (_, ec.getTypeVariableExpansions) )
            .getOrElse{
              val (te: TypeExpr[T], tvExp: TypeVariableExpansions[T]) =
                get(TypedTypeVar(v)).map(expand(ec.addVariableUnderExpansion(v), _))
                  .getOrElse(
                    (t, ec.getTypeVariableExpansions)
                  )
              (te, tvExp.updated(v, te): TypeVariableExpansions[T])
            }
        }
      case LUBType(subtypes) =>
        val (ntSet, tvExp) = this.expand(ec, subtypes.toSeq: Seq[TypeExpr[T]])
        (LUBType(ntSet.toSet), tvExp)
      /*
      case TupleSetRestrictionType(tupleSetType, dimension, on) =>
        val (nTST,tvExp1) = expand(ec, tupleSetType)
        val (nOT,tvExp2) = expand(ec.addAllExpandedVariables(tvExp1), on)
        (TupleSetRestrictionType(nTST, dimension, nOT), tvExp2)
      case TupleSetJunctionType(lhsTupleSetType, rhsTupleSetType, dimensions, collapsing) =>
        val (lhsNT, tvExp1) = expand(ec, lhsTupleSetType)
        val (rhsNT, tvExp2) = expand(ec.addAllExpandedVariables(tvExp1), rhsTupleSetType)
        (TupleSetJunctionType(lhsNT, rhsNT,dimensions, collapsing), tvExp2)
      */
      case LeafType(_) =>
        (t, ec.getTypeVariableExpansions)
      case TupleOf(lgth, contentTypes, contentKeys) =>
        val ctPos = contentTypes.toSeq.collect(_._1)
        val ctTypes = contentTypes.toSeq.collect(_._2)
        val (ntSeq, tvExp) = expand(ec, ctTypes)
        (TupleOf(lgth, (ctPos zip ntSeq).toMap, contentKeys), tvExp)
      case SetOf(contentType, constraints) =>
        val (nt, tvExp) = expand(ec, contentType)
        (SetOf(nt, constraints), tvExp)
    }
  }

  /**
   * Helper function for {@see expand} working on sequences of types.
   * @param tSeq the sequence of types to expand.
   * @return the expanded types and updated typing environment.
   */
  private def expand(ec: ExpansionContext[T], tSeq: Seq[TypeExpr[T]]): (Seq[TypeExpr[T]], TypeVariableExpansions[T]) = {
    // System.out.println(s"===== Expanding $t in $this")
    // System.out.println(s"===== Expanding $tSeq for ${ec.root} in $this")
    tSeq.foldLeft(
      (Seq(): Seq[TypeExpr[T]], ec.getTypeVariableExpansions)
    ) {
      case ((s, prevTVExp), t) =>
        expand(ec.addAllExpandedVariables(prevTVExp), t) match {
          case (nt, nextTVExp) => (s :+ nt: Seq[TypeExpr[T]], nextTVExp)
        }
    }
  }

  /**
   * Recursively replaces type variables by their type according to this typing
   * environment. Returns the expanded type associated to an updated environment
   * taking into account expansions computed during the top level expansion.
   * @param t the type to expand.
   * @return the expanded type and updated typing environment.
   */
  def unfold(t: TypeExpr[T]): (TypeExpr[T], TypingEnvironment[T]) = {
    val (et, tvExp) = expand(ExpansionContext(t), t)
    (et, this ++ tvExp.map{
      case (tv: TypeVariable,v:TypeExpr[T]) => (TypedTypeVar(tv), v)
    })
  }

  /**
   * Returns the same typing environment, but with every value fully expanded.
   * @return the same typing environment, but with every value fully expanded.
   */
  def expandAll(): TypingEnvironment[T] = {
    internalDataStructure.keys.foldLeft(
      new TypingEnvironment[T]()
    ) {
      case (acc, k) => getType(k).map(t => acc + (k -> t)).getOrElse(acc)
    }
  }

  /**
   * If it exists in this typing environment, retrieves the fully expanded type
   * of the typable given in parameter. The returned type is associated to an updated,
   * but equivalent, environment taking into account computed expansions.
   * @param typable the typable to look for.
   * @return the expanded type if it exists in the typing environment (and updated
   *         typing environment).
   */
  def getTypeAndUpdatedEnv(typable: Typable): Option[(TypeExpr[T], TypingEnvironment[T])] =
    internalDataStructure.get(typable).map( unfold )

  /**
   * If it exists in this typing environment, retrieves the fully expanded type
   * of the typable given in parameter.
   * @param typable the typable to look for.
   * @return the expanded type if it exists in the typing environment
   */
  def getType(typable: Typable): Option[TypeExpr[T]] =
    getTypeAndUpdatedEnv(typable).map(_._1)

  /**
   * Retrieves the most precise type of a typable according to this typing environment.
   * If the typable does not occur in this environment, a new type variable is
   * returned and added to the typing environment.
   * @param typable the typable to look for.
   * @return the type found, and this typing environment (updated if need be).
   */
  def getTypeOrTVar(typable: Typable): (TypeExpr[T], TypingEnvironment[T]) =
    getTypeAndUpdatedEnv(typable).getOrElse({
        val freshTVar: TypeExpr[T] = TypeVariable.getFresh
        (freshTVar, this + (typable -> freshTVar))
      })

}
