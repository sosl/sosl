/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.gos

import com.gitlab.mko575.sosl.ids.{AxiomaticSet, Entities, SetTerm, Values}
import com.gitlab.mko575.sosl.ids.common.{Arity, LeafType, SetOf, TypeExpr}
import com.gitlab.mko575.sosl.ids.gos.AttributeLikeRelationsRegister._

import scala.collection.mutable.{Map => MMap}

object AttributeLikeRelationsRegister {

  sealed trait AddresseeReference
  case object SelfRef extends AddresseeReference
  case object SubsetRef extends AddresseeReference
  case object ElementRef extends AddresseeReference

  sealed trait AddresseeType
  case object EntityType extends AddresseeType
  case object ValueType extends AddresseeType

  type AttributeLikeRelationType =
    (AddresseeReference, AddresseeType, AddresseeReference, AddresseeType)
  // type AttributeLikeRelationDescr =
  //   (SetTerm, Option[SetTerm], Option[Arity], Option[Arity], SetTerm)

  def setTermType2AttributeLikeRelationType(t: TypeExpr[AxiomaticSet])
  : Option[(AddresseeReference, AddresseeType)] =
    t match {
      case LeafType(Entities) => Some(ElementRef, EntityType)
      case LeafType(Values) => Some(ElementRef, ValueType)
      case SetOf(LeafType(Entities), _) => Some(SubsetRef, EntityType)
      case SetOf(LeafType(Values), _) => Some(SubsetRef, ValueType)
      case _ => None
    }

  val AddresseeReferenceValues: Set[AddresseeReference] =
      Set(SelfRef, SubsetRef, ElementRef)
  val AddresseeTypeValues: Set[AddresseeType] =
      Set(EntityType, ValueType)
  val allAttributeLikeRelationTypes: Set[AttributeLikeRelationType] =
    for (
      ar1 <- AddresseeReferenceValues;
      at1 <- AddresseeTypeValues;
      ar2 <- AddresseeReferenceValues;
      at2 <- AddresseeTypeValues
    ) yield (ar1, at1, ar2, at2)
}

case class AttributeLikeRelationDescr
(
  domain: SetTerm, domArity: Option[Arity],
  relTerm: Option[SetTerm],
  codomain: SetTerm, codArity: Option[Arity]
)

class AttributeLikeRelationsRegister {

  val store: MMap[AttributeLikeRelationType, Set[AttributeLikeRelationDescr]] = MMap()

  def registerAttributeLikeRelation
  (
    relType: AttributeLikeRelationType,
    relation: AttributeLikeRelationDescr
  ): Unit = {
    store.update(
      relType,
      store.getOrElse(relType, Set.empty) + relation
    )
  }

  def get(relType: AttributeLikeRelationType): Set[AttributeLikeRelationDescr] =
    store.getOrElse(relType, Set())

  def getAll(relTypes: Set[AttributeLikeRelationType]): Set[AttributeLikeRelationDescr] =
    relTypes.flatMap(get)

  def getTermsWithAttributes(relTypes: Set[AttributeLikeRelationType]): Set[SetTerm] =
    getAll(relTypes).collect(_.domain)

  def getAttributesOf(owner: SetTerm, ofTypes: Set[AttributeLikeRelationType])
  : Map[AddresseeReference, Set[AttributeLikeRelationDescr]]
  =
    ofTypes.foldLeft(
      Map(): Map[AddresseeReference, Set[AttributeLikeRelationDescr]]
    )(
      (acc: Map[AddresseeReference, Set[AttributeLikeRelationDescr]], relType: AttributeLikeRelationType)
      =>
        acc + (
          relType._1 ->
            acc.getOrElse(relType._1, Set(): Set[AttributeLikeRelationDescr]).union(
              get(relType).foldLeft(
                Set(): Set[AttributeLikeRelationDescr]
              )(
                (acc, alr) =>
                  alr match {
                    case AttributeLikeRelationDescr(t, _, _, _, _) if t == owner =>
                      acc + alr
                    case _ => acc
                  }
              )
            )
          )
    )

  def getOwnerOfAttribute(attribute: SetTerm, ofTypes: Set[AttributeLikeRelationType])
  : Option[SetTerm]
  =
    ofTypes.filterNot{
      case (SelfRef, _, _, _) => true
      case _ => false
    }.flatMap(get)
      .collectFirst{
      case AttributeLikeRelationDescr(owner, _, Some(t), _, _)
        if t == attribute => owner
    }
}

