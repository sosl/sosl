/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.gos

import com.gitlab.mko575.sosl.ids.SetTerm
import com.gitlab.mko575.sosl.ids.gos.AttributeLikeRelationsRegister.{AddresseeReference, AttributeLikeRelationType, allAttributeLikeRelationTypes}
import com.gitlab.mko575.sosl.ids.gos.OntologyEntityRelationshipDiagram.{ERGraph, ERGraphEdge, ERGraphNode, GenericTermNode}
import scalax.collection.GraphPredef.Param
import scalax.collection.edge.LkDiEdge
import scalax.collection.io.dot.{Id, NodeId}

case class ERDConfiguration
(
  oerd: OntologyEntityRelationshipDiagram,
  attributeLikeRelationsRegister: AttributeLikeRelationsRegister,
  relationTypesDisplayedAsAttributes: Set[AttributeLikeRelationType] = allAttributeLikeRelationTypes
) {

  def getAttributeLikeRelations: Set[AttributeLikeRelationDescr] =
    attributeLikeRelationsRegister.getAll(relationTypesDisplayedAsAttributes)

  def getAttributeLikeNodes: Set[ERGraphNode] =
    getAttributeLikeRelations.flatMap(_.relTerm.flatMap(oerd.getERNodeFor(_)))

  def getAttributeLikeEdges: Set[ERGraph#EdgeT] =
    oerd.getGraph.edges
      .filterNot(
        (e: ERGraph#EdgeT) =>
          e.exists( (n:ERGraph#NodeT) =>
            getAttributeLikeNodes.contains(n.value)
          )
      ).toSet

  def getNodesWithAttributes: Set[ERGraphNode] =
    attributeLikeRelationsRegister
      .getTermsWithAttributes(relationTypesDisplayedAsAttributes)
      .flatMap(oerd.getERNodeFor(_))

  def getAttributesOf(t: SetTerm): Map[AddresseeReference, Set[AttributeLikeRelationDescr]] =
    attributeLikeRelationsRegister
      .getAttributesOf(t, relationTypesDisplayedAsAttributes)

  def getOwnerOfAttributeLikeNode(node: ERGraph#NodeT): Option[ERGraphNode] =
    attributeLikeRelationsRegister
      .getOwnerOfAttribute(node.value.asInstanceOf[GenericTermNode].getTerm, relationTypesDisplayedAsAttributes)
      .flatMap(oerd.getERNodeFor(_))

  /***************************************************************************/

  private var displayedNodes: Option[Set[ERGraph#NodeT]] = None
  private var displayedEdges: Option[Set[ERGraph#EdgeT]] = None

  def getDisplayedNodes: Set[ERGraph#NodeT] =
    displayedNodes match {
      case Some(nodes) => nodes
      case None =>
        computeDisplayedNodesAndEdges
        displayedNodes.getOrElse(oerd.getGraph.nodes.toSet)
    }

  def getDisplayedEdges: Set[ERGraph#EdgeT] =
    displayedEdges match {
      case Some(edges) => edges
      case None =>
        computeDisplayedNodesAndEdges
        displayedEdges.getOrElse(oerd.getGraph.edges.toSet)
    }

  private def computeDisplayedNodesAndEdges: Unit = {
    val assumedDisplayedNodes: Set[ERGraph#NodeT] =
      oerd.getGraph.nodes.toSet
    val assumedDisplayedEdges: Set[ERGraph#EdgeT] =
      oerd.getGraph.edges.toSet
    val computedDisplayedNodes: Set[ERGraph#NodeT] =
      oerd.getGraph.nodes.toSet.filter(
        (n: ERGraph#NodeT) =>
          ! getAttributeLikeNodes.contains(n.value)
          &&
          ! (
            (n.edges.toSet intersect assumedDisplayedEdges).isEmpty
              &&
              ! getNodesWithAttributes.contains(n.value)
            )
      )
    val computedDisplayedEdges: Set[ERGraph#EdgeT] =
      oerd.getGraph.edges.toSet.filterNot(
        (e: ERGraph#EdgeT) =>
          e.edge match {
            case LkDiEdge(src: ERGraph#NodeT, dst: ERGraph#NodeT, label)
              if (getAttributeLikeNodes contains src.value)
                && label.isInstanceOf[AbstractRelationPlace]
            =>
              true
            case _ =>
              false
          }
      )
    displayedNodes = Some(computedDisplayedNodes)
    displayedEdges = Some(computedDisplayedEdges)
  }

  def getNodeIdStr(node: ERGraph#NodeT): Option[String] =
    if (getDisplayedNodes contains node) {
      Some(node.##.toString)
    } else {
      None
    }

  def getPortId4attribute(node: ERGraphNode): Option[String] =
    if (getAttributeLikeNodes contains node) {
      Some(oerd.getGraph.get(node).##.toString)
    } else {
      None
    }

  def getDotID4edgeEnd(node: ERGraph#NodeT): Option[String] = {
    // import scalax.collection.io.dot.implicits._, scalax.collection.io.dot.Record._
    if (getDisplayedNodes contains node) {
      // getNodeIdStr(node).map(NodeId(_))
      getNodeIdStr(node)
    } else {
      if (getAttributeLikeNodes contains node.value) {
        getOwnerOfAttributeLikeNode(node) match {
          case Some(owner: ERGraphNode) =>
            val ownerIdOpt: Option[String] = getNodeIdStr(oerd.getGraph.get(owner))
            val portIdOpt: Option[String] = getPortId4attribute(node.value)
            (ownerIdOpt, portIdOpt) match {
              case (Some(ownerId), Some(portId)) =>
                // Some(NodeId(ownerId, portId))
                Some(s"""${ownerId}:${portId}:w""")
              case _ => None
            }
          case None => None
        }
      } else {
        None
      }
    }
  }

}
