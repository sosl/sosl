/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.gos

sealed trait OldEdge {
  val name: String
  val connections: Set[OldEdgeEndpoint]
}

class BinaryOldEdge(
                  override val name: String,
                  tail: OldEdgeEndpoint,
                  head: OldEdgeEndpoint
                ) extends OldEdge {
  override val connections = Set(tail, head)
}

case class IsSubsetOf(from: SetOldVertex, to: SetOldVertex)
  extends BinaryOldEdge(
    "subsetOf",
    OldEdgeEndpoint(None, None, from),
    OldEdgeEndpoint(None, None, to))

case class IsInstanceOf(from: InstanceOldVertex, to: SetOldVertex)
  extends BinaryOldEdge("subsetOf", OldEdgeEndpoint(None, None, from), OldEdgeEndpoint(None, None, to))

case class HasAttribute(entity: EntitySet, attrName: String, attrType: OldEdgeEndpoint)
  extends BinaryOldEdge(attrName, OldEdgeEndpoint(None, None, entity), attrType) {

  def this(entity: EntitySet, attrName: String, attrType: ValueSet) =
    this(entity, attrName, OldEdgeEndpoint(Some(attrName), None, attrType))
}

case class Association(override val name: String, from: OldEdgeEndpoint, to: OldEdgeEndpoint)
  extends BinaryOldEdge(name, from, to) {

  def this(name: String, from: EntitySet, to: EntitySet) =
    this(name: String, OldEdgeEndpoint(None, None, from), OldEdgeEndpoint(None, None, to))
}
