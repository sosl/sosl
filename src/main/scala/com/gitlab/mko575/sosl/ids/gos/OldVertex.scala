/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.gos

import com.gitlab.mko575.sosl.ids.NamedEntity

sealed trait OldVertex

sealed trait NamedOldVertex extends OldVertex {
  val name: String
}

sealed trait SetOldVertex extends NamedOldVertex {
  override val name: String = set.name
  val set: NamedEntity
}
case class UntypedSet(set: NamedEntity) extends SetOldVertex
case class EntitySet(set: NamedEntity) extends SetOldVertex
case class ValueSet(set: NamedEntity) extends SetOldVertex

sealed trait InstanceOldVertex extends NamedOldVertex
