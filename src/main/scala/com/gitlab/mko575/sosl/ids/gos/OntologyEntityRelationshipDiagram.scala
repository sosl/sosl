/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.gos

import com.gitlab.mko575.sosl.ids.SetId
import com.gitlab.mko575.sosl.ids.common.{Arity, CanonicTuplePos, LeafType, SetOf, TupleOf, TypeExpr, TypeInferrer, TypeVariable, TypedSetTerm, TypingEnvironment}
import com.gitlab.mko575.sosl.ids._
import com.gitlab.mko575.sosl.ids.sos.OntologySeed
import com.gitlab.mko575.sosl.ids.gos.AttributeLikeRelationsRegister._
import com.gitlab.mko575.sosl.ids.gos.OntologyEntityRelationshipDiagram._
import scalax.collection.{Graph, GraphBase}
import scalax.collection.GraphEdge._
import scalax.collection.GraphPredef._
import scalax.collection.config._
import scalax.collection.constrained.constraints.Connected
import scalax.collection.edge.{LkDiEdge, LkDiHyperEdge}
import scalax.collection.edge.Implicits._
import scalax.collection.io.dot._
import scalax.collection.mutable.ArraySet.{Hints, empty}

// import scala.collection.Set
import scala.util.Try

// import scala.collection.{Set, immutable}
// import scala.collection.immutable.Set
import scala.util.{Failure, Success}
import scala.{+:, ::}

object OntologyEntityRelationshipDiagram {

  sealed trait ERGraphNode {
    val oerd: OntologyEntityRelationshipDiagram
    val labelStr: String
  }
  sealed abstract class GenericTermNode
  (oerd: OntologyEntityRelationshipDiagram, t:SetTerm
  ) extends ERGraphNode {
    override val labelStr: String = t.toString(oerd.seed.id)
    def getTerm = t
  }
  case class OSetNode
  (oerd: OntologyEntityRelationshipDiagram, sid:SetId
  ) extends GenericTermNode(oerd, Reference(sid)) {
    override val labelStr: String = sid.toString(oerd.seed.id)
  }
  case class TermNode
  (oerd: OntologyEntityRelationshipDiagram, t:SetTerm
  ) extends GenericTermNode(oerd, t)
  // case class PropNode(p:Property) extends ERGraphNode {
  //   override val labelStr: String = p.toString(seed.id)
  // }

  /** ************************************************************************** */

  type ERGraphEdge[+T] = LkDiEdge[T]
  // type ERGraphEdge[+T <: Relationship] = LkDiEdge[T]
  type ERGraph = Graph[ERGraphNode, ERGraphEdge]

}

@SuppressWarnings(Array("checkstyle:number.of.methods"))
// scalastyle:off number.of.methods
case class OntologyEntityRelationshipDiagram(seed: OntologySeed) {
  // To differentiate hyperedges based on the order of endpoints
  // implicit val kind: CollectionKind = Sequence

  /** ************************************************************************** */

  private val typeInferrer: TypeInferrer[AxiomaticSet] =
    new TypeInferrer[AxiomaticSet](seed)

  private val typingEnvironment: TypingEnvironment[AxiomaticSet] =
    typeInferrer.inferAxiomaticTypes()

  private val inferTypeOf: SetTerm => Try[TypeExpr[AxiomaticSet]] =
    typeInferrer.infer(typingEnvironment)

  /** ************************************************************************** */

  /**
   * The internal representation of this seed as a complete graph.
   */
  private var internalERGraph: Option[ERGraph] = None
  private var nodeAttributes: Map[ERGraph#NodeT, Map[Any, (Option[Arity], Set[SetTerm])]] = Map()
  private val attributeLikeRelationsRegister: AttributeLikeRelationsRegister =
    new AttributeLikeRelationsRegister()

  /**
   * Retrieves the Entity-Relationship (ER) graph corresponding to this ER diagram.
   *
   * @return this ER diagram as a graph.
   */
  def getGraph: ERGraph =
    internalERGraph match {
      case Some(graph) => graph
      case None => buildRawGraph(true)
    }

  /** Builds, stores in {@link internalERGraph} and return the ER diagram as a
   * graph using Graph-for-Scala
   *
   * @return the graph built
   */
  private def buildRawGraph(evRelationsAsAttributes: Boolean): ERGraph = {
    val nodesAndEdges: Set[Param[ERGraphNode, ERGraphEdge]] =
      getSimplifiedProperties.flatMap(getNodesAndEdgesFrom)
    val graph: ERGraph = Graph.from(nodesAndEdges)
    identifyAttributeLikeNodes(graph)
    internalERGraph = Some(graph)
    internalERGraph.get
  }

  /**
   * Returns the set of properties of the represented SOS as an equivalent set
   * of properties that are easier to represent in an Entity-Relationship graph.
   *
   * @return an "equivalent" set of properties easier to handle for ER diagrams.
   */
  private def getSimplifiedProperties: Set[Property] =
    seed.properties.flatMap(
      p =>
        p match {
          case Subset(SetCons(subterms), superset, _) =>
            subterms.map(t => In(t, superset))
          case _ => Set(p)
        }
    )

  /**
   * Builds/retrieves the graph node representing the provided term, if it can
   * be represented in this type of graph (Entity-Relationship graph).
   *
   * @param t the (set) term to be represented.
   * @return the node representing the provided term, if it's rER_Edgeepresentable.
   */
  def getERNodeFor(t: SetTerm): Option[ERGraphNode] =
    t match {
      // TODO: modify to: use sid in OSet instead of leaf; and have get work with sid
      case Reference(sid) => Some(OSetNode(this, sid))
      case _: AxiomaticSet => None
      case _: SetTermNode => Some(TermNode(this, t))
    }

  /**
   * Retrieves all the graph nodes that are representations of parts of the provided
   * term.
   *
   * @param t the (set) term whose fragments representations have to be retrieved.
   * @return the nodes representing fragments of the provided term.
   */
  private def getERNodesFrom(t: SetTerm): Set[ERGraphNode] = {
    val topNode =
      getERNodeFor(t).map(n => Set(n)).getOrElse(Set.empty)
    val subNodes: Set[ERGraphNode] =
      t match {
        case _: SetTermLeaf => Set.empty
        case node: SetTermNode => node.subterms.toSet.flatMap(getERNodesFrom)
      }
    topNode ++ subNodes
  }

  /**
   * Get the type of relation representing the given property.
   *
   * @param p the property whose "ER type" to retrieve.
   * @return the relationship "ER type" of the given property.
   */
  private def getRelationshipType(p: Property): Option[ER_Edge] =
    p match {
      case binP: BinaryProperty =>
        binP match {
          case Subset(_, _, isStrict) => Some(AreRel(isStrict))
          case _: In => Some(IsARel)
        }
      case _: NAryProperty => None
      case _: Predicate => None
    }

  /**
   * Returns all the nodes and edges representing the provided term in an Entity-Relationship
   * graph.
   *
   * @param t the term to transform into ER-graph nodes and edges.
   * @return the ER-graph nodes and edges representing the provided term.
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  private def getNodesAndEdgesFrom(t: SetTerm): Set[Param[ERGraphNode, ERGraphEdge]] = {
    val topNodeOpt: Option[ERGraphNode] = getERNodeFor(t)
    val topNodeAsParams: Set[Param[ERGraphNode, ERGraphEdge]] =
      topNodeOpt.map(n => OuterNode(n)).iterator.to(Set)
    val subElems: Set[Param[ERGraphNode, ERGraphEdge]] =
      t match {
        case _: SetTermLeaf => Set.empty
        case node: SetTermNode =>
          node match {
            case SetCons(subterms) =>
              getNodesAndEdgesForSubterms(topNodeOpt, subterms.toSeq,
                (topN: ERGraphNode, _: Int, subN: ERGraphNode) =>
                  Set(LkDiEdge(subN, topN)(IsARel))
              )
            case TupleCons(subterms, keys) =>
              getNodesAndEdgesForSubterms(topNodeOpt, subterms,
                (topN: ERGraphNode, pos: Int, subN: ERGraphNode) => {
                  val placeId: Option[String] = keys.collectFirst { case (s, pos) => s }
                  Set(
                    LkDiEdge(topN, subN)(
                      new RelationPlace(
                        placeId.orElse(Some(pos.toString)),
                        Some(Arity.one)
                      ) with TuplePlace
                    )
                  )
                }
              )
            // case Complement(set, relativeTo) =>
            // case Powerset(set) =>
            // case term: NAryInfixOpSetTerm =>
            case Restriction(tupleSet, _, _) =>
              getNodesAndEdgesForSubterms(topNodeOpt, Seq(tupleSet),
                (topN: ERGraphNode, _: Int, subN: ERGraphNode) =>
                  Set(LkDiEdge(topN, subN)(AreRel(false)))
              ) ++ node.subterms.toSet.flatMap((t: SetTerm) => getNodesAndEdgesFrom(t))
            // case Junction(lhs, rhs, dimensions, collapsing) =>
            // case Relations(domain, codomain, areAllTotal, areAllSurjective, areAllFunctional, areAllInjective) =>
            case _ => node.subterms.toSet.flatMap((t: SetTerm) => getNodesAndEdgesFrom(t))
          }
      }
    topNodeAsParams union subElems
  }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  /**
   * Returns the nodes and edges representing a "root" term and its subterms
   *
   * @param topNodeOpt  the node (if it exists) representing the "root" term.
   * @param subterms    the subterms
   * @param edgeBuilder a function building an edge between the "root" node and
   *                    one of its subterms node
   * @tparam T the type paramter used for ERGraphEdges
   * @return the set of nodes and edges representing the "root" term and its subterms
   */
  private def getNodesAndEdgesForSubterms[T]
  (
    topNodeOpt: Option[ERGraphNode], subterms: Seq[SetTerm],
    edgesBuilder: (ERGraphNode, Int, ERGraphNode) => Set[Param[ERGraphNode, ERGraphEdge]] // ERGraphEdge[T]
  ): Set[Param[ERGraphNode, ERGraphEdge]] =
    (subterms zip subterms.indices.map(_ + 1))
      .foldLeft(Set(): Set[Param[ERGraphNode, ERGraphEdge]]) {
        case (acc, (st: SetTerm, i: Int)) =>
          val additionalEdge: Set[Param[ERGraphNode, ERGraphEdge]] =
            (topNodeOpt, getERNodeFor(st)) match {
              case (Some(topNode), Some(stNode)) =>
                edgesBuilder(topNode, i, stNode)
              case _ => Set.empty
            }
          acc ++ additionalEdge ++ getNodesAndEdgesFrom(st)
      }

  /**
   * Returns all the nodes and edges representing the binary relation described
   * by the provided parameters.
   *
   * @param relTerm  the term corresponding to this relation.
   * @param domTerm  the term corresponding to its domain.
   * @param domArity the arity of its domain.
   * @param codTerm  the term corresponding to its codomain.
   * @param codArity the arity of its codomain.
   * @param keys     the identifiers for its domain (1) and codomain (2) places.
   * @return the ER-graph nodes and edges representing the binary relation described.
   */
  private def getNodesAndEdgesForBinaryRelation(
                                                 relTerm: SetTerm,
                                                 domTerm: SetTerm, domArity: Option[Arity],
                                                 codTerm: SetTerm, codArity: Option[Arity],
                                                 keys: Map[String, Int]
                                               ): Set[Param[ERGraphNode, ERGraphEdge]] =
    getNodesAndEdgesForRelation(relTerm, Seq((domTerm, domArity), (codTerm, codArity)), keys)

  /**
   * Returns all the nodes and edges representing the binary relation described
   * by the provided parameters.
   *
   * @param relTerm the term corresponding to this relation.
   * @param keys    the identifiers for its places.
   * @return the ER-graph nodes and edges representing the binary relation described.
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  private def getNodesAndEdgesForRelation
  (
    relTerm: SetTerm,
    places: Seq[(SetTerm, Option[Arity])],
    keys: Map[String, Int]
  )
  : Set[Param[ERGraphNode, ERGraphEdge]]
  =
    getERNodeFor(relTerm) match {
      case None => Set.empty
      case Some(lhsNode) =>
        getNodesAndEdgesFrom(relTerm) ++
          (places.indices.map(_ + 1) zip places)
            .foldLeft(Set(): Set[Param[ERGraphNode, ERGraphEdge]]) {
              case (acc, (i, (pTerm, pArity))) =>
                val placeId: Option[String] = keys.collectFirst { case (s, i) => s }
                /*
              val untypedRelType = {
                if (places.size == 2) {
                  i match {
                    case 1 => BinRelDomPlace(placeId, pArity)
                    case 2 => BinRelCodomPlace(placeId, pArity)
                  }
                } else {
                  RelationPlace(placeId, pArity)
                }
              }
              val (relDstTerm, relType) = {
                pTerm match {
                  case Powerset(s) =>
                    (s, untypedRelType :: SetRelPlace)
                  case s: Any  =>
                    (s, untypedRelType :: ElemRelPlace)
                }
                 */
                val (relDstTerm, relType) = {
                  pTerm match {
                    /*
                  case Powerset(s) if places.size == 2 && i == 1 =>
                    (s, new BinRelDomPlace(placeId, pArity) with SetRelPlace)
                  case Powerset(s) if places.size == 2 && i == 2 =>
                    (s, new BinRelCodomPlace(placeId, pArity) with SetRelPlace)
                   */
                    case Powerset(s) =>
                      (s, new RelationPlace(placeId.orElse(Some(i.toString)), pArity) with SetRelPlace)
                    /*
                  case s: Any if places.size == 2 && i == 1 =>
                    (s, new BinRelDomPlace(placeId, pArity) with ElemRelPlace)
                  case s: Any if places.size == 2 && i == 2 =>
                    (s, new BinRelCodomPlace(placeId, pArity) with ElemRelPlace)
                   */
                    case s: Any =>
                      (s, new RelationPlace(placeId.orElse(Some(i.toString)), pArity) with ElemRelPlace)
                  }
                }
                getERNodeFor(relDstTerm) match {
                  case Some(pNode) => {
                    // println(s""">>> Creating edge ${LkDiEdge(lhsNode, pNode)(relType)}""")
                    acc ++ Set(LkDiEdge(lhsNode, pNode)(relType)) ++ getNodesAndEdgesFrom(relDstTerm)
                  }
                  case None => acc
                }
            }
    }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  /**
   * Returns all the nodes and edges representing the provided property in an Entity-Relationship
   * graph.
   *
   * @param p the property to transform into ER-graph nodes and edges.
   * @return the ER-graph nodes and edges representing the provided property.
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  private def getNodesAndEdgesFrom(p: Property): Set[Param[ERGraphNode, ERGraphEdge]] = {
    p match {
      case Subset(lhs, CartesianProduct(ts, ks), _) => {
        val topEdges: Set[Param[ERGraphNode, ERGraphEdge]] =
          getNodesAndEdgesForRelation(
            lhs, ts zip Seq.fill(ts.size)(Some(Arity.any)), ks
          )
        topEdges // ++ Seq(lhs, ts(0), ts(1)).flatMap(getNodesAndEdgesFrom)
      }
      case Subset(lhs, Powerset(t), isStrict) => {
        val topEdges: Set[Param[ERGraphNode, ERGraphEdge]] = {
          (getERNodeFor(lhs), getERNodeFor(t)) match {
            case (Some(lhsNode), Some(rhsNode)) =>
              Set(LkDiEdge(lhsNode, rhsNode)(
                new AreRel(isStrict) with SetRelPlace
              ))
            case _ => Set.empty
          }
        }
        topEdges ++ Seq(lhs, t).flatMap(getERNodesFrom)
      }
      case In(lhs, Relations(dom, cod, isT, isS, isF, isI)) => {
        val topEdges: Set[Param[ERGraphNode, ERGraphEdge]] = {
          getNodesAndEdgesForRelation(
            lhs, Seq((dom, Some(Arity.get(isF, isT))), (cod, Some(Arity.get(isI, isS)))), Map()
          )
        }
        /*
        getNodesAndEdgesForBinaryRelation(
            lhs, dom, Some(Arity.get(isF, isT)), cod, Some(Arity.get(isI, isS)), Map()
          )
           */
        topEdges // ++ Seq(lhs, dom, cod).flatMap(getNodesAndEdgesFrom)
      }
      /*
      case In(TupleCons(places, keys), Reference(sId)) if places.size == 2 => {
        /*
        val preimageType: TypeExpr[AxiomaticSet] =
          new TypeInferrer(seed).infer(typingEnvironment)(places(0))
            .getOrElse(LeafType(World))
        val imageType: TypeExpr[AxiomaticSet] =
          new TypeInferrer(seed).infer(typingEnvironment)(places(1))
            .getOrElse(LeafType(World))
        (preimageType, imageType.getLeaves) match {
          case res @ ( (LeafType(Entities), _) | (SetOf(LeafType(Entities), _), _) )
            if res._2 == Set(LeafType(Values)) =>
            Set()
          case _ =>
         */
            (getERNodeFor(places(0)), getERNodeFor(places(1))) match {
              case (Some(preimageNode), Some(imageNode)) =>
                val preimageId: Option[String] = keys.collectFirst { case (s, 1) => s }
                val imageId: Option[String] = keys.collectFirst { case (s, 2) => s }
                Set(
                  LkDiEdge(preimageNode, imageNode)(
                    new BinaryRelation(
                      Some(sId.toString(seed.id)),
                      preimageId, None,
                      imageId, None
                    ) with TuplePlace
                  )
                )
              case _ => Set.empty
            }
        /*
        }
         */
      }
       */
      case p: BinaryProperty => {
        val topEdges: Set[Param[ERGraphNode, ERGraphEdge]] = {
          (getERNodeFor(p.lhs), getERNodeFor(p.rhs), getRelationshipType(p)) match {
            case (Some(lhsNode), Some(rhsNode), Some(relType)) =>
              Set(LkDiEdge(lhsNode, rhsNode)(relType))
            case _ => Set.empty
          }
        }
        // topEdges ++ Seq(p.lhs, p.rhs).flatMap(getERNodesFrom)
        topEdges ++ Seq(p.lhs, p.rhs).flatMap(getNodesAndEdgesFrom)
      }
      case _ => p.terms.toSeq.flatMap(getNodesAndEdgesFrom).toSet
    }
  }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  /** ************************************************************************** */

  /**
   * Returns the domain and codomain (with arities if avaliblae) of the relation
   * represented by a given node.
   *
   * @param g       the graph whose node belongs to.
   * @param relNode the node representing the relation.
   * @return a quintuplet composed of: the term reprsenting the domain of the relation;
   *         the term representing the relation (if it exists); the domains arity
   *         (if known); the codomain arity (if known); the term representing the codomain.
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  private def extractDomAndRangeFrom(g: ERGraph, relNode: ERGraph#NodeT)
  // : Option[(ERGraph#NodeT, Option[SetTerm], Option[Arity], Option[Arity], SetTerm)]
  : Option[(SetTerm, Option[SetTerm], Option[Arity], Option[Arity], SetTerm)]
  = {
    relNode.outgoing
      .map { case e: Any => (e, e.value.label) }
      .collectFirst({
        case (e, RelationPlace(Some(1), arity)) => (e.target, arity)
        case (e, RelationPlace(Some("1"), arity)) => (e.target, arity)
      })
      // .map{ case e: Any => println(s""">>>>> Collected ${e}"""); e }
      .flatMap {
        case (domNode, domArity) =>
          val domTerm: SetTerm = domNode.value.asInstanceOf[GenericTermNode].getTerm
          relNode.outgoing
            .map { case e: Any => (e, e.value.label) }
            .collectFirst {
              case (e, RelationPlace(Some("2"), arity))
              => (e.target.value.asInstanceOf[GenericTermNode].getTerm, arity)
            }
            .map {
              case (codTerm, codArity) =>
                val nodeTerm: SetTerm = relNode.value.asInstanceOf[GenericTermNode].getTerm
                inferTypeOf(nodeTerm) match {
                  case Failure(_) =>
                    (domTerm, None, domArity, codArity, codTerm)
                  case Success(SetOf(TupleOf(_, _, _), _)) =>
                    (domTerm, Some(nodeTerm), domArity, codArity, codTerm)
                  case Success(TupleOf(_, _, _)) =>
                    val relTerm: Option[SetTerm] =
                      relNode.outgoing
                        /*
                        .map{ case e: Any => (e, e.edge.label) }
                        .collectFirst({
                          case (e, IsARel) =>
                            e.target.value.asInstanceOf[GenericTermNode].getTerm
                        })
                         */
                        /*
                        .map{ case e: Any => e.edge }
                        .collectFirst({
                          case srcNode :~> tgtNode + (label:IsARel)
                            tgtNode.value.asInstanceOf[GenericTermNode].getTerm
                        })
                         */
                        .map { case e: Any => (e, e.edge.label) }
                        .collectFirst({
                          case (e, label) if label == IsARel =>
                            e.target.value.asInstanceOf[GenericTermNode].getTerm
                        })
                    (domTerm, relTerm, domArity, codArity, codTerm)
                  case Success(_) =>
                    (domTerm, None, domArity, codArity, codTerm)
                }
            }
      }
  }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  /**
   * If posssible, registers the relation represented by this node as an attribute like relation.
   *
   * @param g       the current graph the node belongs to.
   * @param relNode the node representing the relation.
   * @param domRef  the type of reference to use for the domain.
   */
  private def registerAttributeLikeRelation
  (g: ERGraph, relNode: ERGraph#NodeT,
   domRef: AddresseeReference, overrideCodRef: Option[AddresseeReference] = None)
  : Unit =
    extractDomAndRangeFrom(g, relNode) match {
      case Some((domTerm, relTerm, domArity, codArity, codTerm)) =>
        val domALRType: Option[(AddresseeReference, AddresseeType)] =
          inferTypeOf(domTerm).map(setTermType2AttributeLikeRelationType).getOrElse(None)
        val codALRType: Option[(AddresseeReference, AddresseeType)] =
          inferTypeOf(codTerm).map(setTermType2AttributeLikeRelationType).getOrElse(None)
        (domALRType, codALRType) match {
          case (Some((_, domType)), Some((codRef, codType))) =>
            val srcTerm: SetTerm = relNode.value.asInstanceOf[GenericTermNode].getTerm
            val updatedCodRef: AddresseeReference = overrideCodRef.getOrElse(codRef)
            val updatedCodTerm: SetTerm =
              updatedCodRef match {
                case SubsetRef => Powerset(codTerm)
                case _ => codTerm
              }
            /*
            println(
              s""">>>>> From $srcTerm, registering relation $relTerm: $domTerm [$domArity x $codArity] $codTerm
                 | as a relation of type
                 | ($domRef, $domType, ${overrideCodRef.getOrElse(codRef)}, $codType)"""
                .stripMargin.linesIterator.mkString("").trim
            )
             */
            attributeLikeRelationsRegister.registerAttributeLikeRelation(
              (domRef, domType, codRef, codType),
              AttributeLikeRelationDescr(
                domTerm, domArity, Some(srcTerm), updatedCodTerm, codArity
              )
            )
          case _ => ()
        }
      case None => ()
    }

  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  private def identifyAttributeLikeNodes[T](graph: ERGraph = getGraph): Unit = {
    // val graph: ERGraph = getGraph
    // println(s""">>>>> Parsing: ${graph.nodes.mkString("\n  - ", "\n  - ", "")}""")
    // println(s""">>>>> Not Parsing: ${graph.edges.mkString("\n  - ", "\n  - ", "")}""")
    val oerd: OntologyEntityRelationshipDiagram = this
    graph.nodes.foreach[Unit](
      node => node.value match {
        case OSetNode(oerd, sid) =>
          inferTypeOf(Reference(sid)) match {
            case Failure(_) => ()
            case Success(SetOf(TupleOf(Some(2), places, _), _)) if places.size == 2 =>
              val domRefOpt: Option[AddresseeReference] =
                places.get(CanonicTuplePos(1)) match {
                  case Some(set: SetOf[AxiomaticSet])
                    if set.contentType.isInstanceOf[LeafType[AxiomaticSet]]
                  =>
                    Some(SubsetRef)
                  case Some(LeafType(_)) =>
                    Some(ElementRef)
                  case _ => None
                }
              val codRefOpt: Option[AddresseeReference] =
                places.get(CanonicTuplePos(2)) match {
                  case Some(set: SetOf[AxiomaticSet])
                    if set.contentType.isInstanceOf[LeafType[AxiomaticSet]]
                  =>
                    Some(SubsetRef)
                  case Some(LeafType(t)) =>
                    Some(ElementRef)
                  case _ => None
                }
              domRefOpt match {
                case Some(domRef) =>
                  registerAttributeLikeRelation(graph, node, domRef, codRefOpt)
                case _ => ()
              }
            case Success(_) => ()
          }
        case termNode: GenericTermNode =>
          inferTypeOf(termNode.getTerm) match {
            case Failure(_) => ()
            case Success(TupleOf(Some(2), places, _)) if places.size == 2 =>
              places.get(CanonicTuplePos(1)) match {
                case Some(set: SetOf[AxiomaticSet])
                  if set.contentType.isInstanceOf[LeafType[AxiomaticSet]]
                =>
                  registerAttributeLikeRelation(graph, node, SelfRef, Some(SelfRef))
                case Some(LeafType(t)) =>
                  registerAttributeLikeRelation(graph, node, SelfRef, Some(SelfRef))
                case _ => ()
              }
            case Success(_) => ()
          }
        case _ => ()
      }
    )
  }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  /*
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  private def extractEntityToValueRelations[T](graph: ERGraph)
  : (Set[Param[ERGraphNode, ERGraphEdge]], Map[ERGraph#NodeT, Map[Any, (Option[Arity], Set[SetTerm])]]) = {
    val binaryEdgesRes
    : (Set[Param[ERGraphNode, ERGraphEdge]], Map[ERGraph#NodeT, Map[Any, (Option[Arity], Set[SetTerm])]]) =
      graph.edges.foldLeft(
        (Set(): Set[Param[ERGraphNode, ERGraphEdge]], Map(): Map[ERGraph#NodeT, Map[Any, (Option[Arity], Set[SetTerm])]])
      ){
        case ((edgeAcc, attrAcc), erEdge) =>
          erEdge.edge match {
            case srcNode :~> tgtNode + (label:BinaryRelation)
              if label.id.nonEmpty =>
              (srcNode.value, tgtNode.value) match {
                case (OSetNode(sid), tgt : GenericTermNode)
                  if inferTypeOf(tgt.getTerm).map(_.getLeaves) == Success(Set(Values)) =>
                  inferTypeOf(Reference(sid)) match {
                    case Failure(_) => (edgeAcc, attrAcc)
                    case Success(LeafType(Entities))
                       | Success(SetOf(LeafType(Entities), _)) =>
                      val labelStr: Any = label.id.getOrElse("")
                      val currentMap: Map[Any, (Option[Arity], Set[SetTerm])] =
                        attrAcc.get(srcNode).getOrElse(Map())
                      val currentValues: Set[SetTerm] =
                        currentMap.get(labelStr).map(_._2).getOrElse(Set())
                      (
                        edgeAcc union Set(erEdge),
                        attrAcc.updated(
                          srcNode,
                          currentMap.updated(labelStr, (None, currentValues union Set(tgt.getTerm)))
                        )
                      )
                    case _ => (edgeAcc, attrAcc)
                  }
                case _ => (edgeAcc, attrAcc)
              }
            case _ => (edgeAcc, attrAcc)
          }
      }
    graph.nodes.foldLeft(binaryEdgesRes){
      case ((remAcc, attrAcc), erNode) =>
        erNode.value match {
          case OSetNode(sid) =>
            inferTypeOf(Reference(sid)) match {
              case Failure(_) => (remAcc, attrAcc)
              case Success(SetOf(TupleOf(places, _), _))
                if places.size == 2
                  && (
                    places(0) == LeafType(Entities)
                      || (
                      places(0).isInstanceOf[SetOf[AxiomaticSet]]
                      && places(0).asInstanceOf[SetOf[AxiomaticSet]].contentType == LeafType(Entities)
                      )
                  ) =>
                val domEdges: Set[graph.EdgeT] =
                  erNode.outgoing.filter(_.value.label.isInstanceOf[BinRelDomPlace])
                if ( domEdges.size == 1 ) {
                  val domNode = domEdges.head.target
                  val domArity: Option[Arity] =
                    domEdges.head.value.label.asInstanceOf[BinRelDomPlace].arity
                  val codomNode: GenericTermNode =
                    erNode.outgoing.filter(_.value.label.isInstanceOf[BinRelCodomPlace])
                      .head.target.value.asInstanceOf[GenericTermNode]
                  val labelStr: Any = Reference(sid).toString(seed.id)
                  val currentMap: Map[Any, (Option[Arity], Set[SetTerm])] =
                    attrAcc.get(domNode).getOrElse(Map())
                  (
                    remAcc union Set(erNode),
                    attrAcc.updated(
                      domNode,
                      currentMap.updated(labelStr, (domArity, Set(codomNode.getTerm)))
                    )
                  )
                } else {
                  (remAcc, attrAcc)
                }
              case _ => (remAcc, attrAcc)
            }
          case _ => (remAcc, attrAcc)
        }
    }
  }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length
   */

  /** ************************************************************************** */

  /** I have no clue what I'm putting as implicit value here, but I do not know:
   *   - where to find a default one;
   *   - or how to programmatically build an immutable graph without it.
   */
  implicit val myConfig: CoreConfig = CoreConfig(orderHint = 5000, Hints(64, 0, 64, 75))
  // implicit val conf: CoreConfig = Connected

  /** TRANSFORMATION TO GRAPHVIZ'S DOT FORMAT */

  /**
   * The GraphViz root configuration
   */
  private val dotRoot: DotRootGraph = DotRootGraph(
    strict = false,
    directed = true,
    id = Some(Id(s"${seed.name}_ERDiagram")),
    attrStmts =
      List(
        DotAttrStmt(Elem.node, List(
          DotAttr(Id("margin"), Id("0.04")),
          // https://graphviz.org/docs/attr-types/colorList/
          DotAttr(Id("style"), Id("radial")), // filled
          // https://graphviz.org/docs/attr-types/colorList/
          DotAttr(Id("gradientangle"), Id("315")),
          // https://graphviz.org/doc/info/colors.html#brewer
          DotAttr(Id("colorscheme"), Id("pastel28"))
        )),
        DotAttrStmt(Elem.edge, List(
          DotAttr(Id("arrowhead"), Id("normal"))
        ))
      ),
    attrList =
      List(
        DotAttr(Id("rankdir"), Id("BT")),
        DotAttr(Id("concentrate"), Id("false")), // true, false
        DotAttr(Id("overlap"), Id("false")), // scale, false
        DotAttr(Id("splines"), Id("spline")) // none, line, polyline, curved, ortho, spline
      )
  )

  /**
   * Get the GraphViz shape style to use for the node having the provided axiomatic type.
   *
   * @param t the axiomatic type of the node whose shape to retrieve.
   * @return the GraphViz shape style, a boolean stating if the shape is a multiline
   *         one, and a boolean stating if the shape is rounded.
   */
  private def getNodeShapeDotAttr(t: TypeExpr[AxiomaticSet])
  : (Seq[DotAttr], Boolean, Boolean) = {
    t match {
      case LeafType(_) if t.getLeaves == Set(Values) =>
        // (Seq(DotAttr(Id("shape"), Id("note"))), false)
        (Seq(DotAttr(Id("shape"), Id("record"))), true, false)
      case LeafType(_) if t.getLeaves != Set(Values) =>
        (Seq(DotAttr(Id("shape"), Id("record"))), true, false)
      case TupleOf(_, _, _) =>
        (Seq(DotAttr(Id("shape"), Id("hexagon"))), false, false)
      case SetOf(TupleOf(_, _, _), _) =>
        (Seq(DotAttr(Id("shape"), Id("diamond"))), false, false)
      case SetOf(_, _) =>
        (Seq(DotAttr(Id("shape"), Id("Mrecord"))), true, true)
      case _ =>
        (Seq(DotAttr(Id("shape"), Id("ellipse"))), false, true)
    }
  }

  private def getNodeColor(t: TypeExpr[AxiomaticSet]): String = {
    if (t.getLeaves == Set(Values)) {
      "white:4"
    } else {
      val elemColor: String = "white:3"
      val setColor: String = "white:6"
      val tupleSetColor: String = "1"
      val defaultColor: String = "2"
      t match {
        case LeafType(_) | TupleOf(_, _, _) => elemColor
        case SetOf(TupleOf(_, _, _), _) => tupleSetColor
        case SetOf(_, _) => setColor
        case _ => defaultColor
      }
    }
  }

  /**
   * Get the GraphViz color style to use for the node having the provided axiomatic type.
   *
   * @param t the axiomatic type of the node whose color to retrieve.
   * @return the GraphViz color style.
   */
  private def getNodeColorDotAttr(t: TypeExpr[AxiomaticSet]): Seq[DotAttr] =
    Seq(DotAttr(Id("fillcolor"), Id(getNodeColor(t))))

  /**
   * Node transformer function to convert ER-graph nodes into GraphViz nodes.
   *
   * @param relsAsAttributes relation types to handle as attributes.
   * @param root             the root of the GraphViz (sub-)graph into which this node will be inserted.
   * @param innerNode        the ER-graph representation of the node to convert.
   * @return the dot representation of the provided ER-graph node.
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  private def nodeTransformer
  (conf: ERDConfiguration, root: DotRootGraph)(innerNode: ERGraph#NodeT)
  : Option[(DotGraph, DotNodeStmt)]
  =
    if (conf.getAttributeLikeNodes contains innerNode.value) {
      None
    } else {
      val oerd: OntologyEntityRelationshipDiagram = this
      val nodeAsTerm: SetTerm =
        innerNode.value match {
          case OSetNode(oerd, sid) => Reference(sid)
          case TermNode(oerd, t) => t
        }
      val labelStr: String = innerNode.value.labelStr
      val attrList: Seq[DotAttr] =
        new TypeInferrer(seed).infer(typingEnvironment)(nodeAsTerm) match {
          case Failure(e) =>
            // println(s""">>> Defaulting for $asTerm due to $e""")
            Seq(
              DotAttr(Id("shape"), Id("ellipse")),
              DotAttr(Id("fillcolor"), Id("2")), // orange
              DotAttr(Id("label"), Id(labelStr))
            )
          case Success(axiomType) =>
            val (shapeAttrs, isRecord, isRounded) = getNodeShapeDotAttr(axiomType)
            if (!isRecord) {
              shapeAttrs
                .appendedAll(getNodeColorDotAttr(axiomType))
                .appendedAll(Seq(DotAttr(Id("label"), Id(labelStr))))
            } else {
              val rcdLabel = {
                s"""<\n<TABLE CELLBORDER="0"${
                  if (isRounded) {
                    " STYLE=\"ROUNDED, RADIAL\""
                  } else {
                    " STYLE=\"RADIAL\""
                  }
                } bgcolor="${getNodeColor(axiomType)}" gradientangle="315">\n<TR><TD align="CENTER" valign="MIDDLE"><B>$labelStr</B></TD></TR>\n<HR/>\n${
                  val attributes: Map[AddresseeReference, Set[AttributeLikeRelationDescr]] =
                    conf.getAttributesOf(nodeAsTerm)
                  // println(s""">>> For $nodeAsTerm found the following attributes: $attributes""")
                  ((axiomType match {
                    case LeafType(_) => List(SelfRef)
                    case _ => List(SelfRef, SubsetRef, ElementRef)
                  }): List[AddresseeReference]).map(
                    (refType: AddresseeReference) =>
                      attributes.get(refType).map(
                        (alrSet: Set[AttributeLikeRelationDescr]) => {
                          alrSet.map(
                            (alr: AttributeLikeRelationDescr) =>
                              attributeLikeRelationDescr2String(conf)(refType, alr)
                            // ).mkString("\\l")
                          ).foldLeft(None: Option[Seq[String]]) {
                            case (None, e) => Some(Seq(e))
                            case (Some(l), e) => Some(l :+ e)
                          }.getOrElse(Seq("<TD></TD>"))
                            .mkString("<TR>", "</TR>\n<TR>", "</TR>\n")
                        }
                      ).getOrElse("")
                    // ).mkString("\\l|")
                  ).mkString("<HR/>\n")
                  // }\\l }\""""
                }</TABLE>>"""
              }
              Seq(
                DotAttr(Id("shape"), Id("plain")),
                DotAttr(Id("style"), Id("\"\"")),
                DotAttr(Id("label"), Id(rcdLabel))
              )
            }
        }
      Some(root, DotNodeStmt(NodeId(innerNode.##), attrList))
    }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  def attributeLikeRelationDescr2String
  (conf: ERDConfiguration)(refType: AddresseeReference, alr: AttributeLikeRelationDescr)
  : String = {
    val attrName: String =
      alr.relTerm.map {
        case t@Reference(_) => t.toString(seed.id)
        case t: Any =>
          getERNodeFor(t).map(
            n =>
              getGraph.nodes
                .get(n)
                .outgoing
                // .map(e => { println(s""">>>>>   Found $e"""); e})
                .find(_.label == IsARel)
                //.map(e => { println(s""">>>>>   Found $e"""); e})
                .map(
                  e => {
                    // println(s""">>>>>   Found ${e.target.value}""")
                    e.target.value
                      .asInstanceOf[GenericTermNode]
                      .getTerm
                      .toString(seed.id)
                  }
                )
                .getOrElse("???")
          ).getOrElse(t.toString(seed.id))
      }.getOrElse(alr.relTerm.toString)
    val attrId: String = {
      alr.relTerm.flatMap(getERNodeFor)
        .flatMap(conf.getPortId4attribute(_))
        // .map("<" + _ + "> ")
        .map((idStr: String) => s""" port=\"$idStr\"""")
        .getOrElse("")
      // if ( attrName.isBlank ) { "" } else { s"""<$attrName> """}
    }
    val arityStr: String = {
      val arityLabel: String = Arity.toAttributeLabel(alr.domArity, alr.codArity)
      if (arityLabel.isEmpty || refType == SelfRef) {
        ""
      } else {
        s"""[$arityLabel] """
      }
    }
    // s"""$attrName: $arityStr${alr.codomain.toString(seed.id)}"""
    s"""<TD$attrId align="LEFT">$attrName: $arityStr${alr.codomain.toString(seed.id)}</TD>"""
  }

  /**
   * Get the GraphViz style to use for the provided ER-graph edge (relationship)
   * type.
   *
   * @param edge the ER-graph edge (relationship) type whose style is to be retrieved.
   * @return the GraphViz style to use for this ER-graph edge type.
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity", "checkstyle:method.length"))
  // scalastyle:off cyclomatic.complexity
  // scalastyle:off method.length
  private def getDotAttr(edge: ER_Edge): List[DotAttr] = {
    edge match {
      case areRel: AreRel =>
        getDotLineAttr(areRel)
          .appendedAll(
            List(
              // DotAttr(Id("label"), Id("are")),
              DotAttr(Id("arrowhead"), Id("onormal")),
              DotAttr(Id("arrowsize"), Id("1.5")),
              DotAttr(Id("style"), Id("bold"))
            )
          )
      case IsARel =>
        List(
          // DotAttr(Id("label"), Id("is a")),
          DotAttr(Id("style"), Id("dashed")),
          DotAttr(Id("arrowhead"), Id("vee")),
        )
      case relEdge@BinaryRelation(id, domId, domArity, codId, codArity) =>
        id.map(i => List(DotAttr(Id("label"), Id(i.toString))))
          .getOrElse(List())
          .appendedAll(getDotLineAttr(relEdge))
          .appendedAll(getDotArrowAttr(relEdge))
          .appendedAll(
            id
              .map(i => List(DotAttr(Id("label"), Id(i.toString))))
              .getOrElse(List())
          )
          .appendedAll(
            getEdgeEndLabelDotAttr("tail", codId, codArity)
          )
          .appendedAll(
            getEdgeEndLabelDotAttr("head", domId, domArity)
          )
      case relEdge: AbstractRelationPlace =>
        getDotLineAttr(relEdge)
          .appendedAll(getDotArrowAttr(relEdge))
          .appendedAll(
            relEdge.placeId
              .map(i => List(DotAttr(Id("label"), Id(i.toString))))
              .getOrElse(List())
          )
          .appendedAll(
            getEdgeEndLabelDotAttr("head", relEdge.placeId, None)
          )
          .appendedAll(
            getEdgeEndLabelDotAttr("tail", None, relEdge.arity)
          )
    }
  }
  // scalastyle:on cyclomatic.complexity
  // scalastyle:on method.length

  /**
   * Get the GraphViz line style to use for the provided ER-graph edge type.
   *
   * @param edge the ER-graph edge type whose line style is to be retrieved.
   * @return the GraphViz line style to use for this ER-graph edge type.
   */
  private def getDotLineAttr(edge: PlaceType): List[DotAttr] =
    edge match {
      case _: SetRelPlace =>
        List(
          // DotAttr(Id("style"), Id("bold")), DotAttr(Id("color"), Id("black:invis:black"))
          // The above solution does not render back arrows, so use the below one
          // DotAttr(Id("style"), Id("solid")), DotAttr(Id("color"), Id("black:black"))
          DotAttr(Id("style"), Id("solid")),
          DotAttr(Id("color"), Id("black:black:invis:black:black")),
          DotAttr(Id("arrowsize"), Id("1.5"))
        )
      case _: ElemRelPlace =>
        List(DotAttr(Id("style"), Id("solid")))
      case _: TuplePlace =>
        List(DotAttr(Id("style"), Id("dashed")))
      case _ =>
        List(DotAttr(Id("style"), Id("solid")))
    }

  /**
   * Get the GraphViz attribute for edge arrow of the ER-edge provided.
   *
   * @param edge the ER-edge whose arrow attribute to retrieve.
   * @return the GraphViz attribute for edge arrow of the ER-edge provided.
   */
  private def getDotArrowAttr(edge: ER_Edge): List[DotAttr] =
    edge match {
      case _: BinaryRelation =>
        List(DotAttr(Id("arrowhead"), Id("normal")))
      case BinRelDomPlace(placeId, arity) =>
        List(
          DotAttr(Id("dir"), Id("back")),
          DotAttr(Id("arrowtail"), Id("normal"))
        )
      case BinRelCodomPlace(placeId, arity) =>
        List(DotAttr(Id("arrowhead"), Id("normal")))
      case RelationPlace(placeId, arity) =>
        List(DotAttr(Id("arrowhead"), Id("normal")))
    }

  /**
   * Get the edge end label for the edge end described by the provided parameters.
   *
   * @param id    identifier of the edge end.
   * @param arity arity of the edge end.
   * @return the edge end label for the edge end described in by the provided parameters.
   */
  private def getEdgeEndLabel(id: Option[Any], arity: Option[Arity])
  : Option[String] = {
    id.map(
      i =>
        (s: Option[String]) =>
          s"""${i.toString}${s.map(s => s"""[$s]""").getOrElse("")}"""
    )
    arity.map(_.toUMLString)
  }

  /**
   * Get the edge end GraphViz attributes for the edge end described by the provided
   * parameters.
   *
   * @param end   the edge end name
   * @param id    identifier of the edge end.
   * @param arity arity of the edge end.
   * @return the edge end attributes for the edge end described in by the provided
   *         parameters.
   */
  private def getEdgeEndLabelDotAttr(end: String, id: Option[Any], arity: Option[Arity])
  : List[DotAttr] = {
    getEdgeEndLabel(id, arity)
      .map(
        l => {
          if (l.equals("")) {
            List()
          } else {
            List(DotAttr(Id(s"""${end}label"""), Id(s"""$l""")))
          }
        }
      ).getOrElse(List())
  }

  /**
   * Edge transformer function to convert ER-graph edges into GraphViz edges.
   *
   * @param root      the root of the GraphViz (sub-)graph into which this node will be inserted.
   * @param innerEdge the ER-graph representation of the edge to convert.
   * @return the dot representation of the provided ER-graph edge.
   */
  private def edgeTransformer
  (conf: ERDConfiguration, root: DotRootGraph)
  (innerEdge: ERGraph#EdgeT)
  : Option[(DotGraph, DotEdgeStmt)] =
    if (! (conf.getDisplayedEdges contains innerEdge)) {
      None
    } else {
      val e = innerEdge.edge
      e match {
        case LkDiEdge(src, dst, label) =>
          (conf.getDotID4edgeEnd(src), conf.getDotID4edgeEnd(dst)) match {
            case (Some(srcId), Some(dstId)) =>
              Some((root,
                DotEdgeStmt(
                  // srcId,
                  NodeId(srcId),
                  // NodeId(src.##),
                  // dstId,
                  NodeId(dstId),
                  // NodeId(dst.##),
                  label match {
                    case rel: ER_Edge => getDotAttr(rel)
                    case _ => List()
                  }
                )
              ))
            case _ => None
          }
      }
    }

  /**
   * Hyperedge transformer function to convert ER-graph hyperedges into GraphViz edges.
   * @param root the root of the GraphViz (sub-)graph into which this node will be inserted.
   * @param innerEdge the ER-graph representation of the hyperedge to convert.
   * @return the dot representation of the provided ER-graph hyperedge.
   */
  private def hyperedgeTransformer
  (conf: ERDConfiguration, root: DotRootGraph)
  (innerEdge: ERGraph#EdgeT)
  : Iterable[(DotGraph, DotEdgeStmt)] = {
    val e = innerEdge.edge
    val label: String =
      e.label.value match {
        case n:SetTermNode => n.symbol
        case p:Property => p.symbol
      }
    e.sources.flatMap(s =>
      e.targets.map(
        t => (root, DotEdgeStmt(
          NodeId(s.##),
          NodeId(t.##),
          List(DotAttr(Id("label"), Id(label)))
        ))
      )
    )
  }

  def getGraphAsDotString(): String =
    // getGraphAsDotString(Set())
    getGraphAsDotString(
      ERDConfiguration(this, attributeLikeRelationsRegister)
    )

  /**
   * Returns a string that can be processed by the GraphViz tool to produce a graphical
   * representation of this Entity-Relationship diagram.
   * @return a (GraphViz-compatible) string representation of this Entity-Relationship diagram.
   */
  def getGraphAsDotString
  (conf: ERDConfiguration)
  : String = {
    // For an obscure reason, the two following lines are required to effectively
    // remove attribute like nodes
    val origNodes: Set[Param[ERGraphNode,ERGraphEdge]] = getGraph.nodes.toSet
    val origEdges: Set[Param[ERGraphNode,ERGraphEdge]] = getGraph.edges.toSet
    /*
    val attrLikeRels: Set[AttributeLikeRelationDescr] =
      conf.getAttributeLikeRelations
    val attrLikeNodes: Set[ERGraphNode] =
      conf.getAttributeLikeNodes
    val nodesWithAttributes: Set[ERGraphNode] =
      conf.getNodesWithAttributes
    val filteredEdges: Set[Param[ERGraphNode,ERGraphEdge]] =
      conf.getAttributeLikeEdges.toSet
    val filteredNodes: Set[Param[ERGraphNode,ERGraphEdge]] =
    /*
  getGraph.nodes
    .filterNot(
      (n: ERGraph#NodeT) => attrLikeNodes.contains(n.value)
    ).toSet
     */
    getGraph.nodes.toSet
    var dotGraph: ERGraph =
      scalax.collection.mutable.Graph.from(filteredNodes union filteredEdges)
    dotGraph.nodes.foreach(
      (n: ERGraph#NodeT) => {
        if (n.edges.isEmpty && ! nodesWithAttributes.contains(n.value)) {
          println(s""">>>>>  Found $n to suppress!""")
          dotGraph -= n
        }
      }
    )
    */
    var dotGraph: ERGraph = getGraph
    val dotStr = dotGraph.toDot(
      dotRoot = dotRoot,
      cNodeTransformer = Some(nodeTransformer(conf, dotRoot)),
      iNodeTransformer = Some(nodeTransformer(conf, dotRoot)),
      edgeTransformer = edgeTransformer(conf, dotRoot),
      hEdgeTransformer = Some(hyperedgeTransformer(conf, dotRoot))
    )
    dotStr.replaceAll("\"([^\"]+:[^\"]+:w)\"","$1")
  }

  /** ************************************************************************** */

}