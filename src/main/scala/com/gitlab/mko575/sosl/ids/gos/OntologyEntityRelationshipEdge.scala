/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.gos

import com.gitlab.mko575.sosl.ids.Property

import scala.language.implicitConversions
import com.gitlab.mko575.sosl.ids.common.Arity

object OntologyEntityRelationshipEdge

/*****************************************************************************/

sealed trait PlaceType

trait DynamicPlaceTypeMixer[TT <: PlaceType] {
  implicit def baseObject[OT](o: Mixin[OT]): OT = o.obj

  def ::[OT](o: OT): Mixin[OT] with TT
  class Mixin[OT] protected[DynamicPlaceTypeMixer](val obj: OT) // protected
}

trait SetRelPlace extends PlaceType
object SetRelPlace extends DynamicPlaceTypeMixer[SetRelPlace] {
  def ::[T](o: T) = new Mixin[T](o) with SetRelPlace
}
trait ElemRelPlace extends PlaceType
object ElemRelPlace extends DynamicPlaceTypeMixer[ElemRelPlace] {
  def ::[T](o: T) = new Mixin[T](o) with ElemRelPlace
}
trait TuplePlace extends PlaceType
object TuplePlace extends DynamicPlaceTypeMixer[TuplePlace] {
  def ::[T](o: T) = new Mixin[T](o) with TuplePlace
}

/*****************************************************************************/

sealed trait ER_Edge
//sealed abstract class ER_Edge(property: Property)

case class AreRel(isStrict: Boolean) extends ER_Edge with PlaceType // subset relationship
case object IsARel extends ER_Edge // member (in) relationship

case class BinaryRelation(
                           id: Option[Any],
                           domPlaceId: Option[Any], domArity: Option[Arity],
                           codPlaceId: Option[Any], codArity: Option[Arity]
                         ) extends ER_Edge with PlaceType
                             /*
case class BinarySetRelation(
                              id: Option[Any],
                              domPlaceId: Option[Any], domArity: Option[Arity],
                              codPlaceId: Option[Any], codArity: Option[Arity]
                            ) extends BinaryRelation(id, domPlaceId, domArity, codPlaceId, codArity)
case class BinaryEleRelation(
                              id: Option[Any],
                              domPlaceId: Option[Any], domArity: Option[Arity],
                              codPlaceId: Option[Any], codArity: Option[Arity]
                            ) extends BinaryRelation(id, domPlaceId, domArity, codPlaceId, codArity)
case class BinaryTuple(
                        id: Option[Any],
                        domPlaceId: Option[Any], domArity: Option[Arity],
                        codPlaceId: Option[Any], codArity: Option[Arity]
                      ) extends BinaryRelation(id, domPlaceId, domArity, codPlaceId, codArity)
                      */

sealed abstract class AbstractRelationPlace(val placeId: Option[Any], val arity: Option[Arity])
  extends ER_Edge with PlaceType
/*
case class TuplePlace(override val placeId: Option[Any], override val arity: Option[Arity])
  extends RelationPlace(placeId, arity)
 */
 case class BinRelDomPlace(override val placeId: Option[Any], override val arity: Option[Arity])
  extends AbstractRelationPlace(placeId, arity)
 case class BinRelCodomPlace(override val placeId: Option[Any], override val arity: Option[Arity])
  extends AbstractRelationPlace(placeId, arity)
 case class RelationPlace(override val placeId: Option[Any], override val arity: Option[Arity])
  extends AbstractRelationPlace(placeId, arity)
