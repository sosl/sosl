/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.gos

import com.gitlab.mko575.sosl.ids.{AxiomaticSet, BinaryProperty, NAryProperty, OSet, Predicate, Property, Reference, SetTerm, SetTermLeaf, SetTermNode}
import com.gitlab.mko575.sosl.ids.identifiers.OSetLocalId
import com.gitlab.mko575.sosl.ids.sos.OntologySeed
import scalax.collection
import scalax.collection.Graph.Config
import scalax.collection.{Graph, mutable}
import scalax.collection.GraphPredef._
import scalax.collection.GraphEdge._
import scalax.collection.edge.{LkDiHyperEdge, LkHyperEdge}
import scalax.collection.edge.Implicits._
import scalax.collection.io.dot._
import scalax.collection.config._
import scalax.collection.mutable.ArraySet.Hints
import scalax.collection.constrained.constraints.Connected

import scala.collection.mutable.{Map => MMap, Set => MSet}
import scala.reflect.ClassTag

case class OntologyRawGraph(seed: OntologySeed) {
  // To differentiate hyperedges based on the order of endpoints
  implicit val kind: CollectionKind = Sequence

  /** ************************************************************************** */

  /*
  class RawGraphNode[T]
  object RawGraphNode {
    implicit object TermWitness extends RawGraphNode[SetTerm]
    implicit object PropertyWitness extends RawGraphNode[Property]
  }
   */

  /*
  sealed trait RawGraphNodeY[A, B]
  object RawGraphNodeY {
    implicit def aInstance[A,B](a: A): RawGraphNodeY[A, B] = new RawGraphNodeY[A, B] {}
    implicit def bInstance[A,B](b: B): RawGraphNodeY[A, B] = new RawGraphNodeY[A, B] {}
  }
   */

  sealed trait RawGraphNode
  case class TermNode(t:SetTerm) extends RawGraphNode
  case class PropNode(p:Property) extends RawGraphNode

  // type RawGraphNode = SetTerm
  type RawGraphEdge[+T] = LkDiHyperEdge[T]
  type RawGraph = Graph[RawGraphNode, RawGraphEdge]
  private var internalRawGraph: Option[RawGraph] = None

  def getGraph: RawGraph =
    internalRawGraph match {
      case Some(graph) => graph
      case None => buildRawGraph
    }

  private def getNodesAndEdgesFrom(t:SetTerm): Set[Param[RawGraphNode,RawGraphEdge]] =
    t match {
      case leaf: SetTermLeaf => Set(TermNode(leaf))
        leaf match {
          case Reference(_) => Set(TermNode(leaf))
          case _: AxiomaticSet => Set()
        }
      case node: SetTermNode => {
        val topNode: RawGraphNode = TermNode(node)
        Set(LkDiHyperEdge(topNode +: node.subterms.toSeq.map(TermNode))(node)) ++
          node.subterms.flatMap(getNodesAndEdgesFrom)
      }
    }

  private def getNodesAndEdgesFrom(p:Property): Set[Param[RawGraphNode,RawGraphEdge]] = {
    val topNode: RawGraphNode = PropNode(p)
    val topEdge = LkDiHyperEdge(topNode +: p.terms.toSeq.map(TermNode))(p)
    Set(topEdge) ++ p.terms.flatMap(getNodesAndEdgesFrom)
  }

  /** I have no clue what I'm putting as implicit value here, but I do not know:
   *   - where to find a default one;
   *   - or how to programmatically build an immutable graph without it.
   */
  implicit val myConfig: CoreConfig = CoreConfig(orderHint = 5000, Hints(64, 0, 64, 75))
  // implicit val conf: CoreConfig = Connected

  private def buildRawGraph: RawGraph = {
    val nodesAndEdges: Set[Param[RawGraphNode,RawGraphEdge]] =
      seed.properties.flatMap(getNodesAndEdgesFrom)
    val graph : RawGraph = Graph.from(nodesAndEdges)
      // Graph.empty ++ List[Param[RawGraphNode,RawGraphEdge]](edges)
      // Graph.empty ++ edges.toList
      // Graph.from(edges)(ClassTag[RawGraphEdge[RawGraphNode]],Graph.empty.config)
      // mutable.Graph.empty
    // edges.foreach( e => graph = graph + e)
    internalRawGraph = Some(graph)
    graph
  }

  /** TRANSFORMATION TO GRAPHVIZ'S DOT FORMAT */

  private val dotRoot: DotRootGraph = DotRootGraph (
      directed  = true,
      id        = Some(Id("RawOGraphOf" + seed.name)),
      attrStmts =
        List(
          DotAttrStmt(Elem.node, List(
            DotAttr(Id("shape"), Id("Mrecord")),
            DotAttr(Id("margin"), Id("0.04")),
            DotAttr(Id("style"), Id("filled")),
            DotAttr(Id("fillcolor"), Id("lightyellow"))
          )),
          DotAttrStmt(Elem.edge, List(
            DotAttr(Id("arrowhead"), Id("empty"))
          ))
        ),
      attrList  =
        List(
          DotAttr(Id("rankdir"), Id("BT"))
        )
    )

  private def nodeTransformer(root: DotRootGraph)(innerNode: RawGraph#NodeT):
    Option[(DotGraph,DotNodeStmt)] = {
    val attrList: Seq[DotAttr] = innerNode.value match {
      case TermNode(t) =>
        t match {
          case _: SetTermLeaf => List(DotAttr(Id("label"), Id(t.toString(seed.id))))
          case n: SetTermNode => List(DotAttr(Id("label"), Id(n.symbol)))
        }
      case PropNode(p) => List(DotAttr(Id("label"), Id(p.symbol)))
    }
    Some(root, DotNodeStmt(NodeId(innerNode.##), attrList))
  }

  private def edgeTransformer(root: DotRootGraph)(innerEdge: RawGraph#EdgeT)
  : Option[(DotGraph,DotEdgeStmt)] = {
    val e = innerEdge.edge
    e match {
      case LkDiHyperEdge(endpoints, label) =>
        Some((root,
          DotEdgeStmt(
            NodeId(e.source.##),
            NodeId(e.target.##),
            List(DotAttr(Id("label"), Id("X_" + label.toString)))
          )
        ))
    }
  }

  private def hyperedgeTransformer(root: DotRootGraph)(innerEdge: RawGraph#EdgeT)
  : Iterable[(DotGraph, DotEdgeStmt)] = {
    val e = innerEdge.edge
    val attrList: (RawGraphNode,RawGraphNode) => Seq[DotAttr] =
      e.label.value match {
        case n:SetTermNode =>
          (_,t) => t match {
            case TermNode(tt) =>
              List(DotAttr(Id("label"), Id(n.subterms.toSeq.indexOf(tt))))
          }
        case p:Property =>
          (_,t) => t match {
            case TermNode(tt) =>
              List(DotAttr(Id("label"), Id(p.terms.toSeq.indexOf(tt))))
          }
      }
    e.sources.flatMap(s => {
      var i = 0;
      e.targets.map(
        t => {
          i += 1
          (root, DotEdgeStmt(
            NodeId(s.##),
            NodeId(t.##),
            List(DotAttr(Id("label"), Id(i)))
          ))
        }
      )
    })
  }

  def getGraphAsDotString: String = {
    val g = getGraph
    val dotStr = g.toDot(
      dotRoot = dotRoot,
      cNodeTransformer = Some(nodeTransformer(dotRoot)),
      edgeTransformer = edgeTransformer(dotRoot),
      hEdgeTransformer = Some(hyperedgeTransformer(dotRoot))
    )
    dotStr
  }

  /** ************************************************************************** */

}