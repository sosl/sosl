/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.sos

import com.gitlab.mko575.sosl.ids.{LocalSetId, OntologyId, SetId}
import com.gitlab.mko575.sosl.ids.{AxiomaticSet, IdentifiableEntity, OSet, Property, Reference, SetTerm, SetTermLeaf, Subset}
import com.gitlab.mko575.sosl.ids.identifiers.OSetLocalId
import com.gitlab.mko575.sosl.ids.{ExtendedNamespace, RootNamespace}

import scala.collection.mutable.{Map => MMap, Set => MSet}

sealed trait OntologyTypes
case object ConceptOntology extends OntologyTypes
case object InstanceOntology extends OntologyTypes

/**
 * Factory for ontologies in the provided root namespece.
 *
 * @param rootNS the root namespace
 */
case class OntologySeedFactory(rootNS: String) {

  /**
   * Build an ontology seed with the provided parameters.
   *
   * @param oType type of the ontology.
   * @param name name of the ontology.
   * @param osets mappings from ontological set to supertype.
   * @return the constructed ontology seed.
   */
  def build(
             oType: OntologyTypes,
             name: String,
             osets: MMap[OSet, MSet[SetTerm]]
           )
  : OntologySeed = {
    val oid : OntologyId = OntologyId(ExtendedNamespace(RootNamespace, rootNS), name)
    val sets : Map[OSetLocalId, OSet] =
      osets.map{ case (s:OSet,_) => (s.lid, s) }.toMap
    val properties: Set[Property] =
      osets.flatMap{
        case (s:OSet,sss:MSet[SetTerm]) =>
          sss.map(stl => Subset(Reference(LocalSetId(s.lid)), stl, isStrict = false))
      }.toSet

    oType match {
      case ConceptOntology => ConceptOntologySeed(oid, name, Map(), sets, properties)
      case InstanceOntology => InstanceOntologySeed(oid, name, Map(), sets, properties)
    }
  }

}
