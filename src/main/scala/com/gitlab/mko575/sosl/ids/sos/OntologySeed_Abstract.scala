/* ****************************************************************************
 * Copyright (C) 2020 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.sos

import com.gitlab.mko575.sosl.ids.common.{Arity, TypingEnvironment, TypeInferrer, TypedSetTerm}
import com.gitlab.mko575.sosl.ids.{GlobalSetId, LocalSetId, OntologyId, SetId, common}
import com.gitlab.mko575.sosl.ids.EquippedProperty.equipProperty
import com.gitlab.mko575.sosl.ids.EquippedSetTerm.equipSetTerm
import com.gitlab.mko575.sosl.ids._
import com.gitlab.mko575.sosl.ids.gos._
import com.gitlab.mko575.sosl.ids.{Namespace, RootNamespace}
import com.gitlab.mko575.sosl.ids.identifiers.OSetLocalId
import com.mxgraph.view.mxGraph

import scala.collection.mutable

// import scala.language.implicitConversions

import java.util.Collection
import scala.annotation.tailrec
import scala.collection.mutable.{Map => MMap, Set => MSet}
import scala.jdk.CollectionConverters._

/*
package object sos {
  type OntologyLocalId = Object
  type OSetLocalId = Object
}
*/


/**
 * Class representing ontology seeds.
 *
 * @param id         Global identifier of the ontology seed.
 * @param name       Name of the ontology seed.
 * @param referencedOntologies Mapping of ontology seeds directly referenced by
 *                             this ontology seed.
 * @param isClosed   Either this ontology definition is an "open" or "closed" World one.
 * @param sets       Set of all categories in the seed, indexed by their identifier.
 * @param properties Properties of the categories of the ontology seed.
 */
// case class OntologySeed(
abstract class OntologySeed(
                         /** x */
                         val id: OntologyId,
                         val name: String,
                         val referencedOntologies: Map[OntologyId,OntologySeed],
                         val isClosed: Boolean,
                         // TODO: Verify the semantics of "opened/closed" for ontologies
                         /** y */
                         val sets: Map[OSetLocalId, OSet],
                         /** z */
                         val properties: Set[Property]
                  ) {

  /**
   * Constructor handling the case where the map of categories is provided as
   * a mutable map.
   *
   * @param id         Global identifier of the ontology seed.
   * @param name       Name of the ontology seed.
   * @param sets       the ontological sets ({@link OSet}).
   * @param properties the properties.
   */
  def this(
            id: OntologyId,
            name: String,
            sets: MMap[OSetLocalId, OSet],
            properties: MSet[Property]
          ) = {
    this(id, name, Map(), false, sets.toMap, properties.toSet)
  }

  /**
   * Constructor handling the case where the map of categories is provided as
   * a mutable map.
   *
   * @param id         Global identifier of the ontology seed.
   * @param name       Name of the ontology seed.
   * @param sets       the ontological sets ({@link OSet})
   * @param properties the properties
   */
  def this(
            id: OntologyId,
            name: String,
            sets: MMap[OSetLocalId, OSet],
            properties: Collection[Property]
          ) = {
    this(id, name, Map(), false, sets.toMap, properties.asScala.toSet)
  }

  /**
   * Constructor handling the case where only a name is provided as
   * identifier and sets and categories are provided as mutable collections.
   *
   * @param name       the name of the ontology.
   * @param sets       the categories
   * @param properties the properties
   */
  def this(
            name: String,
            sets: MMap[OSetLocalId, OSet],
            properties: MSet[Property]
          ) = {
    this(OntologyId(RootNamespace, name), name, sets, properties)
  }

  /**
   * Getter for the global identifier of this ontology seed.
   *
   * @return the global identifier of this ontology seed.
   */
  def getGlobalIdentifier(): OntologyId = id

  /**
   * Setter for the global identifier of this ontology seed.
   *
   * @param oid the new global identifier.
   * @return a copy of the ontology with the new global identifier.
   */
  def setGlobalIdentifier(oid:OntologyId): OntologySeed

  /**
   * Getter for the name of this ontology.
   *
   * @return the name of this ontology.
   */
  def getName(): String = name

  /**
   * Updates the ontology seed with the provided ontological sets and properties
   * @param newSets the ontological sets to add.
   * @param newProps the properties to add.
   * @return an updated copy of this ontology seed.
   */
  def updateWith(newSets: Iterable[(OSetLocalId, OSet)], newProps: Iterable[Property]): OntologySeed

  /**
   * Check if this {@link OntologySeed} defines an {@link OSet} having the
   * provided {@link OSetLocalId}.
   *
   * @param lid the local identifier to check for.
   * @return {@code true} iff this {@link OntologySeed} defines an {@link OSet}
   *         for the provided local identifier.
   */
  def defines(lid: OSetLocalId): Boolean = sets.contains(lid)

  /**
   * Retrieves the {@link OSet} having the provided {@link OSetLocalId}.
   *
   * @param lid the local identifier to look for.
   * @return the {@link OSet} having the provided {@link OSetLocalId}, if it is present.
   */
  def get(lid: OSetLocalId): Option[OSet] = sets.get(lid)

  def get(sid: SetId): Option[OSet] =
    sid match {
      case LocalSetId(lid) => get(lid)
      case GlobalSetId(oid, lid) if oid == id => get(lid)
      case GlobalSetId(oid, lid) if oid != id =>
        referencedOntologies.get(oid).flatMap(_.get(lid))
    }

  def add(oset: OSet): OntologySeed = updateWith(Map(oset.lid -> oset), Set())

  def add(prop: Property): OntologySeed = updateWith(Map(), Set(prop))

  /**
   * Retrieves the ontological sets ({@link OSet}) of this ontology seed
   * ({@link OntologySeed}) that are explicitly enumerable.
   *
   * @return the explicitly enumerable ontological sets of this ontology.
   */
  def getEnumerableOSets(): Map[OSetLocalId,OSet] = sets

  /**
   * Retrieves all the properties that involve only user defined ontological
   * sets whose global identifier is included in the provided ones.
   *
   * @param sids the global identifiers of the ontological sets to restrict the search to.
   * @return the properties involving only those ontological sets.
   */
  def getPropertiesInvolvingOnly(sids: Set[SetId]): Set[Property] =
    // properties.filter(p => EquippedProperty.equip(p).getOSets.toSet subsetOf sids)
    // properties.filter(p => (p:EquippedProperty).getOSets.toSet subsetOf sids)
    properties.filter(p => p.getOSets.toSet subsetOf sids)

  /**
   * Returns the axiomatic superset of the identified ontological set.
   * @param lid the local identifier of the ontological set to look for.
   * @return the axiomatic superset of the identified ontological set.
   */
  def getAxiomaticSupersetOf(lid: OSetLocalId): Option[SetTerm] = {
    if (! sets.contains(lid)) {
      None
    } else {
      sets.get(lid).flatMap(
        s => s.getAxiomaticSuperset.orElse{
          inferAxiomaticSupersets
          sets.get(lid).flatMap(s => s.getAxiomaticSuperset.orElse(None))
        }
      )
    }
  }
  /**
   * Returns the axiomatic superset of the identified ontological set.
   * @param gid the global identifier of the ontological set to look for.
   * @return the axiomatic superset of the identified ontological set.
   */
  def getAxiomaticSupersetOf(gid: GlobalSetId): Option[SetTerm] = {
    if (gid.oid.equals(getGlobalIdentifier())) {
      getAxiomaticSupersetOf(gid.lid)
    } else {
      referencedOntologies.get(gid.oid).flatMap(s => s.getAxiomaticSupersetOf(gid.lid))
    }
  }

  /**
   * Provides a verbose description of the ontology.
   *
   * @return A String describing the ontology seed.
   */
  def toMultilineVerboseString: String = {
    val categories = sets.values.map(s => s.toTypedString).addString(new StringBuilder, ", ")
    val properties =
      this.properties.foldLeft("")(
        (acc: String, p: Property) => s"$acc\n   - ${p.toString(this.id)}"
      )
    s"""Ontology '$name' ($id) :
       | - list of categories: $categories
       | - list of properties: $properties""".stripMargin
  }

  /**
   * Extract an OSet environment from a SetTerm environment. It extracts the
   * values associated to ontological set references from a wider environment.
   *
   * @param env the original environment
   * @return an environment mapping OSet identifiers to their associated value
   */
  private def extractOSetEnv(env:Map[SetTerm,SetTerm]): Map[SetId,SetTerm] =
    env.foldLeft(Map[SetId,SetTerm]())({
      case (acc, (Reference(sid),v)) => acc + (sid -> v)
      case (acc, _) => acc
    })

  /**
   * For every top level set term T appearing in the provided list of properties,
   * tries to compute the axiomatic set term (set term whose leaves are axiomatic
   * sets) which is T's smallest superset (or Least Upper Bound, a.k.a lub),
   * if it is possible to compute it with the provided environment containing
   * the known axiomatic supersets of some set terms.
   *
   * @param env a "typing" environment containing the known axiomatic supersets
   *            of some set terms
   * @param toProcess the properties to process
   * @param delayedProcessing the properties whose processing has been delayed
   *                          due to missing information
   * @return the known smallest axiomatic supersets after processing
   */
  @tailrec
  private def internalComputeAxiomaticSupersets(
    env : Map[SetTerm, SetTerm] // the values are supposed to be smallest axiomatic supersets.
  )(
    toProcess : List[Property],
    delayedProcessing : List[Property]
  ) : Map[SetTerm, SetTerm] = {
    toProcess match {
      case Nil => env
      case (head @ Subset(subset, superset, _)) :: tail =>
        (superset:EquippedSetTerm).getAxiomaticSuperset(this, extractOSetEnv(env)) match {
          case None =>
            internalComputeAxiomaticSupersets(env)(tail, head :: delayedProcessing)
          case Some(axiomaticSuperset) =>
            val updatedAS : SetTerm =
              EquippedSetTerm(env.getOrElse[SetTerm](subset, World))
                .axiomaticGLB(axiomaticSuperset)
            internalComputeAxiomaticSupersets(
              env + (subset ->  updatedAS)
            )(
              delayedProcessing.reverse ::: tail,
              Nil
            )
        }
      case (head @ In(elem, set)) :: tail =>
        (set:EquippedSetTerm).getAxiomaticSuperset(this, extractOSetEnv(env)) match {
          case Some(axiomaticSuperset : AxiomaticSet) =>
            val updatedAS : SetTerm =
              EquippedSetTerm(env.getOrElse[SetTerm](elem, World))
                .axiomaticGLB(axiomaticSuperset)
            internalComputeAxiomaticSupersets(
              env + (elem ->  updatedAS)
            )(
              delayedProcessing.reverse ::: tail,
              Nil
            )
          case Some(Powerset(t)) =>
            val updatedAS : SetTerm =
              EquippedSetTerm(env.getOrElse[SetTerm](elem, World))
                .axiomaticGLB(t)
            internalComputeAxiomaticSupersets(
              env + (elem ->  updatedAS)
            )(
              delayedProcessing.reverse ::: tail,
              Nil
            )
          case Some(Relations(dom,codom,_,_,_,_)) =>
            val domAS : SetTerm =
              EquippedSetTerm(env.getOrElse[SetTerm](elem, World))
                .axiomaticGLB(dom)
            val codomAS: SetTerm =
              EquippedSetTerm(env.getOrElse[SetTerm](elem, World))
                .axiomaticGLB(codom)
            internalComputeAxiomaticSupersets(
              env + (elem ->  CartesianProduct(List(domAS, codomAS), Map()))
            )(
              delayedProcessing.reverse ::: tail,
              Nil
            )
          case _ =>
            internalComputeAxiomaticSupersets(env)(tail, head :: delayedProcessing)
        }
      case _ :: tail => internalComputeAxiomaticSupersets(env)(tail, delayedProcessing)
    }
  }

  /**
   * Computes a typing environment for this SOS mapping SetTerms to axiomatic set terms.
   * @return the computing typing environment.
   */
  def computingTypingEnvironment: Map[SetTerm, SetTerm] = {
    val subsetProps : List[Property] =
      properties.collect({case p:Subset => p})
        .toList.sortBy[Int]((p:Property) => (p:EquippedProperty).getOSets.size)
      //properties.collect({case p@Subset(_,t,_) => p})
        // (t:EquippedSetTerm).getWidth(Map[Category,Int]()) == Some(1)
    val axiomaticSetsTyping : Map[SetTerm,SetTerm] =
      AxiomaticSets.content.foldLeft(Map[SetTerm,SetTerm]())((m,s) => m + (s -> s))
    val types = internalComputeAxiomaticSupersets(axiomaticSetsTyping)(subsetProps, Nil)
    types.removedAll(axiomaticSetsTyping.keys)
  }

  def inferAxiomaticSupersets: Map[SetId,SetTerm] = {
    // println(s"=== Calling inferAxiomaticSupersets on ${getName()}")
    val env: Map[SetId,SetTerm] = extractOSetEnv(computingTypingEnvironment)
    env.map {
      case (sId: SetId, t: SetTerm) => {
        sets.get(sId.lid).map(s => s.setAxiomaticSuperset(t))
        (sId, t)
      }
    }
  }

  def finalizeSeedCreation: Unit = {
    // println(s"=== Calling finalizeSeedCreation on ${getName()}")
    sets.map{ case (_, oSet) => oSet.setOwningSeed(this) }
    inferAxiomaticSupersets
    val typing: TypingEnvironment[AxiomaticSet] = {
      (new TypeInferrer[AxiomaticSet](this)).inferAxiomaticTypes()
    }
    // println(s"===== INFERED TYPING ENV for ontology ${this.id}: $typing")
    sets.map{
      case (lId, oSet) =>
        typing
          .getTypeAndUpdatedEnv(
            // TypedSetTerm(Reference(LocalSetId(lId)))
            TypedSetTerm(Reference(GlobalSetId(id, lId)))
          )
          .map{ case (t, _) => oSet.setAxiomaticType(t)}
    }
  }

  /**
   * Extracts the identifiers of ontological sets corresponding to entities or values.
   *
   * @return the pair composed of, first, the "entities" OSets identifiers and then
   *         the values OSets identifiers.
   */
    // TODO: the axiomatic superset of local OSets should rather be computed
    //  when "finalizing" an ontology parsing. And this function should simply
    //  look into the "sets" field.
  def extractEntitiesAndValues: (Set[SetId], Set[SetId]) = {
    val types = computingTypingEnvironment

    types.foldLeft((Set[SetId](), Set[SetId]()))({
        case ((eSet, vSet), (st @ Reference(sid), axiomaticLUB))
        if sets.contains(sid.lid) => {
          axiomaticLUB match {
            case Entities => (eSet + sid, vSet)
            case Values => (eSet , vSet + sid)
            case _ => (eSet, vSet)
          }
        }
        case (acc, _) => acc
      })
  }

  /**
   * Converts this set-based ontology ({@link OntologySeed}) to a graph-based
   * ontology ({@link OldOntologyGraph}) containing only the class inheritance graph.
   *
   * @return a graph-based ontology representing class inheritances in this set-based
   *         ontology.
   */
  def toInheritanceGraph: OldOntologyGraph = {
    val (entities, values): (Set[SetId], Set[SetId]) = extractEntitiesAndValues
    val vertices : Map[OSetLocalId, SetOldVertex] = (
      entities.foldLeft(Map[OSetLocalId, SetOldVertex]())(
        (map, eCat) => map + (eCat.lid -> EntitySet(sets.get(eCat.lid).head))
      )
      ++
      values.foldLeft(Map[OSetLocalId, SetOldVertex]())(
        (map, vCat) => map + (vCat.lid -> ValueSet(sets.get(vCat.lid).head))
      )
    )

    val edges = properties.foldLeft(MSet[OldEdge]())({
      case (edges, Subset(Reference(subsetId), Reference(supersetId), _))
        if Set[OSetLocalId](subsetId.lid, supersetId.lid) subsetOf vertices.keySet
      =>
        edges | Set(IsSubsetOf(vertices.get(subsetId.lid).head, vertices.get(supersetId.lid).head))
      case (edges, In(r : NamedEntity, Relations(Reference(fId), Reference(tId), _, _, _, _)))
        if (entities contains fId) && (values contains tId)
      =>
        edges | Set(new HasAttribute(EntitySet(sets.get(fId).head), r.name, ValueSet(sets.get(tId).head)))
      case (edges, _) => edges
    })

    OldOntologyGraph(vertices, edges.toSet)
  }

  /**
   * Convert this (set-based) ontology seed ({@link OntologySeed}) to a (graph-based)
   * ontology graph ({@link OldOntologyGraph}).
   *
   * @return the (graph-based) ontology graph ({@link OldOntologyGraph}) corresponding
   *         to this (set-based) ontology seed ({@link OntologySeed}).
   */
  @SuppressWarnings(Array("checkstyle:cyclomatic.complexity"))
  // scalastyle:off cyclomatic.complexity
  def toOntologyGraph: OldOntologyGraph = {
    val inheritanceOntology = toInheritanceGraph
    val (entities, values): (Set[SetId], Set[SetId]) = extractEntitiesAndValues
    val vertices = inheritanceOntology.vertices

    val edges = properties.foldLeft(MSet[OldEdge]())({
      case (edges,
      In(Reference(relId), Relations(Reference(domId), Reference(codomId), total, surj, func, inj))
        )
        if (entities contains domId) && (entities contains codomId)
        && sets.contains(domId.lid) && sets.contains(codomId.lid) // TODO: try to remove that one
      =>
        val domArity =
          (inj, surj) match {
            case (true, true) => Arity.one
            case (true, false) => Arity.optional
            case (false, true) => Arity.some
            case (false, false) => Arity.any
          }
        val codomArity =
          (total, func) match {
            case (true, true) => Arity.one
            case (true, false) => Arity.some
            case (false, true) => Arity.optional
            case (false, false) => Arity.any
          }
        val domEP = OldEdgeEndpoint(None, Some(domArity), EntitySet(sets.get(domId.lid).head))
        val codomEP = OldEdgeEndpoint(None, Some(codomArity), EntitySet(sets.get(codomId.lid).head))
        edges | Set(Association(sets.get(relId.lid).head.name, domEP, codomEP))
      case (edges, Subset(
        Reference(relId),
        CartesianProduct(_ @ Reference(domId) :: Reference(codomId) :: Nil, _), _)
        )
        if (entities contains domId) && (entities contains codomId)
        && sets.contains(domId.lid) && sets.contains(codomId.lid) // TODO: try to remove that one
      =>
        val domEP = OldEdgeEndpoint(None, Some(Arity.any), EntitySet(sets.get(domId.lid).head))
        val codomEP = OldEdgeEndpoint(None, Some(Arity.any), EntitySet(sets.get(codomId.lid).head))
        edges | Set(Association(sets.get(relId.lid).head.name, domEP, codomEP))
      case (edges, _) => edges
    })

    OldOntologyGraph(vertices, inheritanceOntology.edges ++ edges)
  }
  // scalastyle:on cyclomatic.complexity

  def toMXGraph: mxGraph = {
    val graph = new mxGraph
    val parent = graph.getDefaultParent
    val vertexMap = scala.collection.mutable.Map[Any, Object]()

    def createVertex(s: IdentifiableEntity with NamedEntity): Unit = {
      val v = graph.insertVertex(parent, s.lid.toString, s.name, 0.0, 0.0, 10.0, 10.0)
      vertexMap += (s.lid -> v)
    }

    graph.getModel.beginUpdate()
    for ((_, c) <- this.sets) createVertex(c)
    for (prop <- this.properties) {
      prop match {
        case Subset(Reference(subId), Reference(supId), _) =>
          if (!vertexMap.contains(subId.lid)) createVertex(sets.get(subId).head)
          if (!vertexMap.contains(supId.lid)) createVertex(sets.get(supId).head)
          graph.insertEdge(parent, prop.toString, prop, vertexMap(subId.lid), vertexMap(supId.lid))
        case _ => ()
      }
    }
    graph.getModel.endUpdate()
    graph
  }

}

object OntologySeed {
  def unapply(o: OntologySeed)
  : (OntologyId, String, Map[OntologyId,OntologySeed], Boolean,
    Map[OSetLocalId, OSet], Set[Property])
  =
    (o.id, o.name, o.referencedOntologies, o.isClosed, o.sets, o.properties)
}
