/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.sos

import com.gitlab.mko575.sosl.ids.OntologyId
import com.gitlab.mko575.sosl.ids.OSet
import com.gitlab.mko575.sosl.ids.Property
import com.gitlab.mko575.sosl.ids.identifiers.OSetLocalId
import com.gitlab.mko575.sosl.ids.RootNamespace

import java.util.{Collection => JCollection, Map => JMap}
import scala.collection.mutable.{Map => MMap, Set => MSet}
import scala.jdk.CollectionConverters.{CollectionHasAsScala, MapHasAsScala}

case class ConceptOntologySeed(
  override val id: OntologyId,
  override val name: String,
  override val referencedOntologies: Map[OntologyId,OntologySeed],
  override val sets: Map[OSetLocalId, OSet],
  override val properties: Set[Property]
) extends OntologySeed(id, name, referencedOntologies, false, sets, properties) {

  /**
   * Constructor handling the case where the map of categories is provided as
   * a mutable map.
   *
   * @param id         Global identifier of the ontology seed.
   * @param name       Name of the ontology seed.
   * @param sets       the ontological sets ({@link OSet}).
   * @param properties the properties.
   */
  def this(
            id: OntologyId,
            name: String,
            sets: MMap[OSetLocalId, OSet],
            properties: MSet[Property]
          ) = {
    this(id, name, Map(): Map[OntologyId,OntologySeed], sets.toMap, properties.toSet)
  }

  /**
   * Constructor handling the case where the map of categories is provided as
   * a mutable map.
   *
   * @param id         Global identifier of the ontology seed.
   * @param name       Name of the ontology seed.
   * @param referencedOntologies the (super)set of the ontologies referenced in
   *                             this one.
   * @param sets       the ontological sets ({@link OSet})
   * @param properties the properties
   */
  def this(
            id: OntologyId,
            name: String,
            referencedOntologies: JMap[OntologyId,OntologySeed],
            sets: JMap[OSetLocalId, OSet],
            properties: JCollection[Property]
          ) = {
    this(id, name, referencedOntologies.asScala.toMap,
      sets.asScala.toMap, properties.asScala.toSet)
  }

  /**
   * Constructor handling the case where only a name is provided as
   * identifier and sets and categories are provided as mutable collections.
   *
   * @param name       the name of the ontology.
   * @param sets       the categories
   * @param properties the properties
   */
    /*
  def this(
            name: String,
            sets: MMap[OSetLocalId, OSet],
            properties: MSet[Property]
          ) = {
    this(OntologyId(RootNamespace, name), name, sets, properties)
  }
     */

  override def setGlobalIdentifier(oid: OntologyId): ConceptOntologySeed =
    ConceptOntologySeed(oid, name, referencedOntologies, sets, properties)

  override def updateWith(
                           newSets: Iterable[(OSetLocalId, OSet)], newProps: Iterable[Property]
                         ): OntologySeed =
    ConceptOntologySeed(
      id, name, referencedOntologies,
      sets ++ newSets, properties ++ newProps
    )
}
