/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.sos.libraries

import com.gitlab.mko575.sosl.ids.OntologyId
import com.gitlab.mko575.sosl.ids.{OSet, Property}
import com.gitlab.mko575.sosl.ids.identifiers.OSetLocalId
import com.gitlab.mko575.sosl.ids.{ExtendedNamespace, RootNamespace}
import com.gitlab.mko575.sosl.ids.sos.libraries.OS4Integers.rootSetName
import com.gitlab.mko575.sosl.ids.sos.{ConceptOntologySeed, OntologySeed}

import scala.util.control.Exception.allCatch

object OS4Integers extends ConceptOntologySeed(
  OntologyId(ExtendedNamespace(RootNamespace, "SOSL"), rootSetName),
  rootSetName,
  Map(): Map[OntologyId,OntologySeed],
  Map(
    rootSetName -> OSet(rootSetName, rootSetName)
  ): Map[OSetLocalId, OSet],
  Set(): Set[Property]
) {

  private final val rootSetName = "Integers"

  override def get(lid: OSetLocalId): Option[OSet] = {
    super.get(lid) match {
      case Some(s) => Some(s)
      case None =>
        lid match {
          case i:Int => Some(OSet(lid,i.toString))
          case i:Integer => Some(OSet(lid,i.toString))
          case s:String if allCatch.opt(s.toInt).isDefined => Some(OSet(lid,s))
          case _ => None
      }
    }
  }

}
