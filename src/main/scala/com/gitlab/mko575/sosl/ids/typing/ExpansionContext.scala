/* ****************************************************************************
 * Copyright (C) 2021 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0-or-later <https://spdx.org/licenses/GPL-3.0-or-later.html>
 * ***************************************************************************/

package com.gitlab.mko575.sosl.ids.typing

import com.gitlab.mko575.sosl.ids.common.{TypeExpr, TypeVariable, TypedTypeVar}
import com.gitlab.mko575.sosl.ids.typing.ExpansionContext.TypeVariableExpansions

/**
 * Context used while expanding a type expression in a specific typing environment.
 * @param root the (initial/top level) type expression under expansion.
 * @param expandedVariables the type variables expansions finalized o far (useful
 *                          for reuse in an optimization objective)
 * @param variablesUnderExpansion the sequence of nested type variables under expansion
 *                                (if one of them is to be expanded again, then
 *                                there is a recursive type variable definition).
 * @tparam T type leaves types.
 */

object ExpansionContext {
  type TypeVariableExpansions[T <: TypeExpr.LeavesTypeLUB] = Map[TypeVariable, TypeExpr[T]]

  def apply[T <: TypeExpr.LeavesTypeLUB](root: Any): ExpansionContext[T] =
    ExpansionContext(root, Map(): Map[TypeVariable, TypeExpr[T]],
      root match {
        case r: TypedTypeVar[_] => Seq(r.v)
        case _ => Seq(): Seq[TypeVariable]
      }
    )
}

case class ExpansionContext[T <: TypeExpr.LeavesTypeLUB](
                                root: Any,
                                expandedVariables: TypeVariableExpansions[T],
                                variablesUnderExpansion: Seq[TypeVariable]
                              ) {

  def getKnownExpansionOf(v: TypeVariable): Option[TypeExpr[T]] =
    expandedVariables.get(v)

  def getTypeVariableExpansions: TypeVariableExpansions[T] = expandedVariables

  def addExpandedVariable(tv: TypeVariable, te: TypeExpr[T]): ExpansionContext[T] =
    ExpansionContext(root, expandedVariables + (tv -> te), variablesUnderExpansion)

  def addAllExpandedVariables(expansions: TypeVariableExpansions[T]): ExpansionContext[T] =
    ExpansionContext(root, expandedVariables ++ expansions, variablesUnderExpansion)

  def addVariableUnderExpansion(tv: TypeVariable): ExpansionContext[T] =
    ExpansionContext(root, expandedVariables, variablesUnderExpansion :+ tv)
}
