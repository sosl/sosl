package com.gitlab.mko575.sosl.parser;

import static com.gitlab.mko575.sosl.TestLogger.TEST_LOGGER;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import com.gitlab.mko575.sosl.SOSLTestRig;
import com.gitlab.mko575.sosl.TestLogger;
import com.gitlab.mko575.sosl.parser.SOSLParser.SetIdContext;
import java.io.File;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.stream.IntStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class InfrastructureTests {

  @Test
  void testRetrieveFiles() throws IOException {
    String rootPath = "./SOSL_files/unitTestsFiles/";
    String pattern = "glob:**.sosl";
    try {
      ParsingUtils.retrieveFiles(Paths.get(rootPath), pattern, false);
      fail();
    } catch (NoSuchFileException e) {
      assertEquals("./SOSL_files/unitTestsFiles", e.getMessage());
    } catch (IOException e) {
      fail();
    }
  }

  @Test
  void testLogger() {
    assertDoesNotThrow(() -> {
      TestLogger mainLog = TEST_LOGGER;
      IntStream.range(1, 10).forEach(i -> mainLog.decrementIndentation());
      mainLog.setDebugMode();
      mainLog.logError("Testing logError in debug mode ...");
      mainLog.logInfo("Testing logInfo in debug mode ...");
      mainLog.unsetDebugMode();
      mainLog.logError("Testing logError in non debug mode ...");
      mainLog.logInfo("Testing logInfo in non debug mode ...");
    });
  }

  @Disabled("ForestryTest::testCheck should cover it all.")
  @Test
  void testSOSLTestRig() {
    try {
      (new SOSLTestRig())
          .withTree()
          .withGui()
          .withTokens()
          .withTrace()
          .withSLL()
          .withDiagnostics()
          .withEncoding("UTF-8")
          .withPS("test.ps")
          .process(new File(""));
    } catch (IOException e) {
      assertEquals("Is a directory", e.getMessage());
    } catch (Exception e) {
      TEST_LOGGER.logError("Message: [" + e.getClass() + "] " + e.getMessage());
      TEST_LOGGER.logError("Cause: " + e.getCause());
      fail();
    }
    try {
      new SOSLTestRig().process(new File(
          "src/test/resources/SOSL_files/unitTestsFiles/successTests/empty.sosl"
          ));
    } catch (Exception e) {
      TEST_LOGGER.logError("Message: [" + e.getClass() + "] " + e.getMessage());
      TEST_LOGGER.logError("Cause: " + e.getCause());
      fail();
    }
  }

  @SuppressWarnings("deprecation")
  private void exerciseLexerFunctions(Lexer l) {
    assertDoesNotThrow(() -> {
      TEST_LOGGER.logInfo("Tokens: " + l.getTokenNames());
      TEST_LOGGER.logInfo("Vocabulary: " + l.getVocabulary());
      TEST_LOGGER.logInfo("GrammarFileName: " + l.getGrammarFileName());
      TEST_LOGGER.logInfo("RuleNames: " + l.getRuleNames());
      TEST_LOGGER.logInfo("SerializedATN: " + l.getSerializedATN());
      TEST_LOGGER.logInfo("ChannelNames: " + l.getChannelNames());
      TEST_LOGGER.logInfo("ModeNames: " + l.getModeNames());
      TEST_LOGGER.logInfo("ATN: " + l.getATN());
    });
  }

  @Test
  void testSOSLLexerInfrastructure() {
    CharStream cs = CharStreams.fromString("Empty ontology {}");
    exerciseLexerFunctions(new SOSLLexer(cs));
    exerciseLexerFunctions(new SOSLKeywords_asTokensLexer(cs));
    exerciseLexerFunctions(new SOSLWords(cs));
    exerciseLexerFunctions(new LexBasic(cs));
  }

  @SuppressWarnings("deprecation")
  private void exerciseParserFunctions(Parser p) {
    assertDoesNotThrow(() -> {
      TEST_LOGGER.logInfo("Tokens: " + p.getTokenNames());
      TEST_LOGGER.logInfo("GrammarFileName: " + p.getGrammarFileName());
      TEST_LOGGER.logInfo("ATN: " + p.getATN());
      TEST_LOGGER.logInfo("SerializedATN: " + p.getSerializedATN());
      TEST_LOGGER.logInfo("Vocabulary: " + p.getVocabulary());
      TEST_LOGGER.logInfo("RuleNames: " + p.getRuleNames());
    });
  }

  @Test
  void testSOSLParserInfrastructure() {
    SOSLParser p = ParsingUtils.getParserOfString("Empty ontology {}");
    exerciseParserFunctions(p);
    SetIdContext sic = p.setId();
    sic.getRuleIndex();
    (new SetIdContext()).copyFrom(sic);
    (new SetIdContext(p.getContext(), 0)).copyFrom(sic);
    /* */
    CharStream cs = CharStreams.fromString("");
    SOSLKeywords_asTokensLexer l2 = new SOSLKeywords_asTokensLexer(cs);
    l2.mode(SOSLKeywords_asTokensLexer.DEFAULT_MODE);
    SOSLKeywords_asTokensParser p2 = new SOSLKeywords_asTokensParser(new CommonTokenStream(l2));
    exerciseParserFunctions(p2);
    // SetIdContext sic = p2.setId();
    sic.getRuleIndex();
    (new SetIdContext()).copyFrom(sic);
    (new SetIdContext(p2.getContext(), 0)).copyFrom(sic);
  }
}
