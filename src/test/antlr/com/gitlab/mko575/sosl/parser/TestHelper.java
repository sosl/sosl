package com.gitlab.mko575.sosl.parser;

import static com.gitlab.mko575.sosl.TestLogger.TEST_LOGGER;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.InputMismatchException;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.LexerNoViableAltException;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

public class TestHelper {

  /**
   * Return a parser for the input test with the specified parsing configuration.
   *
   * @param lexClass The lexer class to use.
   * @param parClass The parsing class to use.
   * @param input The test to parse.
   * @param lexMode The mode to start lexing.
   * @return The resulting parser.
   */
  static Parser setupParserFor(
      Class<? extends Lexer> lexClass,
      Class<? extends Parser> parClass,
      String input,
      int lexMode) {
    CharStream stream = CharStreams.fromString(input);
    Lexer lexer = null;
    try {
      lexer = lexClass.getDeclaredConstructor(CharStream.class).newInstance(stream);
    } catch (NoSuchMethodException
        | InstantiationException
        | IllegalAccessException
        | InvocationTargetException e) {
      System.err.println("The following error should never occur: " + e);
      throw new Error(e);
    }
    lexer.mode(lexMode);
    Parser parser = null;
    try {
      parser =
          parClass
              .getDeclaredConstructor(TokenStream.class)
              .newInstance(new CommonTokenStream(lexer));
    } catch (InstantiationException
        | IllegalAccessException
        | InvocationTargetException
        | NoSuchMethodException e) {
      System.err.println("The following error should never occur: " + e);
      throw new Error(e);
    }
    return parser;
  }

  /**
   * Start parsing at the provided rule.
   *
   * @param parClass The class of the parser.
   * @param parser The parser to start.
   * @param nonTerminalName The rule to start parsing from.
   * @return The parse tree.
   */
  static ParserRuleContext startParsing(
      Class<? extends Parser> parClass, Parser parser, String nonTerminalName) {
    ParserRuleContext ctx = null;
    try {
      Method m = parClass.getDeclaredMethod(nonTerminalName);
      ctx = (ParserRuleContext) m.invoke(parser);
    } catch (NoSuchMethodException e) {
      fail("The rule name '" + nonTerminalName + "' seems deprecated.");
    } catch (IllegalAccessException | InvocationTargetException e) {
      System.err.println("The following error should never occur: " + e);
      throw new Error(e);
    }
    return ctx;
  }

  /**
   * Assert that the {@link Parser} provided as input ({@code parser}) has gone thru a successful
   * parsing. It checks that: - the next token is EOF (if {@code hasParsedAll} is {@code true}); -
   * no errors occured during parsing.
   *
   * @param testName the name of the test.
   * @param parser the parser to check.
   * @param hasParsedAll {@code true} iff all the input is supposed to have been parsed.
   */
  static void assertDefaultSuccessFrom(String testName, Parser parser, boolean hasParsedAll) {
    TEST_LOGGER.logInfo("Test [" + testName + "]:");
    TEST_LOGGER.incrementIndentation();
    Token currentToken = parser.getCurrentToken();
    TEST_LOGGER.logInfo("current token: " + currentToken);
    TEST_LOGGER
        .logInfo("number of syntax errors: " + parser.getNumberOfSyntaxErrors(), true);
    TEST_LOGGER.decrementIndentation();

    if (hasParsedAll && parser.getCurrentToken().getType() != Token.EOF) {
      Token tok = parser.getCurrentToken();
      String msgInfo =
          String.format(
              "Parser stopped at (%1$d:%2$d) before '%3$s'",
              tok.getLine(), tok.getCharPositionInLine(), tok.getText());
      fail("Input has not been fully parsed: " + msgInfo);
    }
    if (parser.getNumberOfSyntaxErrors() != 0) {
      fail("Parsing terminated with " + parser.getNumberOfSyntaxErrors() + " error(s).");
      // Logger.logInfo("Parsing terminated with " + parser.getNumberOfSyntaxErrors() + "
      // error(s).");
    }
  }

  /**
   * Assert that the ParserRuleContext provided as input ({@code ctx}) is the result of a successful
   * parsing. It checks that: - the context provided in input is not Null ; - the context provided
   * in input dos not contain an exception.
   *
   * @param ctx the context to check.
   */
  static void assertDefaultSuccessFrom(ParserRuleContext ctx) {
    assertNotNull(ctx);
    assertNull(ctx.exception);
  }

  /**
   * Test if the provided input string ({@code input}) parses as the non-terminal whose name is
   * provided ({@code nonTerminalName}).
   *
   * @param nonTerminalName the name of the non-terminal to use for parsing.
   * @param input the string to parse.
   * @param twoPasses if set to true, a two passes process is used.
   */
  static ParserRuleContext assertSuccessfulParsingOfString(
      String nonTerminalName, String input, int lexMode, boolean twoPasses) {
    SOSLParser parser = ParsingUtils.getParserOfString(input, lexMode);
    ParserRuleContext ctx = null;
    try {
      Method m = SOSLParser.class.getDeclaredMethod(nonTerminalName);
      if (twoPasses) {
        ParsingUtils.setParserToFailFastMode(parser);
        try {
          ctx = (ParserRuleContext) m.invoke(parser);
        } catch (InvocationTargetException e) {
          if (e.getCause() instanceof ParseCancellationException) {
            // thrown by BailErrorStrategy
            TEST_LOGGER.logInfo("SLL mode can't do it, switching to LL mode.");
            parser = ParsingUtils.getParserOfString(input, lexMode);
            ParsingUtils.setParserToTryHardMode(parser);
            parser.removeErrorListeners();
            parser.addErrorListener(new TestErrorListener());
            ctx = (ParserRuleContext) m.invoke(parser);
          } else {
            System.err.println("Uncaught exception: " + e.getCause());
          }
        } catch (Exception e) {
          System.err.println("Uncaught exception: " + e);
        }
      } else {
        ctx = (ParserRuleContext) m.invoke(parser);
      }
      TestHelper.assertDefaultSuccessFrom(ctx);
      TestHelper.assertDefaultSuccessFrom(nonTerminalName, parser, true);
    } catch (NoSuchMethodException e) {
      fail(
          "The rule name ("
              + nonTerminalName
              + ") associated to the test ("
              + input
              + ") seems deprecated.");
    } catch (InvocationTargetException e) {
      if (e.getCause() instanceof LexerNoViableAltException) {
        fail(
            "Lexing error for \""
                + input
                + "\" parsed as "
                + nonTerminalName
                + ". The offending token is '"
                + ((LexerNoViableAltException) e.getCause())
                + "'.");
      } else if (e.getCause() instanceof InputMismatchException) {
        fail(
            "Parsing error for \""
                + input
                + "\" parsed as "
                + nonTerminalName
                + ". The offending token is '"
                + ((InputMismatchException) e.getCause()).getOffendingToken()
                + "'.");
      } else {
        Throwable cause = e.getCause();
        fail("Seemingly an error in the way ANTLR is used: " + cause + " has been thrown.");
      }
    } catch (IllegalAccessException e) {
      fail("Seemingly an error in the way ANTLR is used (IllegalAccessException).");
    } finally {
      System.err.println();
    }
    return ctx;
  }

  /**
   * Returns a stream of the parameter and all its children.
   * @param pt the ParseTree to flatten
   * @return a flat stream of the structure
   */
  static Stream<ParseTree> flattenPTree(ParseTree pt) {
    return Stream.concat(
        Stream.of(pt),
        IntStream.rangeClosed(0, pt.getChildCount() - 1).mapToObj(
            i -> pt.getChild(i)
        ).flatMap(TestHelper::flattenPTree));
  }

  static class TestWalkListener extends SOSLBaseListener {

    /** Keep track of the indentation level for output purposes. */
    private transient int indentationLevel;

    public TestWalkListener() {
      indentationLevel = 0;
    }

    private String getOutputPrefix() {
      if (indentationLevel > 0) {
        return "> ".repeat(indentationLevel - 1);
      } else {
        return "< ".repeat(-indentationLevel);
      }
    }

    /**
     * {@inheritDoc}
     *
     * <p>Output the name of the rule.
     */
    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
      indentationLevel += 1;
      String prefix = getOutputPrefix();
      System.out.println(prefix + "rule entered: " + ctx.getText());
    }

    /**
     * {@inheritDoc}
     *
     * <p>Output the name of the rule.
     */
    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
      String prefix = getOutputPrefix();
      System.out.println(prefix + "rule exited: " + ctx.getText());
      indentationLevel -= 1;
    }

    /**
     * {@inheritDoc}
     *
     * <p>Output the name of the node.
     */
    @Override
    public void visitTerminal(TerminalNode node) {
      indentationLevel += 1;
      String prefix = getOutputPrefix();
      System.out.println(prefix + "visiting terminal node: " + node.getText());
      indentationLevel -= 1;
    }

    /**
     * {@inheritDoc}
     *
     * <p>Output the name of the node.
     */
    @Override
    public void visitErrorNode(ErrorNode node) {
      String prefix = getOutputPrefix();
      System.out.println(prefix + "visiting error node: " + node.getText());
      indentationLevel -= 1;
    }
  }

  static class TestVisitor extends SOSLBaseVisitor<Void> {}

  static class TestKeywordsWalkListener extends SOSLKeywords_asTokensBaseListener {}

  static class TestKeywordsVisitor extends SOSLKeywords_asTokensBaseVisitor<Void> {}
}
