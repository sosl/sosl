package com.gitlab.mko575.sosl;

/** Logging functions. */
public class TestLogger {

  /** Execute in debug mode, i.e. with more outputs. */
  protected static boolean debugMode = true;

  /** Set logging in debug mode. */
  public void setDebugMode() {
    debugMode = true;
  }

  /** Set logging in non debug mode. */
  public void unsetDebugMode() {
    debugMode = false;
  }

  /*
  private static org.junit.platform.commons.logging.Logger testLogger =
          LoggerFactory.getLogger(TestHelper.class);
  static Consumer<String> infoLogger = (String s) -> testLogger.info(() -> s);
  */

  /** Prints parameter on error output if in debug mode. */
  // Unused anymore
  /*
  private static final Consumer<String> infoLogger =
      (String s) -> {
        if (debugMode) {
          System.err.println(s);
        }
      };
  *∕

  // ** MAIN STATIC INSTANCE ** //

  /**
   * Static instance to be used by those willing to output something in the
   * main logger.
   */
  public static final TestLogger TEST_LOGGER = new TestLogger();

  // ** DYNAMIC INSTANCES ** //

  private transient int indentationLevel = 0;

  /** Increment the indentation level by 1. */
  public void incrementIndentation() {
    indentationLevel += 1;
  }

  /** Decrement the indentation level by 1. */
  public void decrementIndentation() {
    if (indentationLevel > 0) {
      indentationLevel -= 1;
    }
  }

  private String getLoggingPrefix() {
    return getLoggingPrefix(false);
  }

  private String getLoggingPrefix(boolean last) {
    String res;
    if (indentationLevel > 0) {
      String angle = last ? "└" : "├";
      res = " │  ".repeat(indentationLevel - 1) + " " + angle + "> ";
      // res = " ║ ".repeat(indentationLevel - 1) + " ╠> ";
    } else {
      res = "";
    }
    return res;
  }

  /**
   * Outputs its parameters on the error output if in debug mode.
   *
   * @param msg Message to output.
   */
  public void logError(String msg) {
    if (debugMode) {
      System.err.println(getLoggingPrefix() + "[ERROR]" + msg);
    }
  }

  /**
   * Outputs its parameters on the error output if in debug mode.
   *
   * @param msg Message to output.
   */
  public void logInfo(String msg) {
    logInfo(msg, false);
  }

  /**
   * Outputs its parameter on the error output if in debug mode.
   *
   * @param msg Message to output.
   * @param last true iff this message is the last one for this indentation level.
   */
  public void logInfo(String msg, boolean last) {
    if (debugMode) {
      System.err.println(getLoggingPrefix(last) + msg);
    }
  }

  /**
   * Outputs the message in the 'info' output with one more level of indentation, if in debug mode.
   *
   * @param msg Message to output.
   */
  public void logIndentedInfo(String msg) {
    incrementIndentation();
    logInfo(msg, true);
    decrementIndentation();
  }
}
